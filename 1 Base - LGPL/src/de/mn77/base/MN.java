/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base;

import de.mn77.base.data.util.Lib_Array;


/**
 * @author Michael Nitsche
 */
public class MN {

	/**
	 * @return Returns true, if the given value is between min and max.
	 */
	public static boolean between( final int min, final int max, final int value ) {
		return value >= min && value <= max;
	}

	/**
	 * @return Returns true, if one of the given items is null.
	 */
	public static boolean containsNull( final Object... oa ) {
		for( final Object o : oa )
			if( o != null )
				return false;
		return true;
	}

	/**
	 * @return Returns true if the String is null or contains only whitespace
	 */
	public static boolean isBlank( final String s ) {
		return s == null || s.trim().length() == 0;
	}

	/**
	 * @return Returns true if the given string es null or has a length of 0.
	 */
	public static boolean isEmpty( final String s ) {
		return s == null || s.length() == 0;
	}

	/**
	 * @return Returns true if both objects are null or really equal.
	 */
	public static boolean isEqual( final Object o1, final Object o2 ) {
		if( o1 == null && o2 == null )
			return true;
		if( o1 == null || o2 == null )
			return false;
		return o1.equals( o2 );
	}

	/**
	 * @return Returns true, if the current programm is running from a JAR-File
	 */
	public static final boolean isProductive() {
		final String classpath = System.getProperty( "java.class.path" );
		final String[] paths = classpath.split( System.getProperty( "path.separator" ) );
		return paths[0].toLowerCase().endsWith( ".jar" );

//		try {
//			return !MFileSys.current().knowsDir("src") && !MFileSys.current().knowsDir("de") && !MFileSys.current().knowsDir("org");
//		}
//		catch(final AccessControlException e) { //Wird bei Webstart ausgelöst, wenn ohne VollZugriff
//			return true;
//		}
	}

	public static boolean isTrue( final Boolean b ) {
		return b != null && b;
	}

	/**
	 * @return Chooses between singular and plural for a word
	 */
	public static String numerus( int i, String singular, String plural ) {
		return i <= 1
			? singular
			: plural;
	}

	/**
	 * @return Returns true, if 'search' is within 'arr'.
	 */
	public static boolean or( final Object search, final Object... arr ) {
		return Lib_Array.contains( arr, search );
	}

	public static <T> T replaceNull( final T object, final T ifNull ) {
		return object == null
			? ifNull
			: object;
	}

	public static double sumAllTrue( final boolean... ba ) {
		int count = 0;
		for( final boolean b : ba )
			if( b )
				count++;
		return count;
	}

	/**
	 * @return Returns a array from different items.
	 */
	@SuppressWarnings( "unchecked" )
	public static <T> T[] toArray( final T... t ) {
		return t;
	}

}
