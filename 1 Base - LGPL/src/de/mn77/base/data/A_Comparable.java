/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 2022-09-06
 * @apiNote This abstract class offers defaults for .compareTo and .equals., where .isEqual and .isGreater must be implemented
 */
public abstract class A_Comparable<TA> implements I_Comparable<TA> {

	public final int compareTo( final TA o ) {
		Err.ifNull( o );

		return this.equals( o )
			? 0
			: this.isGreater( o )
				? 1
				: -1;
	}

	@SuppressWarnings( "unchecked" )
	@Override
	public final boolean equals( final Object o ) {
//		Err.ifNull(o);
		// Lazy handling of 'null'

		if( o == null || o.getClass() != this.getClass() )
			return false;
		return this.isEqual( (TA)o );
	}

	@Override
	public abstract boolean isEqual( TA a );

	@Override
	public abstract boolean isGreater( TA a );

	@Override
	public abstract String toString();

}
