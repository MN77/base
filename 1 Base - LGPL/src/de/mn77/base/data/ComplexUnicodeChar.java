/*******************************************************************************
 * Copyright (C): 2023-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data;

import de.mn77.base.data.util.Lib_Compare;

/**
 * @author Michael Nitsche
 * @created 27.09.2023
 */
public class ComplexUnicodeChar implements Comparable<ComplexUnicodeChar> {

	public final int codepoint;
	public final int cpCharCount;
	public final int varSelector;
	public final int vsCharCount;
	private char[] chars = null;


	public ComplexUnicodeChar( int codepoint ) {
		this( codepoint, Character.charCount( codepoint ));
	}

	public ComplexUnicodeChar( final int codepoint, int cpCharCount ) {
		this( codepoint, cpCharCount, -1, 0 );
	}

	public ComplexUnicodeChar( final int codepoint, int cpCharCount, final int varSelector, final int vsCharCount ) {
		this.codepoint = codepoint;
		this.cpCharCount = cpCharCount;
		this.varSelector = varSelector;
		this.vsCharCount = vsCharCount;
	}


	public int getCharCount() {
		return this.cpCharCount + this.vsCharCount;
	}

	public boolean hasVariationSelector() {
		return this.vsCharCount > 0;
	}

	public char[] getChars() {
		if(this.chars == null) {
			char[] c1 = Character.toChars( this.codepoint );

			if(this.vsCharCount > 0) {
				char[] c2 = Character.toChars( this.varSelector );
				char[] ct = new char[c1.length + c2.length];
				System.arraycopy( c1, 0, ct, 0, c1.length );
				System.arraycopy( c2, 0, ct, c1.length, c2.length );
//				MOut.exit( c1, c2, ct );
				return this.chars = ct;
			}
			else
				return this.chars = c1;
		}
		else
			return this.chars;
	}

	@Override
	public String toString() {
		return new String(this.getChars());
	}

	public int compareTo( ComplexUnicodeChar o ) {
		if(o == null)
			return -1;

		if(this.codepoint == o.codepoint) {
			if(this.varSelector == o.varSelector)
				return 0;
			return this.varSelector > o.varSelector
				? 1
				: -1;
		}

		if( Lib_Compare.isGreaterCodepoint(this.codepoint, o.codepoint) )
			return 1;

		return -1;
	}

	@Override
	public boolean equals(Object o) {
		if(o == null)
			return false;
		else if(o instanceof ComplexUnicodeChar other)
			return this.codepoint == other.codepoint && this.varSelector == other.varSelector;
		else
			return false;
	}

	public boolean isComplex() {
		return this.getCharCount() > 1;
	}

}
