/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data;

import de.mn77.base.data.util.Lib_HtmlEntities;


/**
 * @author Michael Nitsche
 * @created 09.05.2022
 */
public class HtmlChars {

	/**
	 * @apiNote Convert all convertable chars to their HTML entity
	 * @implNote https://www.php.net/manual/de/function.htmlentities.php
	 */
	public static String htmlEntities( final String s ) {
		final StringBuilder sb = new StringBuilder();

		for( final char c : s.toCharArray() ) {
			final String trans = Lib_HtmlEntities.charToHtml( c );
			sb.append( trans == null ? c : trans );
		}

		return sb.toString();
	}

	public static String htmlEntitiesDecode( final String s ) {
		final StringBuilder sb = new StringBuilder();

		for( int i = 0; i < s.length(); i++ ) {
			final char c = s.charAt( i );

			if( c == '&' ) {
				final int end = s.indexOf( ';', i + 1 );
				final String entity = s.substring( i, end + 1 );
				final Character trans = Lib_HtmlEntities.htmlToChar( entity );

				if( trans != null ) {
					sb.append( trans );
					i += entity.length() - 1;
				}
				else
					sb.append( c );
			}
			else
				sb.append( c );
		}

		return sb.toString();
	}

	/**
	 * @apiNote Converts some chars, which have a special meaning in HTML: &, ', ", <, >
	 * @implNote https://www.php.net/manual/de/function.htmlspecialchars.php
	 */
	public static String htmlSpecialChars( final String s ) {
		final StringBuilder sb = new StringBuilder();

		for( final char c : s.toCharArray() )
			switch( c ) {
				case '&':
					sb.append( "&amp;" );
					break;
				case '\'':
					sb.append( "&apos;" );
					break;
				case '"':
					sb.append( "&quot;" );
					break;
				case '<':
					sb.append( "&lt;" );
					break;
				case '>':
					sb.append( "&gt;" );
					break;
				default:
					sb.append( c );
			}

		return sb.toString();
	}

}
