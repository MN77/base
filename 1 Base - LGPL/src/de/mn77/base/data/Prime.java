/*******************************************************************************
 * Copyright (C) 2007-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data;

/**
 * @author Michael Nitsche
 */
public class Prime {

	public static boolean isPrime( final int nr ) {
		for( int i = 2; i <= nr / 2; i++ )
			if( nr % i == 0 )
				return false;
		return true;
	}

	public static int next( final int base ) {
		int result = base + 1;
		while( !Prime.isPrime( result ) )
			result++;
		return result;
	}

	public static int prior( final int base ) {
		int result = base - 1;
		while( !Prime.isPrime( result ) )
			result--;
		return result;
	}

}
