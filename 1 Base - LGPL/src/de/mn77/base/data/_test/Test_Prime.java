/*******************************************************************************
 * Copyright (C) 2007-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data._test;

import de.mn77.base.data.Prime;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 *         Erstellt: 06.09.2007
 */
public class Test_Prime {

	public static void main( final String[] args ) {

		try {
			Test_Prime.start();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

	public static void start() throws Exception {
		for( int i = 1; i <= 100; i++ )
			if( Prime.isPrime( i ) )
				MOut.print( i );

		for( int i = 1; i <= 100; i = Prime.next( i++ ) )
			MOut.print( i );

		for( int i = 100; i >= 1; i = Prime.prior( i-- ) )
			MOut.print( i );

		for( int i = 80; i <= 120; i++ )
			if( Prime.isPrime( i ) )
				MOut.print( i );
	}

}
