/*******************************************************************************
 * Copyright (c) 2017 Eric Obermühlner
 * License: MIT
 * https://github.com/eobermuhlner/big-math
 * Version: 2.3.0
 *
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.bigcalc;

import java.math.BigDecimal;
import java.math.MathContext;


/**
 * Calculates exp using the Maclaurin series.
 * See <a href="https://de.wikipedia.org/wiki/Taylorreihe">Wikipedia: Taylorreihe</a>
 *
 * No argument checking or optimizations are done.
 * This implementation is not intended to be called directly.
 */
public class BigExpCalculator extends BigSeriesCalculator {

	public static final BigExpCalculator INSTANCE = new BigExpCalculator();

	private int         n                   = 0;
	private BigRational oneOverFactorialOfN = BigRational.ONE;


	private BigExpCalculator() {
		// prevent instances
	}

	@Override
	protected void calculateNextFactor() {
		this.n++;
		this.oneOverFactorialOfN = this.oneOverFactorialOfN.divide( this.n );
	}

	@Override
	protected BigPowerIterator createPowerIterator( final BigDecimal x, final MathContext mathContext ) {
		return new BigPowerNIterator( x, mathContext );
	}

	@Override
	protected BigRational getCurrentFactor() {
		return this.oneOverFactorialOfN;
	}

}
