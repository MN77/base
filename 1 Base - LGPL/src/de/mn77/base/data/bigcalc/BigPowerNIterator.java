/*******************************************************************************
 * Copyright (c) 2017 Eric Obermühlner
 * License: MIT
 * https://github.com/eobermuhlner/big-math
 * Version: 2.3.0
 *
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.bigcalc;

import java.math.BigDecimal;
import java.math.MathContext;


/**
 * {@link BigPowerIterator} to calculate x<sup>n</sup>.
 */
public class BigPowerNIterator implements BigPowerIterator {

	private final MathContext mathContext;

	private BigDecimal powerOfX;

	private final BigDecimal x;


	public BigPowerNIterator( final BigDecimal x, final MathContext mathContext ) {
		this.x = x;
		this.mathContext = mathContext;

		this.powerOfX = BigDecimal.ONE;
	}

	@Override
	public void calculateNextPower() {
		this.powerOfX = this.powerOfX.multiply( this.x, this.mathContext );
	}

	@Override
	public BigDecimal getCurrentPower() {
		return this.powerOfX;
	}

}
