/*******************************************************************************
 * Copyright (c) 2017 Eric Obermühlner
 * License: MIT
 * https://github.com/eobermuhlner/big-math
 * Version: 2.3.0
 *
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.bigcalc;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.IntStream;

import de.mn77.base.data.struct.SimpleList;


/**
 * A rational number represented as a quotient of two values.
 *
 * Basic calculations with rational numbers (+ - * /) have no loss of precision.
 * This allows to use {@link BigRational} as a replacement for {@link BigDecimal} if absolute accuracy is desired.
 *
 * <a href="http://en.wikipedia.org/wiki/Rational_number">Wikipedia: Rational number</a>
 *
 * <p>
 * The following basic calculations have no loss of precision:
 * </p>
 * <ul>
 * <li>{@link #add(BigRational)}</li>
 * <li>{@link #subtract(BigRational)}</li>
 * <li>{@link #multiply(BigRational)}</li>
 * <li>{@link #divide(BigRational)}</li>
 * <li>{@link #pow(int)}</li>
 * </ul>
 *
 * <p>
 * The following calculations are special cases of the ones listed above and have no loss of precision:
 * </p>
 * <ul>
 * <li>{@link #negate()}</li>
 * <li>{@link #reciprocal()}</li>
 * <li>{@link #increment()}</li>
 * <li>{@link #decrement()}</li>
 * </ul>
 */
public class BigRational implements Comparable<BigRational> {

	public static final BigRational ONE  = new BigRational( 1 );
	public static final BigRational TEN  = new BigRational( 10 );
	public static final BigRational TWO  = new BigRational( 2 );
	public static final BigRational ZERO = new BigRational( 0 );

	private static List<BigRational> bernoulliCache = new SimpleList<>();


	private final BigDecimal denominator;

	private final BigDecimal numerator;


	/**
	 * Calculates the Bernoulli number for the specified index.
	 *
	 * This function calculates the <strong>first Bernoulli numbers</strong> and therefore <code>bernoulli(1)</code> returns -0.5
	 * Note that <code>bernoulli(x)</code> for all odd x &gt; 1 returns 0
	 * See: <a href="https://en.wikipedia.org/wiki/Bernoulli_number">Wikipedia: Bernoulli number</a>
	 */
	public static BigRational bernoulli( final int n ) {
		if( n < 0 )
			throw new ArithmeticException( "Illegal bernoulli(n) for n < 0: n = " + n );
		if( n == 1 )
			return BigRational.valueOf( -1, 2 );
		else if( n % 2 == 1 )
			return BigRational.ZERO;

		synchronized( BigRational.bernoulliCache ) {
			final int index = n / 2;

			if( BigRational.bernoulliCache.size() <= index )
				for( int i = BigRational.bernoulliCache.size(); i <= index; i++ ) {
					final BigRational b = BigRational.calculateBernoulli( i * 2 );
					BigRational.bernoulliCache.add( b );
				}

			return BigRational.bernoulliCache.get( index );
		}
	}

	/**
	 * Returns the largest of the specified rational numbers.
	 *
	 * @param values
	 *            the rational numbers to compare
	 * @return the largest rational number, 0 if no numbers are specified
	 * @see #max(BigRational)
	 */
	public static BigRational max( final BigRational... values ) {
		if( values.length == 0 )
			return BigRational.ZERO;
		BigRational result = values[0];
		for( int i = 1; i < values.length; i++ )
			result = result.max( values[i] );
		return result;
	}

	/**
	 * Returns the smallest of the specified rational numbers.
	 *
	 * @param values
	 *            the rational numbers to compare
	 * @return the smallest rational number, 0 if no numbers are specified
	 */
	public static BigRational min( final BigRational... values ) {
		if( values.length == 0 )
			return BigRational.ZERO;
		BigRational result = values[0];
		for( int i = 1; i < values.length; i++ )
			result = result.min( values[i] );
		return result;
	}

	/**
	 * Creates a rational number of the specified {@link BigDecimal} value.
	 *
	 * @param value
	 *            the double value
	 * @return the rational number
	 */
	public static BigRational valueOf( final BigDecimal value ) {
		if( value.compareTo( BigDecimal.ZERO ) == 0 )
			return BigRational.ZERO;
		if( value.compareTo( BigDecimal.ONE ) == 0 )
			return BigRational.ONE;

		final int scale = value.scale();
		if( scale == 0 )
			return new BigRational( value, BigDecimal.ONE );
		else if( scale < 0 ) {
			final BigDecimal n = new BigDecimal( value.unscaledValue() ).multiply( BigDecimal.ONE.movePointLeft( value.scale() ) );
			return new BigRational( n, BigDecimal.ONE );
		}
		else {
			final BigDecimal n = new BigDecimal( value.unscaledValue() );
			final BigDecimal d = BigDecimal.ONE.movePointRight( value.scale() );
			return new BigRational( n, d );
		}
	}

	/**
	 * Creates a rational number of the specified numerator/denominator BigDecimal values.
	 *
	 * @param numerator
	 *            the numerator {@link BigDecimal} value
	 * @param denominator
	 *            the denominator {@link BigDecimal} value (0 not allowed)
	 * @return the rational number
	 * @throws ArithmeticException
	 *             if the denominator is 0 (division by zero)
	 */
	public static BigRational valueOf( final BigDecimal numerator, final BigDecimal denominator ) {
		return BigRational.valueOf( numerator ).divide( BigRational.valueOf( denominator ) );
	}

	/**
	 * Creates a rational number of the specified {@link BigInteger} value.
	 *
	 * @param value
	 *            the {@link BigInteger} value
	 * @return the rational number
	 */
	public static BigRational valueOf( final BigInteger value ) {
		if( value.compareTo( BigInteger.ZERO ) == 0 )
			return BigRational.ZERO;
		if( value.compareTo( BigInteger.ONE ) == 0 )
			return BigRational.ONE;
		return BigRational.valueOf( value, BigInteger.ONE );
	}

	/**
	 * Creates a rational number of the specified numerator/denominator BigInteger values.
	 *
	 * @param numerator
	 *            the numerator {@link BigInteger} value
	 * @param denominator
	 *            the denominator {@link BigInteger} value (0 not allowed)
	 * @return the rational number
	 * @throws ArithmeticException
	 *             if the denominator is 0 (division by zero)
	 */
	public static BigRational valueOf( final BigInteger numerator, final BigInteger denominator ) {
		return BigRational.of( new BigDecimal( numerator ), new BigDecimal( denominator ) );
	}

	public static BigRational valueOf( final boolean positive, final String integerPart, final String fractionPart, final String fractionRepeatPart, final String exponentPart ) {
		BigRational result = BigRational.ZERO;

		if( fractionRepeatPart != null && fractionRepeatPart.length() > 0 ) {
			final BigInteger lotsOfNines = BigInteger.TEN.pow( fractionRepeatPart.length() ).subtract( BigInteger.ONE );
			result = BigRational.valueOf( new BigInteger( fractionRepeatPart ), lotsOfNines );
		}

		if( fractionPart != null && fractionPart.length() > 0 ) {
			result = result.add( BigRational.valueOf( new BigInteger( fractionPart ) ) );
			result = result.divide( BigInteger.TEN.pow( fractionPart.length() ) );
		}

		if( integerPart != null && integerPart.length() > 0 )
			result = result.add( new BigInteger( integerPart ) );

		if( exponentPart != null && exponentPart.length() > 0 ) {
			final int exponent = Integer.parseInt( exponentPart );
			final BigInteger powerOfTen = BigInteger.TEN.pow( Math.abs( exponent ) );
			result = exponent >= 0 ? result.multiply( powerOfTen ) : result.divide( powerOfTen );
		}

		if( !positive )
			result = result.negate();

		return result;
	}

	/**
	 * Creates a rational number of the specified double value.
	 *
	 * @param value
	 *            the double value
	 * @return the rational number
	 * @throws NumberFormatException
	 *             if the double value is Infinite or NaN.
	 */
	public static BigRational valueOf( final double value ) {
		if( value == 0.0 )
			return BigRational.ZERO;
		if( value == 1.0 )
			return BigRational.ONE;
		if( Double.isInfinite( value ) )
			throw new NumberFormatException( "Infinite" );
		if( Double.isNaN( value ) )
			throw new NumberFormatException( "NaN" );
		return BigRational.valueOf( new BigDecimal( String.valueOf( value ) ) );
	}

	/**
	 * Creates a rational number of the specified int value.
	 *
	 * @param value
	 *            the int value
	 * @return the rational number
	 */
	public static BigRational valueOf( final int value ) {
		if( value == 0 )
			return BigRational.ZERO;
		if( value == 1 )
			return BigRational.ONE;
		return new BigRational( value );
	}

	/**
	 * Creates a rational number of the specified numerator/denominator int values.
	 *
	 * @param numerator
	 *            the numerator int value
	 * @param denominator
	 *            the denominator int value (0 not allowed)
	 * @return the rational number
	 * @throws ArithmeticException
	 *             if the denominator is 0 (division by zero)
	 */
	public static BigRational valueOf( final int numerator, final int denominator ) {
		return BigRational.of( BigDecimal.valueOf( numerator ), BigDecimal.valueOf( denominator ) );
	}

	/**
	 * Creates a rational number of the specified integer and fraction parts.
	 *
	 * <p>
	 * Useful to create numbers like 3 1/2 (= three and a half = 3.5) by calling
	 * <code>BigRational.valueOf(3, 1, 2)</code>.
	 * </p>
	 * <p>
	 * To create a negative rational only the integer part argument is allowed to be negative:
	 * to create -3 1/2 (= minus three and a half = -3.5) call <code>BigRational.valueOf(-3, 1, 2)</code>.
	 * </p>
	 *
	 * @param integer
	 *            the integer part int value
	 * @param fractionNumerator
	 *            the fraction part numerator int value (negative not allowed)
	 * @param fractionDenominator
	 *            the fraction part denominator int value (0 or negative not allowed)
	 * @return the rational number
	 * @throws ArithmeticException
	 *             if the fraction part denominator is 0 (division by zero),
	 *             or if the fraction part numerator or denominator is negative
	 */
	public static BigRational valueOf( final int integer, final int fractionNumerator, final int fractionDenominator ) {
		if( fractionNumerator < 0 || fractionDenominator < 0 )
			throw new ArithmeticException( "Negative value" );

		final BigRational integerPart = BigRational.valueOf( integer );
		final BigRational fractionPart = BigRational.valueOf( fractionNumerator, fractionDenominator );
		return integerPart.isPositive() ? integerPart.add( fractionPart ) : integerPart.subtract( fractionPart );
	}

	/**
	 * Creates a rational number of the specified string representation.
	 *
	 * <p>
	 * The accepted string representations are:
	 * </p>
	 * <ul>
	 * <li>Output of {@link BigRational#toString()} : "integerPart.fractionPart"</li>
	 * <li>Output of {@link BigRational#toRationalString()} : "numerator/denominator"</li>
	 * <li>Output of <code>toString()</code> of {@link BigDecimal}, {@link BigInteger}, {@link Integer}, ...</li>
	 * <li>Output of <code>toString()</code> of {@link Double}, {@link Float} - except "Infinity", "-Infinity" and "NaN"</li>
	 * </ul>
	 *
	 * @param string
	 *            the string representation to convert
	 * @return the rational number
	 * @throws ArithmeticException
	 *             if the denominator is 0 (division by zero)
	 */
	public static BigRational valueOf( final String string ) {
		final String[] strings = string.split( "/" );
		BigRational result = BigRational.valueOfSimple( strings[0] );
		for( int i = 1; i < strings.length; i++ )
			result = result.divide( BigRational.valueOfSimple( strings[i] ) );
		return result;
	}

	private static BigRational calculateBernoulli( final int n ) {
		return IntStream.rangeClosed( 0, n ).parallel().mapToObj( k -> {
			BigRational jSum = BigRational.ZERO;
			BigRational bin = BigRational.ONE;

			for( int j = 0; j <= k; j++ ) {
				final BigRational jPowN = BigRational.valueOf( j ).pow( n );
				if( j % 2 == 0 )
					jSum = jSum.add( bin.multiply( jPowN ) );
				else
					jSum = jSum.subtract( bin.multiply( jPowN ) );

				bin = bin.multiply( BigRational.valueOf( k - j ).divide( BigRational.valueOf( j + 1 ) ) );
			}

			return jSum.divide( BigRational.valueOf( k + 1 ) );
		} ).reduce( BigRational.ZERO, BigRational::add );
	}

	private static int countDigits( final BigInteger number ) {
		final double factor = Math.log( 2 ) / Math.log( 10 );
		final int digitCount = (int)(factor * number.bitLength() + 1);
		if( BigInteger.TEN.pow( digitCount - 1 ).compareTo( number ) > 0 )
			return digitCount - 1;
		return digitCount;
	}

	private static BigRational of( final BigDecimal numerator, final BigDecimal denominator ) {
		if( numerator.signum() == 0 && denominator.signum() != 0 )
			return BigRational.ZERO;
		if( numerator.compareTo( BigDecimal.ONE ) == 0 && denominator.compareTo( BigDecimal.ONE ) == 0 )
			return BigRational.ONE;
		return new BigRational( numerator, denominator );
	}

	private static BigRational valueOfSimple( final String string ) {
		return BigRational.valueOf( new BigDecimal( string ) );
	}

	private BigRational( final BigDecimal num, final BigDecimal denom ) {
		BigDecimal n = num;
		BigDecimal d = denom;

		if( d.signum() == 0 )
			throw new ArithmeticException( "Divide by zero" );

		if( d.signum() < 0 ) {
			n = n.negate();
			d = d.negate();
		}

		this.numerator = n;
		this.denominator = d;
	}

	private BigRational( final int value ) {
		this( BigDecimal.valueOf( value ), BigDecimal.ONE );
	}

	/**
	 * Returns the absolute value of this rational number.
	 *
	 * <p>
	 * The result has no loss of precision.
	 * </p>
	 *
	 * <p>
	 * Examples:
	 * </p>
	 * <ul>
	 * <li><code>BigRational.valueOf(-2).abs()</code> returns <code>BigRational.valueOf(2)</code></li>
	 * <li><code>BigRational.valueOf(2).abs()</code> returns <code>BigRational.valueOf(2)</code></li>
	 * </ul>
	 *
	 * @return the absolute rational number (positive, or 0 if this rational is 0)
	 */
	public BigRational abs() {
		return this.isPositive() ? this : this.negate();
	}

	/**
	 * Calculates the addition (+) of this rational number and the specified argument.
	 *
	 * <p>
	 * This is functionally identical to
	 * <code>this.add(BigRational.valueOf(value))</code>
	 * but slightly faster.
	 * </p>
	 *
	 * <p>
	 * The result has no loss of precision.
	 * </p>
	 *
	 * @param value
	 *            the {@link BigInteger} to add
	 * @return the resulting rational number
	 */
	public BigRational add( final BigInteger value ) {
		if( value.equals( BigInteger.ZERO ) )
			return this;
		return this.add( new BigDecimal( value ) );
	}

	/**
	 * Calculates the addition (+) of this rational number and the specified argument.
	 *
	 * <p>
	 * The result has no loss of precision.
	 * </p>
	 *
	 * @param value
	 *            the rational number to add
	 * @return the resulting rational number
	 */
	public BigRational add( final BigRational value ) {
		if( this.denominator.equals( value.denominator ) )
			return BigRational.of( this.numerator.add( value.numerator ), this.denominator );

		final BigDecimal n = this.numerator.multiply( value.denominator ).add( value.numerator.multiply( this.denominator ) );
		final BigDecimal d = this.denominator.multiply( value.denominator );
		return BigRational.of( n, d );
	}

	/**
	 * Calculates the addition (+) of this rational number and the specified argument.
	 *
	 * <p>
	 * This is functionally identical to
	 * <code>this.add(BigRational.valueOf(value))</code>
	 * but slightly faster.
	 * </p>
	 *
	 * <p>
	 * The result has no loss of precision.
	 * </p>
	 *
	 * @param value
	 *            the int value to add
	 * @return the resulting rational number
	 */
	public BigRational add( final int value ) {
		if( value == 0 )
			return this;
		return this.add( BigInteger.valueOf( value ) );
	}

	@Override
	public int compareTo( final BigRational other ) {
		if( this == other )
			return 0;
		return this.numerator.multiply( other.denominator ).compareTo( this.denominator.multiply( other.numerator ) );
	}

	/**
	 * Calculates the decrement of this rational number (- 1).
	 *
	 * <p>
	 * This is functionally identical to
	 * <code>this.subtract(BigRational.ONE)</code>
	 * but slightly faster.
	 * </p>
	 *
	 * <p>
	 * The result has no loss of precision.
	 * </p>
	 *
	 * @return the decremented rational number
	 */
	public BigRational decrement() {
		return BigRational.of( this.numerator.subtract( this.denominator ), this.denominator );
	}

	/**
	 * Calculates the division (/) of this rational number and the specified argument.
	 *
	 * <p>
	 * This is functionally identical to
	 * <code>this.divide(BigRational.valueOf(value))</code>
	 * but slightly faster.
	 * </p>
	 *
	 * <p>
	 * The result has no loss of precision.
	 * </p>
	 *
	 * @param value
	 *            the {@link BigInteger} to divide (0 is not allowed)
	 * @return the resulting rational number
	 * @throws ArithmeticException
	 *             if the argument is 0 (division by zero)
	 */
	public BigRational divide( final BigInteger value ) {
		if( value.equals( BigInteger.ONE ) )
			return this;

		return this.divide( new BigDecimal( value ) );
	}

	/**
	 * Calculates the division (/) of this rational number and the specified argument.
	 *
	 * <p>
	 * The result has no loss of precision.
	 * </p>
	 *
	 * @param value
	 *            the rational number to divide (0 is not allowed)
	 * @return the resulting rational number
	 * @throws ArithmeticException
	 *             if the argument is 0 (division by zero)
	 */
	public BigRational divide( final BigRational value ) {
		if( value.equals( BigRational.ONE ) )
			return this;

		final BigDecimal n = this.numerator.multiply( value.denominator );
		final BigDecimal d = this.denominator.multiply( value.numerator );
		return BigRational.of( n, d );
	}

	/**
	 * Calculates the division (/) of this rational number and the specified argument.
	 *
	 * <p>
	 * This is functionally identical to
	 * <code>this.divide(BigRational.valueOf(value))</code>
	 * but slightly faster.
	 * </p>
	 *
	 * <p>
	 * The result has no loss of precision.
	 * </p>
	 *
	 * @param value
	 *            the int value to divide (0 is not allowed)
	 * @return the resulting rational number
	 * @throws ArithmeticException
	 *             if the argument is 0 (division by zero)
	 */
	public BigRational divide( final int value ) {
		return this.divide( BigInteger.valueOf( value ) );
	}

	@Override
	public boolean equals( final Object obj ) {
		if( obj == this )
			return true;

		if( !(obj instanceof final BigRational other) || !this.numerator.equals( other.numerator ) )
			return false;
		return this.denominator.equals( other.denominator );
	}

	/**
	 * Returns the fraction part of this rational number.
	 *
	 * <p>
	 * Examples:
	 * </p>
	 * <ul>
	 * <li><code>BigRational.valueOf(3.5).integerPart()</code> returns <code>BigRational.valueOf(0.5)</code></li>
	 * </ul>
	 *
	 * @return the fraction part of this rational number
	 */
	public BigRational fractionPart() {
		return BigRational.of( this.numerator.remainder( this.denominator ), this.denominator );
	}

	/**
	 * Returns the denominator of this rational number as BigDecimal.
	 *
	 * <p>
	 * Guaranteed to not be 0.
	 * </p>
	 * <p>
	 * Guaranteed to be positive.
	 * </p>
	 *
	 * @return the denominator as BigDecimal
	 */
	public BigDecimal getDenominator() {
		return this.denominator;
	}

	/**
	 * Returns the denominator of this rational number as BigInteger.
	 *
	 * <p>
	 * Guaranteed to not be 0.
	 * </p>
	 * <p>
	 * Guaranteed to be positive.
	 * </p>
	 *
	 * @return the denominator as BigInteger
	 */
	public BigInteger getDenominatorBigInteger() {
		return this.denominator.toBigInteger();
	}

	/**
	 * Returns the numerator of this rational number as BigDecimal.
	 *
	 * @return the numerator as BigDecimal
	 */
	public BigDecimal getNumerator() {
		return this.numerator;
	}

	/**
	 * Returns the numerator of this rational number as BigInteger.
	 *
	 * @return the numerator as BigInteger
	 */
	public BigInteger getNumeratorBigInteger() {
		return this.numerator.toBigInteger();
	}

	@Override
	public int hashCode() {
		if( this.isZero() )
			return 0;
		return this.numerator.hashCode() + this.denominator.hashCode();
	}

	/**
	 * Calculates the increment of this rational number (+ 1).
	 *
	 * <p>
	 * This is functionally identical to
	 * <code>this.add(BigRational.ONE)</code>
	 * but slightly faster.
	 * </p>
	 *
	 * <p>
	 * The result has no loss of precision.
	 * </p>
	 *
	 * @return the incremented rational number
	 */
	public BigRational increment() {
		return BigRational.of( this.numerator.add( this.denominator ), this.denominator );
	}

	/**
	 * Returns the integer part of this rational number.
	 *
	 * <p>
	 * Examples:
	 * </p>
	 * <ul>
	 * <li><code>BigRational.valueOf(3.5).integerPart()</code> returns <code>BigRational.valueOf(3)</code></li>
	 * </ul>
	 *
	 * @return the integer part of this rational number
	 */
	public BigRational integerPart() {
		return BigRational.of( this.numerator.subtract( this.numerator.remainder( this.denominator ) ), this.denominator );
	}

	/**
	 * Returns whether this rational number is an integer number without fraction part.
	 *
	 * @return <code>true</code> if this rational number is an integer number, <code>false</code> if it has a fraction part
	 */
	public boolean isInteger() {
		return this.isIntegerInternal() || this.reduce().isIntegerInternal();
	}

	/**
	 * Returns whether this rational number is zero.
	 *
	 * @return <code>true</code> if this rational number is zero (0), <code>false</code> if it is not zero
	 */
	public boolean isZero() {
		return this.numerator.signum() == 0;
	}

	/**
	 * Calculates the multiplication (*) of this rational number and the specified argument.
	 *
	 * <p>
	 * This is functionally identical to
	 * <code>this.multiply(BigRational.valueOf(value))</code>
	 * but slightly faster.
	 * </p>
	 *
	 * <p>
	 * The result has no loss of precision.
	 * </p>
	 *
	 * @param value
	 *            the {@link BigInteger} to multiply
	 * @return the resulting rational number
	 */
	public BigRational multiply( final BigInteger value ) {
		if( this.isZero() || value.signum() == 0 )
			return BigRational.ZERO;
		if( this.equals( BigRational.ONE ) )
			return BigRational.valueOf( value );
		if( value.equals( BigInteger.ONE ) )
			return this;

		return this.multiply( new BigDecimal( value ) );
	}

	/**
	 * Calculates the multiplication (*) of this rational number and the specified argument.
	 *
	 * <p>
	 * The result has no loss of precision.
	 * </p>
	 *
	 * @param value
	 *            the rational number to multiply
	 * @return the resulting rational number
	 */
	public BigRational multiply( final BigRational value ) {
		if( this.isZero() || value.isZero() )
			return BigRational.ZERO;
		if( this.equals( BigRational.ONE ) )
			return value;
		if( value.equals( BigRational.ONE ) )
			return this;

		final BigDecimal n = this.numerator.multiply( value.numerator );
		final BigDecimal d = this.denominator.multiply( value.denominator );
		return BigRational.of( n, d );
	}

	/**
	 * Calculates the multiplication (*) of this rational number and the specified argument.
	 *
	 * <p>
	 * This is functionally identical to
	 * <code>this.multiply(BigRational.valueOf(value))</code>
	 * but slightly faster.
	 * </p>
	 *
	 * <p>
	 * The result has no loss of precision.
	 * </p>
	 *
	 * @param value
	 *            the int value to multiply
	 * @return the resulting rational number
	 */
	public BigRational multiply( final int value ) {
		return this.multiply( BigInteger.valueOf( value ) );
	}

	/**
	 * Negates this rational number (inverting the sign).
	 *
	 * <p>
	 * The result has no loss of precision.
	 * </p>
	 *
	 * <p>
	 * Examples:
	 * </p>
	 * <ul>
	 * <li><code>BigRational.valueOf(3.5).negate()</code> returns <code>BigRational.valueOf(-3.5)</code></li>
	 * </ul>
	 *
	 * @return the negated rational number
	 */
	public BigRational negate() {
		if( this.isZero() )
			return this;

		return BigRational.of( this.numerator.negate(), this.denominator );
	}

	/**
	 * Calculates this rational number to the power (x<sup>y</sup>) of the specified argument.
	 *
	 * <p>
	 * The result has no loss of precision.
	 * </p>
	 *
	 * @param exponent
	 *            exponent to which this rational number is to be raised
	 * @return the resulting rational number
	 */
	public BigRational pow( final int exponent ) {
		if( exponent == 0 )
			return BigRational.ONE;
		if( exponent == 1 )
			return this;

		final BigInteger n;
		final BigInteger d;

		if( exponent > 0 ) {
			n = this.numerator.toBigInteger().pow( exponent );
			d = this.denominator.toBigInteger().pow( exponent );
		}
		else {
			n = this.denominator.toBigInteger().pow( -exponent );
			d = this.numerator.toBigInteger().pow( -exponent );
		}

		return BigRational.valueOf( n, d );
	}

	/**
	 * Calculates the reciprocal of this rational number (1/x).
	 *
	 * <p>
	 * The result has no loss of precision.
	 * </p>
	 *
	 * <p>
	 * Examples:
	 * </p>
	 * <ul>
	 * <li><code>BigRational.valueOf(0.5).reciprocal()</code> returns <code>BigRational.valueOf(2)</code></li>
	 * <li><code>BigRational.valueOf(-2).reciprocal()</code> returns <code>BigRational.valueOf(-0.5)</code></li>
	 * </ul>
	 *
	 * @return the reciprocal rational number
	 * @throws ArithmeticException
	 *             if this number is 0 (division by zero)
	 */
	public BigRational reciprocal() {
		return BigRational.of( this.denominator, this.numerator );
	}

	/**
	 * Reduces this rational number to the smallest numerator/denominator with the same value.
	 *
	 * @return the reduced rational number
	 */
	public BigRational reduce() {
		BigInteger n = this.numerator.toBigInteger();
		BigInteger d = this.denominator.toBigInteger();

		final BigInteger gcd = n.gcd( d );
		n = n.divide( gcd );
		d = d.divide( gcd );

		return BigRational.valueOf( n, d );
	}

	/**
	 * Returns the signum function of this rational number.
	 *
	 * @return -1, 0 or 1 as the value of this rational number is negative, zero or positive.
	 */
	public int signum() {
		return this.numerator.signum();
	}

	/**
	 * Calculates the subtraction (-) of this rational number and the specified argument.
	 *
	 * <p>
	 * This is functionally identical to
	 * <code>this.subtract(BigRational.valueOf(value))</code>
	 * but slightly faster.
	 * </p>
	 *
	 * <p>
	 * The result has no loss of precision.
	 * </p>
	 *
	 * @param value
	 *            the {@link BigInteger} to subtract
	 * @return the resulting rational number
	 */
	public BigRational subtract( final BigInteger value ) {
		if( value.equals( BigInteger.ZERO ) )
			return this;
		return this.subtract( new BigDecimal( value ) );
	}

	/**
	 * Calculates the subtraction (-) of this rational number and the specified argument.
	 *
	 * <p>
	 * The result has no loss of precision.
	 * </p>
	 *
	 * @param value
	 *            the rational number to subtract
	 * @return the resulting rational number
	 */
	public BigRational subtract( final BigRational value ) {
		if( this.denominator.equals( value.denominator ) )
			return BigRational.of( this.numerator.subtract( value.numerator ), this.denominator );

		final BigDecimal n = this.numerator.multiply( value.denominator ).subtract( value.numerator.multiply( this.denominator ) );
		final BigDecimal d = this.denominator.multiply( value.denominator );
		return BigRational.of( n, d );
	}

	/**
	 * Calculates the subtraction (-) of this rational number and the specified argument.
	 *
	 * <p>
	 * This is functionally identical to
	 * <code>this.subtract(BigRational.valueOf(value))</code>
	 * but slightly faster.
	 * </p>
	 *
	 * <p>
	 * The result has no loss of precision.
	 * </p>
	 *
	 * @param value
	 *            the int value to subtract
	 * @return the resulting rational number
	 */
	public BigRational subtract( final int value ) {
		if( value == 0 )
			return this;
		return this.subtract( BigInteger.valueOf( value ) );
	}

	/**
	 * Returns this rational number as a {@link BigDecimal}.
	 *
	 * @return the {@link BigDecimal} value
	 */
	public BigDecimal toBigDecimal() {
		final int precision = Math.max( this.precision(), MathContext.DECIMAL128.getPrecision() );
		return this.toBigDecimal( new MathContext( precision ) );
	}

	/**
	 * Returns this rational number as a {@link BigDecimal} with the precision specified by the {@link MathContext}.
	 *
	 * @param mc
	 *            the {@link MathContext} specifying the precision of the calculated result
	 * @return the {@link BigDecimal}
	 */
	public BigDecimal toBigDecimal( final MathContext mc ) {
		return this.numerator.divide( this.denominator, mc );
	}

	/**
	 * Returns this rational number as a double value.
	 *
	 * @return the double value
	 */
	public double toDouble() {
		// TODO best accuracy or maybe bigDecimalValue().doubleValue() is better?
		return this.numerator.doubleValue() / this.denominator.doubleValue();
	}

	/**
	 * Returns this rational number as a float value.
	 *
	 * @return the float value
	 */
	public float toFloat() {
		return this.numerator.floatValue() / this.denominator.floatValue();
	}

	/**
	 * Returns the string representation of this rational number as integer and fraction parts in the form "integerPart fractionNominator/fractionDenominator".
	 *
	 * <p>
	 * The integer part is omitted if it is 0 (when this absolute rational number is smaller than 1).
	 * </p>
	 * <p>
	 * The fraction part is omitted it it is 0 (when this rational number is an integer).
	 * </p>
	 * <p>
	 * If this rational number is 0, then "0" is returned.
	 * </p>
	 *
	 * <p>
	 * Example: <code>BigRational.valueOf(3.5).toIntegerRationalString()</code> returns <code>"3 1/2"</code>.
	 * </p>
	 *
	 * @return the integer and fraction rational string representation
	 * @see #valueOf(int, int, int)
	 */
	public String toIntegerRationalString() {
		final BigDecimal fractionNumerator = this.numerator.remainder( this.denominator );
		final BigDecimal integerNumerator = this.numerator.subtract( fractionNumerator );
		final BigDecimal integerPart = integerNumerator.divide( this.denominator );

		final StringBuilder result = new StringBuilder();
		if( integerPart.signum() != 0 )
			result.append( integerPart );

		if( fractionNumerator.signum() != 0 ) {
			if( result.length() > 0 )
				result.append( ' ' );
			result.append( fractionNumerator.abs() );
			result.append( '/' );
			result.append( this.denominator );
		}

		if( result.length() == 0 )
			result.append( '0' );

		return result.toString();
	}

	/**
	 * Returns a plain string representation of this rational number without any exponent.
	 *
	 * @return the plain string representation
	 * @see BigDecimal#toPlainString()
	 */
	public String toPlainString() {
		if( this.isZero() )
			return "0";
		if( this.isIntegerInternal() )
			return this.numerator.toPlainString();
		return this.toBigDecimal().toPlainString();
	}

	/**
	 * Returns the string representation of this rational number in the form "numerator/denominator".
	 *
	 * <p>
	 * The resulting string is a valid input of the {@link #valueOf(String)} method.
	 * </p>
	 *
	 * <p>
	 * Examples:
	 * </p>
	 * <ul>
	 * <li><code>BigRational.valueOf(0.5).toRationalString()</code> returns <code>"1/2"</code></li>
	 * <li><code>BigRational.valueOf(2).toRationalString()</code> returns <code>"2"</code></li>
	 * <li><code>BigRational.valueOf(4, 4).toRationalString()</code> returns <code>"4/4"</code> (not reduced)</li>
	 * </ul>
	 *
	 * @return the rational number string representation in the form "numerator/denominator", or "0" if the rational number is 0.
	 * @see #valueOf(String)
	 * @see #valueOf(int, int)
	 */
	public String toRationalString() {
		if( this.isZero() )
			return "0";
		if( this.isIntegerInternal() )
			return this.numerator.toString();
		return this.numerator + "/" + this.denominator;
	}

	@Override
	public String toString() {
		if( this.isZero() )
			return "0";
		if( this.isIntegerInternal() )
			return this.numerator.toString();
		return this.toBigDecimal().toString();
	}

	/**
	 * Returns a rational number with approximatively <code>this</code> value and the specified precision.
	 *
	 * @param precision
	 *            the precision (number of significant digits) of the calculated result, or 0 for unlimited precision
	 * @return the calculated rational number with the specified precision
	 */
	public BigRational withPrecision( final int precision ) {
		return BigRational.valueOf( this.toBigDecimal( new MathContext( precision ) ) );
	}

	/**
	 * Returns a rational number with approximatively <code>this</code> value and the specified scale.
	 *
	 * @param scale
	 *            the scale (number of digits after the decimal point) of the calculated result
	 * @return the calculated rational number with the specified scale
	 */
	public BigRational withScale( final int scale ) {
		return BigRational.valueOf( this.toBigDecimal().setScale( scale, RoundingMode.HALF_UP ) );
	}

	private BigRational add( final BigDecimal value ) {
		return BigRational.of( this.numerator.add( value.multiply( this.denominator ) ), this.denominator );
	}

	private BigRational divide( final BigDecimal value ) {
		final BigDecimal n = this.numerator;
		final BigDecimal d = this.denominator.multiply( value );
		return BigRational.of( n, d );
	}

	/**
	 * Returns whether this rational number is an integer number without fraction part.
	 *
	 * <p>
	 * Will return <code>false</code> if this number is not reduced to the integer representation yet (e.g. 4/4 or 4/2)
	 * </p>
	 *
	 * @return <code>true</code> if this rational number is an integer number, <code>false</code> if it has a fraction part
	 * @see #isInteger()
	 */
	private boolean isIntegerInternal() {
		return this.denominator.compareTo( BigDecimal.ONE ) == 0;
	}

	private boolean isPositive() {
		return this.numerator.signum() > 0;
	}

	/**
	 * Finds the maximum (larger) of two rational numbers.
	 *
	 * @param value
	 *            the rational number to compare with
	 * @return the minimum rational number, either <code>this</code> or the argument <code>value</code>
	 */
	private BigRational max( final BigRational value ) {
		return this.compareTo( value ) >= 0 ? this : value;
	}

	/**
	 * Finds the minimum (smaller) of two rational numbers.
	 *
	 * @param value
	 *            the rational number to compare with
	 * @return the minimum rational number, either <code>this</code> or the argument <code>value</code>
	 */
	private BigRational min( final BigRational value ) {
		return this.compareTo( value ) <= 0 ? this : value;
	}

	// private, because we want to hide that we use BigDecimal internally
	private BigRational multiply( final BigDecimal value ) {
		final BigDecimal n = this.numerator.multiply( value );
		final BigDecimal d = this.denominator;
		return BigRational.of( n, d );
	}

	// TODO what is precision of a rational?
	private int precision() {
		return BigRational.countDigits( this.numerator.toBigInteger() ) + BigRational.countDigits( this.denominator.toBigInteger() );
	}

	private BigRational subtract( final BigDecimal value ) {
		return BigRational.of( this.numerator.subtract( value.multiply( this.denominator ) ), this.denominator );
	}

}
