/*******************************************************************************
 * Copyright (c) 2017 Eric Obermühlner
 * License: MIT
 * https://github.com/eobermuhlner/big-math
 * Version: 2.3.0
 *
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.bigcalc;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;
import java.util.Objects;

import de.mn77.base.data.struct.SimpleList;


/**
 * Utility class to calculate taylor series efficiently until the maximum error (as defined by the precision in the {@link MathContext} is reached.
 *
 * Stores the factors of the taylor series terms so that future calculations will be faster.
 */
public abstract class BigSeriesCalculator {

	private final boolean calculateInPairs;

	private final List<BigRational> factors = new SimpleList<>();


	/**
	 * Constructs a {@link BigSeriesCalculator} that calculates single terms.
	 */
	protected BigSeriesCalculator() {
		this( false );
	}

	/**
	 * Constructs a {@link BigSeriesCalculator} with control over whether the sum terms are calculated in pairs.
	 *
	 * <p>
	 * Calculation of pairs is useful for taylor series where the terms alternate the sign.
	 * In these cases it is more efficient to calculate two terms at once check then whether the acceptable error has been reached.
	 * </p>
	 *
	 * @param calculateInPairs
	 *            <code>true</code> to calculate the terms in pairs, <code>false</code> to calculate single terms
	 */
	protected BigSeriesCalculator( final boolean calculateInPairs ) {
		this.calculateInPairs = calculateInPairs;
	}

	/**
	 * Calculates the series for the specified value x and the precision defined in the {@link MathContext}.
	 *
	 * @param x
	 *            the value x
	 * @param mathContext
	 *            the {@link MathContext}
	 * @return the calculated result
	 */
	public BigDecimal calculate( final BigDecimal x, final MathContext mathContext ) {
		final BigDecimal acceptableError = BigDecimal.ONE.movePointLeft( mathContext.getPrecision() + 1 );

		final BigPowerIterator powerIterator = this.createPowerIterator( x, mathContext );

		BigDecimal sum = BigDecimal.ZERO;
		BigDecimal step;
		int i = 0;

		do {
			BigRational factor;
			BigDecimal xToThePower;

			factor = this.getFactor( i );
			xToThePower = powerIterator.getCurrentPower();
			powerIterator.calculateNextPower();
			step = factor.getNumerator().multiply( xToThePower ).divide( factor.getDenominator(), mathContext );
			i++;

			if( this.calculateInPairs ) {
				factor = this.getFactor( i );
				xToThePower = powerIterator.getCurrentPower();
				powerIterator.calculateNextPower();
				final BigDecimal step2 = factor.getNumerator().multiply( xToThePower ).divide( factor.getDenominator(), mathContext );
				step = step.add( step2 );
				i++;
			}

			sum = sum.add( step );
			//System.out.println(sum + " " + step);
		}
		while( step.abs().compareTo( acceptableError ) > 0 );

		return sum.round( mathContext );
	}

	/**
	 * Calculates the factor of the next term.
	 */
	protected abstract void calculateNextFactor();

	/**
	 * Creates the {@link BigPowerIterator} used for this series.
	 *
	 * @param x
	 *            the value x
	 * @param mathContext
	 *            the {@link MathContext}
	 * @return the {@link BigPowerIterator}
	 */
	protected abstract BigPowerIterator createPowerIterator( BigDecimal x, MathContext mathContext );

	/**
	 * Returns the factor of the highest term already calculated.
	 * <p>
	 * When called for the first time will return the factor of the first term (index 0).
	 * </p>
	 * <p>
	 * After this call the method {@link #calculateNextFactor()} will be called to prepare for the next term.
	 * </p>
	 *
	 * @return the factor of the highest term
	 */
	protected abstract BigRational getCurrentFactor();

	/**
	 * Returns the factor of the term with specified index.
	 *
	 * All mutable state of this class (and all its subclasses) must be modified in this method.
	 * This method is synchronized to allow thread-safe usage of this class.
	 *
	 * @param index
	 *            the index (starting with 0)
	 * @return the factor of the specified term
	 */
	protected synchronized BigRational getFactor( final int index ) {

		while( this.factors.size() <= index ) {
			final BigRational factor = this.getCurrentFactor();
			this.addFactor( factor );
			this.calculateNextFactor();
		}

		return this.factors.get( index );
	}

	private void addFactor( final BigRational factor ) {
		this.factors.add( Objects.requireNonNull( factor, "Factor cannot be null" ) );
	}

}
