/*******************************************************************************
 * Copyright (c) 2017 Eric Obermühlner
 * License: MIT
 * https://github.com/eobermuhlner/big-math
 * Version: 2.3.0
 *
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.bigcalc;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import de.mn77.base.data.util.Lib_BigMath;


/**
 * @created 13.10.2021
 * @implNote Check with https://keisan.casio.com/calculator
 */
public class Lib_BigCalc {

	private static final BigDecimal DOUBLE_MAX_VALUE           = BigDecimal.valueOf( Double.MAX_VALUE );
	private static final int        EXPECTED_INITIAL_PRECISION = 15;

	private static volatile BigDecimal log10Cache;
	private static final Object        log10CacheLock = new Object();

	private static volatile BigDecimal log2Cache;
	private static final Object        log2CacheLock = new Object();

	private static volatile BigDecimal log3Cache;
	private static final Object        log3CacheLock = new Object();


	public static BigDecimal exp( final BigDecimal x, final MathContext mathContext ) {
		Lib_BigMath.checkMathContextNoUnlimited( mathContext );
		if( x.signum() == 0 )
			return BigDecimal.ONE;

		return Lib_BigCalc.expIntegralFractional( x, mathContext );
	}

	public static int exponent( final BigDecimal value ) {
		return value.precision() - value.scale() - 1;
	}

	public static BigDecimal fractionalPart( final BigDecimal value ) {
		return value.subtract( Lib_BigCalc.integralPart( value ) );
	}

	public static BigDecimal integralPart( final BigDecimal value ) {
		return value.setScale( 0, RoundingMode.DOWN );
	}

	public static boolean isDoubleValue( final BigDecimal value ) {
		if( value.compareTo( Lib_BigCalc.DOUBLE_MAX_VALUE ) > 0 || value.compareTo( Lib_BigCalc.DOUBLE_MAX_VALUE.negate() ) < 0 )
			return false;

		return true;
	}

	public static BigDecimal log( final BigDecimal x, final MathContext mathContext ) {
		Lib_BigMath.checkMathContextNoUnlimited( mathContext );
		if( x.signum() <= 0 )
			throw new ArithmeticException( "Illegal log(x) for x <= 0: x = " + x );
		if( x.compareTo( BigDecimal.ONE ) == 0 )
			return BigDecimal.ZERO;

		BigDecimal result;

		switch( x.compareTo( BigDecimal.TEN ) ) {
			case 0:
				result = Lib_BigCalc.logTen( mathContext );
				break;
			case 1:
				result = Lib_BigCalc.logUsingExponent( x, mathContext );
				break;
			default:
				result = Lib_BigCalc.logUsingTwoThree( x, mathContext );
		}

		return Lib_BigMath.round( result, mathContext );
	}

	public static BigDecimal mantissa( final BigDecimal value ) {
		final int exponent = Lib_BigCalc.exponent( value );
		if( exponent == 0 )
			return value;

		return value.movePointLeft( exponent );
	}

	public static BigDecimal pow( final BigDecimal x, final BigDecimal y, final MathContext mathContext ) {
		Lib_BigMath.checkMathContextNoUnlimited( mathContext );
		if( x.signum() == 0 )
			switch( y.signum() ) {
				case 0:
					return Lib_BigMath.round( BigDecimal.ONE, mathContext );
				case 1:
					return Lib_BigMath.round( BigDecimal.ZERO, mathContext );
			}

		// TODO optimize y=0, y=1, y=10^k, y=-1, y=-10^k

		try {
			final long longValue = y.longValueExact();
			return Lib_BigCalc.pow( x, longValue, mathContext );
		}
		catch( final ArithmeticException ex ) {
			// ignored
		}

		if( Lib_BigCalc.fractionalPart( y ).signum() == 0 )
			return Lib_BigCalc.powInteger( x, y, mathContext );

		// x^y = exp(y*log(x))
		final MathContext mc = new MathContext( mathContext.getPrecision() + 6, mathContext.getRoundingMode() );
		final BigDecimal result = Lib_BigCalc.exp( y.multiply( Lib_BigCalc.log( x, mc ), mc ), mc );

		return Lib_BigMath.round( result, mathContext );
	}

	public static BigDecimal pow( BigDecimal x, long y, final MathContext mathContext ) {
		final MathContext mc = mathContext.getPrecision() == 0 ? mathContext : new MathContext( mathContext.getPrecision() + 10, mathContext.getRoundingMode() );

		// TODO optimize y=0, y=1, y=10^k, y=-1, y=-10^k

		if( y < 0 ) {
			final BigDecimal value = Lib_BigCalc.reciprocal( Lib_BigCalc.pow( x, -y, mc ), mc );
			return Lib_BigMath.round( value, mathContext );
		}

		BigDecimal result = BigDecimal.ONE;

		while( y > 0 ) {

			if( (y & 1) == 1 ) {
				// odd exponent -> multiply result with x
				result = result.multiply( x, mc );
				y -= 1;
			}

			if( y > 0 )
				// even exponent -> square x
				x = x.multiply( x, mc );

			y >>= 1;
		}

		return Lib_BigMath.round( result, mathContext );
	}

	public static BigDecimal reciprocal( final BigDecimal x, final MathContext mathContext ) {
		return BigDecimal.ONE.divide( x, mathContext );
	}

	private static BigDecimal expIntegralFractional( final BigDecimal x, final MathContext mathContext ) {
		final BigDecimal integralPart = Lib_BigCalc.integralPart( x );

		if( integralPart.signum() == 0 )
			return Lib_BigCalc.expTaylor( x, mathContext );

		final BigDecimal fractionalPart = x.subtract( integralPart );

		final MathContext mc = new MathContext( mathContext.getPrecision() + 10, mathContext.getRoundingMode() );

		final BigDecimal z = BigDecimal.ONE.add( fractionalPart.divide( integralPart, mc ) );
		final BigDecimal t = Lib_BigCalc.expTaylor( z, mc );

		final BigDecimal result = Lib_BigCalc.pow( t, integralPart.intValueExact(), mc );

		return Lib_BigMath.round( result, mathContext );
	}

	private static BigDecimal expTaylor( BigDecimal x, final MathContext mathContext ) {
		final MathContext mc = new MathContext( mathContext.getPrecision() + 6, mathContext.getRoundingMode() );

		x = x.divide( BigDecimal.valueOf( 256 ), mc );

		BigDecimal result = BigExpCalculator.INSTANCE.calculate( x, mc );
		result = Lib_BigCalc.pow( result, 256, mc );
		return Lib_BigMath.round( result, mathContext );
	}

	private static BigDecimal logTen( final MathContext mathContext ) {
		BigDecimal result = null;

		synchronized( Lib_BigCalc.log10CacheLock ) {

			if( Lib_BigCalc.log10Cache != null && mathContext.getPrecision() <= Lib_BigCalc.log10Cache.precision() )
				result = Lib_BigCalc.log10Cache;
			else {
				Lib_BigCalc.log10Cache = Lib_BigCalc.logUsingNewton( BigDecimal.TEN, mathContext );
				return Lib_BigCalc.log10Cache;
			}
		}

		return Lib_BigMath.round( result, mathContext );
	}

	private static BigDecimal logThree( final MathContext mathContext ) {
		BigDecimal result = null;

		synchronized( Lib_BigCalc.log3CacheLock ) {

			if( Lib_BigCalc.log3Cache != null && mathContext.getPrecision() <= Lib_BigCalc.log3Cache.precision() )
				result = Lib_BigCalc.log3Cache;
			else {
				Lib_BigCalc.log3Cache = Lib_BigCalc.logUsingNewton( Lib_BigMath.THREE, mathContext );
				return Lib_BigCalc.log3Cache;
			}
		}

		return Lib_BigMath.round( result, mathContext );
	}

	private static BigDecimal logTwo( final MathContext mathContext ) {
		BigDecimal result = null;

		synchronized( Lib_BigCalc.log2CacheLock ) {

			if( Lib_BigCalc.log2Cache != null && mathContext.getPrecision() <= Lib_BigCalc.log2Cache.precision() )
				result = Lib_BigCalc.log2Cache;
			else {
				Lib_BigCalc.log2Cache = Lib_BigCalc.logUsingNewton( Lib_BigMath.TWO, mathContext );
				return Lib_BigCalc.log2Cache;
			}
		}

		return Lib_BigMath.round( result, mathContext );
	}

	private static BigDecimal logUsingExponent( final BigDecimal x, final MathContext mathContext ) {
		final MathContext mcDouble = new MathContext( mathContext.getPrecision() << 1, mathContext.getRoundingMode() );
		final MathContext mc = new MathContext( mathContext.getPrecision() + 4, mathContext.getRoundingMode() );

		final int exponent = Lib_BigCalc.exponent( x );
		final BigDecimal mantissa = Lib_BigCalc.mantissa( x );

		BigDecimal result = Lib_BigCalc.logUsingTwoThree( mantissa, mc );
		if( exponent != 0 )
			result = result.add( BigDecimal.valueOf( exponent ).multiply( Lib_BigCalc.logTen( mcDouble ), mc ) );
		return result;
	}

	private static BigDecimal logUsingNewton( final BigDecimal x, final MathContext mathContext ) {
		// https://en.wikipedia.org/wiki/Natural_logarithm in chapter 'High Precision'
		// y = y + 2 * (x-exp(y)) / (x+exp(y))

		final int maxPrecision = mathContext.getPrecision() + 20;
		final BigDecimal acceptableError = BigDecimal.ONE.movePointLeft( mathContext.getPrecision() + 1 );

		BigDecimal result;
		int adaptivePrecision;
		final double doubleX = x.doubleValue();

		if( doubleX > 0.0 && Lib_BigCalc.isDoubleValue( x ) ) {
			result = BigDecimal.valueOf( Math.log( doubleX ) );
			adaptivePrecision = Lib_BigCalc.EXPECTED_INITIAL_PRECISION;
		}
		else {
			result = x.divide( Lib_BigMath.TWO, mathContext );
			adaptivePrecision = 1;
		}

		BigDecimal step;

		do {
			adaptivePrecision *= 3;
			if( adaptivePrecision > maxPrecision )
				adaptivePrecision = maxPrecision;
			final MathContext mc = new MathContext( adaptivePrecision, mathContext.getRoundingMode() );

			final BigDecimal expY = Lib_BigCalc.exp( result, mc );
			step = Lib_BigMath.TWO.multiply( x.subtract( expY ) ).divide( x.add( expY ), mc );
			result = result.add( step );
		}
		while( adaptivePrecision < maxPrecision || step.abs().compareTo( acceptableError ) > 0 );

		return result;
	}

	private static BigDecimal logUsingTwoThree( final BigDecimal x, final MathContext mathContext ) {
		final MathContext mcDouble = new MathContext( mathContext.getPrecision() << 1, mathContext.getRoundingMode() );
		final MathContext mc = new MathContext( mathContext.getPrecision() + 4, mathContext.getRoundingMode() );

		int factorOfTwo = 0;
		int powerOfTwo = 1;
		int factorOfThree = 0;
		int powerOfThree = 1;

		double value = x.doubleValue();

		if( value < 0.01 ) {
			// do nothing
		}
		else if( value < 0.1 )
			while( value < 0.6 ) {
				value *= 2;
				factorOfTwo--;
				powerOfTwo <<= 1;
			}
		else if( value < 0.115 ) { // (0.1 - 0.11111 - 0.115) -> (0.9 - 1.0 - 1.035)
			factorOfThree = -2;
			powerOfThree = 9;
		}
		else if( value < 0.14 ) { // (0.115 - 0.125 - 0.14) -> (0.92 - 1.0 - 1.12)
			factorOfTwo = -3;
			powerOfTwo = 8;
		}
		else if( value < 0.2 ) { // (0.14 - 0.16667 - 0.2) - (0.84 - 1.0 - 1.2)
			factorOfTwo = -1;
			powerOfTwo = 2;
			factorOfThree = -1;
			powerOfThree = 3;
		}
		else if( value < 0.3 ) { // (0.2 - 0.25 - 0.3) -> (0.8 - 1.0 - 1.2)
			factorOfTwo = -2;
			powerOfTwo = 4;
		}
		else if( value < 0.42 ) { // (0.3 - 0.33333 - 0.42) -> (0.9 - 1.0 - 1.26)
			factorOfThree = -1;
			powerOfThree = 3;
		}
		else if( value < 0.7 ) { // (0.42 - 0.5 - 0.7) -> (0.84 - 1.0 - 1.4)
			factorOfTwo = -1;
			powerOfTwo = 2;
		}
		else if( value < 1.4 ) { // (0.7 - 1.0 - 1.4) -> (0.7 - 1.0 - 1.4)
			// do nothing
		}
		else if( value < 2.5 ) { // (1.4 - 2.0 - 2.5) -> (0.7 - 1.0 - 1.25)
			factorOfTwo = 1;
			powerOfTwo = 2;
		}
		else if( value < 3.5 ) { // (2.5 - 3.0 - 3.5) -> (0.833333 - 1.0 - 1.166667)
			factorOfThree = 1;
			powerOfThree = 3;
		}
		else if( value < 5.0 ) { // (3.5 - 4.0 - 5.0) -> (0.875 - 1.0 - 1.25)
			factorOfTwo = 2;
			powerOfTwo = 4;
		}
		else if( value < 7.0 ) { // (5.0 - 6.0 - 7.0) -> (0.833333 - 1.0 - 1.166667)
			factorOfThree = 1;
			powerOfThree = 3;
			factorOfTwo = 1;
			powerOfTwo = 2;
		}
		else if( value < 8.5 ) { // (7.0 - 8.0 - 8.5) -> (0.875 - 1.0 - 1.0625)
			factorOfTwo = 3;
			powerOfTwo = 8;
		}
		else if( value < 10.0 ) { // (8.5 - 9.0 - 10.0) -> (0.94444 - 1.0 - 1.11111)
			factorOfThree = 2;
			powerOfThree = 9;
		}
		else
			while( value > 1.4 ) { // never happens when called by logUsingExponent()
				value /= 2;
				factorOfTwo++;
				powerOfTwo <<= 1;
			}

		BigDecimal correctedX = x;
		BigDecimal result = BigDecimal.ZERO;

		if( factorOfTwo > 0 ) {
			correctedX = correctedX.divide( BigDecimal.valueOf( powerOfTwo ), mc );
			result = result.add( Lib_BigCalc.logTwo( mcDouble ).multiply( BigDecimal.valueOf( factorOfTwo ), mc ) );
		}
		else if( factorOfTwo < 0 ) {
			correctedX = correctedX.multiply( BigDecimal.valueOf( powerOfTwo ), mc );
			result = result.subtract( Lib_BigCalc.logTwo( mcDouble ).multiply( BigDecimal.valueOf( -factorOfTwo ), mc ) );
		}

		if( factorOfThree > 0 ) {
			correctedX = correctedX.divide( BigDecimal.valueOf( powerOfThree ), mc );
			result = result.add( Lib_BigCalc.logThree( mcDouble ).multiply( BigDecimal.valueOf( factorOfThree ), mc ) );
		}
		else if( factorOfThree < 0 ) {
			correctedX = correctedX.multiply( BigDecimal.valueOf( powerOfThree ), mc );
			result = result.subtract( Lib_BigCalc.logThree( mcDouble ).multiply( BigDecimal.valueOf( -factorOfThree ), mc ) );
		}

		if( x == correctedX && result == BigDecimal.ZERO )
			return Lib_BigCalc.logUsingNewton( x, mathContext );

		return result.add( Lib_BigCalc.logUsingNewton( correctedX, mc ), mc );
	}

	private static BigDecimal powInteger( BigDecimal x, BigDecimal integerY, final MathContext mathContext ) {
		if( Lib_BigCalc.fractionalPart( integerY ).signum() != 0 )
			throw new IllegalArgumentException( "Not integer value: " + integerY );

		if( integerY.signum() < 0 )
			return BigDecimal.ONE.divide( Lib_BigCalc.powInteger( x, integerY.negate(), mathContext ), mathContext );

		final MathContext mc = new MathContext( Math.max( mathContext.getPrecision(), -integerY.scale() ) + 30, mathContext.getRoundingMode() );

		BigDecimal result = BigDecimal.ONE;

		while( integerY.signum() > 0 ) {
			BigDecimal halfY = integerY.divide( Lib_BigMath.TWO, mc );

			if( Lib_BigCalc.fractionalPart( halfY ).signum() != 0 ) {
				// odd exponent -> multiply result with x
				result = result.multiply( x, mc );
				integerY = integerY.subtract( BigDecimal.ONE );
				halfY = integerY.divide( Lib_BigMath.TWO, mc );
			}

			if( halfY.signum() > 0 )
				// even exponent -> square x
				x = x.multiply( x, mc );

			integerY = halfY;
		}

		return Lib_BigMath.round( result, mathContext );
	}

}
