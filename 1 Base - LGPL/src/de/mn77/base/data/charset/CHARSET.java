/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.charset;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;


/**
 * @author Michael Nitsche
 * @created 27.10.2022
 */
public class CHARSET {

	public static final Charset DEFAULT = Charset.defaultCharset();

	public static final Charset CP_1252     = Charset.forName( CHARSET.STRING_CP_1252 );
	public static final Charset CP_850      = Charset.forName( CHARSET.STRING_CP_850 );
	public static final Charset ISO_8859_1  = StandardCharsets.ISO_8859_1;
	public static final Charset ISO_8859_15 = Charset.forName( CHARSET.STRING_ISO_8859_15 );
	public static final Charset US_ASCII    = StandardCharsets.US_ASCII;
	public static final Charset UTF_16      = StandardCharsets.UTF_16;
	public static final Charset UTF_16BE    = StandardCharsets.UTF_16BE;
	public static final Charset UTF_16LE    = StandardCharsets.UTF_16LE;
	public static final Charset UTF_32      = Charset.forName( CHARSET.STRING_UTF_32 );
	public static final Charset UTF_8       = StandardCharsets.UTF_8;

	public static final String STRING_CP_1252     = "Cp1252"; // = Windows
	public static final String STRING_CP_850      = "Cp850";  // = DOS
	public static final String STRING_ISO_8859_15 = "ISO-8859-15";
	public static final String STRING_UTF_32      = "UTF-32";
	public static final String STRING_UTF_8       = "UTF-8";  // = Linux

}
