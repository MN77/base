/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.charset;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 27.10.2022
 */
public class ConvertCharset {

	public static byte[] toBytes( final String s, final Charset cs ) {
		Err.ifNull( s, cs );
		return s.getBytes( cs );
	}

	public static String toString( final byte[] ba ) {
		return ConvertCharset.iBytesToString( ba, (Charset)null, Charset.defaultCharset() );
	}

	public static String toString( final byte[] ba, final Charset cs ) {
		return ConvertCharset.iBytesToString( ba, cs, Charset.defaultCharset() );
	}

	public static String toString( final byte[] ba, final String charsetName ) {
		return ConvertCharset.iBytesToString( ba, charsetName, Charset.defaultCharset() );
	}

	public static String toUTF8( final byte[] ba, final Charset cs ) {
		return ConvertCharset.iBytesToString( ba, cs, StandardCharsets.UTF_8 );
	}

	public static String toUTF8( final byte[] ba, final String charsetName ) {
		return ConvertCharset.iBytesToString( ba, charsetName, StandardCharsets.UTF_8 );
	}

	private static String iBytesToString( final byte[] ba, final Charset cs, final Charset def ) {
		Err.ifNull( ba );

		return cs == null || cs.equals( def )
			? new String( ba, def )
			: new String( ba, cs );
	}

	private static String iBytesToString( final byte[] ba, final String charsetName, final Charset def ) {
		Err.ifNull( ba );

		try {
			return charsetName == null || charsetName.equals( def.name() )
				? new String( ba, def )
				: new String( ba, charsetName );
		}
		catch( final UnsupportedEncodingException e ) {
			Err.show( e );
			return new String( ba ); // Lazy
		}
	}

}
