/*******************************************************************************
 * Copyright (C) 2008-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.constant;

/**
 * @author Michael Nitsche
 * @created 03.05.2008
 */
public class DATE {

	public static final String[] NAME_MONTH_DE = { "Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober",
		"November", "Dezember" };
	public static final String[] NAME_MONTH_EN = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October",
		"November", "December" };

	public static final String[] NAME_DAYOFWEEK_DE = { "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag" };
	public static final String[] NAME_DAYOFWEEK_EN = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

	public static final int[] DAYS_OF_MONTH_MIN = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

}
