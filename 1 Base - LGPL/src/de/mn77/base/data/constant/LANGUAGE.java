/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.constant;

import de.mn77.base.sys.Sys;


/**
 * @author Michael Nitsche
 * @apiNote
 *          Languages, that are used in this library
 */
public enum LANGUAGE {

	DE( "Deutsch", "de" ),
	EN( "English", "en" ),;
//	BY("Bairisch", "by");


	public final String name;
	public final String id;


	public static LANGUAGE getDefault() {
		final String lang = Sys.getLanguage();
		if( lang.toLowerCase().startsWith( "de" ) )
			return DE;

		return EN;
	}


	LANGUAGE( final String name, final String id ) {
		this.name = name;
		this.id = id;
	}

}
