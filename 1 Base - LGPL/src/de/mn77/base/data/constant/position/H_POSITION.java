/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.constant.position;

/**
 * @author Michael Nitsche
 *         18.04.2009 Erstellt
 *         26.04.2009 Unterschied zum alten System:
 *         - Alle Konstanten-Objekte in einer Klasse als Enums, nicht in verschiedenen Klassen
 *         - Rückgabewert in den Interfaces ist die Enum-Klasse, nicht die Schnittstelle
 *
 *         Achtung:
 *         - Die Interfaces dürfen nicht voneinander erben!!!
 */
public class H_POSITION {

	protected enum CENTER implements POSITION_CENTER, POSITION_H, POSITION_V, POSITION_STRAIGHT, POSITION {
		CENTER
	}

	protected enum EDGE implements POSITION {
		TOP_LEFT,
		TOP_RIGHT,
		BOTTOM_LEFT,
		BOTTOM_RIGHT
	}

	protected enum H implements POSITION_H, POSITION_STRAIGHT, POSITION {
		LEFT,
		RIGHT
	}

	protected enum V implements POSITION_V, POSITION_STRAIGHT, POSITION {
		TOP,
		BOTTOM
	}

}
