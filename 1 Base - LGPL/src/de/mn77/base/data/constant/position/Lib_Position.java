/*******************************************************************************
 * Copyright (C) 2009-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.constant.position;

import de.mn77.base.data.constant.ALIGN;
import de.mn77.base.data.constant.VALIGN;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 09.03.2009
 *
 *          Tools for handling the POSITIONs
 */
public class Lib_Position {

	public static POSITION_H getHorizontal( final POSITION p ) {
		if( Lib_Position.isLeft( p ) )
			return POSITION_H.LEFT;
		if( Lib_Position.isVertical( p ) )
			return POSITION_H.CENTER;
		if( Lib_Position.isRight( p ) )
			return POSITION_H.RIGHT;
		return null;
	}

	public static POSITION_V getVertical( final POSITION p ) {
		if( Lib_Position.isTop( p ) )
			return POSITION_V.TOP;
		if( Lib_Position.isHorizontal( p ) )
			return POSITION_V.CENTER;
		if( Lib_Position.isBottom( p ) )
			return POSITION_V.BOTTOM;
		return null;
	}

	public static boolean isBottom( final POSITION p ) {
		return p == POSITION.BOTTOM || p == POSITION.BOTTOM_LEFT || p == POSITION.BOTTOM_RIGHT;
	}

	public static boolean isHorizontal( final POSITION p ) {
		return p == POSITION.LEFT || p == POSITION.CENTER || p == POSITION.RIGHT;
	}

	public static boolean isLeft( final POSITION p ) {
		return p == POSITION.LEFT || p == POSITION.TOP_LEFT || p == POSITION.BOTTOM_LEFT;
	}

	public static boolean isRight( final POSITION p ) {
		return p == POSITION.RIGHT || p == POSITION.TOP_RIGHT || p == POSITION.BOTTOM_RIGHT;
	}

	public static boolean isTop( final POSITION p ) {
		return p == POSITION.TOP || p == POSITION.TOP_LEFT || p == POSITION.TOP_RIGHT;
	}

	public static boolean isVertical( final POSITION p ) {
		return p == POSITION.TOP || p == POSITION.CENTER || p == POSITION.BOTTOM;
	}

	public static <T> T selectH( final POSITION_H ph, final T ifLeft, final T ifCenter, final T ifRight ) {
		if( ph == POSITION_H.LEFT )
			return ifLeft;
		if( ph == POSITION_H.CENTER )
			return ifCenter;
		if( ph == POSITION_H.RIGHT )
			return ifRight;
		throw Err.impossible( ph );
	}

	public static <T> T selectV( final POSITION_V pv, final T ifTop, final T ifCenter, final T ifBottom ) {
		if( pv == POSITION_V.TOP )
			return ifTop;
		if( pv == POSITION_V.CENTER )
			return ifCenter;
		if( pv == POSITION_V.BOTTOM )
			return ifBottom;
		throw Err.impossible( pv );
	}

	public static ALIGN toAlign( final POSITION_H pos ) {
		return pos == POSITION_H.LEFT
			? ALIGN.LEFT
			: pos == POSITION_H.RIGHT
				? ALIGN.RIGHT
				: ALIGN.CENTER;
	}

	public static VALIGN toAlign( final POSITION_V pos ) {
		return pos == POSITION_V.TOP
			? VALIGN.TOP
			: pos == POSITION_V.BOTTOM
				? VALIGN.BOTTOM
				: VALIGN.CENTER;
	}

	public static POSITION combine( POSITION_H posH, POSITION_V posV ) {
		if(posH == POSITION_H.LEFT)
			return posV == POSITION_V.TOP
				? POSITION.TOP_LEFT
				: posV == POSITION_V.BOTTOM
					? POSITION.BOTTOM_LEFT
					: POSITION.LEFT;

		if(posH == POSITION_H.CENTER)
			return posV == POSITION_V.TOP
				? POSITION.TOP
				: posV == POSITION_V.BOTTOM
					? POSITION.BOTTOM
					: POSITION.CENTER;

		if(posH == POSITION_H.RIGHT)
			return posV == POSITION_V.TOP
				? POSITION.TOP_RIGHT
				: posV == POSITION_V.BOTTOM
					? POSITION.BOTTOM_RIGHT
					: POSITION.RIGHT;

		throw Err.impossible( posH, posV );
	}

}
