/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.constant.position._test;

import de.mn77.base.data.constant.position.POSITION;
import de.mn77.base.data.constant.position.POSITION_H;
import de.mn77.base.data.constant.position.POSITION_V;


/**
 * @author Michael Nitsche
 */
public class Test_Position {

	public static void main( final String[] args ) {
		Test_Position.test1( POSITION.LEFT );
		Test_Position.test2( POSITION_V.TOP );
		Test_Position.test3( POSITION.BOTTOM_RIGHT );
		Test_Position.test3( POSITION.CENTER );
	}

	private static void test1( final POSITION_H a ) {}

	private static void test2( final POSITION_V a ) {}

	private static void test3( final POSITION a ) {}

}
