/*******************************************************************************
 * Copyright (C) 2007-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.container;

import java.lang.reflect.Field;
import java.util.Map;

import de.mn77.base.data.struct.SimpleMap;
import de.mn77.base.data.util.Lib_Class;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 09.07.2007
 */
public abstract class A_BoxStack {

	private SimpleMap<String, I_Box<?>> content = null; // SimpleMap keeps the order! So no HashMap here! 
	private String                      name;


	@Override
	public boolean equals( final Object o ) {
		if( o == null || !(o instanceof A_BoxStack) )
			return false;
		return o.toString().equals( this.toString() );
	}

	public Map<String, I_Box<?>> getBoxes() {
		if( this.content != null )
			return this.content;
		this.content = new SimpleMap<>();
		final Class<?> clazz = this.getClass();

		try {
			final Field[] fields = clazz.getFields();

			for( final Field field : fields ) {
				final Object o = field.get( this );
				if( o instanceof I_Box<?> )
					this.content.put( field.getName(), (I_Box<?>)o );
			}
		}
		catch( final Exception e ) {
			Err.show( e );
		}

		return this.content;
	}

//	public I_Box<?> getBox(String schluessel) {
//		for(Group2<String,I_Box> g : this.gKisten())
//			if(g.equals(schluessel))
//				return g.o2;
//		throw Fehler.text.da_Ungueltig(schluessel);
//	}
//
//	public void set(I_KeyList<String,String> daten) {
//		for(Group2<String,String> g : daten) {
//			String s=g.o1;
//			String w=g.o2;
//
//			I_Box kiste=this.gKiste(s);
//			if(kiste==null)
//				Fehler.text.da_Ungueltig(s);
//
//			Class<?> gTyp=kiste.gTyp();
//			if(gTyp.equals(String.class))
//				kiste.set(w);
//			else if(gTyp.equals(I_DateTime.class))
//				kiste.set(w==null ? null : new MDateTime(w));
//			else if(gTyp.equals(Integer.class))
//				kiste.set(w==null ? null : Integer.parseInt(w));
//			else if(gTyp.equals(Boolean.class))
//				kiste.set(w==null ? null : Boolean.parseBoolean(w));
//			else Err.todo(gTyp.getName());
//		}
//	}

	public String getName() {
		if( this.name != null )
			return this.name;
		this.name = Lib_Class.getName( this );
		return this.name;
	}

	@Override
	public String toString() {
		return this.getName() + ":\n" + this.getBoxes();
	}

}
