/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.container;

import de.mn77.base.data.struct.SimpleQueue;


/**
 * @author Michael Nitsche
 * @apiNote
 *          Um eine Variable gleichzeitig mehreren Threads sauber bereitstellen zu können.
 *          Diese Lösung basiert auf einem Thread-Management.
 * @implNote
 *           Evtl. noch realisieren, daß beliebige Lesevorgänge parallel möglich sind,
 *           außer wenn jemand schreibt. Und nur je einen Schreibvorgang
 */
public final class Bottleneck<T> implements I_Box<T> {

	private T                   value = null;
	private SimpleQueue<Thread> waitlist;
	private int                 count = 0;


	public Bottleneck() {
		this.waitlist = new SimpleQueue<>();
	}

	public Bottleneck( final T startvalue ) {
		this();
		this.value = startvalue;
	}


	public synchronized T get() {
		this.lock();
		final T result = this.value;
		this.unlock();
		return result;
	}

	public synchronized void set( final T t ) {
		this.lock();
		this.value = t;
		this.unlock();
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + "(" + this.get() + ")";
	}


	// PRIVATE

	private synchronized void lock() {

		if( this.count > 0 ) {
			final Thread t = Thread.currentThread();
			this.waitlist.add( t );

			try {
				t.wait();
			}
			catch( final InterruptedException e ) {}
		}

		this.count++;
	}

	private synchronized void unlock() {
		this.count--;
		if( this.count > 0 )
			this.waitlist.next().notify();
	}

}
