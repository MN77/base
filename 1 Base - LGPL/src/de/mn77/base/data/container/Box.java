/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.container;

import de.mn77.base.data.constant.ACCESS;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class Box<T> implements I_Box<T> {

	public final ACCESS   access;
	private T             value   = null;
	private boolean       isEmpty = true;
	private final boolean couldBeNull;


	private static final void checkRead( final ACCESS access ) {
		if( access != ACCESS.NONE )
			return;
		throw Err.accessProtected();
	}

	private static final void checkWrite( final ACCESS access, final boolean isEmpty ) {
		if( isEmpty || access == ACCESS.FULL )
			return;
		throw Err.accessProtected();
	}

	public Box() {
		this( ACCESS.READ_ONLY );
	}

	public Box( final ACCESS access ) {
		this.access = access;
		this.couldBeNull = true;
	}


	public Box( final ACCESS access, final boolean couldBeNull, final T value ) {
		this.access = access;
		this.couldBeNull = couldBeNull;
		if( !couldBeNull )
			Err.ifNull( value );
		this.set( value );
	}

	public Box( final ACCESS access, final T value ) {
		this( access, true, value );
	}

	public synchronized T get() {
		Box.checkRead( this.access );
		return this.value;
	}

	public boolean isSet() {
		return !this.isEmpty;
	}

	public synchronized void set( final T t ) {
		if( !this.couldBeNull )
			Err.ifNull( t );
		Box.checkWrite( this.access, this.isEmpty );
		this.value = t;
		this.isEmpty = false;
	}

	@Override
	public String toString() {
		String result = this.getClass().getSimpleName();
		if( this.access != ACCESS.NONE )
			result += "(" + this.get() + ")";
		return result;
	}

}
