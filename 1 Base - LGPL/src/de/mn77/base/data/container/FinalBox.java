/*******************************************************************************
 * Copyright (C) 2023-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.container;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 2023-04-05
 * @apiNote Wert kann nur einmal gesetzt, aber beliebig oft gelesen werden.
 */
public class FinalBox<T> implements I_Box<T> {

	private T       value = null;
	private boolean isSet = false;


	public FinalBox() {}

	public FinalBox( final T value ) {
		this.set( value );
	}


	public synchronized T get() {
		if( !this.isSet )
			throw Err.invalid( "No value set!" );
		return this.value;
	}

	public boolean isInit() {
		return this.value != null;
	}

	public synchronized void set( final T t ) {
		if( this.isSet )
			throw Err.invalid( "Value already set!" );
//		Err.ifNull( t );
		this.value = t;
		this.isSet = true;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append( this.getClass().getSimpleName() );
		sb.append( '(' );
		sb.append( this.get() );
		sb.append( ')' );
		return sb.toString();
	}

}
