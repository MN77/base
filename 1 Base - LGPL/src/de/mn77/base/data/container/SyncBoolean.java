/*******************************************************************************
 * Copyright (C) 2023-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.container;

/**
 * @author Michael Nitsche
 * @created 01.05.2023
 * @apiNote
 *          Um einen Bool-Wert gleichzeitig mehreren Threads sauber bereitstellen zu können.
 *          Der Zugriff auf .get() und .set() ist synchronisiert.
 */
public class SyncBoolean {

	private boolean value;


	public SyncBoolean( final boolean initValue ) {
		this.value = initValue;
	}


	public synchronized boolean get() {
		return this.value;
	}

	public synchronized void set( final boolean b ) {
		this.value = b;
	}

	public synchronized void setTrue() {
		this.value = true;
	}

	public synchronized void setFalse() {
		this.value = false;
	}

	public synchronized boolean isTrue() {
		return this.value == true;
	}
	
	public synchronized boolean isFalse() {
		return this.value == false;
	}
	
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + "(" + this.value + ")";
	}

}
