/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.convert;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 24.11.2010
 */
public class ConvertArray {

	public static int[] toIntArray( final Integer[] ia ) {
		Err.ifNull( ia );
		final int[] result = new int[ia.length];
		for( int i = 0; i < ia.length; i++ )
			result[i] = ia[i];
		return result;
	}

	public static <TA> SimpleList<TA> toSimpleList( final TA[] arr ) {
		final SimpleList<TA> result = new SimpleList<>( arr.length );
		result.addMore( arr );
		return result;
	}

	public static String toString( final String delimiter, final Object... arr ) {
		final StringBuilder sb = new StringBuilder();

		for( int i = 0; i < arr.length; i++ ) {
			if( i > 0 )
				sb.append( delimiter );
			sb.append( arr[i] );
		}

		return sb.toString();
	}

	public static String toString( final String delimiter, final String... arr ) {
		return String.join( delimiter, arr );
	}

	public static String[] toStringArray( final int... ia ) {
		Err.ifNull( ia );
		final String[] result = new String[ia.length];
		for( int i = 0; i < ia.length; i++ )
			result[i] = "" + ia[i];
		return result;
	}

	public static <TA> String[] toStringArray( final TA[] oa ) {
		Err.ifNull( oa );
		if( oa instanceof String[] )
			return (String[])oa;

		final String[] result = new String[oa.length];
		for( int i = 0; i < oa.length; i++ )
			result[i] = ConvertObject.toText( oa[i] );

		return result;
	}

}
