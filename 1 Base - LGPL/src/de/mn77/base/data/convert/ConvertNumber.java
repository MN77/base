/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.convert;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 23.02.2008
 */
public class ConvertNumber {

	private static final String chars_hex = "0123456789ABCDEF";

	private static final char[] chars_hex_array = ConvertNumber.chars_hex.toCharArray();


	public static byte[] fromHex( final String s ) {
		if( s.length() % 2 != 0 )
			Err.invalid( s );
		final byte[] result = new byte[s.length() / 2];
		Integer buffer = null;

		final byte[] ba = s.toUpperCase().getBytes();
		int pt = 0;
		for( final byte b : ba )
			if( buffer == null ) //high
				buffer = ConvertNumber.chars_hex.indexOf( b ) * 16;
			else { //low
				result[pt] = (byte)(buffer + ConvertNumber.chars_hex.indexOf( b ));
				buffer = null;
				pt++;
			}
		return result;
	}

	public static String toHex( final byte[] ba ) {
		final StringBuilder sb = new StringBuilder();
		int i;

		for( final byte b : ba ) {
//			high = ((int)ba[i] & 0x000000FF) / 16;
//			low  = ((int)ba[i] & 0x000000FF) % 16;
			i = b & 0xFF; // macht aus den negativen Bytezahlen richtige Integer-Pluszahlen
			sb.append( ConvertNumber.chars_hex_array[i / 16] );
			sb.append( ConvertNumber.chars_hex_array[i % 16] );
		}

		return sb.toString();
	}

	/**
	 * Umgekehrt einfach mit (byte)
	 */
	public static int toInt( final byte b ) {
		return b & 255;
	}

	public static String toString( final Integer integer ) {
		if( integer == null )
			return null;
		return "" + integer;
	}

}
