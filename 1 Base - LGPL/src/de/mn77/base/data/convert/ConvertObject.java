/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.convert;

import de.mn77.base.data.form.FormString;


/**
 * @author Michael Nitsche
 */
public class ConvertObject {

	public static String toString( final Object o ) {
		return o == null
			? null
			: o instanceof String
				? (String)o
				: o.toString();
	}

	/**
	 * @apiNote Converts an Object to a usefully String. Strings will be quoted.
	 */
	public static String toStringIdent( final Object o ) {
		return o instanceof String
			? FormString.quote( (String)o, '"', '\\', false )
			: ConvertObject.toStringOutput( o );
	}

	/**
	 * @apiNote Converts an Object to a usefully String.
	 */
	public static String toStringOutput( final Object o ) {
		if( o == null )
			return "null";

		if( o instanceof String )
			return (String)o;

		if( o.getClass().isArray() ) {
			if( o instanceof boolean[] )
				return ConvertObject.toTextDebug( (boolean[])o );
			if( o instanceof byte[] )
				return ConvertObject.toTextDebug( (byte[])o );
			if( o instanceof char[] )
				return ConvertObject.toTextDebug( (char[])o );
			if( o instanceof short[] )
				return ConvertObject.toTextDebug( (short[])o );
			if( o instanceof int[] )
				return ConvertObject.toTextDebug( (int[])o );
			if( o instanceof long[] )
				return ConvertObject.toTextDebug( (long[])o );
			if( o instanceof float[] )
				return ConvertObject.toTextDebug( (float[])o );
			if( o instanceof double[] )
				return ConvertObject.toTextDebug( (double[])o );
			return ConvertObject.toTextDebug( (Object[])o );
		}

		if( o instanceof Boolean )
			return "" + o;
		if( o instanceof Byte )
//			return "(byte)" + o;
			return o + "b"; // 123b is in Java not valid
		if( o instanceof Character )
			return ConvertObject.toTextDebug( (Character)o );
		if( o instanceof Short )
//			return "(short)" + o;
			return o + "s"; // 123s is in Java not valid
		if( o instanceof Integer )
			return "" + o;
		if( o instanceof Long )
			return "" + o + "l";
		if( o instanceof Float )
			return "" + o + "f";
		if( o instanceof Double )
			return "" + o + "d";

//		if(o instanceof ArrayList)
//			return ConvObject.toTextDebug((ArrayList<?>)o);

		if( o instanceof Class<?> )
			return (((Class<?>)o).isInterface()
				? "(Interface)"
				: "(Class)") + ((Class<?>)o).getSimpleName(); //Klasse.gName(o)
		if( o.getClass() == Object.class )
			return "Object";

		return o.toString();
	}

	/**
	 * Wandelt das Objekt so einfach und direkt wie möglich in Text um.
	 * null wird zu "null"!!!
	 */
	public static String toText( final Object o ) {
		return o == null
			? "null"
			: o instanceof String
				? (String)o
				: o.toString();
	}

	public static String toTextDebug( final boolean[] r ) {
		final StringBuilder b = new StringBuilder();
		for( final boolean element : r )
			b.append( "," + element );
		return "boolean[" + (b.length() == 0 ? "" : b.substring( 1 )) + "]";
	}

	public static String toTextDebug( final byte[] r ) {
		final StringBuilder b = new StringBuilder();
		for( final byte element : r )
			b.append( "," + element );
		return "byte[" + (b.length() == 0 ? "" : b.substring( 1 )) + "]";
	}

	public static String toTextDebug( final char c ) {
		return c > 31 && c < 127
			? "(char)" + c
			: "(char)" + (int)c;
	}

	public static String toTextDebug( final char[] r ) {
		final StringBuilder b = new StringBuilder();
		for( final char element : r )
			b.append( "," + ConvertObject.toTextDebug( element ) );
		return "char[" + (b.length() == 0 ? "" : b.substring( 1 )) + "]";
	}

	public static String toTextDebug( final double[] r ) {
		final StringBuilder b = new StringBuilder();
		for( final double element : r )
			b.append( "," + ConvertObject.toStringIdent( element ) );
		return "double[" + (b.length() == 0 ? "" : b.substring( 1 )) + "]";
	}

	public static String toTextDebug( final float[] r ) {
		final StringBuilder b = new StringBuilder();
		for( final float element : r )
			b.append( "," + ConvertObject.toStringIdent( element ) );
		return "float[" + (b.length() == 0 ? "" : b.substring( 1 )) + "]";
	}

	public static String toTextDebug( final int[] r ) {
		final StringBuilder b = new StringBuilder();
		for( final int element : r )
			b.append( "," + element );
		return "int[" + (b.length() == 0 ? "" : b.substring( 1 )) + "]";
	}

	public static String toTextDebug( final long[] r ) {
		final StringBuilder b = new StringBuilder();
		for( final long element : r )
			b.append( "," + ConvertObject.toStringIdent( element ) );
		return "long[" + (b.length() == 0 ? "" : b.substring( 1 )) + "]";
	}

	public static String toTextDebug( final Object[] oa ) {
		if( oa == null )
			return "null";

		final String type = oa.getClass().toString().replaceFirst( "^.*\\.([A-Za-z_]+);.*", "$1" );
		final StringBuilder result1 = new StringBuilder( type + "[" );
		for( int i = 0; i < oa.length; i++ )
			result1.append( "\n   " + ConvertObject.toStringIdent( oa[i] ) + (i == oa.length - 1
				? ""
				: ",") );
		final String result2 = result1.toString();
		return (result2.length() < 100
			? result2.replaceAll( "\n   ", "" )
			: result2) + "]";
	}

	public static String toTextDebug( final short[] r ) {
		final StringBuilder b = new StringBuilder();
		for( final short element : r )
			b.append( "," + element );
		return "short[" + (b.length() == 0 ? "" : b.substring( 1 )) + "]";
	}

//	public static String toTextDebug(final String s, boolean quote) {
//		return quote
//			? FormString.quote(s, '"', '\\')
//			: s;
//		StringBuilder sb = new StringBuilder(s.length());
//		if(quote)
//			sb.append('"');
//
//		for(char c : s.toCharArray()) {
////			if(c == '\t')
////				sb.append("\\t");
////			else if( c == '\n')
////				sb.append("\\n");
////			else if(c < 32) {
////				sb.append("\\u");
////				sb.append( FormNumber.width(4, Hex.toHex(c), false) );
////			}
////			else
////				sb.append(c);
//			sb.append( FormString.quo );
//		}
//
//		if(quote)
//			sb.append('"');
//		return sb.toString();
//	}

//	public static String toTextDebug(final ArrayList<?> al) {
//		if(al == null)
//			return "null";
//
//		final StringBuilder sb = new StringBuilder("[");
//		for(int i = 0; i < al.size(); i++) {
//			if(i > 0)
//				sb.append(", ");
//			Object alo = al.get(i);
//			if(alo instanceof String)
//				sb.append( ConvObject.toTextDebug((String)al.get(i), true) );
//			else
//				sb.append( ConvObject.toTextDebug(al.get(i)) );
//		}
//		sb.append(']');
//		return sb.toString();
//	}

}
