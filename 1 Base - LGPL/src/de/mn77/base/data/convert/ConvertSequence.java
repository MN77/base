/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.convert;

import java.util.Collection;
import java.util.List;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.atomic.IntList;
import de.mn77.base.data.util.Lib_Array;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 *         TODO ConvertIterable, ConvertBundle, ConvertCollection, ConvertList, ConvertData ?
 */
public class ConvertSequence {

	/**
	 * @apiNote Generates a String with all items in the Iterable delimited by a newline. Optional a newline can be added before or appended.
	 */
	public static <T> String linesToString( final Iterable<String> data, final boolean nlBefore, final boolean nlAfter ) {
		final StringBuilder sb = new StringBuilder();
		boolean hasLines = false;

		for( final String s : data ) {
			if( !hasLines )
				hasLines = true;
			else
				sb.append( '\n' );

			sb.append( s );
		}

		if( hasLines ) {
			if( nlBefore )
				sb.insert( 0, '\n' );
			if( nlAfter )
				sb.append( '\n' );
		}

		return sb.toString();
	}

	@SuppressWarnings( "unchecked" )
	public static <T> T[] toArray( final Class<T> type, final Collection<T> list ) {
		Err.ifNull( list );
		T[] result = null;
		if( type == null )
			result = (T[])new Object[list.size()];
		else
			result = Lib_Array.newArray( type, list.size() );

		int i = 0;

//		MOut.dev(klasse, folge);
		for( final T t : list ) {
			result[i] = t;
			i++;
		}

		return result;
	}

	@SuppressWarnings( "unchecked" )
	public static <T> T[] toArray( final List<T> list ) {
		return ConvertSequence.toArray( list.size() == 0 || list.get( 0 ) == null
			? null
			: (Class<T>)list.get( 0 ).getClass(), list );
	}

	public static <T> String toIdent( final String delimiter, final Iterable<T> list ) {
		String result = "";
		boolean first = true;

		for( final T t : list ) {
			result += (first
				? ""
				: delimiter) + ConvertObject.toStringIdent( t );
			first = false;
		}

		return result;
	}

	public static int[] toIntArray( final Iterable<Integer> numbers ) {
		final IntList result = new IntList();
		for( final Integer num : numbers )
			result.add( num );
		return result.toArray();
	}

	public static int[] toIntArray( final List<Integer> numbers ) {
		final int[] result = new int[numbers.size()];
		for( int i = 0; i < numbers.size(); i++ )
			result[i] = numbers.get( i );
		return result;
	}

	public static SimpleList<Integer> toListInt( final Iterable<?> numbers ) {
		final SimpleList<Integer> result = new SimpleList<>();

		for( final Object o : numbers )
			if( o instanceof final Integer i )
				result.add( i );
			else
				result.add( Integer.parseInt( "" + o ) );

		return result;
	}

	public static <T> String toString( final char delimiter, final Iterable<T> list ) {
		final StringBuilder sb = new StringBuilder();
		boolean first = true;

		for( final T t : list ) {
			if( first )
				first = false;
			else
				sb.append( delimiter );

			sb.append( t );
		}

		return sb.toString();
	}

	/**
	 * @apiNote null will be added like an empty string.
	 */
	public static <T> String toString( final String delimiter, final Iterable<T> list ) {
		final StringBuilder sb = new StringBuilder();
		boolean first = true;

		for( final T t : list ) {
			if( first )
				first = false;
			else
				sb.append( delimiter );

			if( t != null )
				sb.append( t.toString() );
		}

		return sb.toString();
	}

}
