/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.convert;

import java.util.Collection;
import java.util.HashSet;

import de.mn77.base.data.datetime.I_Date;
import de.mn77.base.data.datetime.I_DateTime;
import de.mn77.base.data.datetime.MDate;
import de.mn77.base.data.datetime.MDateTime;
import de.mn77.base.data.filter.CutString;
import de.mn77.base.data.search.SearchString;
import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.atomic.IntList;
import de.mn77.base.data.struct.table.type.TypeTable2;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 24.11.2010
 */
public class ConvertString {

	// Siehe CSV_Tabelle: TODO Abgleichen!!!
	public static String[] csvLineToString( final String line, final char delimiter_field, final char delimiter_col ) {
		final SimpleList<String> result = new SimpleList<>();
		boolean open = false;
		boolean double_tf = false;
		String field = "";

		for( int i = 0; i < line.length(); i++ ) {
			final char c = line.charAt( i );
			final boolean last = i == line.length() - 1;

//				MOut.dev(feld, offen, p, c);
			if( c == delimiter_field ) {

				if( double_tf ) {
					double_tf = false;
					field += c;
					continue;
				}

				if( open ) {
					if( last || line.charAt( i + 1 ) != delimiter_field )
						open = false;
					else
						double_tf = true;
				}
				else
					open = true;

				continue;
			}

			if( !open && c == delimiter_col ) {
				result.add( field );
				field = "";
				if( last )
					result.add( "" );
				continue;
			}

			field += c;
		}

		if( field.length() > 0 )
			result.add( field );
		return result.toArray( new String[result.size()] );
	}

	/**
	 * Die 2. Spalte enthält den Bool-Wert, ob es sich bei dem Wert in Spalte 1 um einen Trenner handelt.
	 */
	public static TypeTable2<String, Boolean> splitToTable( final String s, final String... delimiters ) {
		final TypeTable2<String, Boolean> result = new TypeTable2<>( String.class, Boolean.class );
		final TypeTable2<Integer, String> tab = SearchString.indexesMulti( s, delimiters );
		int start = 0;

		for( int rowIndex = 0; rowIndex < tab.size(); rowIndex++ ) {
			final int delimiterIndex = tab.getColumn0().get( rowIndex );
			final String delimiter = tab.getColumn1().get( rowIndex );

			final int end = delimiterIndex;
			if( start <= end )
				result.add( s.substring( start, end ), false );

			result.add( delimiter, true );

			start = delimiterIndex + delimiter.length();
		}

		if( start < s.length() )
			result.add( s.substring( start ), false );

		return result;
	}

	public static Boolean toBoolean( final String s ) {
		if( s == null )
			return null;

		try {
			return Boolean.parseBoolean( s );
		}
		catch( final Exception e ) {
			return null;
		}
	}

	public static Integer toInteger( final String s ) {
		if( s == null )
			return null;

		try {
			return Integer.parseInt( s );
		}
		catch( final NumberFormatException e ) {
			return null;
		}
	}

	public static IntList toIntList( final char delimiter, final String s ) {
		final I_List<String> list = ConvertString.toList( delimiter, s );
		final IntList result = new IntList();

		for( final String item : list )
			result.add( Integer.parseInt( item ) );

		return result;
	}

	/**
	 * @apiNote Split a String by delimiters and return a Set with all containing items.
	 *          Empty items will be removed.
	 */
	public static HashSet<String> toItemSet( final char[] delimiters, String s ) {
		Err.ifNull( s );
		s = s.trim();
		final HashSet<String> result = new HashSet<>();
		if( s.length() == 0 )
			return result;

		int start = 0;
		int pt = 0;

		for( final char c : s.toCharArray() ) {
			for( final char d : delimiters )
				if( d == c ) {
					final String part = s.substring( start, pt ).trim();
					if( part.length() > 0 )
						result.add( part );
					start = pt + 1;
					break;
				}

			pt++;
		}

		// Empty buffer
		if( start < pt ) {
			final String part = s.substring( start, pt ).trim();
			if( part.length() > 0 )
				result.add( part );
		}

		return result;
	}

	public static I_List<String> toLines( final String s ) {
		Err.ifNull( s );
//		return ConvString.toList("\n", s);

		final SimpleList<String> result = new SimpleList<>();
		int start = 0;
		for( int i = 0; i < s.length(); i++ )
			if( s.charAt( i ) == '\n' ) {
				result.add( s.substring( start, i ) );
				start = i + 1;
			}

		if( start < s.length() )
			result.add( s.substring( start ) );

		return result;
	}

	public static I_List<String> toList( final char delimiter, final String s ) {
		Err.ifNull( s );
		final SimpleList<String> result = new SimpleList<>();
		if( s.length() == 0 )
			return result;

		int start = 0;
		int pt = 0;

		for( final char c : s.toCharArray() ) {

			if( c == delimiter ) {
				result.add( s.substring( start, pt ) );
				start = pt + 1;
			}

			pt++;
		}

		if( start < pt )
			result.add( s.substring( start, pt ) );
		if( start == pt && s.charAt( pt - 1 ) == delimiter )
			result.add( "" );

		return result;
	}

	/**
	 * Alternativ: text.split(trenner); (mit Regex)
	 * Diese Funktion hier arbeitet aber irgendwie genauer!
	 */
	public static I_List<String> toList( final String delimiter, final String s ) {
		Err.ifEmpty( delimiter );
		Err.ifNull( s );

		final SimpleList<String> result = new SimpleList<>();
		if( s.length() == 0 )
			return result;

		int start = 0;
		if( s.indexOf( delimiter ) == 0 )
			result.add( "" ); //Falls Trenner am Anfang
		else
			start -= delimiter.length();

		while( start < s.length() ) {
			start += delimiter.length();
			int next = s.indexOf( delimiter, start );

			if( next < 0 ) // nothing found
				next = s.length();
			if( start >= s.length() )
				result.add( "" );
			else
				result.add( CutString.cut( s, start, next - start ) );

			start += next - start;
		}

		return result;
	}

	@SuppressWarnings( "unchecked" )
	public static <T> T toObject( final String s, final Class<T> c ) {
		if( s == null )
			return null;
		Err.ifNull( c );
		if( c == String.class )
			return (T)s;
		if( c == Integer.class )
			return (T)(Integer)Integer.parseInt( s );
		if( c == I_Date.class )
			return (T)new MDate( s );
		if( c == Boolean.class )
			return (T)(Boolean)Boolean.parseBoolean( s );
		if( c == I_DateTime.class )
			return (T)new MDateTime( s );

		throw Err.todo( "Unknown Class to convert", s, c );
	}

	public static I_List<String> toSearchWords( String s ) {
		Err.ifNull( s );
		s = s.trim();
		final SimpleList<String> result = new SimpleList<>();
		if( s.length() == 0 )
			return result;

		int start = 0;
		int pt = 0;

		for( final char c : s.toCharArray() ) {

			if( c == ' ' ) {
				final String word = s.substring( start, pt ).trim();
				if( word.length() > 0 )
					result.add( word );
				start = pt + 1;
			}

			pt++;
		}

		if( start < pt ) {
			final String word = s.substring( start, pt ).trim();
			if( word.length() > 0 )
				result.add( word );
		}

		return result;
	}

	public static String[] toStringArray( final char delimiter, final String s ) {
		final Collection<String> c = ConvertString.toList( delimiter, s );
		return c.toArray( new String[c.size()] );
	}

	/** Liefert, im Gegensatz zu String.split, auch leere Zeilen! **/
	public static String[] toStringArray( final String delimiter, final String s ) {
		final Collection<String> c = ConvertString.toList( delimiter, s );
		return c.toArray( new String[c.size()] );
	}

	public static Iterable<String> toWords( final String s ) {
		final SimpleList<String> result = new SimpleList<>();
		final int len = s.length();
		int start = 0;

		for( int i = 0; i < len; i++ ) {
			final char c = s.charAt( i );

			if( c < '0' || c > '9' && c < 'A' || c > 'Z' && c < 'a' || c > 'z' && c < 'À' || c > 'ÿ' ) {
				// TODO passen die Werte?!?  https://www.torsten-horn.de/techdocs/ascii.htm

				if( i != start ) {	// TODO Testen und auch in andere "toWords"-Funktion
					final String word = s.substring( start, i );
					result.add( word );
				}

				start = i + 1;
			}
		}

		if( start < len ) {
			final String word = s.substring( start );
			result.add( word );
		}

		return result;
	}

	public static Iterable<String> toWords( final String s, final char[] delimiters ) {
		final SimpleList<String> result = new SimpleList<>();
		final int len = s.length();
		int start = 0;

		for( int i = 0; i < len; i++ ) {
			final char c = s.charAt( i );

			for( final char tr : delimiters )
				if( tr == c ) {

					if( i != start ) {
						final String word = s.substring( start, i );
						result.add( word );
					}

					start = i + 1;
					break;
				}
		}

		if( start < len ) {
			final String word = s.substring( start );
			result.add( word );
		}

		return result;
	}

}
