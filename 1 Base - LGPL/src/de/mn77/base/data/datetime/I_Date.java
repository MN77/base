/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.datetime;

/**
 * @author Nitsche Michael
 *
 * @apiNote
 *          Gültige Jahre: 1600-2999
 *          Notiz: die erste Woche im Jahr mit einem Donnerstag ist die 1.KW
 */
public interface I_Date extends I_DT_Common<I_Date>, I_HasDate {

	int diffDays( I_Date d );

	int diffMonths( I_Date d );

	int diffYears( I_Date d );

	int getDayOfYear();

	int getQuarterOfYear();

	int getWeek();

	int getWeekOfYear();


	I_Date getAddDays( int days );

	I_Date getAddMonths( int months );

	I_Date getAddWeeks( int weeks );
	
	I_Date getAddYears( int years );


	I_Date getSetDay( int day );

	I_Date getSetMonth( int month );

	I_Date getSetYear( int year );

}
