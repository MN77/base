/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.datetime;

/**
 * @author Michael Nitsche
 */
public interface I_DateTime extends I_DT_Common<I_DateTime>, I_HasDate, I_HasTime {

	int diffDays( I_DateTime datetime );

	long diffHours( I_DateTime datetime );

	long diffMilliseconds( I_DateTime datetime );

	long diffMinutes( I_DateTime datetime );

	int diffMonths( I_DateTime datetime );

	long diffSeconds( I_DateTime datetime );

	int diffYears( I_DateTime datetime );


	I_DateTime getAddDays( int days );

	I_DateTime getAddHours( long hours );

	I_DateTime getAddMilliseconds( long milliseconds );

	I_DateTime getAddMinutes( long minutes );

	I_DateTime getAddMonths( int months );

	I_DateTime getAddSeconds( long seconds );

	I_DateTime getAddYears( int years );


	I_Date getDate();

	I_DateTime getSetDay( int day );

	I_DateTime getSetHours( int hour );

	I_DateTime getSetMilliseconds( int milliseconds );

	I_DateTime getSetMinutes( int minute );

	I_DateTime getSetMonth( int month );

	I_DateTime getSetSeconds( int second );


	I_DateTime getSetYear( int year );

	I_Time getTime();

}
