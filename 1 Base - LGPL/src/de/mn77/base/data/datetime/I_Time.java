/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.datetime;

/**
 * @author Michael Nitsche
 */
public interface I_Time extends I_DT_Common<I_Time>, I_HasTime {

	int diffHours( I_Time time );

	int diffMilliseconds( I_Time uhrzeit ); // Range of Integer is enough

	int diffMinutes( I_Time time );

	int diffSeconds( I_Time uhrzeit );


	I_Time getAddHours( int hours );

	I_Time getAddMilliseconds( int milliseconds );

	I_Time getAddMinutes( int minutes );

	I_Time getAddSeconds( int seconds );


	I_Time getSetHours( int hour );

	I_Time getSetMilliseconds( int milliseconds );

	I_Time getSetMinutes( int minute );

	I_Time getSetSeconds( int second );


	int getValueSeconds();

	I_Time toRoundedHHMM();

}
