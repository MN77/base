/*******************************************************************************
 * Copyright (C) 2008-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.datetime;

/**
 * @author Michael Nitsche
 * @created 2008-05-03
 */
public interface I_WorldTime extends I_DT_Common<I_WorldTime>, I_HasDate, I_HasTime {

//	long diff(WELTZEIT was, I_Worldtime zeitpunkt);

	I_DateTime getDateTime();

	int getUTC_Offset_MSek();

	String toStringDefault();

	String toStringRFC822();

//	I_Worldtime getAdd(WELTZEIT einheit, int anzahl);
//
//	I_Worldtime getSub(WELTZEIT einheit, int anzahl);

//	S_Weltzeit getAddWorkingDays(int tage);
//	S_Weltzeit getAddWorkingDays(int tage);

//	I_Worldtime getSet(WELTZEIT einheit, int wert);

}
