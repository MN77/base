/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.datetime;

import java.util.Calendar;
import java.util.GregorianCalendar;

import de.mn77.base.data.constant.DATE;
import de.mn77.base.data.constant.LANGUAGE;
import de.mn77.base.data.datetime.format.FORM_DATE;
import de.mn77.base.data.form.FormNumber;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class Lib_DateTime {

	private static final int YEAR_CENTURY_CHANGE_FELT_DIFF = 20;


	public static long addDays( final int year, final int month, final int day, final int to_add ) {
		final GregorianCalendar gc = new GregorianCalendar( year, month - 1, day );
		gc.add( Calendar.DAY_OF_MONTH, to_add );
		return gc.getTimeInMillis();
	}

	public static long addDays( final int year, final int month, final int day, final int hour, final int min, final int sec, final int msec, final int add_days ) {
		final GregorianCalendar gc = new GregorianCalendar( year, month - 1, day );
		gc.add( Calendar.DAY_OF_MONTH, add_days );
		return gc.getTimeInMillis() + Lib_DateTime.toMilliSec( hour, min, sec, msec );
	}

	public static long addMonths( final int year, final int month, final int day, final int hour, final int min, final int sec, final int msec, final int add_months ) {
		final GregorianCalendar gc = new GregorianCalendar( year, month - 1, day );
		gc.add( Calendar.MONTH, add_months );
		return gc.getTimeInMillis() + Lib_DateTime.toMilliSec( hour, min, sec, msec );
	}

	public static long addYears( final int year, final int month, final int day, final int hour, final int min, final int sec, final int msec, final int add_years ) {
		final GregorianCalendar gc = new GregorianCalendar( year, month - 1, day );
		gc.add( Calendar.YEAR, add_years );
		return gc.getTimeInMillis() + Lib_DateTime.toMilliSec( hour, min, sec, msec );
	}

	public static final int daysOfMonth( final int year, final int month ) {
		Err.ifOutOfBounds( 1, 12, month );
		return month == 2 && Lib_DateTime.isLeapYear( year )
			? 29
			: DATE.DAYS_OF_MONTH_MIN[month - 1];
	}

	public static final int daysOfYear( final int year ) {
		return Lib_DateTime.isLeapYear( year )
			? 366
			: 365;
	}

	public static final boolean isLeapYear( final int year ) {
		return year % 400 == 0 || year % 4 == 0 && year % 100 != 0;
	}

	public static final String nameDayOfWeek( final int day, final LANGUAGE lang ) {
		Err.ifOutOfBounds( 0, 7, day );

		switch( lang ) {
			case DE:
				return day == 0
					? DATE.NAME_DAYOFWEEK_DE[6]
					: DATE.NAME_DAYOFWEEK_DE[day - 1];
			case EN:
				return day == 0
					? DATE.NAME_DAYOFWEEK_EN[6]
					: DATE.NAME_DAYOFWEEK_EN[day - 1];

			default:
				throw Err.invalid( lang );
		}
	}

	public static Integer parseMonth( String month ) {
		month = month.toLowerCase();

		for( int n = 1; n <= 12; n++ ) {
			final String de = DATE.NAME_MONTH_DE[n - 1].toLowerCase();
			final String en = DATE.NAME_MONTH_EN[n - 1].toLowerCase();
			if( month.equals( de ) || month.equals( de.substring( 0, 3 ) ) || month.equals( en ) || month.equals( en.substring( 0, 3 ) ) )
				return n;
		}

		return null;
	}

	public static long toMilliSec( final double days ) {
		return Math.round( days * 86400000 ); // days * 24 * 60 * 60 * 1000
	}

	public static long toMilliSec( final int year, final int month, final int day ) {
		return new GregorianCalendar( year, month - 1, day ).getTimeInMillis();
	}

	public static long toMilliSec( final int hour, final int min, final int sec, final int msec ) {
		long result = msec;
		result += (long)sec * 1000;
		result += (long)min * 1000 * 60;
		result += (long)hour * 1000 * 60 * 60;
		return result;
	}

	public static long toMilliSec( final int year, final int month, final int day, final int std, final int min, final int sek, final int msek ) {
		final long result = new GregorianCalendar( year, month - 1, day ).getTimeInMillis();
		return result + Lib_DateTime.toMilliSec( std, min, sek, msek );
	}

	/**
	 * Wandelt Sekunden in HH:MM:SS
	 */
	public static String toText( long seconds ) {
		final int h = (int)(seconds / 3600);
		seconds = seconds - h * 3600;
		final int m = (int)(seconds / 60);
		seconds = seconds - m * 60;
		final int s = (int)seconds;
		return FormNumber.width( 2, h, false ) + ":" + FormNumber.width( 2, m, false ) + ":" + FormNumber.width( 2, s, false );
	}

	/**
	 * @return This returns true if the given time in msec has not 000 msec.
	 */
	public static boolean usesMilliSeconds( final long ms ) {
		return ms % 1000 != 0;
	}

	public static final int yearShortToLong( final String yy ) {
		Err.ifNull( yy );
		Err.ifNot( 2, yy.length(), "Only 2 chars allowed for year" );
		final int result = Integer.parseInt( yy );
		int current = Integer.parseInt( new MDate().toString( FORM_DATE.YEAR_2 ) );
		current += Lib_DateTime.YEAR_CENTURY_CHANGE_FELT_DIFF;
		final String curCentStr = new MDate().toString( FORM_DATE.YEAR_4 );
		final int curCent = Integer.parseInt( curCentStr.substring( 0, 2 ) ) * 100;

		return result + (result < current
			? curCent
			: curCent - 100);
	}

}
