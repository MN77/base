/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.datetime;

import java.time.LocalDate;
import java.time.temporal.IsoFields;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import de.mn77.base.data.A_Comparable;
import de.mn77.base.data.constant.LANGUAGE;
import de.mn77.base.data.datetime.format.FORM_DATE;
import de.mn77.base.data.form.FormDateTime;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 */
public class MDate extends A_Comparable<I_Date> implements I_Date {

	private final int year;
	private final int month;
	private final int day;


	public MDate() {
		// Eigentlich unsinnig, aber sonst wird auf einem UTC-Rechner (Server) die Sommerzeit nicht erkannt.
//		gc.setTimeZone( TimeZone.getTimeZone("ECT") ); //TODO Prüfen!!!
		this( new GregorianCalendar() );
	}

	public MDate( final Calendar gc ) {
		Err.ifNull( gc );
		this.year = gc.get( Calendar.YEAR );
		this.month = gc.get( Calendar.MONTH ) + 1;
		this.day = gc.get( Calendar.DAY_OF_MONTH );
	}

	public MDate( final Date date ) {
		Err.ifNull( date );
		final GregorianCalendar gc = new GregorianCalendar();
		gc.setTimeInMillis( date.getTime() );
		this.year = gc.get( Calendar.YEAR );
		this.month = gc.get( Calendar.MONTH ) + 1;
		this.day = gc.get( Calendar.DAY_OF_MONTH );
	}

	public MDate( final int year, final int month, final int day ) {
		this.iCheck( year, month, day );
		this.day = day;
		this.month = month;
		this.year = year;
	}

	public MDate( final long ms ) {
		final GregorianCalendar gc = new GregorianCalendar();
		gc.setTimeInMillis( ms );
		this.year = gc.get( Calendar.YEAR );
		this.month = gc.get( Calendar.MONTH ) + 1;
		this.day = gc.get( Calendar.DAY_OF_MONTH );
	}

	public MDate( String date ) {
		Err.ifNull( date );
		final int laenge = date.length();

		try {
//			TODO regex raus!

//			2018-10-27T04:00:00.000Z	// ISO8601
			if( date.length() >= 11 && date.charAt( 10 ) == 'T' )
				date = date.substring( 0, 10 );

			//2007-06-18
			if( date.matches( "[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]" ) ) {
				this.year = this.iParse( 4, date.substring( 1 - 1, 4 ) );
				this.month = this.iParse( 2, date.substring( 6 - 1, 7 ) );
				this.day = this.iParse( 2, date.substring( 9 - 1, 10 ) );
			}

			//18.06.2007
			else if( date.matches( "[0-3][0-9].[0-1][0-9].[1-2][0-9][0-9][0-9]" ) ) {
				this.year = this.iParse( 4, date.substring( 7 - 1, 10 ) );
				this.month = this.iParse( 2, date.substring( 4 - 1, 5 ) );
				this.day = this.iParse( 2, date.substring( 1 - 1, 2 ) );
			}
			//18.06.07
			else if( date.matches( "[0-3][0-9].[0-1][0-9].[0-9][0-9]" ) ) {
				this.year = Lib_DateTime.yearShortToLong( date.substring( 7 - 1, 8 ) );
				this.month = this.iParse( 2, date.substring( 4 - 1, 5 ) );
				this.day = this.iParse( 2, date.substring( 1 - 1, 2 ) );
			}
			//TODO prüfen! Theoretisch sollte dies mit Länge 5 auch schon passen! Danach Beispiel hier rein!
			else if( laenge >= 6 && laenge <= 10 && date.matches( "^[0-3]?[0-9]\\.[01]?[0-9]\\.[12]?[0-9]{1,3}$" ) ) {
				final String regex = "^([0-9]+)\\.([0-9]+)\\.([0-9]+)$";
				final String tjahr = date.replaceFirst( regex, "$3" );
				if( tjahr.length() == 1 || tjahr.length() == 3 )
					throw Err.invalid( date );

				this.day = Integer.parseInt( date.replaceFirst( regex, "$1" ) );
				this.month = Integer.parseInt( date.replaceFirst( regex, "$2" ) );
				this.year = tjahr.length() == 2
					? Lib_DateTime.yearShortToLong( tjahr )
					: Integer.parseInt( tjahr );
			}
			//2010-5-1
			else if( laenge >= 8 && laenge <= 10 && date.matches( "^[12]?[0-9]{1,3}-[01]?[0-9]-[0-3]?[0-9]$" ) ) {
				final String regex = "^([0-9]+)-([0-9]+)-([0-9]+)$";
				this.day = Integer.parseInt( date.replaceFirst( regex, "$3" ) );
				this.month = Integer.parseInt( date.replaceFirst( regex, "$2" ) );
				this.year = Integer.parseInt( date.replaceFirst( regex, "$1" ) );
			}
			else
				throw Err.invalid( date );
		}
		catch( final Err_Runtime f ) {
			throw Err.wrap( f, "Invalid String!", date );
		}
	}


	// --- Get ---

	public int diffDays( final I_Date d ) {
		final long current = Lib_DateTime.toMilliSec( this.year, this.month, this.day );
		final long target = Lib_DateTime.toMilliSec( d.getYear(), d.getMonth(), d.getDay() );
		final double diff_ms = target - current;
		final double diffdays = diff_ms / 1000 / 60 / 60 / 24;
		int days = (int)diffdays;
		if( Math.abs( diffdays - (int)diffdays ) > 0.9d ) // Damit nicht auch andere Sachen gefiltert werden! Tests waren so alle ok!
			days += diffdays > 0
				? 1
				: -1; //Manchmal geht eine Stunde ab, vmtl. Sommerzeit
		return days;
	}

	public int diffMonths( final I_Date d ) { //Ok, getestet!
		final boolean dig = d.isGreater( this );
		final I_Date small = dig
			? this
			: d;
		final I_Date big = dig
			? d
			: this;

		int result = small.diffYears( big ) * 12; //nur ganze Jahre
		int k1 = big.getMonth() - small.getMonth();
		if( k1 < 0 || k1 == 0 && big.getDay() < small.getDay() )
			k1 += 12;
		result += k1;

		if( big.getDay() < small.getDay() )
			result--;
		if( !dig )
			result = -result;
		return result;
	}

	public int diffYears( final I_Date d ) { //Ok, getestet!
		final boolean dig = d.isGreater( this );
		final I_Date small = dig
			? this
			: d;
		final I_Date big = dig
			? d
			: this;
		int result = big.getYear() - small.getYear();
		if( big.getMonth() < small.getMonth() || big.getMonth() == small.getMonth() && big.getDay() < small.getDay() )
			result--;
		if( !dig )
			result = -result;
		return result;
	}

	public I_Date getAddDays( final int amount ) { //Ok, getestet!
//		long aktuell=Zeit.zuMilliSek(this.getYear(), this.getMonth(), this.getDay());
//		aktuell+=((long)anzahl)*24*60*60*1000;
		final long added = Lib_DateTime.addDays( this.getYear(), this.getMonth(), this.getDay(), amount );
		return new MDate( added );
	}

	public I_Date getAddMonths( final int amount ) { //Ok, getestet!
		final GregorianCalendar gc = this.iGetGregorian();
		gc.add( Calendar.MONTH, amount );
		return new MDate( gc );
	}

	public I_Date getAddWeeks( int amount ) {
		return this.getAddDays( amount * 7 );
	}

	public I_Date getAddYears( final int amount ) { //Ok, getestet!
		final GregorianCalendar gc = this.iGetGregorian();
		gc.add( Calendar.YEAR, amount );
		return new MDate( gc );
	}

	public int getDay() {
		return this.day;
	}

	/**
	 * @return Returns the day of the week. Monday=1, Sunday=7
	 */
	public int getDayOfWeek() {
		final GregorianCalendar gc = this.iGetGregorian();
		int dow = gc.get( Calendar.DAY_OF_WEEK ) - 1;
		return dow == 0
			? 7
			: dow;
	}

	public int getDayOfYear() {
		final GregorianCalendar gc = this.iGetGregorian();
		return gc.get( Calendar.DAY_OF_YEAR );
	}

	public int getQuarterOfYear() {
		if( this.month >= 1 && this.month <= 3 )
			return 1;
		if( this.month >= 4 && this.month <= 6 )
			return 2;
		if( this.month >= 7 && this.month <= 9 )
			return 3;
		return 4;
	}

	public int getWeek() {
		return LocalDate.of( this.year, this.month, this.day ).get( IsoFields.WEEK_OF_WEEK_BASED_YEAR );
	}

	public int getWeekOfYear() {
		final GregorianCalendar gc = this.iGetGregorian();
//		gc.getFirstDayOfWeek();
//		gc.getMinimalDaysInFirstWeek();
		int woy = gc.get( Calendar.WEEK_OF_YEAR );

		if(this.month == 1 && woy > 50)
			return 0;

		final MDate base = new MDate(this.year,1,1);
		if(base.getDayOfWeek() > 1 && base.getWeek() < 50)
			woy--;
		return woy;
	}

	public int getMonth() {
		return this.month;
	}

	public I_Date getSetDay( final int newDay ) {
		return new MDate( this.year, this.month, newDay );
	}

	public I_Date getSetMonth( final int newMonth ) {
		return new MDate( this.year, newMonth, this.day );
	}

	public I_Date getSetYear( final int newYear ) {
		return new MDate( newYear, this.month, this.day );
	}

	public long getValueMilliseconds() {
		return Lib_DateTime.toMilliSec( this.getYear(), this.getMonth(), this.getDay() );
	}

	public int getYear() {
		return this.year;
	}

	@Override
	public boolean isEqual( final I_Date date ) {
		Err.ifNull( date );
		return date.getYear() == this.getYear() && date.getMonth() == this.getMonth() && date.getDay() == this.getDay();
	}

	@Override
	public boolean isGreater( final I_Date date ) {
		final long target = Lib_DateTime.toMilliSec( date.getYear(), date.getMonth(), date.getDay() );
		final long current = Lib_DateTime.toMilliSec( this.getYear(), this.getMonth(), this.getDay() );
		return current > target;
	}

	@Override
	public String toString() {
		return FormDateTime.composeISO( this );
	}

	/**
	 * @see FORM_DATE
	 * @implNote
	 *           TODO Lang?
	 */
	public String toString( final Object... format ) {
		return format.length == 0
			? this.toString()
			: FormDateTime.compose( this, LANGUAGE.EN, format );
	}

	public String toStringDE() {
		return FormDateTime.composeDE( this );
	}

	public String toStringFileSysSlim() {
		return FormDateTime.composeSlim( this );
	}

	public String toStringFileSysWide() {
		return FormDateTime.composeISO( this );
	}

	// PRIVATE

	private void iCheck( final int year, final int month, final int day ) {
		Err.ifOutOfBounds( 1600, 2999, year );
		Err.ifOutOfBounds( 1, 12, month );
		Err.ifOutOfBounds( 1, Lib_DateTime.daysOfMonth( year, month ), day );
	}

	private GregorianCalendar iGetGregorian() {
		final long current = Lib_DateTime.toMilliSec( this.getYear(), this.getMonth(), this.getDay() );
		final GregorianCalendar gc = new GregorianCalendar();
		gc.setTimeInMillis( current );
		return gc;
	}

	private int iParse( final int length, final String s ) {
		Err.ifNot( length, s.length(), "Length of string not as expected" );
		final String result = s;//FILT.TEXT.trimmen(false,t);
		if( s.length() == 0 )
			Err.invalid( s );
		if( !result.equals( result.trim() ) )
			Err.invalid( s );

		try {
			return Integer.valueOf( result );
		}
		catch( final Exception e ) {
			throw Err.invalid( s );
		}
	}

}
