/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.datetime;

import java.util.Calendar;
import java.util.GregorianCalendar;

import de.mn77.base.data.A_Comparable;
import de.mn77.base.data.datetime.format.FORM_DATETIME;
import de.mn77.base.data.filter.CutString;
import de.mn77.base.data.form.FormDateTime;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 */
public class MDateTime extends A_Comparable<I_DateTime> implements I_DateTime {

	private final I_Date date;
	private final I_Time time;


	public MDateTime() {
		this.date = new MDate();
		this.time = new MTime();
	}

	public MDateTime( final Calendar c ) {
		Err.ifNull( c );
		this.date = new MDate( c );
		this.time = new MTime( c );
	}

	public MDateTime( final I_Date date, final I_Time time ) {
		Err.ifNull( date, time );
		this.date = date;
		this.time = time;
	}

	public MDateTime( final int year, final int month, final int day, final int hour, final int min, final int sec ) {
		this.date = new MDate( year, month, day );
		this.time = new MTime( hour, min, sec, 0 );
	}

	public MDateTime( final int year, final int month, final int day, final int hour, final int min, final int sec, final int msec ) {
		this.date = new MDate( year, month, day );
		this.time = new MTime( hour, min, sec, msec );
	}

	public MDateTime( final long milliSec ) {
		final GregorianCalendar c = new GregorianCalendar();
		c.setTimeInMillis( milliSec );
		this.date = new MDate( c );
		this.time = new MTime( c );
	}

	public MDateTime( String s ) {
		s = s.trim();
		Err.ifEmpty( s );
		if( s.length() < 10 )
			throw Err.invalid( "String too short!", s );

		try {

			// 01.02.2003 ...
			// 2003-02-01 ...
			if( s.charAt( 10 ) == ' ' ) {
				final String datestring = s.substring( 0, 10 );
				this.date = new MDate( datestring );
				final String timestring = s.substring( 11 );
				final int tsLen = timestring.length();

				// ... 12:34
				// ... 12:34:56
				// ... 12:34:56.789
				if( tsLen == 5 || tsLen == 8 || tsLen == 12 ) {
					this.time = new MTime( timestring );
					return;
				}
			}
			else // 20030201123456
			if( s.matches( "[1-2][0-9][0-9][0-9][0-1][0-9][0-3][0-9][0-2][0-9][0-5][0-9][0-5][0-9]" ) ) {
				this.date = new MDate( CutString.relCut( s, 1, 4 ) + "-" + CutString.relCut( s, 5, 2 ) + "-" + CutString.relCut( s, 7, 2 ) );
				this.time = new MTime( CutString.relCut( s, 9, 2 ) + ":" + CutString.relCut( s, 11, 2 ) + ":" + CutString.relCut( s, 13, 2 ) );
				return;
			}

			throw Err.invalid( "Unknown format!", s );
		}
		catch( final Err_Runtime f ) {
			throw f;
		}
		catch( final Exception e ) {
			throw Err.invalid( "Invalid String!", s );
		}
	}


	public int diffDays( final I_DateTime to ) {
		return (int)this.diffHours( to ) / 24;
	}

	public long diffHours( final I_DateTime to ) {
		return this.diffMinutes( to ) / 60;
	}

	public long diffMilliseconds( final I_DateTime to ) {
		final long current = Lib_DateTime.toMilliSec( this.getYear(), this.getMonth(), this.getDay(), this.getHours(), this.getMinutes(), this.getSeconds(), this.getMilliseconds() );
		final long other = Lib_DateTime.toMilliSec( to.getYear(), to.getMonth(), to.getDay(), to.getHours(), to.getMinutes(), to.getSeconds(), to.getMilliseconds() );
		return other - current;
	}

	public long diffMinutes( final I_DateTime to ) {
		return this.diffSeconds( to ) / 60;
	}

	public int diffMonths( final I_DateTime to ) {
		throw Err.todo( this.date, this.time, to );
	}

	public long diffSeconds( final I_DateTime to ) {
		return this.diffMilliseconds( to ) / 1000;
	}

	public int diffYears( final I_DateTime to ) {
		throw Err.todo( this.date, this.time, to );
	}

	public I_DateTime getAddDays( final int days ) {
		final long current = Lib_DateTime.addDays( this.getYear(), this.getMonth(), this.getDay(), this.getHours(), this.getMinutes(), this.getSeconds(), this.getMilliseconds(), days );
		return new MDateTime( current );
	}

	public I_DateTime getAddHours( final long hours ) {
		long current = this.getValueMilliseconds();
		current += hours * 60 * 60 * 1000;
		return new MDateTime( current );
	}

	public I_DateTime getAddMilliseconds( final long milliseconds ) {
		long current = this.getValueMilliseconds();
		current += milliseconds;
		return new MDateTime( current );
	}

	public I_DateTime getAddMinutes( final long minutes ) {
		long current = this.getValueMilliseconds();
		current += minutes * 60 * 1000;
		return new MDateTime( current );
	}

	public I_DateTime getAddMonths( final int months ) {
		final long current = Lib_DateTime.addMonths( this.getYear(), this.getMonth(), this.getDay(), this.getHours(), this.getMinutes(), this.getSeconds(), this.getMilliseconds(), months );
		return new MDateTime( current );
	}

	public I_DateTime getAddSeconds( final long seconds ) {
		long current = this.getValueMilliseconds();
		current += seconds * 1000;
		return new MDateTime( current );
	}

	public I_DateTime getAddYears( final int years ) {
		final long current = Lib_DateTime.addYears( this.getYear(), this.getMonth(), this.getDay(), this.getHours(), this.getMinutes(), this.getSeconds(), this.getMilliseconds(), years );
		return new MDateTime( current );
	}

	public I_Date getDate() {
		return this.date;
	}

	public int getDay() {
		return this.date.getDay();
	}

	public int getDayOfWeek() {
		return this.date.getDayOfWeek();
	}

	public int getHours() {
		return this.time.getHours();
	}

	public int getMilliseconds() {
		return this.time.getMilliseconds();
	}

	public int getMinutes() {
		return this.time.getMinutes();
	}

	public int getMonth() {
		return this.date.getMonth();
	}

	public int getSeconds() {
		return this.time.getSeconds();
	}

	public I_DateTime getSetDay( final int newDay ) {
		return new MDateTime( this.date.getYear(), this.date.getMonth(), newDay, this.time.getHours(), this.time.getMinutes(), this.time.getSeconds() );
	}

	public I_DateTime getSetHours( final int newHour ) {
		return new MDateTime( this.date.getYear(), this.date.getMonth(), this.date.getDay(), newHour, this.time.getMinutes(), this.time.getSeconds() );
	}

	public I_DateTime getSetMilliseconds( final int newMilliSeconds ) {
		return new MDateTime( this.date.getYear(), this.date.getMonth(), this.date.getDay(), this.time.getHours(), this.time.getMinutes(), this.time.getSeconds(), newMilliSeconds );
	}

	public I_DateTime getSetMinutes( final int newMinute ) {
		return new MDateTime( this.date.getYear(), this.date.getMonth(), this.date.getDay(), this.time.getHours(), newMinute, this.time.getSeconds() );
	}

	public I_DateTime getSetMonth( final int newMonth ) {
		return new MDateTime( this.date.getYear(), newMonth, this.date.getDay(), this.time.getHours(), this.time.getMinutes(), this.time.getSeconds() );
	}

	public I_DateTime getSetSeconds( final int newSecond ) {
		return new MDateTime( this.date.getYear(), this.date.getMonth(), this.date.getDay(), this.time.getHours(), this.time.getMinutes(), newSecond );
	}

	public I_DateTime getSetYear( final int newYear ) {
		return new MDateTime( newYear, this.date.getMonth(), this.date.getDay(), this.time.getHours(), this.time.getMinutes(), this.time.getSeconds() );
	}

	public I_Time getTime() {
		return this.time;
	}

	public long getValueMilliseconds() {
		return this.date.getValueMilliseconds() + this.time.getValueMilliseconds();
	}

	public int getYear() {
		return this.date.getYear();
	}

	@Override
	public boolean isEqual( final I_DateTime dateTime ) {
		return this.date.isEqual( dateTime.getDate() ) && this.time.isEqual( dateTime.getTime() );
	}

	@Override
	public boolean isGreater( final I_DateTime than ) {
		final long current = Lib_DateTime.toMilliSec( this.getYear(), this.getMonth(), this.getDay(), this.getHours(), this.getMinutes(), this.getSeconds(), 0 );
		final long other = Lib_DateTime.toMilliSec( than.getYear(), than.getMonth(), than.getDay(), than.getHours(), than.getMinutes(), than.getSeconds(), 0 );
		final long diff_ms = current - other;
		return diff_ms > 0;
	}

	@Override
	public String toString() {
		return FormDateTime.compose( this, FORM_DATETIME.GROUP_DEFAULT_FULL );
	}

	public String toString( final Object... format ) {
		return FormDateTime.compose( this, format );
	}

	public String toStringDE() {
		return FormDateTime.composeDE( this );
	}

	public String toStringFileSysSlim() {
		return FormDateTime.composeFileSysSlim( this );
	}

	public String toStringFileSysWide() {
		return FormDateTime.composeFileSysWide( this );
	}

	public String toStringShort() {
		return FormDateTime.composeShort( this );
	}

	public boolean usesMilliseconds() {
		return this.time.usesMilliseconds();
	}

}
