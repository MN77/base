/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.datetime;

import java.sql.Time;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import de.mn77.base.data.A_Comparable;
import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.datetime.format.FORM_TIME;
import de.mn77.base.data.form.FormDateTime;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class MTime extends A_Comparable<I_Time> implements I_Time {

	private final int value;


	public MTime() {
		final GregorianCalendar greg = new GregorianCalendar();
//		greg.setTimeZone(TimeZone.getTimeZone("ECT")); //TODO prüfen!
		greg.setTimeZone( TimeZone.getDefault() ); // TimeZone.getTimeZone("ECT")
		this.value = this.iEncode(
			greg.get( Calendar.HOUR_OF_DAY ),
			greg.get( Calendar.MINUTE ),
			greg.get( Calendar.SECOND ),
			greg.get( Calendar.MILLISECOND ) );
	}

	public MTime( final Calendar c ) {
		Err.ifNull( c );
		this.value = this.iEncode(
			c.get( Calendar.HOUR_OF_DAY ),
			c.get( Calendar.MINUTE ),
			c.get( Calendar.SECOND ),
			c.get( Calendar.MILLISECOND ) );
	}

	public MTime( final int milliSecDay ) {
		this.value = this.iNormalize( milliSecDay );
	}

	public MTime( final int hour, final int min, final int sec ) {
		this( hour, min, sec, 0 );
	}

	public MTime( final int hour, final int min, final int sec, final int millisec ) {
		this.value = this.iEncode(
			this.iCheck24( hour ),
			this.iCheck60( min ),
			this.iCheck60( sec ),
			this.iCheck1000( millisec ) );
	}

	public MTime( final long epochUtcMilliSec ) {
		final GregorianCalendar greg = new GregorianCalendar();
		greg.setTimeZone( TimeZone.getDefault() ); // TimeZone.getTimeZone("ECT")
		greg.setTimeInMillis( epochUtcMilliSec );
		this.value = this.iEncode(
			greg.get( Calendar.HOUR_OF_DAY ),
			greg.get( Calendar.MINUTE ),
			greg.get( Calendar.SECOND ),
			greg.get( Calendar.MILLISECOND ) );
	}

	public MTime( String s ) {
		Err.ifNull( s );
		if( s.length() == 0 )
			Err.invalid( "Empty string" );
		String hh = "0";
		String mm = "0";
		String ss = "0";
		String ms = "0";

		final int indexDot = s.indexOf( '.' );

		if( indexDot > -1 ) {
			ms = s.substring( indexDot + 1 );
			s = s.substring( 0, indexDot );
		}

		final List<String> parts = ConvertString.toList( ":", s );

		if( indexDot > -1 && parts.size() != 3 )
			throw Err.invalid( "Use HH:MM:SS.xxx" );

		switch( parts.size() ) {
			case 3:
				ss = parts.get( 2 );
			case 2:
				mm = parts.get( 1 );
			case 1:
				hh = parts.get( 0 );
				break;
			default:
				throw Err.invalid( "Only 0-2 double dots are allowed!" );
		}

		this.value = this.iEncode(
			this.iCheck24( this.iParseNumber( hh ) ),
			this.iCheck60( this.iParseNumber( mm ) ),
			this.iCheck60( this.iParseNumber( ss ) ),
			this.iCheck1000( this.iParseNumber( ms ) ) );
	}

	public MTime( final Time time ) {
		Err.ifNull( time );
		final GregorianCalendar gc = new GregorianCalendar();
		gc.setTimeInMillis( time.getTime() );
		this.value = this.iEncode(
			gc.get( Calendar.HOUR_OF_DAY ),
			gc.get( Calendar.MINUTE ),
			gc.get( Calendar.SECOND ),
			gc.get( Calendar.MILLISECOND ) );
	}


	// --- Read ---

	public int diffHours( final I_Time other ) {
		return this.diffSeconds( other ) / 60 / 60;
	}

	public int diffMilliseconds( final I_Time other ) {
		return (int)other.getValueMilliseconds() - this.value;
	}

	public int diffMinutes( final I_Time other ) {
		return this.diffSeconds( other ) / 60;
	}

	public int diffSeconds( final I_Time other ) {
		return this.diffMilliseconds( other ) / 1000;
	}

	public I_Time getAddHours( final int amount ) {
		Err.ifOutOfBounds( 572, amount ); // Because of max. Integer value
		final int sum = this.value + amount * 60 * 60 * 1000;
		return new MTime( this.iNormalize( sum ) );
	}

	public I_Time getAddMilliseconds( final int amount ) {
		Err.ifOutOfBounds( 2061083647, amount ); // Because of max. Integer value
		final int sum = this.value + amount;
		return new MTime( this.iNormalize( sum ) );
	}

	public I_Time getAddMinutes( final int amount ) {
		Err.ifOutOfBounds( 34351, amount ); // Because of max. Integer value
		final int sum = this.value + amount * 60 * 1000;
		return new MTime( this.iNormalize( sum ) );
	}

	public I_Time getAddSeconds( final int amount ) {
		Err.ifOutOfBounds( 2061083, amount ); // Because of max. Integer value
		final int sum = this.value + amount * 1000;
		return new MTime( this.iNormalize( sum ) );
	}

	public int getHours() {
		return this.value / 1000 / 60 / 60; // = faster
	}

	public int getMilliseconds() {
		return this.iDecode( this.value )[3];
	}

	public int getMinutes() {
		return this.iDecode( this.value )[1];
	}

	public int getSeconds() {
		return this.iDecode( this.value )[2];
	}

	public I_Time getSetHours( final int newHour ) {
		final int[] current = this.iDecode( this.value );
		return new MTime( newHour, current[1], current[2], current[3] );
	}

	public I_Time getSetMilliseconds( final int milliseconds ) {
		final int[] current = this.iDecode( this.value );
		return new MTime( current[0], current[1], current[2], milliseconds );
	}

	public I_Time getSetMinutes( final int newMinute ) {
		final int[] current = this.iDecode( this.value );
		return new MTime( current[0], newMinute, current[2], current[3] );
	}

	public I_Time getSetSeconds( final int newSecond ) {
		final int[] current = this.iDecode( this.value );
		return new MTime( current[0], current[1], newSecond, current[3] );
	}

	public long getValueMilliseconds() {
		return this.value;
	}

	public int getValueSeconds() {
		return this.value / 1000;
	}

	@Override
	public boolean isEqual( final I_Time t ) {
		return t.getHours() == this.getHours() && t.getMinutes() == this.getMinutes() && t.getSeconds() == this.getSeconds();
	}

	@Override
	public boolean isGreater( final I_Time than ) {
		final long other = Lib_DateTime.toMilliSec( than.getHours(), than.getMinutes(), than.getSeconds(), 0 );
		final long current = Lib_DateTime.toMilliSec( this.getHours(), this.getMinutes(), this.getSeconds(), 0 );
		return current > other;
	}

	public I_Time toRoundedHHMM() {
		final int rounded = (int)Math.round( this.value / 60000d ) * 60000;
		return new MTime( rounded );
	}

	@Override
	public String toString() {
		return FormDateTime.compose( this, FORM_TIME.GROUP_HHMMSS_DDD );
	}

	/** @see FORM_TIME **/
	public String toString( final Object... form ) {
		return FormDateTime.compose( this, form );
	}

	public String toStringDE() {
		return FormDateTime.compose( this, FORM_TIME.GROUP_HHMMSS );
	}

	public String toStringFileSysSlim() {
		return FormDateTime.compose( this, FORM_TIME.GROUP_HHMMSS_SLIM );
	}

	public String toStringFileSysWide() {
		return FormDateTime.compose( this, FORM_TIME.GROUP_HH_MM_SS );
	}

	public String toStringShort() {
		return FormDateTime.compose( this, FORM_TIME.GROUP_HHMMSS );
	}

	public String toStringSlim() {
		return FormDateTime.compose( this, FORM_TIME.GROUP_HHMMSS_DDD_SLIM );
	}

	public boolean usesMilliseconds() {
		return Lib_DateTime.usesMilliSeconds( this.value );
	}


	// --- Private ---

	private int iCheck1000( final int value ) {
		Err.ifOutOfBounds( 0, 999, value );
		return value;
	}

	private int iCheck24( final int hour ) {
		Err.ifOutOfBounds( 0, 23, hour );
		return hour;
	}

	private int iCheck60( final int value ) {
		Err.ifOutOfBounds( 0, 59, value );
		return value;
	}

	private int[] iDecode( int v ) {
		final int[] result = new int[4];
		result[0] = v / 1000 / 60 / 60;
		v -= result[0] * 1000 * 60 * 60;
		result[1] = v / 1000 / 60;
		v -= result[1] * 1000 * 60;
		result[2] = v / 1000;
		v -= result[2] * 1000;
		result[3] = v;
		return result;
	}

//	private int iToSec(final int hour, final int min, final int sec) {
//		return hour * 60 * 60 + min * 60 + sec;
//	}

//	private MTime iSecToTime(int sec) {
//		int hh = sec / 3600;
//		sec -= hh * 3600;
//		final int mm = sec / 60;
//		sec -= mm * 60;
//		final int ss = sec;
//		while(hh > 23)
//			hh -= 24;
//		return new MTime(hh, mm, ss);
//	}

	private int iEncode( final int hh, final int mm, final int ss, final int ms ) {
		return hh * 1000 * 60 * 60
			+ mm * 1000 * 60
			+ ss * 1000
			+ ms;
	}

	private int iNormalize( int sum ) {
		final int day = 86400000; // 24 * 60 * 60 * 1000;
		while( sum < 0 )
			sum += day;
		while( sum > day )
			sum -= day;
		return sum;
	}

	private int iParseNumber( final String s ) {
		final int len = s.length();
		if( s == null || len == 0 )
			return 0;
		Err.ifOutOfBounds( 1, 3, len );

		try {
			return Integer.valueOf( s );
		}
		catch( final Exception e ) {
			throw Err.invalid( s );
		}
	}

}
