/*******************************************************************************
 * Copyright (C) 2008-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.datetime;

import java.util.Calendar;
import java.util.GregorianCalendar;

import de.mn77.base.data.A_Comparable;
import de.mn77.base.data.datetime.format.FORM_DATE;
import de.mn77.base.data.datetime.format.FORM_TIME;
import de.mn77.base.data.datetime.format.FORM_WORLDTIME;
import de.mn77.base.data.filter.CutString;
import de.mn77.base.data.form.FormNumber;
import de.mn77.base.data.util.Lib_Math;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 2008-05-03
 */
public class MWorldTime extends A_Comparable<I_WorldTime> implements I_WorldTime {

	private final I_DateTime datetime;
	private final int        utc_offset_msek;


	public MWorldTime() {
		this.datetime = new MDateTime();
		final GregorianCalendar gc = new GregorianCalendar();
		this.utc_offset_msek = gc.get( Calendar.ZONE_OFFSET ) + gc.get( Calendar.DST_OFFSET );
	}

	public MWorldTime( final I_DateTime zeitpunkt, final int utc_offset_std ) {
		Err.ifNull( zeitpunkt );
		this.datetime = zeitpunkt;
		Err.ifOutOfBounds( 12, utc_offset_std );
		this.utc_offset_msek = utc_offset_std * 60 * 60 * 1000;
	}

	public MWorldTime( final String text ) {
		Err.ifEmpty( text );
		final int laenge = text.length();
		Err.ifOutOfBounds( 14, 31, laenge );

		try {

			//"Fri, 02 May 2008 19:17:31 +0200" =RFC822   //"Fri, 02 May 2008 19:17:31 GMT"
			if( laenge >= 29 && laenge <= 31 && text.matches( "^[A-Z][a-z]{2}, [0-9]{2} [A-Z][a-z]{2} [0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2} .*$" ) ) {
				final Integer monat = Lib_DateTime.parseMonth( CutString.relCut( text, 9, 3 ) );
				if( monat == null )
					Err.invalid( "Unknown month", CutString.relCut( text, 9, 3 ) );
				final I_Date datum = new MDate( Integer.parseInt( CutString.relCut( text, 6, 2 ) ), monat, Integer.parseInt( CutString.relCut( text, 13, 4 ) ) );
				final I_Time uhrzeit = new MTime( CutString.relCut( text, 18, 8 ) );
				this.datetime = new MDateTime( datum, uhrzeit );

				final String rest = CutString.relCut( text, 27, false );
				Integer h;
				Integer m;

				if( rest.length() == 5 && rest.matches( "^[\\+-][0-9]{4}$" ) ) { //+0200
					if( rest.charAt( 0 ) != '+' )
						Err.todo();
					h = Integer.parseInt( CutString.relCut( rest, 2, 2 ) );
					m = Integer.parseInt( CutString.relCut( rest, 4, 2 ) );
				}
				else if( rest.length() == 4 && rest.matches( "^[0-9]{4}$" ) ) { //0200
					h = Integer.parseInt( CutString.relCut( rest, 1, 2 ) );
					m = Integer.parseInt( CutString.relCut( rest, 3, 2 ) );
				}
				else {
					if( rest.toUpperCase().equals( "GMT" ) )
						h = 0;
					else if( rest.toUpperCase().equals( "CEST" ) )
						h = 2;
					else if( rest.toUpperCase().equals( "CET" ) )
						h = 1;
					else
						throw Err.invalid( "Unknown time format!", text );

					m = 0;
				}

				this.utc_offset_msek = (m + h * 60) * 60 * 1000;
			}
			//"2008-08-12T16:50:47+02:00"
			else if( text.matches( "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}[\\+-][0-9]{2}:[0-9]{2}" ) ) {
				final MDateTime zp = new MDateTime( text.replaceFirst( "T", " " ).substring( 0, 19 ) );
				this.datetime = zp;
				Integer h, m;
				if( text.charAt( 20 - 1 ) != '+' )
					Err.todo();
				h = Integer.parseInt( CutString.relCut( text, 21, 2 ) );
				m = Integer.parseInt( CutString.relCut( text, 24, 2 ) );
				this.utc_offset_msek = (m + h * 60) * 60 * 1000;
			}
			//"2008-05-02 19:17:31"
			else if( text.matches( "[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}" ) ) {
				final MDateTime zp = new MDateTime( text );
				this.datetime = zp;
				final GregorianCalendar gc = new GregorianCalendar( zp.getYear(), zp.getMonth() - 1, zp.getDay(),
					zp.getHours(), zp.getMinutes(), zp.getSeconds() );
//				MOut.dev(gc.get(Calendar.ZONE_OFFSET),gc.get(Calendar.DST_OFFSET));
				this.utc_offset_msek = gc.get( Calendar.ZONE_OFFSET ) + gc.get( Calendar.DST_OFFSET );
			}
			else
				throw Err.invalid( "Unknown time format!", text );
		}
		catch( final Exception e ) {
			throw e;
		}
	}


//	public I_WorldTime getAddDays(final int days) {
//		I_DateTime dt2 = this.datetime;
//		dt2 = this.datetime.getAddDays(days);
//		return new MWorldTime(dt2, this.utc_offset_msek);
//	}

	public I_DateTime getDateTime() {
		return this.datetime;
	}

	public int getDay() {
		return this.datetime.getDay();
	}

	public int getDayOfWeek() {
		return this.datetime.getDayOfWeek();
	}

	public int getHours() {
		return this.datetime.getDay();
	}

	public int getMilliseconds() {
		throw Err.todo( this );
	}

	public int getMinutes() {
		return this.datetime.getDay();
	}

	public int getMonth() {
		return this.datetime.getDay();
	}

	public int getSeconds() {
		return this.datetime.getDay();
	}

	public int getUTC_Offset_MSek() {
		return this.utc_offset_msek;
	}

	public long getValueMilliseconds() {
		throw Err.todo( this );
	}

	public int getYear() {
		return this.datetime.getDay();
	}

	@Override
	public boolean isEqual( final I_WorldTime zeit ) {
		return this.datetime.isEqual( zeit.getDateTime() ) && this.utc_offset_msek == zeit.getUTC_Offset_MSek();
	}

	@Override
	public boolean isGreater( final I_WorldTime zeit ) {
		throw Err.todo();
	}

	@Override
	public String toString() {
		return this.toStringDefault();
	}

	public String toString( final Object... format ) {
		String result = "";
		for( final Object o : format )
			if( o instanceof String )
				result += o;
			else if( o instanceof FORM_DATE || o instanceof FORM_TIME )
				result += this.datetime.toString( new Object[]{ o } );
			else if( o == FORM_WORLDTIME.GMT_5 ) {
				final int gmt_min = Lib_Math.absBetrag( this.utc_offset_msek ) / 1000 / 60;
				result += this.utc_offset_msek >= 0 ? "+" : "-";
				final int h = gmt_min / 60;
				final int m = gmt_min - h * 60;
				result += FormNumber.width( 2, Lib_Math.roundDown( h ), false );
				result += FormNumber.width( 2, Lib_Math.roundDown( m ), false );
			}
			else
				Err.invalid( o );
		return result;
	}

	public String toStringDE() {
		throw Err.todo( this );
	}

	public String toStringDefault() {
		return this.toStringRFC822();
	}

	public String toStringFileSysSlim() {
		throw Err.todo( this );
	}

	public String toStringFileSysWide() {
		throw Err.todo( this );
	}

	public String toStringRFC822() {
		return this.toString( FORM_WORLDTIME.GROUP_RFC822 );
	}

	public String toStringShort() {
		throw Err.todo( this );
	}

	public boolean usesMilliseconds() {
		throw Err.todo( this );
	}

}
