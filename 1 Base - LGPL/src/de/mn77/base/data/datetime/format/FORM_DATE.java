/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.datetime.format;

/**
 * @author Michael Nitsche
 */
public enum FORM_DATE {

	YEAR,
	YEAR_2,
	YEAR_4,

	MONTH,
	MONTH_2,
	MONTH_TEXT,
	MONTH_TEXT_3,

	WEEK,
	WEEK_2,
	WEEK_MONTH,
	WEEK_MONTH_1,

	DAY,
	DAY_2,
	DAY_WEEK,
	DAY_WEEK_1,
	DAY_WEEK_TEXT_2,
	DAY_WEEK_TEXT_3,
	DAY_WEEK_TEXT,
	DAY_YEAR_3,
	TAG_JAHR,

	_TODAY,
	_LEAPYEAR,
	_DAYS_OF_MONTH;


//	public static final Object[] GROUP_DB
//		= new Object[]{ FORM_DATE.JAHR_4, FORM_DATE.MONAT_2, FORM_DATE.TAG_2 }; //jjjjmmtt


	/**
	 * jjjjmmtt
	 */
	public static final Object[] GROUP_SHORT = { FORM_DATE.YEAR_4, FORM_DATE.MONTH_2, FORM_DATE.DAY_2 };

	/**
	 * jjjj-mm-tt
	 */
	public static final Object[] GROUP_ISO = { FORM_DATE.YEAR_4, "-", FORM_DATE.MONTH_2, "-", FORM_DATE.DAY_2 };

	/**
	 * tt.mm.jjjj
	 **/
	public static final Object[] GROUP_DE = { FORM_DATE.DAY_2, ".", FORM_DATE.MONTH_2, ".", FORM_DATE.YEAR_4 };

	/**
	 * tt.mm.jj
	 */
	public static final Object[] GROUP_DE_SHORT = { FORM_DATE.DAY_2, ".", FORM_DATE.MONTH_2, ".", FORM_DATE.YEAR_2 };

}
