/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.datetime.format;

/**
 * @author Michael Nitsche
 */
public interface FORM_DATETIME {

	/**
	 * jjjj-mm-tt_hh-mm-ss
	 */
	Object[] GROUP_FILESYS = { FORM_DATE.YEAR_4, "-", FORM_DATE.MONTH_2, "-", FORM_DATE.DAY_2, "_", FORM_TIME.STD_2, "-", FORM_TIME.MIN_2, "-", FORM_TIME.SEK_2 };

	/**
	 * jjjjmmtt_hhmmss
	 */
	Object[] GROUP_FILESYS_SLIM = { FORM_DATE.YEAR_4, FORM_DATE.MONTH_2, FORM_DATE.DAY_2, "_", FORM_TIME.STD_2, FORM_TIME.MIN_2, FORM_TIME.SEK_2 };

	/**
	 * jjjj-mm-tt hh:mm:ss
	 */
	Object[] GROUP_DEFAULT = { FORM_DATE.YEAR_4, "-", FORM_DATE.MONTH_2, "-", FORM_DATE.DAY_2, " ", FORM_TIME.STD_2, ":", FORM_TIME.MIN_2, ":", FORM_TIME.SEK_2 };

	/**
	 * jjjj-mm-tt hh:mm:ss.nnn
	 */
	Object[] GROUP_DEFAULT_FULL = { FORM_DATE.YEAR_4, "-", FORM_DATE.MONTH_2, "-", FORM_DATE.DAY_2, " ", FORM_TIME.STD_2, ":", FORM_TIME.MIN_2, ":", FORM_TIME.SEK_2, ".", FORM_TIME.MSEK_3 };

	/**
	 * tt.mm.jjjj hh:mm:ss
	 */
	Object[] GROUP_DE = { FORM_DATE.DAY_2, ".", FORM_DATE.MONTH_2, ".", FORM_DATE.YEAR_4, " ", FORM_TIME.STD_2, ":", FORM_TIME.MIN_2, ":", FORM_TIME.SEK_2 };

}
