/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.datetime.format;

/**
 * @author Michael Nitsche
 */
public enum FORM_TIME {

	STD,
	STD_2,

	MIN,
	MIN_2,

	SEK,
	SEK_2,

	MSEK,
	MSEK_3;


	/**
	 * HH:MM:SS.nnn
	 */
	public static final Object[] GROUP_HHMMSS_DDD = { FORM_TIME.STD_2, ":", FORM_TIME.MIN_2, ":", FORM_TIME.SEK_2, ".", FORM_TIME.MSEK_3 };

	/**
	 * HHMMSS.nnn
	 */
	public static final Object[] GROUP_HHMMSS_DDD_SLIM = { FORM_TIME.STD_2, FORM_TIME.MIN_2, FORM_TIME.SEK_2, ".", FORM_TIME.MSEK_3 };

	/**
	 * HH:MM:SS
	 */
	public static final Object[] GROUP_HHMMSS = { FORM_TIME.STD_2, ":", FORM_TIME.MIN_2, ":", FORM_TIME.SEK_2 };

	/**
	 * HH:MM
	 */
	public static final Object[] GROUP_HHMM = { FORM_TIME.STD_2, ":", FORM_TIME.MIN_2 };

	/**
	 * MM:SS
	 */
	public static final Object[] GROUP_MMSS = { FORM_TIME.MIN_2, ":", FORM_TIME.SEK_2 };

	/**
	 * HHMMSS
	 */
	public static final Object[] GROUP_HHMMSS_SLIM = { FORM_TIME.STD_2, FORM_TIME.MIN_2, FORM_TIME.SEK_2 };

	/**
	 * HH-MM-SS
	 */
	public static final Object[] GROUP_HH_MM_SS = { FORM_TIME.STD_2, "-", FORM_TIME.MIN_2, "-", FORM_TIME.SEK_2 };

}
