/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.datetime.format;

/**
 * @author Michael Nitsche
 * @created 29.04.2020
 */
public enum FORM_WORLDTIME {

	GMT,
	GMT_5,
	UTC,
	MEZ,
	MESZ;


	/**
	 * "Wed, 10 Jan 2020 12:35:44 GMT"
	 */
	public static final Object[] GROUP_RFC822 = {
		FORM_DATE.DAY_WEEK_TEXT_3,
		", ",
		FORM_DATE.DAY_2,
		" ",
		FORM_DATE.MONTH_TEXT_3,
		" ",
		FORM_DATE.YEAR_4,
		" ",
		FORM_TIME.STD_2,
		":",
		FORM_TIME.MIN_2,
		":",
		FORM_TIME.SEK_2,
		" ",
		FORM_WORLDTIME.GMT_5
	};

}
