/*******************************************************************************
 * Copyright (C) 2023-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.filter;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 02.12.2023
 */
public class CutString { // TODO Rename to U_CutString oder U_StringCut (besser)

	private static final String EMPTY = "";


	private CutString() {}


	public static String afterFirst( final String s, final char ch, final boolean emptyIfMissing ) {
		Err.ifNull( s );
		for( int i = 0; i < s.length(); i++ )
			if( s.charAt( i ) == ch )
				return s.substring( i + 1 );
		return emptyIfMissing ? CutString.EMPTY : s;
	}

	public static String afterLast( final String s, final char ch, final boolean emptyIfMissing ) {
		Err.ifNull( s );
		for( int i = s.length() - 1; i >= 0; i-- )
			if( s.charAt( i ) == ch )
				return s.substring( i + 1 );
		return emptyIfMissing ? CutString.EMPTY : s;
	}

	public static String before( final String s, final char[] chars, final boolean last ) {
		return CutString.iTill( s, chars, last, 0 );
	}

	public static String beforeFirst( final String s, final char ch, final boolean emptyIfMissing ) {
		Err.ifNull( s );
		for( int i = 0; i < s.length(); i++ )
			if( s.charAt( i ) == ch )
				return s.substring( 0, i );
		return emptyIfMissing ? CutString.EMPTY : s;
	}

	public static String beforeLast( final String s, final char ch, final boolean emptyIfMissing ) {
		Err.ifNull( s );
		for( int i = s.length() - 1; i >= 0; i-- )
			if( s.charAt( i ) == ch )
				return s.substring( 0, i );
		return emptyIfMissing ? CutString.EMPTY : s;
	}

	public static String between( final String s, final String left, final String right, final int nr ) {
		int nr_pos = 0;
		int start = -1;

		while( nr_pos < nr ) {
			start = s.indexOf( left, start + 1 );
			if( start == -1 )
				break;
			nr_pos++;
		}

		start += left.length();
		int end = s.indexOf( right, start );
		if( end == -1 )
			end = s.length();
		return s.substring( start, end );
	}

	public static String between( final String s, final String left, final String right ) {
		return CutString.between( s, left, right, 1 );
	}

	public static String cut( final String s, final int startIndex, final boolean toLeft ) {
		Err.ifNull( s );
		Err.ifOutOfBounds( 0, s.length() - 1, startIndex );

		return toLeft
			? s.substring( 0, startIndex + 1 )
			: s.substring( startIndex );
	}

//	public static String till( final String search, final boolean last, final String s ) {
//		Err.ifNull( s );
//		Err.ifEmpty( search );
//
//		if( last ) {
//			for( int i = s.length() - 1; i >= 0; i-- )
//				if( s.indexOf( search, i ) > -1 )
//					return FilterString.relCut( s, i + search.length(), true );
//		}
//		else {
//			final int i = s.indexOf( search );
//			if( i >= 0 )
//				return FilterString.relCut( s, i + search.length(), true );
//		}
//
//		return s;
//	}

	public static String cut( final String s, final int startIndex, final int length ) {
		Err.ifNull( s );
		final int sLength = s.length();
		Err.ifOutOfBounds( 0, sLength - 1, startIndex );
		final int end = startIndex + length;
		Err.ifOutOfBounds( 0, sLength - startIndex, length );
		return s.substring( startIndex, end );
	}

	public static String from( final String s, final String search, final boolean last ) {
		Err.ifNull( search, s );
		Err.ifTooSmall( 1, search.length() );

		if( last ) {
			int start = 0;
			int index = -1;
			int tmp = -1;
			boolean min1 = false;

			while( (tmp = s.indexOf( search, start )) > -1 ) {
				min1 = true;
				index = tmp;
				start = tmp + 1;
			}

			return !min1
				? s
				: CutString.cut( s, index, false );
		}
		else {
			final int index = s.indexOf( search );
			return index < 0
				? s
				: CutString.cut( s, index, false );
		}
	}

	public static String fromFirst( final String s, final char ch, final boolean emptyIfMissing ) {
		Err.ifNull( s );
		for( int i = 0; i < s.length(); i++ )
			if( s.charAt( i ) == ch )
				return s.substring( i );
		return emptyIfMissing ? CutString.EMPTY : s;
	}

	public static String fromLast( final String s, final char ch, final boolean emptyIfMissing ) {
		Err.ifNull( s );
		for( int i = s.length() - 1; i >= 0; i-- )
			if( s.charAt( i ) == ch )
				return s.substring( i );
		return emptyIfMissing ? CutString.EMPTY : s;
	}

	public static String limit( final String s, final int maxLength ) {
		return s.length() > maxLength
			? CutString.cut( s, maxLength - 1, true )
			: s;
	}

	public static String relCut( final String s, final int relStartPos, final boolean toLeft ) {
		Err.ifNull( s );
		Err.ifTooSmall( Math.abs( relStartPos ), s.length() );

		final int from = relStartPos < 0
			? s.length() + relStartPos + 1
			: relStartPos;
		if( toLeft )
			return s.substring( 0, from );
		else
			return s.substring( from - 1 );
	}

	public static String relCut( final String s, final int relStartPos, final int length_right ) {
		Err.ifNull( s, relStartPos );
		if( relStartPos < 0 )
			Err.ifToBig( -relStartPos, length_right );
		if( relStartPos > 0 )
			Err.ifTooSmall( -relStartPos, length_right );
		Err.ifTooSmall( Math.abs( relStartPos ), s.length() );
		if( relStartPos < 0 )
			Err.ifTooSmall( -relStartPos + -length_right - (length_right == 0
				? 0
				: 1), s.length() );
		if( relStartPos > 0 )
			Err.ifTooSmall( relStartPos + length_right - (length_right == 0
				? 0
				: 1), s.length() );

		final int from1 = relStartPos < 0
			? s.length() + relStartPos + 1
			: relStartPos;
		final int to1 = from1 + length_right + (length_right < 0
			? +1
			: -1);

		if( length_right == 0 )
			return "";

		return s.substring( to1 < from1
			? to1 - 1
			: from1 - 1,
			to1 < from1
				? from1
				: to1 );
	}

	public static String right( final String s, final int len ) {
		int start = s.length() - len;
		if( start < 0 )
			start = 0;
		return s.substring( start );
	}

	/**
	 * gibt "" zurück, wenn start nicht enthalten ist. Ansonsten alles ab dem letzten vorkommen von Start
	 * Ist z.B. zum Herausfiltern eines Suffix vom Dateinamen
	 */
	public static String right( final String s, final String start ) { // TODO Rename to after/from
		if( !s.contains( start ) )
			return "";
		final int from = s.lastIndexOf( start );
		if( from == -1 )
			return "";
		return s.substring( from );
	}

	public static String till( final String s, final char[] chars, final boolean last ) {
		return CutString.iTill( s, chars, last, 1 );
	}

	public static String tillFirst( final String s, final char ch, final boolean emptyIfMissing ) {
		Err.ifNull( s );
		for( int i = 0; i < s.length(); i++ )
			if( s.charAt( i ) == ch )
				return s.substring( 0, i + 1 );
		return emptyIfMissing ? CutString.EMPTY : s;
	}

	public static String tillLast( final String s, final char ch, final boolean emptyIfMissing ) {
		Err.ifNull( s );
		for( int i = s.length() - 1; i >= 0; i-- )
			if( s.charAt( i ) == ch )
				return s.substring( 0, i + 1 );
		return emptyIfMissing ? CutString.EMPTY : s;
	}

	private static String iTill( final String s, final char[] chars, final boolean last, final int offset ) {
		Err.ifNull( chars, s );
		Err.ifTooSmall( 1, chars.length );
		final String charsStr = new String( chars );

		if( last ) {
			for( int i = s.length() - 1; i >= 0; i-- )
				if( charsStr.indexOf( s.charAt( i ) ) > -1 )
					return CutString.relCut( s, i + offset, true );
		}
		else
			for( int i = 0; i <= s.length() - 1; i++ )
				if( charsStr.indexOf( s.charAt( i ) ) > -1 )
					return CutString.relCut( s, i + offset, true );

		return s;
	}

}
