/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.filter;

import java.util.List;

import de.mn77.base.data.ComplexUnicodeChar;
import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.search.SearchArray;
import de.mn77.base.data.util.Lib_Unicode;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class FilterString {

	/**
	 * Returns all chars from left, that are in chars, up to any other char
	 */
	public static String matchingLeft( final String chars, final String s ) {
		int end = -1;
		for( final char c : s.toCharArray() )
			if( chars.indexOf( c ) > -1 )
				end++;
			else
				return s.substring( 0, end + 1 );
		return s;
	}

	public static String only( final char[] chars, final String s ) {
		Err.ifNull( chars, s );
		final StringBuilder sb = new StringBuilder();
		final char[] sca = s.toCharArray();
		final String chars_str = new String( chars );
		for( final char sc : sca )
			if( chars_str.indexOf( sc ) >= 0 )
				sb.append( sc );
		return sb.toString();
	}

	public static String only( final char[] chars, final String s, final char replace ) {
		Err.ifNull( chars, s );
		final StringBuilder sb = new StringBuilder();
		final char[] sca = s.toCharArray();
		final String chars_str = new String( chars );
		for( final char sc : sca )
			sb.append( chars_str.indexOf( sc ) >= 0
				? sc
				: replace );
		return sb.toString();
	}

	public static char relCharAt( final String s, final int relPosition ) {
		Err.ifEmpty( s );
		Err.ifOutOfBounds( -s.length(), s.length(), relPosition );

		return relPosition < 0
			? s.charAt( s.length() + relPosition )
			: s.charAt( relPosition - 1 );
	}

	public static String removeChar( final char ch, final String s ) {
		Err.ifNull( s );
		final StringBuilder sb = new StringBuilder( s.length() );
		final char[] sca = s.toCharArray();
		for( final char sc : sca )
			if( ch != sc )
				sb.append( sc );
		return sb.toString();
	}

	public static String removeChars( final char[] chars, final String s ) {
		Err.ifNull( chars, s );
		final StringBuilder sb = new StringBuilder( s.length() );
		final char[] sca = s.toCharArray();
		final String chars_str = new String( chars );
		for( final char sc : sca )
			if( chars_str.indexOf( sc ) < 0 )
				sb.append( sc );
		return sb.toString();
	}

	/**
	 * @apiNote Removes all lines, which contains nothing or only whitspaces.
	 */
	public static String removeEmptyLines( final String text ) {
		final StringBuilder sb = new StringBuilder();
		final Iterable<String> lines = ConvertString.toLines( text );

		for( String line : lines ) {
			line = line.trim();
			if( line.length() > 0 )
				sb.append( line + '\n' );
		}

		sb.deleteCharAt( sb.length() - 1 );
		return sb.toString();
	}

//	public static String trim(String s, char[] zeichen, boolean links, boolean rechts) {
//	String zs=new String(zeichen);
//	if(links)
//		while(s.length()>0 && zs.indexOf(s.charAt(0))>=0)
//			s=s.substring(1);
//	if(rechts)
//		while(s.length()>0 && zs.indexOf(s.charAt(s.length()-1))>=0)
//			s=s.substring(0, s.length()-1);
//	return s;
//}

	/**
	 * Removes one trailing linebreak, if existing
	 */
	public static String removeNewlineEnd( final String s ) {
		if( s != null && s.length() >= 1 && s.charAt( s.length() - 1 ) == '\n' )
			return s.substring( 0, s.length() - 1 );
		return s;
	}

	public static String removeNull( final String s ) {
		return FilterString.removeChar( (char)0, s );
	}

	/**
	 * Wenn Backslash oder Dollar in replacement(in) benötigt, dann diesen vorher hier durchlaufen lassen:
	 * Matcher.quoteReplacement();
	 * Backslash in replacement(in) müssen Doppelt übergeben werden, um einen zu erhalten!
	 */
	public static String replace( final String search, final String replacement, final String s ) {
		Err.ifNull( search, replacement, s );
		return s.replaceAll( "\\Q" + search + "\\E", replacement );
	}

	public static String trim( final String s ) {
		Err.ifNull( s );
		return s.trim();
	}

	/**
	 * Removes all characters from left and/or right, which is ASC/Unicode ' ' (Whitespace) or below.
	 */
	public static String trim( final String s, final boolean left, final boolean right ) {
		Err.ifNull( s );
		final int len = s.length();
		if( len == 0 )
			return "";

		int start = 0;
		int end = len - 1;

		if( left )
			while( start < len && s.charAt( start ) <= ' ' )
				start++;
		if( right )
			while( end >= start && s.charAt( end ) <= ' ' )
				end--;
		return s.substring( start, end + 1 );
	}

	/**
	 * Schnellster im Leistungstest.
	 *
	 * statt FilterString.trim(s, new char[] {'\t',' '}, true, true);
	 * ggf. String.trim() verwenden!!!
	 */
	public static String trim( final String s, final char[] chars, final boolean left, final boolean right ) {
		int ptL = 0;
		int ptR = s.length() - 1;

		if( left )
			while( ptL < ptR && SearchArray.contains( chars, s.charAt( ptL ) ) )
				ptL++;
		if( right )
			while( ptR >= ptL && SearchArray.contains( chars, s.charAt( ptR ) ) )
				ptR--;
		return s.substring( ptL, ptR + 1 );
	}

	public static String trim( final String s, final ComplexUnicodeChar[] chars, final boolean left, final boolean right ) {
		int ptL = 0;
		int ptR = s.length() - 1;

		if( left )
			while( ptL < ptR && SearchArray.contains( chars, Lib_Unicode.charAt( s, ptL ) ) )
				ptL++;
		if( right )
			while( ptR >= ptL && SearchArray.contains( chars, Lib_Unicode.charAt( s, ptR ) ) )
				ptR--;
		return s.substring( ptL, ptR + 1 );
	}

	/**
	 * @apiNote Removes all 'c'-Chars from left and/or right
	 */
	public static String trimChar( final String s, final char c, final boolean left, final boolean right ) {
		int ptL = 0;
		int ptR = s.length() - 1;

		if( left )
			while( ptL < ptR && s.charAt( ptL ) == c )
				ptL++;
		if( right )
			while( ptR >= ptL && s.charAt( ptR ) == c )
				ptR--;

		return s.substring( ptL, ptR + 1 );
	}

	/**
	 * @apiNote Removes all newline '\n'-Chars from left and/or right
	 */
	public static String trimNewLines( final String s, final boolean left, final boolean right ) {
		return FilterString.trimChar( s, '\n', left, right );
	}

	/**
	 * @apiNote Removes all space chars ' ' from the left and right (and only those)
	 */
	public static String trimSpace( final String s, final boolean left, final boolean right ) {
		return FilterString.trimChar( s, ' ', left, right );
	}

	public static String trimToASCII( final String s ) {
		Err.ifNull( s );
		final StringBuilder sb = new StringBuilder();
		for( final char c : s.toCharArray() )
			if( c < 256 )
				sb.append( c );
		return sb.toString();
	}

	/**
	 * @apiNote Unites multible chars of 'c' to only a single 'c'.
	 *          'o': "foo" --> "fo"
	 */
	public static String uniteDoubles( final char c, final String s ) {
		Err.ifNull( s );
		final StringBuilder sb = new StringBuilder( s.length() );
		char prior = 0;
		final char[] sca = s.toCharArray();

		for( final char sc : sca )
			if( !(sc == prior && prior == c) ) {
				sb.append( sc );
				prior = sc;
			}

		return sb.toString();
	}

	public static String uniteDoubles( final ComplexUnicodeChar cuc, final String s ) {
		Err.ifNull( s );
		final StringBuilder sb = new StringBuilder( s.length() );
		ComplexUnicodeChar prior = null;
		final List<ComplexUnicodeChar> sca = Lib_Unicode.toComplexCharList( s );

		for( final ComplexUnicodeChar sc : sca ) {
			if( prior == null || !(sc.equals( prior ) && prior.equals( cuc )) )
				sb.append( sc );
			prior = sc;
		}

		return sb.toString();
	}

	/**
	 * @apiNote Unites all rows of a character to a single char.
	 */
	public static String uniteDoubles( final String s ) {
		Err.ifNull( s );
		final StringBuilder sb = new StringBuilder( s.length() );
		char prior = 0;
		final char[] sca = s.toCharArray();

		for( final char sc : sca ) {
			if( sc != prior )
				sb.append( sc );
			prior = sc;
		}

		return sb.toString();
	}

}
