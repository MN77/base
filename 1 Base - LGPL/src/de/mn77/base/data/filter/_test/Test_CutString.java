/*******************************************************************************
 * Copyright (C) 2023-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.filter._test;

import de.mn77.base.data.filter.CutString;
import de.mn77.base.sys.MOut;

/**
 * @author	Michael Nitsche
 * @created	02.12.2023
 */
public class Test_CutString {

	public static void main( String[] args ) {
		final String s1 = "abcdedcba";
		final String s2 = "zyxwvwxyz";

		MOut.print( CutString.beforeFirst( s1, 'c', true ) );
		MOut.print( CutString.beforeFirst( s2, 'c', true ) );
		MOut.print( CutString.beforeLast( s1, 'c', true ) );
		MOut.print( CutString.beforeLast( s2, 'c', true ) );
		MOut.print("---");

		MOut.print( CutString.tillFirst( s1, 'c', true ) );
		MOut.print( CutString.tillFirst( s2, 'c', true ) );
		MOut.print( CutString.tillLast( s1, 'c', true ) );
		MOut.print( CutString.tillLast( s2, 'c', true ) );
		MOut.print("---");

		MOut.print( CutString.fromFirst( s1, 'c', true ) );
		MOut.print( CutString.fromFirst( s2, 'c', true ) );
		MOut.print( CutString.fromLast( s1, 'c', true ) );
		MOut.print( CutString.fromLast( s2, 'c', true ) );
		MOut.print("---");

		MOut.print( CutString.afterFirst( s1, 'c', true ) );
		MOut.print( CutString.afterFirst( s2, 'c', true ) );
		MOut.print( CutString.afterLast( s1, 'c', true ) );
		MOut.print( CutString.afterLast( s2, 'c', true ) );
	}

}
