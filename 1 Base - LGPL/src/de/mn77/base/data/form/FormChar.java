/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.form;

import de.mn77.base.data.util.Lib_Unicode;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 11.09.2021
 */
public class FormChar {

	/**
	 * @apiNote: This function put a backslash before EOF and other backslashes.
	 */
	public static String escapeSlashEOF( final char c ) {

		switch( c ) {
			case '\0': // EOF
				return "\\0";
			case '\\':
				return "\\\\";
			default:
				return "" + c;
		}
	}

	/**
	 * @apiNote: Convert a single char to backslash-representation
	 */
	public static String escapeSpecialChar( final char c ) {
		if( c > 255 )
			return Lib_Unicode.charToUnicode( c );
		else
			switch( c ) {
				// Also possible: 1,2,3,4,5,6,7,13,16,17,20,21,22,23,24,25,26,27,30,31 | 32,...
				// Also: \\s

				case '\0': // EOF
					return "\\0";
				case '\n':
					return "\\n";
				case '\t':
					return "\\t";
				case '\r':
					return "\\r";
				case '\f':
					return "\\f";
				case '\b':
					return "\\b";
				case '\\':
					return "\\\\";
//				case '\'':
//					return "\\'";
//				case '"':
//					return "\\\"";
				default:
					if( c < 32 )
						return Lib_Unicode.charToUnicode( c );
//						return "\\u" + FormNumber.width( 4, NumSys_Hex.toHex( c ), false );
					else
						return "" + c;
//						return Character.toString(c);
			}
	}

	/**
	 * @apiNote: This function escapes some chars, to prevent sql injections
	 */
	public static String escapeSqlSecure( final char c, final boolean quotes, final boolean doubleQuotes ) {
		if( !quotes && doubleQuotes )
			Err.invalid( "Use 'quotes' = true for doubleQuotes!" );

		switch( c ) {
//				case '^':	// Is this useful?
			case '\0':
				return "\\0";
			case '\n':
				return "\\n";
			case '\r':
				return "\\r";
			case '\\':
				return "\\\\";
			case '\u001A':
				return "\\u001A";
			default:
				if( quotes )
					switch( c ) {
						case '\'':
						case '"':
							return doubleQuotes ? "" + c + c : "\\" + c;
					}
				return "" + c;
		}
	}

	public static void main( final String[] args ) {
		final String v = "Foo\tb'a\"r\nb\3a\5\u0005k\0p";
		final String s = FormString.quote( v, '"', '\\', true );
//		String s = FormString.quote(v, '\'', '\\', false);
		MOut.print( s );

//		MOut.print(quoteSpecialChar);
	}

	public static int unescapeSpecialChars( final String s, final int index, final StringBuilder sb ) {

		// Regular char
		if( s.charAt( index ) != '\\' || index == s.length() - 1 ) {
			sb.append( s.charAt( index ) );
			return 1;
		}

		// Escaped char
		final char c = s.charAt( index + 1 );

		switch( c ) {
			case '0':
				sb.append( '\0' );
				return 2;
			case 'n':
				sb.append( '\n' );
				return 2;
			case 't':
				sb.append( '\t' );
				return 2;
			case 'r':
				sb.append( '\r' );
				return 2;
			case 'f':
				sb.append( '\f' );
				return 2;
			case 'b':
				sb.append( '\b' );
				return 2;

			case '\\':
				sb.append( '\\' );
				return 2;

			case '\'':
				sb.append( '\'' );
				return 2;
			case '\"':
				sb.append( '"' );
				return 2;

			case 'u':
				if( index + 5 > s.length() - 1 )
					throw new Err_Runtime( "Invalid unicode sentence", s );
				sb.append( Lib_Unicode.parseUnicodeToChar( s.substring( index, index + 6 ) ) );
				return 6;

			default:
//				throw new Err_Runtime("Unknown escape sequence", s.substring(index));
//				MOut.line("Unknown escape sequence", s.substring(index));
				sb.append( s.charAt( index ) );
				return 1;
		}
	}

}
