/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.form;

import de.mn77.base.data.constant.ALIGN;
import de.mn77.base.data.constant.DATE;
import de.mn77.base.data.constant.LANGUAGE;
import de.mn77.base.data.datetime.I_Date;
import de.mn77.base.data.datetime.I_DateTime;
import de.mn77.base.data.datetime.I_Time;
import de.mn77.base.data.datetime.Lib_DateTime;
import de.mn77.base.data.datetime.format.FORM_DATE;
import de.mn77.base.data.datetime.format.FORM_DATETIME;
import de.mn77.base.data.datetime.format.FORM_TIME;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class FormDateTime {

	// --- Time ---

	public static String compose( final I_Date date, final LANGUAGE lang, final Object... format ) {
		Err.ifNull( format );
		Err.ifTooSmall( 1, format.length );
		final StringBuilder sb = new StringBuilder();
		for( final Object o : format )
			if( o instanceof String )
				sb.append( o );
			else if( o == FORM_DATE.YEAR_4 )
				sb.append( FormNumber.width( 4, date.getYear(), false ) );
			else if( o == FORM_DATE.YEAR_2 ) {
				final String tmp = "" + date.getYear();
				if( tmp.length() >= 2 )
					sb.append( tmp.substring( tmp.length() - 2, tmp.length() ) );
				else
					sb.append( FormString.width( tmp, 2, '0', ALIGN.RIGHT, false ) );
			}
			else if( o == FORM_DATE.YEAR )
				sb.append( "" + date.getYear() );
			else if( o == FORM_DATE.MONTH )
				sb.append( date.getMonth() );
			else if( o == FORM_DATE.MONTH_2 )
				sb.append( "" + FormNumber.width( 2, date.getMonth(), false ) );
			else if( o == FORM_DATE.MONTH_TEXT )
				sb.append( DATE.NAME_MONTH_DE[date.getMonth() - 1] );
			else if( o == FORM_DATE.MONTH_TEXT_3 )
				sb.append( DATE.NAME_MONTH_DE[date.getMonth() - 1].substring( 0, 3 ) );
			else if( o == FORM_DATE.DAY )
				sb.append( date.getDay() );
			else if( o == FORM_DATE.DAY_2 )
				sb.append( FormNumber.width( 2, date.getDay(), false ) );
			else if( o == FORM_DATE._DAYS_OF_MONTH )
				sb.append( FormNumber.width( 2, Lib_DateTime.daysOfMonth( date.getMonth(), date.getYear() ), false ) );
			else if( o == FORM_DATE.DAY_WEEK_TEXT_3 )
				sb.append( Lib_DateTime.nameDayOfWeek( date.getDayOfWeek(), lang ).substring( 0, 3 ) );
			else
				Err.invalid( o );
		return sb.toString();
	}

	// --- Date ---

	public static String compose( final I_Date date, final Object... format ) {
		return FormDateTime.compose( date, LANGUAGE.getDefault(), format );
	}

	public static String compose( final I_DateTime time, final Object... format ) {
		Err.ifNull( time );
		if( format == null || format.length == 0 )
			return FormDateTime.compose( time );

		final StringBuilder sb = new StringBuilder();
		for( final Object o : format )
			if( o instanceof String )
				sb.append( o );
			else if( o instanceof FORM_DATE )
				sb.append( FormDateTime.compose( time.getDate(), LANGUAGE.EN, o ) ); //TODO Lang als Parameter?
			else if( o instanceof FORM_TIME )
				sb.append( FormDateTime.compose( time.getTime(), o ) );
			else
				Err.invalid( o );
		return sb.toString();
	}

	public static String compose( final I_Time time, final Object... format ) {
		Err.ifNull( time );
		final StringBuilder sb = new StringBuilder();

		for( final Object o : format )
			if( o instanceof String )
				sb.append( o );
			else
				switch( (FORM_TIME)o ) {
					case STD_2:
						sb.append( FormNumber.width( 2, time.getHours(), false ) );
						break;
					case STD:
						sb.append( time.getHours() );
						break;
					case MIN_2:
						sb.append( FormNumber.width( 2, time.getMinutes(), false ) );
						break;
					case MIN:
						sb.append( time.getMinutes() );
						break;
					case SEK_2:
						sb.append( FormNumber.width( 2, time.getSeconds(), false ) );
						break;
					case SEK:
						sb.append( time.getSeconds() );
						break;
					case MSEK:
						sb.append( time.getMilliseconds() );
						break;
					case MSEK_3:
						sb.append( FormNumber.width( 3, time.getMilliseconds(), false ) );
						break;
				}

		return sb.toString();
	}

	public static String composeDE( final I_Date date ) {
		return FormDateTime.compose( date, LANGUAGE.DE, FORM_DATE.GROUP_DE );
	}

	public static String composeDE( final I_DateTime datetime ) {
		return FormDateTime.compose( datetime, FORM_DATETIME.GROUP_DE );
	}

	// --- DateTime ---

	public static String composeFileSysSlim( final I_DateTime datetime ) {
		return FormDateTime.compose( datetime, FORM_DATETIME.GROUP_FILESYS_SLIM );
	}

	public static String composeFileSysWide( final I_DateTime datetime ) {
		return FormDateTime.compose( datetime, FORM_DATETIME.GROUP_FILESYS );
	}

	public static String composeFull( final I_DateTime datetime ) {
		return FormDateTime.compose( datetime, FORM_DATETIME.GROUP_DEFAULT_FULL );
	}

	public static String composeISO( final I_Date date ) {
		return FormDateTime.compose( date, LANGUAGE.EN, FORM_DATE.GROUP_ISO );
	}

	public static String composeShort( final I_DateTime datetime ) {
		return FormDateTime.compose( datetime, FORM_DATETIME.GROUP_DEFAULT );
	}

	public static String composeSlim( final I_Date date ) {
		return FormDateTime.compose( date, LANGUAGE.EN, FORM_DATE.GROUP_SHORT );
	}

}
