/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.form;

import de.mn77.base.data.constant.ALIGN;
import de.mn77.base.data.util.Lib_Math;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class FormNumber {

	public static String right( final int fieldwidth, final long number ) {
		return FormString.width( "" + number, fieldwidth, ' ', ALIGN.RIGHT, false );
	}

	public static String round( final double d, final int digits ) {
		Err.ifOutOfBounds( 0, 16, digits );
		final double multi = digits == 0
			? 1
			: Lib_Math.power( 10, digits );
		final double rounded = Math.round( d * multi ) / multi;
		String result = "" + rounded;
		final int idx = result.indexOf( '.' );
		final int len = result.length();
		int dec = len - idx - 1;

		for( ; dec < digits; dec++ )
			result += "0";

		return result;
	}

	/**
	 * "width" means only numbers! The minus-char is excluded
	 */
	public static String width( final int width, final long number, final boolean cutOverflow ) {
		return FormNumber.width( width, "" + number, cutOverflow );
	}

	/**
	 * "width" means only numbers! The minus-char is excluded
	 */
	public static String width( int width, final String number, final boolean cutOverflow ) {
		final boolean negative = number.charAt( 0 ) == '-';
		final String s = negative
			? number.substring( 1 )
			: number;
		if( negative )
			width--;
		if( width <= s.length() )
			if( !cutOverflow )
				return "" + number;
			else
				throw Err.todo( width, number, cutOverflow );
		return (negative
			? "-"
			: "") + FormString.width( s, width, '0', ALIGN.RIGHT, false );
	}

}
