/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.form;

import de.mn77.base.data.constant.ALIGN;
import de.mn77.base.data.constant.CHARS;
import de.mn77.base.data.filter.CutString;
import de.mn77.base.data.filter.FilterString;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.data.util.Lib_Unicode;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 25.11.2010
 */
public class FormString {

	public static String escapeSlashes( final String s ) {
		Err.ifNull( s );
		final StringBuilder sb = new StringBuilder( s.length() );
		for( final char c : s.toCharArray() )
			sb.append( FormChar.escapeSlashEOF( c ) );
		return sb.toString();
	}

	/**
	 * @apiNote Replaces every special char. \n --> \\n
	 */
	public static String escapeSpecialChars( final String s, final boolean singleQuote, final boolean doubleQuote ) {
		Err.ifNull( s );
		final StringBuilder sb = new StringBuilder( (int)(s.length() * 1.1) );

		for( final char c : s.toCharArray() )
			switch( c ) {
				case '\'':
					sb.append( singleQuote ? "\\'" : c );
					break;
				case '\"':
					sb.append( doubleQuote ? "\\\"" : c );
					break;

				default:
					sb.append( FormChar.escapeSpecialChar( c ) ); // Bugfix: Use only one function for all
			}

		return sb.toString();
	}

	/**
	 * @apiNote To prevent SQL-Injections, add a backslash before some chars: NUL (ASCII 0), \n, \r, \, 0x1a (Control-Z, ASCII 26)
	 * @param quotes:
	 *            Also escape single and double quotes ('")
	 * @param doubleQuotes:
	 *            When using 'quotes', duplicate quotes insted of adding a backslash before.
	 */
	public static String escapeSqlSecureChars( final String s, final boolean quotes, final boolean doubleQuotes ) {
		Err.ifNull( s );
		final StringBuilder sb = new StringBuilder( s.length() );
		for( final char c : s.toCharArray() )
			sb.append( FormChar.escapeSqlSecure( c, quotes, doubleQuotes ) );
		return sb.toString();
	}

	/**
	 * Ersetzt Platzhalter in einem Template durch Werte.
	 * %a > werte[0]
	 * %b > werte[1]
	 * ...
	 *
	 * Nicht vergleichbar mit String.format, da hier ein Platzhalter auch mehrfach verwendet werden kann!
	 */
	public static String fill( String template, final String... values ) {
		Err.ifNull( template, values );
		for( final String s : values )
			Err.ifNull( s );
		Err.ifToBig( 27, values.length );

		for( int i = 0; i < values.length; i++ ) {
			final String suche = "%" + CHARS.ABC_LOWER.charAt( i );
			template = template.replaceAll( suche, values[i] );
		}

		return template;
	}

	/**
	 * @apiNote This is the same as width, but positive length will align the text to the left, negative lenght is align to the right
	 */
	public static String length( final int length, final String s, final boolean cutOverflow ) {
		return length >= 0
			? FormString.width( s, length, ' ', ALIGN.LEFT, cutOverflow )
			: FormString.width( s, Math.abs( length ), ' ', ALIGN.RIGHT, cutOverflow );
	}

	/**
	 * Wenn der Text länger als der angegebene Wert ist, wird er so gekürzt
	 * und Abschluss angehängt, dass er genau "laengeMax" Zeichen hat.
	 * Ist der Text kürzer oder gleich "laengeMax", wird "abschluss" ignoriert!
	 *
	 * Beispiel:
	 * "testtesttest" , 10, "..." > "testtes..."
	 * "testtest" , 10, "..." > "testtest"
	 */
	public static String limit( final String s, final int maxLength, final String cutSuffix ) {
		Err.ifTooSmall( cutSuffix.length(), maxLength );
		return s.length() <= maxLength
			? s
			: CutString.relCut( s, maxLength - cutSuffix.length(), true ) + cutSuffix;
	}

	/**
	 * @apiNote
	 *          Surround a string with quote.
	 *          If quote is inside the string, insideprefix will be placed before.
	 *          If escape is true, a backslash will be placed before special chars.
	 *          If escapeSpecialChars is false, a backslash will be placed only before backslashes and a EOF (0).
	 *
	 * @implNote
	 *           Important: Quoting backslash would be genererally not necessary at quoting with different char than backslash. But quoting 0 is sensefull, so backslashes must also quoted.
	 */
	public static String quote( final String value, final char quote, final char insideprefix, final boolean escapeSpecialChars ) {
		Err.ifNull( value );
		final StringBuilder sb = new StringBuilder();
		sb.append( quote );

		for( final char c : value.toCharArray() )
			if( c == quote ) {
				sb.append( insideprefix );
				sb.append( quote );
			}
			else
				sb.append( escapeSpecialChars ? FormChar.escapeSpecialChar( c ) : FormChar.escapeSlashEOF( c ) );

		sb.append( quote );
		return sb.toString();
	}

	/**
	 * Surround the string with single quotes and duplicate quotes inside the text.
	 * Special chars will not be escaped, except backslash and 0!
	 */
	public static String quoteForSQL( final String s ) {
		Err.ifNull( s ); // no null here, it should be used "IS NULL" and "= NULL"!
		return FormString.quote( s, '\'', '\'', false );
	}

	public static String quoteSpecialChars( final String value ) {
		final StringBuilder sb = new StringBuilder();
		for( final char c : value.toCharArray() )
			sb.append( FormChar.escapeSpecialChar( c ) );
		return sb.toString();
	}

	public static String toFilename( final String s ) {
		Err.ifNull( s );
		final String allowed = CHARS.GERMAN_LOWER.toUpperCase() + CHARS.GERMAN_LOWER.toLowerCase() + CHARS.NUMBERS + "_- #.,'(){}[]&"; //TODO Erweitern!!!
		return FilterString.only( allowed.toCharArray(), s );
	}

	public static String toSQL_Identifier( final String s ) {
		Err.ifNull( s );
		// Nicht nur uppercase, MySql ist z.B. case-sensitive!
		return FilterString.only( (CHARS.ABC_LOWER + CHARS.ABC_LOWER.toUpperCase() + "_").toCharArray(), s );
	}

	public static String unescapeSpecialChars( final String s ) { // , final boolean singleQuote, final boolean doubleQuote
		Err.ifNull( s );
		final StringBuilder sb = new StringBuilder( (int)(s.length() * 1.1) );
		final int len = s.length();

		for( int i = 0; i < len; i++ )
			i += FormChar.unescapeSpecialChars( s, i, sb ) - 1;

		return sb.toString();
	}

	/**
	 * @apiNote String must be trimmed! If no quote is found, nothing will be changed
	 */
	public static String unquote( String value, final char quote, final char insideprefix ) {
		Err.ifNull( value );

		int len = value.length();
		int left = 0;
		int right = len;

		if( len > 0 && value.charAt( 0 ) == quote )
			left = 1;
		else
			return value;

		if( len > 1 && value.charAt( len - 1 ) == quote )
			right -= 1;

		value = value.substring( left, right );
		len = value.length();

		final StringBuilder sb = new StringBuilder();

		for( int i = 0; i < len; i++ ) {
			final char c = value.charAt( i );

			// Remove insideprefix quotes
			if( c == insideprefix ) {
				final String check = value.substring( i, Math.min( len, i + 2 ) );

				if( check.equals( "" + insideprefix + quote ) ) {
					sb.append( quote );
					i++;
					continue;
				}
			}

//			if(removeSlashes)
			i += FormChar.unescapeSpecialChars( value, i, sb ) - 1;
//			else
//				sb.append(c);
		}

		return sb.toString();
	}

	/**
	 * @apiNote Cut or fill and align a string to the given width.
	 * @implNote Supports full unicode
	 */
	public static String width( String t, final int width, final char fill, final ALIGN align, final boolean cutOverflow ) {
		Err.ifTooSmall( 0, width );
		Err.ifNull( t, align );

		final int space = width - Lib_Unicode.length( t );	// t.length()

		if( align == ALIGN.CENTER )
			if( space >= 0 )
				t = Lib_String.sequence( fill, (int)Math.floor( space / 2d ) ) + t + Lib_String.sequence( fill, (int)Math.ceil( space / 2d ) );
			else
				t = cutOverflow
					? t.substring( 0, t.length() + space )
					: t;
		if( align == ALIGN.LEFT )
			if( space >= 0 )
				t += Lib_String.sequence( fill, space );
			else
				t = cutOverflow
					? t.substring( 0, t.length() + space )
					: t;
		if( align == ALIGN.RIGHT )
			if( space >= 0 )
				t = Lib_String.sequence( fill, space ) + t;
			else
				t = cutOverflow
					? t.substring( -space, t.length() )
					: t;
		return t;
	}

	/**
	 * @deprecated: siehe length(....
	 */
	public static String width( final int width, final String s, final boolean cutOverflow ) {
		return FormString.width( s, width, ' ', ALIGN.LEFT, cutOverflow );
	}

}
