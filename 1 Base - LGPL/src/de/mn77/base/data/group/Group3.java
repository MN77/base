/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.group;

import de.mn77.base.data.convert.ConvertObject;


/**
 * @author Michael Nitsche
 */
public class Group3<TA, TB, TC> {

	public final TA o1;
	public final TB o2;
	public final TC o3;


	public Group3( final TA o1, final TB o2, final TC o3 ) {
		this.o1 = o1;
		this.o2 = o2;
		this.o3 = o3;
	}


	@Override
	public String toString() {
		return "Group3(" + ConvertObject.toStringOutput( this.o1 ) + "," + ConvertObject.toStringOutput( this.o2 ) + "," + ConvertObject.toStringOutput( this.o3 ) + ")";
	}

}
