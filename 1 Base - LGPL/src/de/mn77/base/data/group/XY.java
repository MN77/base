/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.group;

/**
 * @author Michael Nitsche
 * @created 20.10.2022
 */
public class XY {

	public final int x;
	public final int y;


	public XY() {
		this( 0, 0 );
	}

	public XY( final int x, final int y ) {
		this.x = x;
		this.y = y;
	}


	@Override
	public boolean equals( final Object o ) {
		if( o instanceof final XY other )
			return other.x == this.x && other.y == this.y;

		return false;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append( this.getClass().getSimpleName() );
		sb.append( '<' );
		sb.append( this.x );
		sb.append( ',' );
		sb.append( this.y );
		sb.append( '>' );
		return sb.toString();
	}

	public int x() {
		return this.x;
	}

	public int y() {
		return this.y;
	}

}
