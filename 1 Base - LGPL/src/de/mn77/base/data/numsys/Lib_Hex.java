/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.numsys;

import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 15.08.2022
 *
 * @apiNote Some functions to deal with the HEX number system.
 */
public class Lib_Hex {

	/**
	 * @apiNote Fast conversion from bytes to a Hex-Uppercase-String
	 */
	public static String bytesToHexHighString( final byte[] data ) {
		return Lib_Hex.iBytesToHexString( NumSys_Hex.charsHigh, data );
	}

	/**
	 * @apiNote Fast conversion from bytes to a Hex-Uppercase-String
	 */
	public static String bytesToHexLowString( final byte[] data ) {
		return Lib_Hex.iBytesToHexString( NumSys_Hex.charsLow, data );
	}

	/**
	 * @apiNote Fast conversion from bytes to a Hex-Uppercase-String
	 */
	public static String bytesToHexString( final byte[] data ) {
		return Lib_Hex.bytesToHexHighString( data );
	}

	/**
	 * @apiNote Fast conversion from a Hex-Uppercase-String (0-9A-F) to bytes
	 */
	public static byte[] hexHighStringToBytes( final String data ) {
		return Lib_Hex.iHexStringToBytes( 65, data );
	}

	/**
	 * @apiNote Fast conversion from a Hex-Lowercase-String (0-9a-f) to bytes
	 */
	public static byte[] hexLowStringToBytes( final String data ) {
		return Lib_Hex.iHexStringToBytes( 97, data );
	}

	/**
	 * @apiNote Fast conversion from a Hex-String to bytes
	 */
	public static byte[] hexStringToBytes( final String data ) {
		return Lib_Hex.iHexStringToBytes( 65, data.toUpperCase() );
	}

	public static void main( final String[] args ) {
		final byte[] data = "Entwickeln Sie 𝄞 mit V€rgnügen".getBytes();
//		byte[] data = "Hallo Welt!".getBytes();

		final String s = Lib_Hex.bytesToHexHighString( data );
		MOut.print( s );
		MOut.echo( NumSys_Hex.toHex( data[0] ) );
		MOut.print( NumSys_Hex.toHex( data[1] ) );

		final byte[] ba = Lib_Hex.hexHighStringToBytes( s );
		final String s2 = new String( ba );
		MOut.print( s2 );
	}

	public static int hexToInt( final String hex ) {
//		return Integer.parseInt(hex, 16);

		int multi = 1;
		int sum = 0;

		for( int i = hex.length() - 1; i >= 0; i-- ) {
			final char c = hex.charAt( i );
			if( c >= '0' && c <= '9' )
				sum += (c - 48) * multi;
			else if( c == 'a' || c == 'A' )
				sum += 10 * multi;
			else if( c == 'b' || c == 'B' )
				sum += 11 * multi;
			else if( c == 'c' || c == 'C' )
				sum += 12 * multi;
			else if( c == 'd' || c == 'D' )
				sum += 13 * multi;
			else if( c == 'e' || c == 'E' )
				sum += 14 * multi;
			else if( c == 'f' || c == 'F' )
				sum += 15 * multi;
			else
				throw new NumberFormatException( "Malformed hex number: " + hex );

			multi *= 16;
		}

		return sum;
	}

	private static String iBytesToHexString( final char[] chars, final byte[] data ) {
		final StringBuilder sb = new StringBuilder( data.length * 2 );

		for( final byte b : data )
			if( b < 0 ) {
				sb.append( chars[b + 256 >> 4] );
				sb.append( chars[(b + 256 | 240) ^ 240] );
			}
			else {
				sb.append( chars[b >> 4] );
				sb.append( chars[(b | 240) ^ 240] );
			}

		return sb.toString();
	}

	private static byte[] iHexStringToBytes( final int letterA, final String data ) {
		final byte[] result = new byte[data.length() / 2];
		int pointer = 0;

		for( int i = 0; i < data.length(); i += 2 ) {
			byte b = (byte)0;
			final char c0 = data.charAt( i );
			final char c1 = data.charAt( i + 1 );

			if( c0 >= '0' && c0 <= '9' )
				b += c0 - 48;
			else
				b += c0 - letterA + 10;
			b = (byte)(b << 4);

			if( c1 >= '0' && c1 <= '9' )
				b += c1 - 48;
			else
				b += c1 - letterA + 10;

			result[pointer] = b;
			pointer++;
		}

		return result;
	}

}
