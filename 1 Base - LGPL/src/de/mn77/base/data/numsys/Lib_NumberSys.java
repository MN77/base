/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.numsys;

import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 * @implNote
 *           zeichen als String speichern und mit indexof auswerten!
 */
public class Lib_NumberSys {

	public static int decode( final char[] chars, final String s ) {
		int result = 0;
		char c;
		int level = 1;

		for( int i = s.length() - 1; i >= 0; i-- ) {
			c = s.charAt( i );
			if( Lib_NumberSys.iIndex( chars, c ) < 0 )
				throw new IllegalArgumentException( "Unknown char: " + c + " in " + chars.toString() );
			result = result + Lib_NumberSys.iIndex( chars, c ) * level;
			level = level * chars.length;
		}

		return result;
	}

	public static String encode( final char[] chars, int i ) {
		if( i < 0 )
			throw new Err_Runtime( "Value is to small", "Value: " + i, "Min: 0" );

		String result = "";
		int rem;

		while( i > 0 ) {
			rem = i % chars.length;
			result = chars[rem] + result;
			i = (i - rem) / chars.length;
		}

		return result.equals( "" )
			? "" + chars[0]
			: result;
	}

	public static String encode( final char[] chars, long l ) {
		if( l < 0 )
			throw new Err_Runtime( "Value is to small", "Value: " + l, "Min: 0" );

		String result = "";
		int rem;

		while( l > 0 ) {
			rem = (int)(l % chars.length);
			result = chars[rem] + result;
			l = (l - rem) / chars.length;
		}

		return result.equals( "" )
			? "" + chars[0]
			: result;
	}

	private static int iIndex( final char[] chars, final char search ) {
		int result = 0;
		while( result < chars.length && chars[result] != search )
			result++;
		if( result < chars.length && chars[result] != search )
			result = -1;
		return result;
	}

}
