/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.numsys;

/**
 * @author Michael Nitsche
 * @created 09.09.2022
 */
public class NumSys_Base64 {

	public static final char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();


	public static int decode( final String s ) {
		return Lib_NumberSys.decode( NumSys_Base64.chars, s );
	}

	public static String encode( final int i ) {
		return Lib_NumberSys.encode( NumSys_Base64.chars, i );
	}

	public static String encode( final long l ) {
		return Lib_NumberSys.encode( NumSys_Base64.chars, l );
	}

}
