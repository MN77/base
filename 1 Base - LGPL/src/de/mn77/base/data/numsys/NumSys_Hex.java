/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.numsys;

/**
 * @author Michael Nitsche
 */
public class NumSys_Hex {

	public static final char[] charsHigh = "0123456789ABCDEF".toCharArray();
	public static final char[] charsLow  = "0123456789abcdef".toCharArray();


	public static int fromHex( final String s ) {
		return Lib_NumberSys.decode( NumSys_Hex.charsHigh, s.toUpperCase() );
	}

	public static int fromHexHigh( final String s ) {
		return Lib_NumberSys.decode( NumSys_Hex.charsHigh, s );
	}

	public static int fromHexLow( final String s ) {
		return Lib_NumberSys.decode( NumSys_Hex.charsLow, s );
	}

	/**
	 * Test
	 */
	public static void main( final String[] args ) {
		String h;

		for( int i = 0; i < 256; i += 9 ) {
			h = NumSys_Hex.toHex( i );
			System.out.println( i + " : " + h + " : " + NumSys_Hex.fromHex( h ) );
		}
	}

	public static String toHex( final int i ) {
		return NumSys_Hex.toHexHigh( i );
	}

	public static String toHexHigh( final int i ) {
		return Lib_NumberSys.encode( NumSys_Hex.charsHigh, i );
	}

	public static String toHexLow( final int i ) {
		return Lib_NumberSys.encode( NumSys_Hex.charsLow, i );
	}

}
