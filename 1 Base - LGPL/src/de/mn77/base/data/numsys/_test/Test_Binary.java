/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Specific-Library <https://www.mn77.de>.
 *
 * MN77-Specific-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.numsys._test;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import de.mn77.base.data.numsys.NumSys_Binary;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 23.07.2021
 */
public class Test_Binary {

	public static void main( final String[] args ) {
//		MOut.print( Binary.toBin( 55348 ) );
//		MOut.print( Binary.toBin( 56606 ) );
//		MOut.print( Binary.toBin( 128514 ) );

		MOut.print( NumSys_Binary.toBin( 53534 ) );


		final String rawString = "Entwickeln Sie 𝄞 mit V€rgnügen";

		final byte[] bytes = rawString.getBytes( StandardCharsets.UTF_8 );
		final String utf8EncodedString1 = new String( bytes, StandardCharsets.UTF_8 );


		final ByteBuffer buffer = StandardCharsets.UTF_8.encode( rawString );
		final String utf8EncodedString2 = StandardCharsets.UTF_8.decode( buffer ).toString();


		MOut.temp( rawString, utf8EncodedString1, utf8EncodedString2 );

		final byte[] bytes16 = rawString.getBytes( StandardCharsets.UTF_16 );
		final String utf16EncodedString = new String( bytes16, StandardCharsets.UTF_16 );

		MOut.temp( utf16EncodedString );

		final byte[] ba = { 0, 1, -47, 30 }; //209
		final String utf16EncodedString2 = new String( ba, StandardCharsets.UTF_16 );

		MOut.temp( utf16EncodedString2 );


//		String h;
//		for(int i = 0; i < 256; i++) {
//			h = Binary.toBin(i);
//			System.out.println(Binary.fromBin(h) + " : " + h);
//		}
	}

}
