/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.search;

/**
 * @author Michael Nitsche
 * @created 25.05.2019
 */
public class SearchArray {

	public static boolean contains( final int[] sa, final int search ) {
		for( final int t : sa )
			if( t == search )
				return true;
		return false;
	}

	public static boolean contains( final String[] sa, final String search, final boolean case_sensitiv ) {
		return SearchArray.indexOf( sa, search, case_sensitiv ) != null;
	}

	public static boolean contains(final char[] data, final char c ) {
		for( final char s : data )
			if( s == c )
				return true;
		return false;
	}

	public static <T> boolean contains( final T[] array, final T search ) {
		for( final T t : array )
//			if(Lib_Compare.istGleich(t, das)) return true; //TODO testen
			if( t != null && search != null && t.equals( search ) || t == null && search == null )
				return true;
		return false;
	}

	/**
	 * @return true if all Objects in 'search' are within 'pool'
	 */
	public static <T> boolean containsAll( final T[] pool, final T[] search ) {
		for( final T s : search )
			if( !SearchArray.contains( pool, s ) )
				return false;
		return true;
	}

	/**
	 * @return true if one Object in 'search' is within 'pool'
	 */
	public static <T> boolean containsAny( final T[] pool, final T[] search ) {
		for( final T s : search )
			if( SearchArray.contains( pool, s ) )
				return true;
		return false;
	}

	public static Integer indexOf( final String[] sa, String search, final boolean case_sensitiv ) {

		if( case_sensitiv ) {
			for( int i = 0; i < sa.length; i++ )
				if( sa[i].equals( search ) )
					return i;
		}
		else {
			search = search.toLowerCase();
			for( int i = 0; i < sa.length; i++ )
				if( sa[i].toLowerCase().equals( search ) )
					return i;
		}

		return null;
	}

}
