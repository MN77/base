/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.search;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.mn77.base.data.ComplexUnicodeChar;
import de.mn77.base.data.struct.atomic.IntList;
import de.mn77.base.data.struct.table.type.TypeTable2;
import de.mn77.base.data.struct.table.type.TypeTable3;
import de.mn77.base.data.util.Lib_Unicode;


/**
 * @author Michael Nitsche
 * @created 07.12.2010
 */
public class SearchString {

	public static int countChar( final String s, final char c ) {
		int result = 0;
		for( final char sc : s.toCharArray() )
			if( sc == c )
				result++;
		return result;
	}

	public static int countChar( final String s, final ComplexUnicodeChar c ) {
		int result = 0;
		for( final ComplexUnicodeChar cuc : Lib_Unicode.toComplexCharList( s ) )
			if( cuc.equals( c ) )
				result++;
		return result;
	}

	public static IntList indexes( final String text, final String search ) {
		final IntList result = new IntList();
		int index = -1;
		while( (index = text.indexOf( search, index + 1 )) > -1 )
			result.add( index );
		return result;
	}

	/**
	 * Sucht z.B. nach HTML-Tags! Achtung, die Such-Strings dürfen sich nicht überschneiden!
	 */
	public static TypeTable2<Integer, String> indexesMulti( final String text, final String... search ) {
		final TypeTable2<Integer, String> result = new TypeTable2<>( Integer.class, String.class );

		for( final String s : search ) {
			final IntList indexes = SearchString.indexes( text, s );
			for( final Integer i : indexes )
				result.add( i, s );
		}

		result.sort( 1, 2 );
		return result;
	}

	public static int lastIndexOf( final char c, final String s ) {
		for( int i = s.length() - 1; i >= 0; i-- )
			if( s.charAt( i ) == c )
				return i;
		return -1;
	}

	/**
	 * @apiNote Splits the search string by whitespaces to words. Returns true if 'str' contains all words.
	 *          This function is case insensitive
	 *          To split a string to Words, look at ConvString.toSearchWords
	 */
	public static boolean matchSearch( String str, final List<String> words ) {
		str = str.toLowerCase();
		for( final String word : words )
			if( !str.contains( word ) )
				return false;
		return true;
	}

	/**
	 * @apiNote Wie indexof char ... nur für mehrere char's
	 * @return Index of the first match
	 */
	public static int search( final char[] chars, final String s, final int offset ) {
		int result = -1;

		for( final char c : chars ) {
			final int idx = s.indexOf( c, offset );
			if( idx > -1 )
				if( result == -1 || result > idx )
					result = idx;
		}

//		MOut.dev("Suche.first: ",chars,s,fromPos,res+1);
		return result;
	}

	/**
	 * @return TypTable3<Start,End,Textpart>
	 */
	public static TypeTable3<Integer, Integer, String> search( final String text, final String regex ) {
		final TypeTable3<Integer, Integer, String> result = new TypeTable3<>( Integer.class, Integer.class, String.class );
		final Pattern p = Pattern.compile( regex );
		final Matcher matcher = p.matcher( text );

		while( matcher.find() )
			result.add( matcher.start(), matcher.end(), matcher.group() );

//		if(matcher.matches()) { //TODO Noch keine Ahnung wofür!
//			int start=matcher.start();
//			int ende =matcher.end();
//            MOut.warnung("m1.matches() start = " + start + " end = " + ende);
//		}
		return result;
	}

}
