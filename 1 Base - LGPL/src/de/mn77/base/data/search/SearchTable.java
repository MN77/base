/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.search;

import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.data.struct.table.type.I_TypeTable;
import de.mn77.base.error.Err;


public class SearchTable {

	public static <T> I_Table<T> filter( final I_Table<T> tab, final int col, String regex, final boolean case_sensitiv ) {
		Err.ifNull( tab, regex );
		Err.ifOutOfBounds( 0, tab.width() - 1, col );
		Err.ifTooSmall( 0, tab.width() - 1 );
		final I_Table<T> result = new ArrayTable<>( tab.width() );
		if( !case_sensitiv )
			regex = "(?i)" + regex;

		for( final T[] ta : tab )
			if( ("" + ta[col]).matches( regex ) )
				result.add( ta );

		return result;
	}

	public static <TG> I_List<TG> filterColumn( final I_TypeTable<TG> tab, final int col, String regex, final boolean case_sensitiv ) {
		Err.ifNull( tab, regex );
		Err.ifOutOfBounds( 0, tab.width() - 1, col );
		Err.ifTooSmall( 1, tab.width() );
		final I_List<TG> result = new SimpleList<>();
		if( !case_sensitiv )
			regex = "(?i)" + regex;

		for( int row = 0; row < tab.size(); row++ ) {
			final Object cell = tab.get( col, row );
			if( cell != null && cell.toString().matches( regex ) )
				result.add( tab.get( row ) );
		}

		return result;
	}

}
