/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct;

import java.util.LinkedList;


/**
 * @author Michael Nitsche
 * @created 2022-10-15
 */
public class BufferQueue<T> {

	private final LinkedList<T> cache;
	private int                 pointer = 0;
	private T                   current = null;


	public BufferQueue() {
		this.cache = new LinkedList<>();
	}

	@SuppressWarnings( "unchecked" )
	public BufferQueue( final T... oa ) {
		this.cache = new LinkedList<>();
		for( final T o : oa )
			this.add( o );
	}


	public void add( final T t ) {
		this.cache.addLast( t );
	}

	public synchronized void clear() {
		this.cache.clear();
		this.reset();
	}

	/**
	 * Return: Last Item from getNext()
	 */
	public synchronized T current() {
		return this.current;
	}

	public boolean isEmpty() {
		if( this.cache.isEmpty() )
			return true;
		return this.pointer >= this.cache.size();
	}

	public int length() {
		return this.cache.size() - this.pointer;
	}

	public synchronized T next() {

		if( this.isEmpty() ) {
			this.current = null;
			return null;
		}

		this.current = this.cache.get( this.pointer );
		this.pointer++;
		return this.current;
	}

	/**
	 * @apiNote Reset pointer and reactivate all available items.
	 */
	public synchronized void reset() {
		this.pointer = 0;
		this.current = null;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();

		for( final T o : this.cache ) {
			if( sb.length() > 0 )
				sb.append( ',' );
			sb.append( o );
		}

		return sb.toString();
	}

}
