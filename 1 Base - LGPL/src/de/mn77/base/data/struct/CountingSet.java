/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct;

import java.util.Collection;
import java.util.Iterator;

import de.mn77.base.data.I_Copyable;
import de.mn77.base.data.I_Describe;
import de.mn77.base.data.constant.ALIGN;
import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.atomic.IntList;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.data.tablestyle.TableStyler;
import de.mn77.base.data.util.Lib_Describe;
import de.mn77.base.data.util.U_RadixSort;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 2024-08-11
 */
public class CountingSet<T> implements I_Describe, I_Iterable<Group2<T, Integer>>, I_Copyable<CountingSet<T>> { // I_Collection<T>, I_Search

	private final SimpleList<T> keys;
	private final IntList       hits;


	public CountingSet() {
		this.keys = new SimpleList<>();
		this.hits = new IntList();
	}

	public CountingSet( final int initSize ) {
		this.keys = new SimpleList<>( initSize );
		this.hits = new IntList( initSize );
	}


	public void clear() {
		this.keys.clear();
		this.hits.clear();
	}

	public boolean contains( final T key ) {
		return this.keys.contains( key );
	}

	public boolean containsAll( final Collection<T> c ) {
		return this.keys.containsAll( c );
	}

	public CountingSet<T> copy() {
		final int size = this.keys.size();
		final CountingSet<T> result = new CountingSet<>( size );
		for( int i = 0; i < size; i++ )
			result.put( this.keys.get( i ), this.hits.get( i ) );

		return result;
	}

	public int dec( final T key ) {
		final int keyIdx = this.keys.indexOf( key );

		if( keyIdx >= 0 ) {
			final int curValue = this.hits.get( keyIdx );

			if( curValue == 0 ) {}
			else {
				final int newValue = curValue - 1;
				this.hits.set( keyIdx, newValue );
				return newValue;
			}
		}
//		else {
//			this.keys.remove( keyIdx );
//			this.hits.remove( keyIdx );
//		}
		return 0;
	}

	public void describe() {
		MOut.print( this.toDescribe() );
	}

	public Group2<T, Integer> get( final int index ) {
		return new Group2<>( this.keys.get( index ), this.hits.get( index ) );
	}

	public int get( final T key ) {
		final int keyIdx = this.keys.indexOf( key );
		return this.hits.get( keyIdx );
	}

	public void inc( final T key ) {
		final int keyIdx = this.keys.indexOf( key );

		if( keyIdx >= 0 ) {
			final int curValue = this.hits.get( keyIdx );
			this.hits.set( keyIdx, curValue + 1 );
		}
		else {
			this.keys.add( key );
			this.hits.add( 1 );
		}
	}

	public void incAll( final Iterable<? extends T> c ) {
		for( final T t : c )
			this.inc( t );
	}

	@SuppressWarnings( "unchecked" )
	public void incMore( final T... ta ) {
		for( final T t : ta )
			this.inc( t );
	}

	public boolean isEmpty() {
		return this.keys.isEmpty();
	}

	public Iterator<Group2<T, Integer>> iterator() {
		return new Iterator<>() {

			int next = 0;


			public boolean hasNext() {
				return this.next < CountingSet.this.keys.size();
			}

			public Group2<T, Integer> next() {
				return CountingSet.this.get( this.next++ );
			}

		};
	}

	public I_Series<T> keys() {
		return this.keys;
	}

	public void put( final T key, final int value ) {
		final int keyIdx = this.keys.indexOf( key );

		if( keyIdx >= 0 )
			this.hits.set( keyIdx, value );
		else {
			this.keys.add( key );
			this.hits.add( value );
		}
	}

	public int remove( final Object key ) {
		final int keyIdx = this.keys.indexOf( key );

		if( keyIdx >= 0 ) {
			final int result = this.hits.get( keyIdx );
			this.keys.removeIndex( keyIdx );
			this.hits.removeIndex( keyIdx );
			return result;
		}
		else
			return 0;
	}

	public void reset() {
		for( int i = 0; i < this.keys.size(); i++ )
			this.hits.set( i, 0 );
	}

	public T selectFirst( final T[] values ) {
		if( values == null || values.length == 0 )
			Err.invalid( "Got empty array" );

		T result = null;
		int amount = -1;

		for( final T value : values ) {
			final int keyIdx = this.keys.indexOf( value );

			if( keyIdx >= 0 ) {
				final int keyHits = this.hits.get( keyIdx );

				if( keyHits > amount ) {
					amount = keyHits;
					result = value;
				}
			}
		}

		return result;
	}

	public int size() {
		return this.keys.size();
	}

	public void sort( final boolean decrement ) {
		final int[] hitsArray = this.hits.toArray();
//		MOut.trace( this.toDescribe() );
		U_RadixSort.sortNumbers( decrement, hitsArray, this.keys );
		this.hits.replace( hitsArray );
//		MOut.trace( this.toDescribe() );
	}

	public String toDescribe() {
		return Lib_Describe.toDescribe( this );
	}

	public String toIdent() {
		return Lib_Describe.toIdent( this, this.keys.size() );
	}

	public T[] toKeyArray( final T[] arr ) {
		return this.keys.toArray( arr );
	}

	@Override
	public String toString() {
		final I_Table<Object> table = new ArrayTable<>( 2 );
		for( final Group2<T, Integer> e : this )
			table.addRow( e.o1, e.o2 );

		final TableStyler styler = new TableStyler();
		styler.alignColumn( 0, ALIGN.LEFT );
		styler.alignColumn( 1, ALIGN.RIGHT );
		styler.setBorder( true );
		return styler.compute( table );
	}

	public int[] values() {
		return this.hits.toArray();
	}

}
