/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct;

import java.util.LinkedList;


/**
 * @author Michael Nitsche
 * @created 2019-05-01
 */
public class HistoryList<T> {

	private final LinkedList<T> list;
	private int                 pointer = 0; // 0=null, Negativ ist in History zurück


	public HistoryList() {
		this.list = new LinkedList<>();
	}


	public void add( final T t ) {
		this.pointer = 0;
		if( t == null || !this.list.isEmpty() && t.equals( this.list.getLast() ) )
			return;
		this.list.addLast( t );
	}

	public T back() {
		if( this.list.size() == 0 )
			return null;
		this.pointer--;
		if( Math.abs( this.pointer ) > this.list.size() )
			this.pointer = 0 - this.list.size();
		return this.list.get( this.list.size() + this.pointer );
	}

	public T forward() {
		this.pointer++;
		if( this.pointer > 0 )
			this.pointer = 0;
		if( this.pointer == 0 )
			return null;
		return this.list.get( this.list.size() + this.pointer );
	}

	public boolean isEmpty() {
		return this.list.isEmpty();
	}

	public int size() {
		return this.list.size();
	}

}
