/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct;

import java.util.Collection;


/**
 * @author Michael Nitsche
 * @created 13.10.2022
 * @apiNote A changeable collection of objects.
 * @implNote List|Set -> Collection -> Sequence -> Series -> Iterable
 * @see I_Sequence
 */
public interface I_Collection<T> extends I_Sequence<T>, Collection<T> {

	I_Collection<T> copy();

	@SuppressWarnings( "unchecked" )
	void addMore( T... ta );

	void ensureCapacity( int wanted );

//	T delete(int index); // Das ist verwirrend: List.remove   List.delete			ist beides das Gleiche oder?

	/**
	 * @return Returns true, if the object was inserted and the List was changed. The result can be false, if the object was already in a Set.
	 * @implNote This is important, to follow the same line with ".add"
	 */
	boolean insert( int index, T object );

	/**
	 * @deprecated Please use I_Collection.removeIndex(int index)
	 */
	@Deprecated
	T remove( final int index );

	T removeFirst();

	T removeIndex( int index ); // Alternative zu "List.remove(int)", was gerne vertauscht wird mit "Collection.remove(Object)"

	T removeLast();

	/**
	 * @deprecated Please use I_Collection.removeOne or .removeEvery
	 */
	@Deprecated
	boolean remove( final Object o );

	/**
	 * @apiNote Removes one (normaly the first) occurance
	 * @return Returns true, if this collection has been changed.
	 */
	boolean removeOne( final Object o );

	/**
	 * @apiNote Set this object to that index. If the object can't be set, an error will be thrown!
	 */
	T set( int index, T object );

}
