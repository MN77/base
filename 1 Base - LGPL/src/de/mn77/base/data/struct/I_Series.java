/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct;

import java.util.Collection;

import de.mn77.base.data.I_Copyable;
import de.mn77.base.data.struct.search.I_Search;


/**
 * @author Michael Nitsche
 * @created 19.11.2022
 * @apiNote A read only, non changable and fixed length but searchable and convertable series of objects.
 * @see I_Iterable
 */
public interface I_Series<TI> extends I_Iterable<TI>, I_Search, I_Copyable<I_Series<TI>> {

	boolean contains( Object o );
	boolean containsAll( Collection<?> c );

	Object[] toArray();
	TI[] toArray( Class<TI> type );
	<TI> TI[] toArray( TI[] a );

}
