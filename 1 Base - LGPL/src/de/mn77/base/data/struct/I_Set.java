/*******************************************************************************
 * Copyright (C) 2023-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct;

import java.util.Set;


/**
 * @author Michael Nitsche
 * @created 12.05.2023
 */
public interface I_Set<T> extends Set<T>, I_Collection<T> {

	boolean addAll( final Iterable<? extends T> c );
	void addMore( final T... ta );
	void addStrict( final T e );
	I_Set<T> copy();
	void ensureCapacity( int wanted );

	void insertStrict( final int index, final T object );

	boolean removeAll( final Iterable<?> c );

}
