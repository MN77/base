/*******************************************************************************
 * Copyright (C) 2006-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import de.mn77.base.data.convert.ConvertObject;
import de.mn77.base.data.form.FormNumber;
import de.mn77.base.data.struct.atomic.IntList;
import de.mn77.base.data.util.Lib_Describe;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 2006-11-21
 * @reworked 2022-10-13
 */
public class PotList<TA> implements I_Collection<I_Iterable<TA>> {

	private SimpleList<SimpleList<TA>> pots;


	public PotList() {
		this.pots = new SimpleList<>();
	}

	public PotList( final int length ) {
		this();
		Err.ifTooSmall( 0, length );

		for( int i = 0; i < length; i++ )
			this.pots.add( new SimpleList<>() );
	}


	public boolean add( final I_Iterable<TA> pot ) {
		return this.pots.add( this.iMakePot( pot ) );
	}

	public boolean add( final Iterable<TA> objects ) {
		return this.pots.add( this.iMakePot( objects ) );
	}

	@SuppressWarnings( "unchecked" )
	public boolean add( final TA... objects ) {
		return this.pots.add( this.iMakePot( objects ) );
	}

	public boolean addAll( final Collection<? extends I_Iterable<TA>> c ) {
		for( final I_Iterable<TA> s : c )
			this.pots.add( this.iMakePot( s ) );
		return true;
	}

	@SuppressWarnings( "unchecked" )
	public void addMore( final I_Iterable<TA>... ta ) {
		for( final I_Iterable<TA> pot : ta )
			this.add( pot );
	}

	public void clear() {
		this.pots.clear();
	}

	public boolean contains( final Object o ) {
		Err.ifNull( o );

		for( final I_Iterable<TA> pot : this.pots )
			for( final TA ta : pot )
				if( o.equals( ta ) )
					return true;
		return false;
	}

	public boolean containsAll( final Collection<?> c ) {
		throw Err.todo();
	}

	public PotList<TA> copy() {
		final PotList<TA> result = new PotList<>();
		result.addAll( this.pots );
		return result;
	}

	public void exchange( final int indexA, final int indexB ) {
		this.pots.exchange( indexA, indexB );
	}

	public I_Iterable<TA> get( final int row ) {
		Err.ifOutOfSize( this.pots, row );
		return this.pots.get( row );
	}

	public boolean insert( final int index, final I_Iterable<TA> pot ) {
		return this.pots.insert( index, this.iMakePot( pot ) );
	}

	public boolean isEmpty() {
		return this.pots.isEmpty();
	}

	public Iterator<I_Iterable<TA>> iterator() {
		return new Iterator<>() {

			private int next = 0;


			public boolean hasNext() {
				return this.next < PotList.this.size();
			}

			public I_Iterable<TA> next() {
				return PotList.this.pots.get( this.next++ );
			}

			public void remove() {
				Err.todo();
			}

		};
	}

	public int maxDepth() {
		int max = 0;
		for( final I_Iterable<TA> l : this.pots )
			max = Math.max( max, l.size() );
		return max;
	}

	public void potAdd( final int row, final TA object ) {
		Err.ifOutOfSize( this.pots, row );
		this.pots.get( row ).add( object );
	}

	public void potAddAll( final int row, final Iterable<TA> objects ) {
		Err.ifOutOfSize( this.pots, row );

		for( final TA t : objects )
			this.pots.get( row ).add( t );
	}

	public boolean potRemove( final int index, final TA object ) {
		return this.pots.get( index ).remove( object );
	}

	@Deprecated // Please use .removeIndex(int index)
	public I_Iterable<TA> remove( int index ) {
		return this.removeIndex( index );
	}

	public boolean remove( final Object o ) {
		throw Err.todo();
	}

	public boolean removeOne( final Object o ) {
		throw Err.todo();
	}

	public boolean removeAll( final Collection<?> c ) {
		throw Err.todo();
	}

	public I_Iterable<TA> removeFirst() {
		return this.pots.removeFirst();
	}

	public I_Iterable<TA> removeIndex( final int index ) {
		return this.pots.removeIndex( index );
	}

	public I_Iterable<TA> removeLast() {
		return this.pots.removeLast();
	}

	public boolean retainAll( final Collection<?> c ) {
		throw Err.todo();
	}

	public void reverse() {
		this.pots.reverse();
	}

	public IntList searchAll( final Object o ) {
		final IntList result = new IntList();

		for( int y = 0; y < this.pots.size(); y++ ) {
			final SimpleList<TA> pot = this.pots.get( y );
			for( final TA element : pot )
				if( element.equals( o ) )
					result.add( y );
		}

		return result;
	}

	public Integer searchFirst( final Object o ) {

		for( int y = 0; y < this.pots.size(); y++ ) {
			final SimpleList<TA> pot = this.pots.get( y );
			for( final TA element : pot )
				if( element.equals( o ) )
					return y;
		}

		return null;
	}

	public I_Iterable<TA> set( final int index, final I_Iterable<TA> pot ) {
		return this.pots.set( index, this.iMakePot( pot ) );
	}

	public int size() {
		return this.pots.size();
	}

	public void sort() {
//		this.pots.sort();
		throw Err.invalid( "This PotList can't be sorted!" );
	}

	public void sortLike( final int[] newOrder ) {
		this.pots.sortLike( newOrder );
	}

//	@SuppressWarnings("unchecked")
//	public void put(final int row, final TA... objects) {
//		while(row > this.pots.size()) {
//			final SimpleList<TA> tl = new SimpleList<>();
//			this.pots.add(tl);
//		}
//
//		for(final TA t : objects)
//			this.pots.get(row).add(t);
//	}

	public void sortRandom() {
		this.pots.sortRandom();
	}

	public Object[] toArray() {
		return this.pots.toArray();
	}

	public I_Iterable<TA>[] toArray( final Class<I_Iterable<TA>> clazz ) {
//		return this.pots.toArray(clazz);
		throw Err.todo();
	}

	public <T> T[] toArray( final T[] a ) {
		return this.pots.toArray( a );
	}

	public String toDescribe() {
		return Lib_Describe.toDescribe( this );
	}

	public String toIdent() {
		return Lib_Describe.toIdent( this, this.pots.size() );
	}

	@Override
	public String toString() {
		String result = "";
		final int width = ("" + this.pots.size()).length();
		int nr = 1;

		for( final I_Iterable<TA> pot : this.pots ) {
			result += FormNumber.width( width, nr, false ) + " ";
			nr++;
			String result2 = "(";
			int nr2 = 1;

			for( final TA item : pot ) {
				result2 += (nr2 == 1
					? ""
					: ",") + ConvertObject.toStringIdent( item );
				nr2++;

				if( result2.length() > 100 ) {
					result2 += "...";
					break;
				}
			}

			result += result2 + ")\n";
			if( nr > 100 )
				break;
		}

		return result;
	}

	private SimpleList<TA> iMakePot( final Iterable<TA> objects ) {
		final SimpleList<TA> newPot = new SimpleList<>();
		for( final TA t : objects )
			newPot.add( t );
		return newPot;
	}

	private SimpleList<TA> iMakePot( final TA[] pot ) {
		final SimpleList<TA> newPot = new SimpleList<>();
		Collections.addAll( newPot, pot );
		return newPot;
	}

	public void ensureCapacity( int wanted ) {
		this.pots.ensureCapacity( wanted );
	}

}
