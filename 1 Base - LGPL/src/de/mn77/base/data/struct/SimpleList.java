/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import de.mn77.base.data.convert.ConvertObject;
import de.mn77.base.data.struct.atomic.IntList;
import de.mn77.base.data.struct.sort.Sort;
import de.mn77.base.data.util.Lib_Describe;
import de.mn77.base.data.util.Lib_Random;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 13.10.2022
 * @apiNote This is a minimal and fast list implementation. (Alternate for ArrayList)
 */
public class SimpleList<T> implements I_List<T> {

	private class Itr implements Iterator<T> {

		int cursor = 0;

		int lastReturn = -1;


		public boolean hasNext() {
			return this.cursor != SimpleList.this.size();
		}

		public T next() {

			try {
				final int i = this.cursor;
				final T next = SimpleList.this.get( i );
				this.lastReturn = i;
				this.cursor = i + 1;
				return next;
			}
			catch( final IndexOutOfBoundsException e ) {
				throw new NoSuchElementException();
			}
		}

	}

	private class ListItr extends Itr implements ListIterator<T> {

		ListItr( final int index ) {
			this.cursor = index;
		}

		public void add( final T e ) {

			try {
				final int i = this.cursor;
				SimpleList.this.add( i, e );
				this.lastReturn = -1;
				this.cursor = i + 1;
			}
			catch( final IndexOutOfBoundsException ex ) {
				throw new ConcurrentModificationException();
			}
		}

		public boolean hasPrevious() {
			return this.cursor != 0;
		}

		public int nextIndex() {
			return this.cursor;
		}

		public T previous() {

			try {
				final int i = this.cursor - 1;
				final T previous = SimpleList.this.get( i );
				this.lastReturn = this.cursor = i;
				return previous;
			}
			catch( final IndexOutOfBoundsException e ) {
				throw new NoSuchElementException();
			}
		}

		public int previousIndex() {
			return this.cursor - 1;
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}

		public void set( final T e ) {
			if( this.lastReturn < 0 )
				throw new IllegalStateException();

			try {
				SimpleList.this.set( this.lastReturn, e );
			}
			catch( final IndexOutOfBoundsException ex ) {
				throw new ConcurrentModificationException();
			}
		}

	}


	private static final int DEFAULT_INIT_SIZE = 10;	// Default for ArrayList = 10
	private static final int MAX_ARRAY_SIZE    = Integer.MAX_VALUE - 8;

	private T[] data;
	private int size = 0;


	@SuppressWarnings( "unchecked" )
	public SimpleList() {
		this.data = (T[])new Object[SimpleList.DEFAULT_INIT_SIZE];
	}

	@SuppressWarnings( "unchecked" )
	public SimpleList( final int initSize ) {
		this.data = (T[])new Object[initSize];
	}

	public SimpleList( final T[] arr ) {
		Err.ifNull( arr );
		this.data = arr;
		this.size = arr.length;
	}

	public void add( final int index, final T element ) {
		this.insert( index, element );
	}

	@Override
	public boolean add( final T object ) {
		this.ensureGrow( 1 );
		this.data[this.size] = object;
		this.size++;
		return true;
	}

	@Override
	public boolean addAll( final Collection<? extends T> c ) {
		Err.ifNull( c );
		this.ensureGrow( c.size() );
		for( final T t : c )
			this.add( t );
		return true;
	}

	public boolean addAll( final int index, final Collection<? extends T> c ) {
		throw Err.todo();
	}

	@SuppressWarnings( "unchecked" )
	@Override
	public void addMore( final T... ta ) {
		Err.ifNull( ta );
		this.ensureGrow( ta.length );

		for( final T t : ta )
			this.add( t );
	}

	@Override
	public void clear() {
		this.size = 0;
	}

	@Override
	public boolean contains( final Object o ) {
		Err.ifNull( o );
		for( int i = 0; i < this.size; i++ )
			if( this.data[i].equals( o ) )
				return true;
		return false;
	}

	@Override
	public boolean containsAll( final Collection<?> c ) {
		Err.ifNull( c );
		for( final Object o : c )
			if( !this.contains( o ) )
				return false;
		return true;
	}

	@Override
	@SuppressWarnings( "unchecked" )
	public SimpleList<T> copy() {
		final T[] newArray = (T[])new Object[this.size];
		System.arraycopy( this.data, 0, newArray, 0, this.size );
		return new SimpleList<>( newArray );
	}

	public void ensureCapacity( final int wanted ) {
		final int grow = wanted - this.size;
		if( grow > 0 )
			this.ensureGrow( grow );
	}

	public void ensureGrow( final int amount ) {
		final int needed = this.size + amount;
		if( needed <= this.data.length )
			return;

		final int grow = this.size <= 20
			? 10
			: this.size / 2;
		final int newCapacity = needed + grow; // ArrayList is growing only by 'amount'

		if( newCapacity <= 0 || newCapacity > SimpleList.MAX_ARRAY_SIZE )	// Prevent overrun
			throw new Err_Runtime( "Maximum list size exceeded!" );

//		MOut.trace("SimpleList.grow " + this.size + " --> " + newCapacity);

		this.data = Arrays.copyOf( this.data, newCapacity );
	}

	public void exchange( final int indexA, final int indexB ) {
		Err.ifOutOfBounds( 0, this.data.length - 1, indexA );
		Err.ifOutOfBounds( 0, this.data.length - 1, indexB );

		if( indexA == indexB ) {
			MOut.warning( "Change of " + indexA + " and " + indexB + " not necessary!" );
			return;
		}

		final T buffer = this.data[indexA];
		this.data[indexA] = this.data[indexB];
		this.data[indexB] = buffer;
	}

	@Override
	public T get( final int index ) {
		this.iCheckBounds( index );
		return this.data[index];
	}

	public int indexOf( final Object o ) {
		final Integer result = this.searchFirst( o );
		return result != null ? result : -1;
	}

	/**
	 * @return Result is always 'true'
	 */
	@Override
	public boolean insert( final int index, final T object ) {

		if( index == this.size ) {
			this.add( object );
			return true;
		}

		this.iCheckBounds( index );
		this.ensureGrow( 1 );
		System.arraycopy( this.data, index, this.data, index + 1, this.data.length - index - 1 );
		this.data[index] = object;
		this.size++;
		return true;
	}

	@Override
	public boolean isEmpty() {
		return this.size == 0;
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<>() {

			int pointer = 0;


			public boolean hasNext() {
				return this.pointer < SimpleList.this.size;
			}

			public T next() {
				return SimpleList.this.data[this.pointer++];
			}

		};
	}

	public int lastIndexOf( final Object o ) {
		Err.ifNull( o );
		for( int i = this.size - 1; i >= 0; i-- )
			if( this.data[i].equals( o ) )
				return i;
		return -1;
	}

	public ListIterator<T> listIterator() {
		return this.listIterator( 0 );
	}

	public ListIterator<T> listIterator( final int index ) {
		this.ensureCapacity( index + 1 );
		return new ListItr( index );
	}

	@Deprecated // Please use .removeIndex(int index)
	public T remove( final int index ) {
		return this.removeIndex( index );
	}

	/**
	 * @deprecated Please use .removeOne or .removeEvery
	 */
	@Deprecated
	public boolean remove( final Object o ) {
		return this.removeEvery( o ) > 0;
	}

	@Override
	public boolean removeOne( final Object o ) {
		Err.ifNull( o );

		for( int i = this.size - 1; i >= 0; i-- )
			if( this.data[i].equals( o ) ) {
				this.removeIndex( i );
				return true;
			}

		return false;
	}

	@Override
	public int removeEvery( final Object o ) {
		Err.ifNull( o );
		int changes = 0;

		for( int i = this.size - 1; i >= 0; i-- )
			if( this.data[i].equals( o ) ) {
				this.removeIndex( i );
				changes++;
			}

		return changes;
	}

	@Override
	public boolean removeAll( final Collection<?> c ) {
		Err.ifNull( c );
		boolean changed = false;

		for( final Object o : c ) {
			final boolean removed = this.remove( o );
			if( removed )
				changed = true;
		}

		return changed;
	}

	public T removeFirst() {
		return this.removeIndex( 0 );
	}

	@Override
	public T removeIndex( final int index ) {
		this.iCheckBounds( index );
		final T old = this.data[index];
		System.arraycopy( this.data, index + 1, this.data, index, this.data.length - index - 1 );
		this.size--;
		return old;
	}

	public T removeLast() {
		return this.removeIndex( this.size - 1 );
	}

	@Override
	public boolean retainAll( final Collection<?> c ) {
		Err.ifNull( c );
		throw Err.todo();
	}

	public void reverse() {
		final int size = this.data.length;

		final T[] buffer = Arrays.copyOf( this.data, size );
		for( int i = size - 1; i >= 0; i-- )
			buffer[i] = this.data[i];

		this.data = buffer;
	}

	public IntList searchAll( final Object o ) {
		Err.ifNull( o );
		final IntList result = new IntList();

		for( int i = 0; i < this.size; i++ )
			if( this.data[i].equals( o ) )
				result.add( i );

		return result;
	}

	public Integer searchFirst( final Object o ) {
		Err.ifNull( o );
		for( int i = 0; i < this.size; i++ )
			if( this.data[i].equals( o ) )
				return i;
		return null;
	}

	@Override
	public T set( final int index, final T object ) {
		this.iCheckBounds( index );
		final T old = this.data[index];
		this.data[index] = object;
		return old;
	}

	@Override
	public int size() {
		return this.size;
	}

	public void sort() {
//		Arrays.sort(this.data);
		Sort.sortable( this, false );
	}

	public void sortLike( final int[] newOrder ) {
		final int size = this.size;	// This will remove all empty slots. But, after "sort" mostly no more items will be added.
		Err.ifNot( size, newOrder.length, "Length of list and indexes must be equal" );

		final T[] buffer = Arrays.copyOf( this.data, size );
		for( int i = 0; i < size; i++ )
			buffer[i] = this.data[newOrder[i]];
		this.data = buffer;
	}

	public void sortRandom() {
		if( this.data.length <= 1 )
			return;
		final int[] rnd = Lib_Random.getIntArraySet( 0, this.size - 1 );
		this.sortLike( rnd );
	}

	public List<T> subList( final int fromIndex, final int toIndex ) {
		final T[] copy = Arrays.copyOfRange( this.data, fromIndex, toIndex );
		return new SimpleList<>( copy );
	}

	@Override
	public Object[] toArray() {
		final Object[] result = new Object[this.size];
		System.arraycopy( this.data, 0, result, 0, this.size );
		return result;
	}

	@SuppressWarnings( "unchecked" )
	public T[] toArray( final Class<T> clazz ) {
		Err.ifNull( clazz );
		final T[] result = (T[])Array.newInstance( clazz, this.size );
		System.arraycopy( this.data, 0, result, 0, this.size );
		return result;
	}

	@SuppressWarnings( "hiding" )
	@Override
	public <T> T[] toArray( T[] a ) {
		Err.ifNull( a );

		if( a.length < this.size )
			a = Arrays.copyOf( a, this.size );

		System.arraycopy( this.data, 0, a, 0, this.size );
		return a;
	}

	@Override
	public String toDescribe() {
		return Lib_Describe.toDescribe( this );
	}

	@Override
	public String toIdent() {
		return Lib_Describe.toIdent( this, this.size );
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append( '[' );

		for( int i = 0; i < this.size; i++ ) {
			if( i != 0 )
				sb.append( ',' );
			sb.append( ConvertObject.toStringIdent( this.data[i] ) );
		}

		sb.append( ']' );
		return sb.toString();
	}

	private void iCheckBounds( final int index ) {
		if( index < 0 || index >= this.size )
			throw new Err_Runtime( "Index out of bounds", "Allowed are 0-" + (this.size - 1) + ", but got " + index );
	}

}
