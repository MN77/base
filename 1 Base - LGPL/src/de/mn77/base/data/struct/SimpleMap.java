/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import de.mn77.base.data.convert.ConvertObject;
import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @apiNote Respektiert die Reihenfolge!
 * @implNote keys = Set, values = List
 */
public class SimpleMap<TA, TB> implements Map<TA, TB>, I_Iterable<Group2<TA, TB>> {

	private class Entry implements Map.Entry<TA, TB> {

		private final int index;


		private Entry( final int index ) {
			this.index = index;
		}

		public TA getKey() {
			return SimpleMap.this.keys.get( this.index );
		}

		public TB getValue() {
			return SimpleMap.this.values.get( this.index );
		}

		public TB setValue( final TB value ) {
			throw Err.invalid( value );
		}

	}


	protected SimpleList<TB>    values;
	private final SimpleSet<TA> keys;


//	public void changeKey(final TA key_old, final TA key_new) { // TODO rename?
//		final int pos = this.keys.search(key_old);
//		this.keys.set(pos, key_new);
//	}


	public SimpleMap() {
		this.values = new SimpleList<>();
		this.keys = new SimpleSet<>();
	}

	public void add( final TA key, final TB object ) {
		if( this.keys.contains( key ) )
			Err.invalid( "Key is already known: " + key );
		this.keys.add( key );
		this.values.add( object );
	}

//	public int width() {
//		return 2;
//	}

//	public SimpleSet<TA> getKeys() {
//		return this.keys.copy();
//	}

//	public Object get(final int col, final int row) {
//		Err.ifOutOfBounds(0, 1, col);
//		return col == 0
//			? this.keys.get(row)
//			: this.objects.get(row);
//	}

	public void clear() {
		this.keys.clear();
		this.values.clear();
	}

	public boolean containsKey( final Object key ) {
		return this.keys.contains( key );
	}

	public boolean containsValue( final Object value ) {
		return this.values.contains( value );
	}

	public SimpleMap<TA, TB> copy() {
		final SimpleMap<TA, TB> result = new SimpleMap<>();
		for( final Group2<TA, TB> g : this )
			result.add( g.o1, g.o2 );
		return result;
	}

	public Set<Map.Entry<TA, TB>> entrySet() {
		final SimpleSet<Map.Entry<TA, TB>> result = new SimpleSet<>();
		for( int i = 0; i < this.keys.size(); i++ )
			result.add( new Entry( i ) );

		return result;
	}

	public Group2<TA, TB> get( final int rowIndex ) {
		return new Group2<>( this.keys.get( rowIndex ), this.values.get( rowIndex ) );
	}

	public TB get( final Object key ) {
		final Integer index = this.keys.searchFirst( key );
		if( index == null )
			Err.invalid( "Unknown key: " + key );
		return this.values.get( index );
	}

//	public void set(final TA key, final TB object) {
//		final Integer index = this.keys.searchFirst(key);
//		if(index == null)
//			this.add(key, object);
//		else
//			this.replace(key, object);
//	}

//	public void exchange(final int pos_a, final int pos_b) {
//		this.keys.exchange(pos_a, pos_b);
//		this.values.exchange(pos_a, pos_b);
//	}

//	public void sortLike(final int[] so) {
//		this.values.sortLike(so);
//		this.keys.sortLike(so);
//	}

//	public void sortRandom() {
//		if(this.size() <= 1)
//			return;
//
//		final int[] rnd = Lib_Random.getIntArraySet(1, this.size());
//		this.sortLike(rnd);
//	}

//	public String toDescribe() {
//		return this.toString();
//	}

	public TB getOrDefault( final Object key, final TB ifMissing ) {
		final Integer index = this.keys.searchFirst( key );
		return index == null
			? ifMissing
			: this.values.get( index );
	}

//	public void reverse() {
//		this.keys.reverse();
//		this.values.reverse();
//	}

//	public String toIdent() {
//		final StringBuilder sb = new StringBuilder();
//		sb.append(this.getClass().getSimpleName());
//		sb.append('<');
//		sb.append(this.keys.size());
//		sb.append('>');
//		return sb.toString();
//	}

//	public SimpleList<TB> getObjects() {
//		return this.values.copy();
//	}

//	public SimpleList<TA> searchKey(final TB objekt) {
//		final SimpleList<TA> result = new SimpleList<>();
//		for(int i = 0; i < this.values.size(); i++)
//			if(this.values.get(i).equals(objekt))
//				result.add(this.keys.get(i));
//		return result;
//	}

//	public void sortLike(int[] so) {
//		if(this.geschuetzt)
//			throw Fehler.sicherheit.da_Geschuetzt();
////		throw Err.todo();
//	}

	public boolean isEmpty() {
		return this.keys.size() == 0;
	}

	public Iterator<Group2<TA, TB>> iterator() {
		return new Iterator<>() {

			int next = 0;


			public boolean hasNext() {
				return this.next < SimpleMap.this.size();
			}

			public Group2<TA, TB> next() {
				return new Group2<>( SimpleMap.this.keys.get( this.next ), SimpleMap.this.values.get( this.next++ ) );
			}

			public void remove() {
				Err.forbidden();
			}

		};
	}

	public Set<TA> keySet() {
		return this.keys.copy();
	}

	public TB put( final TA key, final TB value ) {
		final Integer index = this.keys.searchFirst( key );

		if( index == null ) {
			this.keys.add( key );
			this.values.add( value );
			return null;
		}
		else
			return this.values.set( index, value );
	}

	public void putAll( final Map<? extends TA, ? extends TB> m ) {
		throw Err.todo();
	}

	public TB remove( final Object key ) {
		final Integer index = this.keys.searchFirst( key );
		if( index == null )
			Err.invalid( "Unknown key: " + key );
		this.keys.removeIndex( index );
		final TB result = this.values.get( index );
		this.values.removeIndex( index );
		return result;
	}

	public TB replace( final TA key, final TB newObject ) {
		throw Err.todo();
//		final Integer index = this.keys.searchFirst(key);
//		if(index == null)
//			Err.invalid("Unknown key: " + key);
//		return this.values.set(index, newObject);
	}

	public int size() {
		return this.keys.size();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();

		for( int row = 0; row < this.size(); row++ ) {
			sb.append( ConvertObject.toStringIdent( this.keys.get( row ) ) );
			sb.append( "," );
			sb.append( ConvertObject.toStringIdent( this.values.get( row ) ) );
			sb.append( "\n" );
		}

		// Remove last linebreak
		if( sb.length() > 0 )
			sb.deleteCharAt( sb.length() - 1 );

		return sb.toString();
	}

	public Collection<TB> values() {
		return this.values.copy();
	}

	public String toDescribe() {
		return this.toString();
	}

	public String toIdent() {
		return "SimpleMap<"+this.size()+">";
	}

}
