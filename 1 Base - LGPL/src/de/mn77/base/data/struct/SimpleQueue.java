/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct;

import java.util.Iterator;
import java.util.LinkedList;


/**
 * @author Michael Nitsche
 * @created 2022-10-15
 */
public class SimpleQueue<T> implements Iterable<T> {

	private final LinkedList<T> cache;


	public SimpleQueue() {
		this.cache = new LinkedList<>();
	}

	@SuppressWarnings( "unchecked" )
	public SimpleQueue( final T... oa ) {
		this();
		for( final T o : oa )
			this.add( o );
	}


	public boolean add( final T t ) {
		this.cache.addLast( t );
		return true;
	}

	public boolean isEmpty() {
		return this.cache.isEmpty();
	}

	public Iterator<T> iterator() {
		return new Iterator<>() {

			public boolean hasNext() {
				return !SimpleQueue.this.cache.isEmpty();
			}

			public T next() {
				return SimpleQueue.this.next();
			}

		};
	}

	public int length() {
		return this.cache.size();
	}

	/**
	 * @return Returns the next item and removes it from the queue.
	 */
	public synchronized T next() {
		if( this.isEmpty() )
			return null;

		return this.cache.removeFirst();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();

		for( final T o : this.cache ) {
			if( sb.length() > 0 )
				sb.append( ',' );
			sb.append( o );
		}

		return sb.toString();
	}

}
