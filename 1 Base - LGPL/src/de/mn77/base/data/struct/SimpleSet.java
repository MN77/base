/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct;

import java.util.Collection;
import java.util.Iterator;

import de.mn77.base.data.struct.atomic.IntList;
import de.mn77.base.data.struct.search.I_Search;
import de.mn77.base.data.util.Lib_Describe;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 * @created 2022-10-15
 */
public class SimpleSet<T> implements I_Collection<T>, I_Search, I_Set<T> {

	private final SimpleList<T> data;


	public SimpleSet() {
		this.data = new SimpleList<>();
	}

	public SimpleSet( final Collection<T> c ) {
		this.data = new SimpleList<>();
		this.addAll( c );
	}

	@SuppressWarnings( "unchecked" )
	public SimpleSet( final T... objects ) {
		this.data = new SimpleList<>();
		this.addMore( objects );
	}


	public boolean add( final T e ) {
		if( !this.iIsValid( e ) )
			return false;
		return this.data.add( e );
	}

	public boolean addAll( final Collection<? extends T> c ) {
		this.data.ensureGrow( c.size() );
		for( final T t : c )
			this.add( t );

		return c.size() > 0;
	}

	public boolean addAll( final Iterable<? extends T> c ) {
		boolean result = false;

		for( final T t : c ) {
			this.add( t );
			result = true;
		}

		return result;
	}

	public void addMore( final T... ta ) {
		this.data.ensureGrow( ta.length );
		for( final T t : ta )
			this.add( t );
	}

	public void addStrict( final T e ) {
		this.iCheckThrow( e );
		this.data.add( e );
	}

	public void clear() {
		this.data.clear();
	}

	public boolean contains( final Object o ) {
		return this.data.contains( o );
	}

	public boolean containsAll( final Collection<?> c ) {
		return this.data.containsAll( c );
	}

	public SimpleSet<T> copy() {
		final SimpleSet<T> result = new SimpleSet<>();
		result.addAll( this.data );
		return result;
	}

	public void ensureCapacity( final int wanted ) {
		this.data.ensureCapacity( wanted );
	}

	public void exchange( final int indexA, final int indexB ) {
		this.data.exchange( indexA, indexB );
	}

	public T get( final int index ) {
		return this.data.get( index );
	}

	public boolean insert( final int index, final T object ) {

		if( !this.iIsValid( object ) )
			return false;
		else {
			this.data.insert( index, object );
			return true;
		}
	}

	public void insertStrict( final int index, final T object ) {
		this.iCheckThrow( object );
		this.data.insert( index, object );
	}

	public boolean isEmpty() {
		return this.data.isEmpty();
	}

//	public boolean put( final T e ) {
//		final boolean valid = this.iValid( e );
//		if( valid )
//			this.data.add( e );
//		return valid;
//	}
//
//	public boolean putAll( final Collection<? extends T> c ) {
//		this.data.ensureGrow( c.size() );
//		boolean changed = false;
//
//		for( final T t : c ) {
//			final boolean added = this.put( t );
//			if( added )
//				changed = true;
//		}
//
//		return changed;
//	}
//
//	public void putMore( final T... ta ) {
//		this.data.ensureGrow( ta.length );
//		for( final T t : ta )
//			this.put( t );
//	}

	public Iterator<T> iterator() {
		return this.data.iterator();
	}

	/**
	 * @apiNote This function is only to prevent using 'index' as 'object'
	 * @deprecated
	 */
	@Deprecated
	public T remove( final int index ) {
		throw Err.invalid( "Please use '.removeIndex'" );
	}

	public boolean remove( final Object o ) {
		return this.removeOne( o );
	}

	public boolean removeOne( final Object o ) {
//		return this.data.remove(o);
		final Integer index = this.data.searchFirst( o );

		if( index != null ) {
			this.data.removeIndex( index );
			return true;
		}

		return false;
	}

	public boolean removeAll( final Collection<?> c ) {
		return this.removeAll( (Iterable<?>)c );
	}

	public boolean removeAll( final Iterable<?> c ) {
//		return this.data.removeAll(c);
		boolean changed = false;

		for( final Object o : c ) {
			final boolean removed = this.remove( o );
			if( removed )
				changed = true;
		}

		return changed;
	}

	public T removeFirst() {
		return this.data.removeFirst();
	}

	public T removeIndex( final int index ) {
		return this.data.removeIndex( index );
	}

	public T removeLast() {
		return this.data.removeLast();
	}

	public boolean retainAll( final Collection<?> c ) {
		return this.data.retainAll( c );
	}

	public void reverse() {
		this.data.reverse();
	}

	public IntList searchAll( final Object o ) {
		final IntList result = new IntList();
		final Integer found = this.data.searchFirst( o );
		if( found != null )
			result.add( found );
		return result;
	}

	public Integer searchFirst( final Object o ) {
		return this.data.searchFirst( o );
	}

	/**
	 * @apiNote Throws an error, if the object is already in the Set!
	 */
	public T set( final int index, final T object ) {
		this.iCheckThrow( object );
		return this.data.set( index, object );
	}

	public int size() {
		return this.data.size();
	}

	public void sort() {
		this.data.sort();
	}

	public void sortLike( final int[] newOrder ) {
		this.data.sortLike( newOrder );
	}

	public void sortRandom() {
		this.data.sortRandom();
	}

	public Object[] toArray() {
		return this.data.toArray();
	}

	public T[] toArray( final Class<T> type ) {
		return this.data.toArray( type );
	}

	public <T> T[] toArray( final T[] a ) {
		return this.data.toArray( a );
	}

	public String toDescribe() {
		return Lib_Describe.toDescribe( this );
	}

	public String toIdent() {
		return Lib_Describe.toIdent( this, this.data.size() );
	}

	@Override
	public String toString() {
		return this.data.toString();
	}

	private void iCheckThrow( final T t ) {
		if( !this.iIsValid( t ) )
			throw new Err_Runtime( "Invalid item for Set", "Object already known: " + t.toString() );
	}

	private boolean iIsValid( final T t ) {
		Err.ifNull( t );
		for( final T o : this.data )
			if( o.equals( t ) )
				return false;
		return true;
	}

}
