/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct._test;

import de.mn77.base.data.struct.BufferQueue;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 28.04.2019
 */
public class Test_BufferQueue {

	public static void main( final String[] args ) {
		final BufferQueue<Character> cl = new BufferQueue<>();
		Test_BufferQueue.stats( cl );

		cl.add( 'a' );
		cl.add( 'b' );
		cl.add( 'c' );
		MOut.print( "Add a,b,c" );
		Test_BufferQueue.stats( cl );

		MOut.print( cl.next() );
		Test_BufferQueue.stats( cl );

		MOut.print( cl.next() );
		Test_BufferQueue.stats( cl );

		MOut.print( cl.next() );
		Test_BufferQueue.stats( cl );

		MOut.print( cl.next() );
		Test_BufferQueue.stats( cl );

		cl.add( 'd' );
		MOut.print( "Add d" );
		Test_BufferQueue.stats( cl );

		MOut.print( cl.next() );
		Test_BufferQueue.stats( cl );

		MOut.print( cl.next() );
		Test_BufferQueue.stats( cl );

		cl.reset();
		MOut.print( "Reset" );
		Test_BufferQueue.stats( cl );

		MOut.print( cl.next() );
		Test_BufferQueue.stats( cl );

		MOut.print( cl.next() );
		Test_BufferQueue.stats( cl );

		cl.clear();
		MOut.print( "Clear" );
		Test_BufferQueue.stats( cl );
	}

	private static void stats( final BufferQueue<Character> cl ) {
		MOut.print( "isEmpty: " + cl.isEmpty() + " ; Length: " + cl.length() + " ; Current " + cl.current() );
		MOut.print( "isEmpty: " + cl.isEmpty() + " ; Length: " + cl.length() + " ; Current " + cl.current() );
		MOut.print( "-----" );
	}

}
