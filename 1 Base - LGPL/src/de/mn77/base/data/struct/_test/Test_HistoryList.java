/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct._test;

import de.mn77.base.data.struct.HistoryList;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 28.04.2019
 */
public class Test_HistoryList {

	public static void main( final String[] args ) {
		final HistoryList<Integer> cl = new HistoryList<>();
		Test_HistoryList.stats( cl );

		cl.add( 1 );
		cl.add( 2 );
		cl.add( 3 );

		Test_HistoryList.stats( cl );

		MOut.print( cl.back() );

		Test_HistoryList.stats( cl );

		MOut.print( cl.back() );

		Test_HistoryList.stats( cl );

		MOut.print( cl.forward() );
		MOut.print( cl.forward() );
		MOut.print( cl.forward() );
		MOut.print( cl.forward() );

		Test_HistoryList.stats( cl );

		MOut.print( cl.back() );
		MOut.print( cl.back() );

		Test_HistoryList.stats( cl );

		cl.add( 4 );
		cl.add( 4 );
		MOut.print( "Add 2x 4" );

		Test_HistoryList.stats( cl );

		MOut.print( cl.back() );
		MOut.print( cl.back() );
		MOut.print( cl.back() );
		MOut.print( cl.back() );
		MOut.print( cl.back() );

		Test_HistoryList.stats( cl );

		MOut.print( cl.forward() );
		MOut.print( cl.forward() );
		MOut.print( cl.forward() );
		MOut.print( cl.forward() );
		MOut.print( cl.forward() );
		MOut.print( cl.forward() );
		MOut.print( cl.forward() );

		Test_HistoryList.stats( cl );
	}

	private static void stats( final HistoryList<Integer> cl ) {
//		MOut.print("--> isEmpty: "+cl.isEmpty()+" ; Length: "+ cl.getLength());
	}

}
