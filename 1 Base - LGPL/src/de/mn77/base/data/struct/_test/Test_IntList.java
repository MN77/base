/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct._test;

import de.mn77.base.data.struct.atomic.IntList;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 20.02.2021
 */
public class Test_IntList {

	public static void main( final String[] args ) {
		final int[] init = { 6, 9, 3, 1 };

		final IntList l = new IntList( init );
		MOut.print( l.size(), "---" );
		l.add( 5 );
		l.add( 4 );
		MOut.print( l.size(), l.get( 1 ), l.get( 2 ), l.toArray(), "---" );

		for( final Integer i : l )
			MOut.print( i );
		MOut.print( "---" );

		for( int i = 1; i <= 20; i++ )
			l.add( 100 + i );
		MOut.print( l, l.size() );
	}

}
