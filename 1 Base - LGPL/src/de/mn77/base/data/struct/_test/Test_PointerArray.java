/*******************************************************************************
 * Copyright (C) 2009-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct._test;

import de.mn77.base.data.struct.array.PointerArray;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 22.06.2009
 */
public class Test_PointerArray {

	public static void main( final String[] args ) {

		try {
			Test_PointerArray.start();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

	public static void start() throws Throwable {
		final PointerArray<Integer> api = new PointerArray<>( Integer.class, 3 );
		api.put( 2 );
		api.put( 3 );
		MOut.print( api.getRawArray(), api.areItemsEqual() );
		api.put( 4 );
		api.put( 5 );
		MOut.print( api.getRawArray(), api.areItemsEqual() );
		api.put( 5 );
		MOut.print( api.getRawArray(), api.areItemsEqual() );
		api.put( 5 );
		MOut.print( api.getRawArray(), api.areItemsEqual() );
		api.put( 6 );
		MOut.print( api.getRawArray(), api.areItemsEqual() );

		MOut.print( api.toIdent() );
		MOut.print( api.toDescribe() );
	}

}
