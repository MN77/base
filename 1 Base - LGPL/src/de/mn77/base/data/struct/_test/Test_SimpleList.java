/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct._test;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 */
public class Test_SimpleList {

	public static void main( final String[] args ) {

		try {
			final SimpleList<String> l = new SimpleList<>();

			l.add( "ab" );
			MOut.print( l );

			l.addMore( "cd", "ef", "gh" );
			MOut.print( l );

			l.add( "ij" );
			MOut.print( l );

			l.insert( 3, "UU" );
			MOut.print( l );

			l.insert( 0, "AA" );
			MOut.print( l );

			l.insert( 7, "ZZ" );
			MOut.print( l );

			l.removeIndex( 0 );
			MOut.print( l );

			l.removeIndex( 3 );
			MOut.print( l );

			l.removeIndex( 5 );
			MOut.print( l );

			MOut.print( l.toIdent() );
			MOut.print( l.size() );
		}
		catch( final Throwable t ) {
			Err.show( t );
		}
	}

}
