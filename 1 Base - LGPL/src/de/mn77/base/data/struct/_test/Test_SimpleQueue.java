/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct._test;

import de.mn77.base.data.struct.SimpleQueue;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 28.04.2019
 */
public class Test_SimpleQueue {

	public static void main( final String[] args ) {
		final SimpleQueue<Integer> cl = new SimpleQueue<>();
		Test_SimpleQueue.stats( cl );

		cl.add( 1 );
		cl.add( 2 );
		cl.add( 3 );

		Test_SimpleQueue.stats( cl );

		MOut.print( cl.next() );

		Test_SimpleQueue.stats( cl );

		MOut.print( cl.next() );

		Test_SimpleQueue.stats( cl );

		MOut.print( cl.next() );

		Test_SimpleQueue.stats( cl );

		MOut.print( cl.next() );

		Test_SimpleQueue.stats( cl );

		cl.add( 4 );
		MOut.print( "Add 4" );

		Test_SimpleQueue.stats( cl );

		MOut.print( cl.next() );

		Test_SimpleQueue.stats( cl );

		MOut.print( cl.next() );

		Test_SimpleQueue.stats( cl );
	}

	private static void stats( final SimpleQueue<Integer> cl ) {
		MOut.print( "isEmpty: " + cl.isEmpty() + " ; Length: " + cl.length() );
		MOut.print( "isEmpty: " + cl.isEmpty() + " ; Length: " + cl.length() );
		MOut.print( "-----" );
	}

}
