/*******************************************************************************
 * Copyright (C) 2009-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.array;

import java.util.Iterator;

import de.mn77.base.data.convert.ConvertObject;
import de.mn77.base.data.struct.I_Iterable;
import de.mn77.base.data.util.Lib_Array;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 2009-06-22
 * @reworked 2025-01-24
 */
public class PointerArray<T> implements I_Iterable<T> {

	private class Iter implements Iterator<T> {

		int cursor = 0;


		public boolean hasNext() {
			return this.cursor < PointerArray.this.array.length;
		}

		public T next() {
			final int i = this.cursor; // safe way
			final T next = PointerArray.this.array[i];
			this.cursor = i + 1;
			return next;
		}

	}


	private final T[]      array;
	private final Class<T> clazz;
	private int            pointer = 0; // To next item


	public static <T1> PointerArray<T1> create( final Class<T1> clazz, final int size ) {
		return new PointerArray<>( clazz, size );
	}


	public PointerArray( final Class<T> clazz, final int size ) {
		Err.ifTooSmall( 1, size );
		this.array = Lib_Array.newArray( clazz, size );
		this.clazz = clazz;
	}

	public PointerArray( final Class<T> clazz, final T[] arr ) {
		Err.ifTooSmall( 1, arr.length );
		this.array = arr;
		this.clazz = clazz;
	}


	/**
	 * @apiNote Add item to the current pointer position, Throwing error, if pointer out of array
	 */
	public void add( final T t ) {
		if( this.pointer >= this.array.length )
			throw Err.forbidden( "Pointer out of array" );

		this.array[this.pointer++] = t;
	}

	public boolean areItemsEqual() {
		final T a = this.array[0];

		for( int i = 1; i < this.array.length; i++ ) {
			final T b = this.array[i];

			if( a == null && b == null )
				continue;
			if( a == null || b == null || !a.equals( b ) )
				return false;
		}

		return true;
	}

	public Double calcAverage() {
		double sum = 0d;
		for( final T t : this.array )
			if( t != null && t instanceof Number ) {
				final Double d = ((Number)t).doubleValue();
				sum += d;
			}
			else
				return null;
		return sum / this.array.length;
	}

	public Double calcAverage( final double minVar, final int minCount ) {
		if( !this.isFilled() )
			return null;
		final Double avg = this.calcAverage();
		if( avg == null )
			return null;

		double sum = 0d;
		int count = 0;
		for( final T t : this.array )
			if( t != null && t instanceof Number ) {
				final Double d = ((Number)t).doubleValue();

				if( Math.abs( d - avg ) <= minVar ) {
					sum += d;
					count++;
				}
			}
		return count < minCount ? null : sum / count;
	}

	public Double calcAverageIgnoreNull() {
		double sum = 0d;
		int count = 0;
		for( final T t : this.array )
			if( t != null && t instanceof Number ) {
				final double d = ((Number)t).doubleValue();
				sum += d;
				count++;
			}
		return sum / count;
	}

	public Double calcAverageIgnoreNull( final int minCount ) {
		double sum = 0d;
		int count = 0;
		for( final T t : this.array )
			if( t != null && t instanceof Number ) {
				final Double d = ((Number)t).doubleValue();
				sum += d;
				count++;
			}
		if( minCount > count )
			return null;
		return sum / count;
	}

	public Double calcAverageIgnoreNullTrim( final int minCount ) {
		double sum = 0d;
		Double min = Double.MAX_VALUE;
		Double max = Double.MIN_VALUE;
		int count = 0;
		for( final T t : this.array )
			if( t != null && t instanceof Number ) {
				final Double d = ((Number)t).doubleValue();
				sum += d;
				min = Math.min( min, d );
				max = Math.max( max, d );
				count++;
			}
		count -= 2;
		if( minCount > count )
			return null;
		sum -= min + max;
		return sum / count;
	}

	public Double calcMaxDiff() {
		Double min = null;
		Double max = null;
		for( final T t : this.array )
			if( t != null && t instanceof Number ) {
				final Double d = ((Number)t).doubleValue();
				if( min == null || min > d )
					min = d;
				if( max == null || max < d )
					max = d;
			}
		return min == null || max == null ? null : max - min;
	}

	/**
	 * @apiNote Set all items in the array to null and put the pointer to 0
	 */
	public void clear() {
		this.fill( null );
	}

	/**
	 * @apiNote Fill the array with the given item and move the pointer to 0
	 */
	public void fill( final T t ) {
		for( int i = 0; i < this.array.length; i++ )
			this.array[i] = t;
		this.pointer = 0;
	}

	public T get( final int i ) {
		return this.array[i];
	}

	public T[] getRawArray() {
		return this.array;
	}

	public boolean isEmpty() {
		for( final T element : this.array )
			if( element != null )
				return false;
		return this.pointer == 0;
	}

	public boolean isFilled() {
		for( final T element : this.array )
			if( element == null )
				return false;
		return true;
	}

	public Iterator<T> iterator() {
		return new Iter();
	}

	/**
	 * @apiNote Add item to the current pointer position and increase pointer. If the pointer is out of array, move it
	 *          back to 0. (Cycle)
	 */
	public void put( final T t ) {
		if( this.pointer >= this.array.length )
			this.pointer = 0;

		this.array[this.pointer++] = t;
	}

	/**
	 * @apiNote Move pointer to first item
	 */
	public void reset() {
		this.pointer = 0;
	}

	public void set( final int i, final T t ) {
		this.array[i] = t;
	}

	public int size() {
		return this.array.length;
	}

	public String toDescribe() {
		final StringBuilder sb = new StringBuilder();
		sb.append( '[' );

		for( int i = 0; i < this.array.length; i++ ) {
			if( i > 0 )
				sb.append( ',' );
			sb.append( ConvertObject.toStringIdent( this.array[i] ) );
		}
		sb.append( ']' );

		sb.append( '<' );
		sb.append( this.clazz.getSimpleName() );
		sb.append( ',' );
		sb.append( this.pointer );
		sb.append( '>' );

		return sb.toString();
	}

	public String toIdent() {
		final StringBuilder sb = new StringBuilder();
		sb.append( this.getClass().getSimpleName() );

		sb.append( '[' );
		sb.append( this.array.length );
		sb.append( ']' );

		sb.append( '<' );
		sb.append( this.clazz.getSimpleName() );
		sb.append( ',' );
		sb.append( this.pointer );
		sb.append( '>' );

		return sb.toString();
	}

}
