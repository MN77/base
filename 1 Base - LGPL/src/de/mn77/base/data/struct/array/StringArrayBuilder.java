/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.array;

import java.util.Collection;

import de.mn77.base.data.I_Copyable;
import de.mn77.base.data.I_Describe;
import de.mn77.base.data.convert.ConvertObject;
import de.mn77.base.data.util.Lib_Describe;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 * @created 16.10.2024
 * @apiNote A minimal and fast String[]-Builder (Alternate for ArrayList or SimpleList)
 * @implNote The speed improvement comes from minimalizing grow actions and returning the data array.
 */
public class StringArrayBuilder implements I_Copyable<StringArrayBuilder>, I_Describe {

	private static final String[] EMPTY_ARRAY = {};

	private final String[] data;
	private int            size = 0;


	public StringArrayBuilder( final int initSize ) {
		this.data = new String[initSize];
	}

	public StringArrayBuilder( final String[] arr ) {
		Err.ifNull( arr );
		this.data = arr;
		this.size = arr.length;
	}


	public boolean add( final String object ) {
		this.iCheckAdd( 1 );
		this.data[this.size] = object;
		this.size++;
		return true;
	}

	public boolean addAll( final Collection<String> c ) {
		Err.ifNull( c );
		this.iCheckAdd( c.size() );
		for( final String t : c )
			this.add( t );
		return true;
	}

	public void addMore( final String... ta ) {
		Err.ifNull( ta );
		this.iCheckAdd( ta.length );

		for( final String t : ta )
			this.add( t );
	}

	public void clear() {
		this.size = 0;
	}

	public StringArrayBuilder copy() {
		final String[] newArray = new String[this.size];
		System.arraycopy( this.data, 0, newArray, 0, this.size );
		return new StringArrayBuilder( newArray );
	}

	public boolean isEmpty() {
		return this.size == 0;
	}

	public int size() {
		return this.size;
	}

	public String[] toArray() {
		if( this.data.length == this.size )
			return this.data;
		else if( this.size == 0 )
			return StringArrayBuilder.EMPTY_ARRAY;
		else {
			final String[] result = new String[this.size];
			System.arraycopy( this.data, 0, result, 0, this.size );
			return result;
		}
	}

	@Override
	public String toDescribe() {
		return Lib_Describe.toDescribe( this );
	}

	@Override
	public String toIdent() {
		return Lib_Describe.toIdent( this, this.size );
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append( '[' );

		for( int i = 0; i < this.size; i++ ) {
			if( i != 0 )
				sb.append( ',' );
			sb.append( ConvertObject.toStringIdent( this.data[i] ) );
		}

		sb.append( ']' );
		return sb.toString();
	}

	private void iCheckAdd( final int amount ) {
		if( this.size + amount > this.data.length )
			throw new Err_Runtime( "Maximum array size exceeded!" );
	}

}
