/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.atomic;

import java.util.Iterator;
import java.util.RandomAccess;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 * @created 2021-02-20
 */
public class ByteList implements RandomAccess, Iterable<Byte> {

	private byte[] data;
	private int    size = 0;


	public ByteList() {
		this.data = new byte[10]; // DEFAULT 10
		this.size = 0;
	}

	public ByteList( final byte[] ba ) {
		Err.ifNull( ba );
		this.data = ba;
		this.size = ba.length;
	}

	public ByteList( final int initSize ) {
		this.data = new byte[initSize];
		this.size = 0;
	}


	public void add( final byte value ) {
		this.iCheckGrow( 1 );
		this.data[this.size] = value;
		this.size++;
	}

	public byte get( final int idx ) {
		this.iCheckBounds( idx );
		return this.data[idx];
	}

	public Iterator<Byte> iterator() {
		return new Iterator<>() {

			private int next = 0;


			public boolean hasNext() {
				return this.next < ByteList.this.size;
			}

			public Byte next() {
				return ByteList.this.data[this.next++];
			}

			public void remove() {
				Err.todo();
			}

		};
	}

	public void set( final int idx, final byte value ) {
		this.iCheckBounds( idx );
		this.data[idx] = value;
	}

	public int size() {
		return this.size;
	}

	public byte[] toArray() {
		final byte[] copy = new byte[this.size];
		System.arraycopy( this.data, 0, copy, 0, this.size );
		return copy;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append( '[' );

		for( int i = 0; i < this.size; i++ ) {
			if( i != 0 )
				sb.append( ',' );
			sb.append( this.data[i] );
		}

		sb.append( ']' );
		return sb.toString();
	}

	private void iCheckBounds( final int index ) {
		Err.ifOutOfBounds( 0, this.size - 1, index );
	}

	/**
	 * Grow by 50% of current size, minimum is 10
	 */
	private void iCheckGrow( final int toAdd ) {
		if( this.size + toAdd <= this.data.length )
			return;

		if( this.size > Integer.MAX_VALUE / 2 )
			throw new Err_Runtime( "Maximum of PIntList reached" );
		final int grow = this.size <= 20 ? 10 : this.size / 2;
		final int newSize = this.size + toAdd + grow;
//		MOut.temp("Grow "+this.size+" --> "+newSize);

		final byte[] copy = new byte[newSize];
		System.arraycopy( this.data, 0, copy, 0, this.size );
		this.data = copy;
	}

}
