/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.atomic;

import java.util.Iterator;
import java.util.RandomAccess;

import de.mn77.base.data.I_Describe;
import de.mn77.base.data.util.Lib_Describe;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 * @created 2021-02-20
 * @implNote I_Collection or I_Sequence is not possible, because of atomic "int get()"
 */
public class IntList implements RandomAccess, Iterable<Integer>, I_Describe {

	private int[] data;
	private int   size = 0;


	public IntList() {
		this.data = new int[10]; // DEFAULT 10
		this.size = 0;
	}

	public IntList( final int initSize ) {
		this.data = new int[initSize];
		this.size = 0;
	}

	public IntList( final int[] ia ) {
		Err.ifNull( ia );
		this.data = ia;
		this.size = ia.length;
	}


	public void add( final int value ) {
		this.iCheckGrow( 1 );
		this.data[this.size] = value;
		this.size++;
	}

	public void addAll( final int... values ) {
		final int len = values.length;
		this.iCheckGrow( len );
		for( int i = 0; i < len; i++ )
			this.data[this.size + i] = values[i];
		this.size += len;
	}

	public boolean contains( final int x ) {
		for( int i = 0; i < this.size; i++ )
			if( this.data[i] == x )
				return true;
		return false;
	}

	public int get( final int idx ) {
		this.iCheckBounds( idx );
		return this.data[idx];
	}

	public boolean isEmpty() {
		return this.size == 0;
	}

	public Iterator<Integer> iterator() {
		return new Iterator<>() {

			private int next = 0;


			public boolean hasNext() {
				return this.next < IntList.this.size;
			}

			public Integer next() {
				return IntList.this.data[this.next++];
			}

			public void remove() {
				Err.todo();
			}

		};
	}

	public void set( final int idx, final int value ) {
		this.iCheckBounds( idx );
		this.data[idx] = value;
	}

	public int size() {
		return this.size;
	}

	public int[] toArray() {
		final int[] copy = new int[this.size];
		System.arraycopy( this.data, 0, copy, 0, this.size );
		return copy;
	}

	public String toDescribe() {
		final StringBuilder sb = new StringBuilder();
		sb.append( this.getClass().getSimpleName() );
		sb.append( '(' );

		for( int i = 0; i < this.size; i++ ) {
			if( i != 0 )
				sb.append( ',' );
			sb.append( this.data[i] );
		}

		sb.append( ')' );
		return sb.toString();
	}

	public String toIdent() {
		return Lib_Describe.toIdent( this, this.size );
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append( '[' );

		for( int i = 0; i < this.size; i++ ) {
			if( i != 0 )
				sb.append( ',' );
			sb.append( this.data[i] );
		}

		sb.append( ']' );
		return sb.toString();
	}

	private void iCheckBounds( final int index ) {
		Err.ifOutOfBounds( 0, this.size - 1, index );
	}

	/**
	 * Grow by 50% of current size, minimum is 10
	 */
	private void iCheckGrow( final int toAdd ) {
		if( this.size + toAdd <= this.data.length )
			return;

		if( this.size > Integer.MAX_VALUE / 2 )
			throw new Err_Runtime( "Maximum of IntList reached" );
		final int grow = this.size <= 20 ? 10 : this.size / 2;
		final int newSize = this.size + toAdd + grow;
//		MOut.temp("Grow "+this.size+" --> "+newSize);

		final int[] copy = new int[newSize];
		System.arraycopy( this.data, 0, copy, 0, this.size );
		this.data = copy;
	}

	public void clear() {
		this.size = 0;
	}

	public int removeIndex( int index ) {
		this.iCheckBounds( index );
		final int old = this.data[index];
		System.arraycopy( this.data, index + 1, this.data, index, this.data.length - index - 1 );
		this.size--;
		return old;
	}

	public void replace( int[] newData ) {
		this.data = newData;
		this.size = newData.length;
	}

	public Integer searchFirst( int value ) {
		for( int i = 0; i < this.size; i++ )
			if( this.data[i] == value )
				return i;
		return null;
	}

}
