/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.keypot;

import java.util.Iterator;

import de.mn77.base.data.convert.ConvertObject;
import de.mn77.base.data.struct.SimpleSet;
import de.mn77.base.data.struct.sort.Sort;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public abstract class A_KeyPot<TA, TB> implements I_KeyPot<TA, TB> {

	protected SimpleSet<TA> keys;


	public A_KeyPot() {
		this.keys = new SimpleSet<>();
	}


	public void addAll( final TA key, final Iterable<TB> objects ) {
		for( final TB o : objects )
			this.add( key, o );
	}

	@SuppressWarnings( "unchecked" )
	public void addItems( final TA key, final TB... objects ) {
		for( final TB o : objects )
			this.add( key, o );
	}

	public KeyPotItem<TA, TB> get( final int zeile ) {
		final TA ta = this.keys.get( zeile );
		return new KeyPotItem<>( ta, this.getPot( ta ) );
	}

	public SimpleSet<TA> getKeys() {
		return this.keys.copy();
	}

	public boolean isEmpty() {
		return this.keys.size() == 0;
	}

	public Iterator<KeyPotItem<TA, TB>> iterator() {
		return new Iterator<>() {

			int next = 0;


			public boolean hasNext() {
				return A_KeyPot.this.size() > this.next;
			}

			public KeyPotItem<TA, TB> next() {
				final TA ta = A_KeyPot.this.keys.get( this.next++ );
				return new KeyPotItem<>( ta, A_KeyPot.this.getPot( ta ) );
			}

			public void remove() {
				Err.forbidden();
			}

		};
	}

	public boolean knows( final TA key ) {
		return this.keys.searchFirst( key ) != null;
	}

	public int size() {
		return this.keys.size();
	}

	public void sort() {
		Sort.sortable( this, false );
	}

	public void sortRandom() {
		Err.todo( "Testen" );
		Sort.sortableRandom( this );
	}

	public String toDescribe() {
		final StringBuilder sb = new StringBuilder();

		for( int index = 0; index < this.size(); index++ ) {
			final TA key = this.keys.get( index );
			sb.append( ConvertObject.toStringIdent( key ) + ": " );
			sb.append( ConvertObject.toStringIdent( this.getPot( key ) ) + "\n" );
//			sb.append(ConvObject.toStringIdent(this.get(1, index)) + "\n");
		}

		return sb.toString();
	}

	public String toIdent() {
		final StringBuilder sb = new StringBuilder();
		sb.append( this.getClass().getSimpleName() );
		sb.append( '<' );
		sb.append( this.size() );
		sb.append( '>' );
		return sb.toString();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();

		for( int index = 0; index < this.size(); index++ ) {
			final TA key = this.keys.get( index );
			sb.append( ConvertObject.toText( key ) + ": " );
			sb.append( ConvertObject.toText( this.getPot( key ) ) + "\n" );	// .get(1, index)
		}

		return sb.toString();
	}

}
