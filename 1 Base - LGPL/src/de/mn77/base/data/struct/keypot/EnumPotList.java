/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.keypot;

import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 * @created 13.10.2022
 * @apiNote Mostly this is used for mapping EventHandlers to Events
 * @implNote TODO Maybe its possible to use a generic Enum-Type and Enum.ordinal
 */
public class EnumPotList<E> {

//	private static int            maxKeys  = 0;
//	private static int            maxItems = 0;
	private SimpleList<Enum<?>>   keys = null;
	private SimpleList<I_List<E>> pots = null;


	public synchronized void add( final Enum<?> ident, final E item ) {
		if( ident == null )
			throw new Err_Runtime( "Given item is null!" );
		if( item == null )
			return;

		if( this.keys == null ) {
			this.keys = new SimpleList<>();
			this.pots = new SimpleList<>();
		}

		I_List<E> pot = this.iSearch( ident );

		if( pot == null ) {
			this.keys.add( ident );
			pot = new SimpleList<>();
			this.pots.add( pot );
		}

		pot.add( item );

//		if(this.keys.size() > EnumPotMap.maxKeys)
//			EnumPotMap.maxKeys = this.keys.size();
//		if(pot.size() > EnumPotMap.maxItems)
//			EnumPotMap.maxItems = pot.size();
//		MOut.line("EnumPotMap-Max: " + EnumPotMap.maxKeys + " / " + EnumPotMap.maxItems);
	}

	public Iterable<E> get( final Enum<?> ident ) {
		if( this.keys == null || ident == null )
			return null;

		final I_List<E> pot = this.iSearch( ident );
		return pot == null
			? null
			: pot;
	}

	private I_List<E> iSearch( final Enum<?> ident ) {
		for( int i = 0; i < this.keys.size(); i++ )
			if( ident == this.keys.get( i ) ) // The function ".equals" calls "==", so it's directly used.
				return this.pots.get( i );

		return null;
	}

}
