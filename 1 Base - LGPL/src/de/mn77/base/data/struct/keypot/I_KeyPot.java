/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.keypot;

import de.mn77.base.data.struct.I_Collection;
import de.mn77.base.data.struct.sort.I_Sortable;


/**
 * @author Michael Nitsche
 */
public interface I_KeyPot<TA, TB> extends I_Sortable<KeyPotItem<TA, TB>> {	// TODO von Map erben?!?

	void add( TA key, TB object );
	void addAll( TA key, Iterable<TB> objects );

	@SuppressWarnings( "unchecked" )
	void addItems( TA key, TB... objects );

	I_KeyPot<TA, TB> copy();

	I_Collection<TA> getKeys();	// TODO Rename to keySet ?!? TODO Return Set ?!?

	I_Collection<TB> getPot( TA key );

	I_Collection<TB> getPot( final TA key, final I_Collection<TB> ifMissing );

	boolean isEmpty();

//	TB			replace(TA key, TB neu);
//	TB			set(TA key, TB neu);

	I_Collection<TB> remove( TA key );

	/**
	 * @return Returns true, if one or more objects were removed.
	 */
	boolean remove( TA key, TB object );

}
