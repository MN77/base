/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.keypot;

import de.mn77.base.data.convert.ConvertObject;
import de.mn77.base.data.struct.I_Sequence;


/**
 * @author Michael Nitsche
 * @created 2022-10-11
 */
public class KeyPotItem<TA, TB> {

	public final TA             key;
	public final I_Sequence<TB> pot;


	public KeyPotItem( final TA key, final I_Sequence<TB> pot ) {
		this.key = key;
		this.pot = pot;
	}


	@Override
	public String toString() {
		return this.getClass().getSimpleName() + "(" + ConvertObject.toStringOutput( this.key ) + "," + ConvertObject.toStringOutput( this.pot ) + ")";
	}

}
