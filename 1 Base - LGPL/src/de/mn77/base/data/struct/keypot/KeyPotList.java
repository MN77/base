/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.keypot;

import de.mn77.base.data.struct.I_Collection;
import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 */
public class KeyPotList<TA, TB> extends A_KeyPot<TA, TB> implements I_KeyPot<TA, TB> {

	private final SimpleList<I_List<TB>> pots;


	public KeyPotList() {
		super();
		this.pots = new SimpleList<>();
	}


	public void add( final TA key, final TB object ) {
		Integer pos = this.keys.searchFirst( key );

		if( pos == null ) {
			this.keys.add( key );
			final SimpleList<TB> emptyList = new SimpleList<>();
			this.pots.add( emptyList );
			pos = this.keys.searchFirst( key );
		}

		this.pots.get( pos ).add( object );
	}

	public void clear( final TA key ) {
		final Integer index = this.keys.searchFirst( key );
		if( index == null )
			Err.invalid( key, "Invalid Key" );

		this.pots.get( index ).clear();
	}

	public KeyPotList<TA, TB> copy() {
		throw Err.todo();
	}

	public void exchange( final int row_a, final int row_b ) {
		Err.ifEqual( row_a, 0 );
		Err.ifEqual( row_b, 0 );
		Err.ifOutOfBounds( 1, this.size(), Math.abs( row_a ) );
		Err.ifOutOfBounds( 1, this.size(), Math.abs( row_b ) );

		if( row_a == row_b ) {
			MOut.warning( "Exchange of " + row_a + " and " + row_b + " not necessary!" );
			return;
		}

		this.keys.exchange( row_a, row_b );
		this.pots.exchange( row_a, row_b );
	}

	public Object get( final int col, final int row ) {
		if( col == 0 )
			return this.keys.get( row );
		else if( col == 1 )
			return this.pots.get( row );
		else
			throw Err.invalid( col );
	}

	public I_Collection<TB> getPot( final TA key ) {
		final Integer index = this.keys.searchFirst( key );
		Err.ifNull( index );
		return this.pots.get( index );
	}

	public I_Collection<TB> getPot( final TA key, final I_Collection<TB> ifMissing ) {
		final Integer pos = this.keys.searchFirst( key );
		if( pos == null )
			return ifMissing;
		return this.pots.get( pos );
	}

	public I_Collection<TB> remove( final TA key ) {
		final Integer index = this.keys.searchFirst( key );
		if( index == null )
			Err.invalid( key, "Invalid Key" );
		final I_Collection<TB> result = this.pots.get( index );
		this.keys.removeIndex( index );
		this.pots.removeIndex( index );
		return result;
	}

	public boolean remove( final TA key, final TB object ) {
		final Integer index = this.keys.searchFirst( key );
		if( index == null )
			Err.invalid( key, "Invalid Key" );

		boolean result = false;

		final I_List<TB> pot = this.pots.get( index );
		if( pot.contains( object ) )
			if( pot.remove( object ) )
				result = true;
			else
				Err.invalid( object, "Unknown Object" );

		return result;
	}

	public void reverse() {
		this.pots.reverse();
	}

	public void sortLike( final int[] newOrder ) {
		this.keys.sortLike( newOrder );
		this.pots.sortLike( newOrder );
	}

}
