/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.keypot;

import de.mn77.base.data.struct.I_Collection;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.SimpleSet;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class KeyPotSet<TA, TB> extends A_KeyPot<TA, TB> implements I_KeyPot<TA, TB> {

	private final SimpleList<SimpleSet<TB>> pots;


	public KeyPotSet() {
		super();
		this.pots = new SimpleList<>();
	}


	public void add( final TA key, final TB object ) {
		this.put( key, object );
	}

	public void clear( final TA key ) {
		final Integer index = this.keys.searchFirst( key );
		if( index == null )
			Err.invalid( key, "Invalid Key" );

		this.pots.get( index ).clear();
	}

	public KeyPotSet<TA, TB> copy() {
		throw Err.todo();
	}

	public void exchange( final int pos_a, final int pos_b ) {
		Err.todo();
	}

	public Object get( final int col, final int row ) {
		if( col == 1 )
			return this.keys.get( row );
		else if( col == 2 )
			return this.pots.get( row );
		else
			throw Err.invalid( col );
	}

	public SimpleSet<TB> getPot( final TA key ) {
		final Integer index = this.keys.searchFirst( key );
		Err.ifNull( index );
		return this.pots.get( index );
	}

	public I_Collection<TB> getPot( final TA key, final I_Collection<TB> ifMissing ) {
		final Integer index = this.keys.searchFirst( key );
		if( index == null )
			return ifMissing;
		return this.pots.get( index );
	}

	public boolean put( final TA key, final TB object ) {
		Integer keyIndex = this.keys.searchFirst( key );

		if( keyIndex == null ) {
			this.keys.add( key );
			final SimpleSet<TB> emptySet = new SimpleSet<>();
			this.pots.add( emptySet );
			keyIndex = this.keys.searchFirst( key );
		}

		return this.pots.get( keyIndex ).add( object );
	}

	public boolean putAll( final TA key, final Iterable<TB> objects ) {
		boolean result = true;

		for( final TB o : objects ) {
			final boolean r = this.put( key, o );
			if( !r )
				result = false;
		}

		return result;
	}

	@SuppressWarnings( "unchecked" )
	public KeyPotSet<TA, TB> putAllItems( final TA key, final TB... objects ) {
		for( final TB o : objects )
			this.put( key, o );
		return this;
	}

	public SimpleSet<TB> remove( final TA key ) {
		throw Err.todo();
	}

	public boolean remove( final TA key, final TB object ) {
		final Integer index = this.keys.searchFirst( key );

		if( index != null )
			return this.pots.get( index ).remove( object );
		else
			return false;
	}

	public void reverse() {
		this.pots.reverse();
	}

	public void sortLike( final int[] so ) {
		Err.todo();
	}

	@SuppressWarnings( "unchecked" )
	public TB[][] getValues() {
		final int size=this.pots.size();
		TB[][] result = (TB[][])new Object[size][];
		for(int i=0; i<size; i++)
			result[i]=(TB[])this.pots.get( i ).toArray();
		return result;
	}
}
