/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.sort;

import de.mn77.base.data.util.Lib_Compare;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 06.09.2022
 */
public class Lib_Sort {

	public static boolean isHeavier( final I_Sortable<?> list, final int row, final int thanRow, final boolean desc ) {
		return Lib_Sort.relation( list, row, thanRow, desc ) > 0;
	}

	/**
	 * @param columnOrder
	 *            defines the columns to sort. Where positive (1,2,3,...) sort ascending, negative (-1,-2,-3,...) descending. Zero is not allowed.
	 */
	public static boolean isHeavier( final I_TableSort<?> tab, final int row, final int thanRow, final int... columnOrder ) {
		return Lib_Sort.relation( tab, row, thanRow, columnOrder ) > 0;
	}

	public static int relation( final I_Sortable<?> list, final int row1, final int row2, final boolean desc ) {
		final Object o1 = list.get( row1 );
		final Object o2 = list.get( row2 );

		if( Lib_Compare.isEqual( o1, o2 ) )
			return 0;
		else {
			boolean isGreater = Lib_Compare.isGreater( o1, o2 );

			if( desc )
				isGreater = !isGreater;

			return isGreater
				? 1
				: -1;
		}
	}

	/**
	 * @param columnOrder
	 *            defines the columns to sort. Where positive (1,2,3,...) sort ascending, negative (-1,-2,-3,...) descending. Zero is not allowed.
	 */
	public static int relation( final I_TableSort<?> tab, final int row1, final int row2, final int... columnOrder ) {
		int result = 0;

		for( int columnOrderIdx = 0; result == 0 && columnOrder.length > columnOrderIdx; columnOrderIdx++ ) {
			final int currentColumn = columnOrder[columnOrderIdx];
			if( currentColumn == 0 )
				Err.invalid( "Got 0 as order column" );

			final int columnIndex = currentColumn > 0
				? currentColumn - 1
				: -1 - currentColumn;

			final Object o1 = tab.get( columnIndex, row1 );
			final Object o2 = tab.get( columnIndex, row2 );

			if( Lib_Compare.isEqual( o1, o2 ) )
				result = 0;
			else {
				boolean isGreater = Lib_Compare.isGreater( o1, o2 );

				if( currentColumn < 0 )
					isGreater = !isGreater;

				return isGreater
					? 1
					: -1;
			}
		}

		return result;
	}

}
