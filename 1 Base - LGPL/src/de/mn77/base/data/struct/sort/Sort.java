/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.sort;

import java.util.Collections;
import java.util.List;

import de.mn77.base.data.struct.sort.plain.GnomeSort;
import de.mn77.base.data.struct.sort.plain.MergeSort;
import de.mn77.base.data.struct.sort.table.GnomeSortX;
import de.mn77.base.data.struct.sort.table.MergeSortX;
import de.mn77.base.data.util.Lib_Random;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 26.11.2010
 */
public class Sort {

	private static final GnomeSortX gnomeX = new GnomeSortX();
	private static final MergeSortX mergeX = new MergeSortX();
	private static final GnomeSort  gnome  = new GnomeSort();
	private static final MergeSort  merge  = new MergeSort();


	public static <T extends Comparable<? super T>> void list( final List<T> list, final boolean desc ) {
		Collections.sort( list );
		if( desc )
			Collections.reverse( list );
	}

	public static <T extends Comparable<? super T>> void reverse( final List<T> list ) {
		Collections.reverse( list );
	}

	public static void sortable( final I_Sortable<?> tab, final boolean desc ) {
		Err.ifNull( tab );

		if( tab.size() <= 10 ) // Macht aber nur sehr wenig aus. Evtl. macht diese Abfrage schon das Plus kaputt!
			Sort.gnome.sort( tab, desc );
		else
			Sort.merge.sort( tab, desc );
	}

	public static void sortableRandom( final I_Sortable<?> s ) {
		if( s.size() <= 1 )
			return;

		final int[] rnd = Lib_Random.getIntArraySet( 1, s.size() );
		s.sortLike( rnd );
	}

	public static void table( final I_TableSort<?> tab, int... relColumnOrder ) {
		Err.ifNull( tab );

		if( relColumnOrder == null || relColumnOrder.length == 0 )
			// Default is to sort by all columns: 1,2,3,4,...
			relColumnOrder = Sort.iDefaultColumnOrder( tab.width() );
		else
			for( final int i : relColumnOrder )
				Err.ifOutOfBounds( 1, tab.width(), Math.abs( i ) );

		if( tab.size() <= 10 ) // Macht aber nur sehr wenig aus. Evtl. macht diese Abfrage schon das Plus kaputt!
			Sort.gnomeX.sort( tab, relColumnOrder );
		else
			Sort.mergeX.sort( tab, relColumnOrder );
	}

	private static int[] iDefaultColumnOrder( final int width ) {
		// Default is to sort by all columns: 1,2,3,4,...
		final int[] result = new int[width];
		for( int i = 0; i < width; i++ )
			result[i] = i + 1;
		return result;
	}

}
