/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.sort.plain;

import de.mn77.base.data.struct.sort.I_Sortable;
import de.mn77.base.data.struct.sort.Lib_Sort;


/**
 * @author Michael Nitsche
 * @created 26.11.2010
 */
public class MergeSort extends A_SortAlgo {

	@Override
	protected void pSortImpl( final I_Sortable<?> list, final boolean desc ) {
		if( list.size() <= 1 )
			return;

		final int len = list.size();
		final int[] dat = new int[len];

		for( int i = 0; i < len; i++ )
			dat[i] = i;

		this.part( list, desc, dat, 0, len - 1 );

		list.sortLike( dat );
	}

	private void part( final I_Sortable<?> s, final boolean desc, final int[] dat, final int left, final int right ) {
		if( left == right )
			return;

		final int len = right - left + 1;

		if( len > 2 ) {
			// Separate
			final int part = len / 2;
			final int l1 = left;
			final int r1 = left + part;
			final int l2 = r1 + 1;
			final int r2 = right;

			this.part( s, desc, dat, l1, r1 );
			this.part( s, desc, dat, l2, r2 );

			// Merge
			int p1 = l1;
			int p2 = l2;
			int pi = 0;
			final int[] tmp = new int[len];

			while( p1 <= r1 || p2 <= r2 ) {
				final int i1 = p1 > r1
					? -1
					: dat[p1];
				final int i2 = p2 > r2
					? -1
					: dat[p2];
				final int rel = p1 > r1
					? 1
					: p2 > r2
						? -1
						: Lib_Sort.relation( s, i1, i2, desc );

				if( rel > 0 ) {
					tmp[pi] = i2;
					p2++;
				}
				else {
					tmp[pi] = i1;
					p1++;
				}

				pi++;
			}

			for( int i = 0; i < len; i++ )
				dat[left + i] = tmp[i];
		}

		// Sort
		else {
			final int il = dat[left];
			final int ir = dat[right];

			if( Lib_Sort.relation( s, il, ir, desc ) > 0 ) {
				dat[left] = ir;
				dat[right] = il;
			}
		}
	}

}
