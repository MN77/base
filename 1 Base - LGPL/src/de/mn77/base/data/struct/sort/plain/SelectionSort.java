/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.sort.plain;

import de.mn77.base.data.struct.sort.I_Sortable;
import de.mn77.base.data.struct.sort.Lib_Sort;


/**
 * @author Michael Nitsche
 */
public class SelectionSort extends A_SortAlgo {

	@Override
	protected void pSortImpl( final I_Sortable<?> list, final boolean desc ) {
		int smallest;

		for( int l = 0; l < list.size() - 1; l++ ) {
			smallest = l;

			for( int m = l + 1; m < list.size(); m++ )
				if( Lib_Sort.isHeavier( list, smallest, m, desc ) )
					smallest = m;

			if( l != smallest )
				list.exchange( smallest, l );
		}
	}

}
