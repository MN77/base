/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.sort.table;

import de.mn77.base.data.struct.sort.I_TableSort;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 */
public abstract class A_SortAlgoX implements I_SortAlgoX {

	public final void sort( final I_TableSort<?> table, int... columnOrder ) {
		Err.ifNull( table );
		if( columnOrder == null || columnOrder.length == 0 )
			columnOrder = new int[]{ 1 };

		for( final int i : columnOrder )
			Err.ifOutOfBounds( 1, table.width(), Math.abs( i ) );

		if( columnOrder.length > 10 )
			MOut.warning( "The width is greater than 10, sorting might take some time!" );
		if( table.size() > 100000 )
			MOut.warning( "The size is greater than 100000, sorting might take some time!" );

		this.pSortImpl( table, columnOrder );
	}

	protected abstract void pSortImpl( I_TableSort<?> table, int[] columnOrder );

}
