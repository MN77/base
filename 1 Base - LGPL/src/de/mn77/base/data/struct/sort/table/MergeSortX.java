/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.sort.table;

import de.mn77.base.data.struct.sort.I_TableSort;
import de.mn77.base.data.struct.sort.Lib_Sort;


/**
 * @author Michael Nitsche
 * @created 26.11.2010
 */
public class MergeSortX extends A_SortAlgoX {

	@Override
	protected void pSortImpl( final I_TableSort<?> table, final int[] columnOrder ) {

		if( table.size() > 0 ) {
			final int len = table.size();
			final int[] dat = new int[len];
			for( int i = 0; i < len; i++ )
				dat[i] = i;
			this.part( table, columnOrder, dat, 0, len - 1 );
			table.sortLike( dat );
		}
	}

	private void part( final I_TableSort<?> table, final int[] columnOrder, final int[] dat, final int left, final int right ) {
		if( left == right )
			return;

		final int len = right - left + 1;

		if( len > 2 ) {
			// Separate
			final int part = len / 2;
			final int l1 = left;
			final int r1 = left + part;
			final int l2 = r1 + 1;
			final int r2 = right;

			this.part( table, columnOrder, dat, l1, r1 );
			this.part( table, columnOrder, dat, l2, r2 );

			// Merge
			int p1 = l1;
			int p2 = l2;
			int pi = 0;
			final int[] tmp = new int[len];

			while( p1 <= r1 || p2 <= r2 ) {
				final int i1 = p1 > r1
					? -1
					: dat[p1];
				final int i2 = p2 > r2
					? -1
					: dat[p2];
				final int rel = p1 > r1
					? 1
					: p2 > r2
						? -1
						: Lib_Sort.relation( table, i1, i2, columnOrder );

				if( rel > 0 ) {
					tmp[pi] = i2;
					p2++;
				}
				else {
					tmp[pi] = i1;
					p1++;
				}

				pi++;
			}

			for( int i = 0; i < len; i++ )
				dat[left + i] = tmp[i];
		}

		// Sort
		else {
			final int il = dat[left];
			final int ir = dat[right];

			if( Lib_Sort.relation( table, il, ir, columnOrder ) > 0 ) {
				dat[left] = ir;
				dat[right] = il;
			}
		}
	}

}
