/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.sort.table._test;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.sort.plain.BubbleSort;
import de.mn77.base.data.struct.sort.plain.GnomeSort;
import de.mn77.base.data.struct.sort.plain.MergeSort;
import de.mn77.base.data.struct.sort.plain.QuickSort;
import de.mn77.base.data.struct.sort.plain.SelectionSort;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 11.10.2022
 */
public class Test_SortAlgorithms {

	public static void main( final String[] args ) {
		final SimpleList<Integer> list = new SimpleList<>();
		list.addMore( 4, 8, 5, 2, 9, 1, 6, 0, 3, 7 );
		SimpleList<Integer> copy = list;

		final BubbleSort bubble = new BubbleSort();
		copy = list.copy();
		bubble.sort( copy );
		MOut.print( copy );

		final GnomeSort gnome = new GnomeSort();
		copy = list.copy();
		gnome.sort( copy );
		MOut.print( copy );

		final MergeSort merge = new MergeSort();
		copy = list.copy();
		merge.sort( copy );
		MOut.print( copy );

		final QuickSort quick = new QuickSort();
		copy = list.copy();
		quick.sort( copy );
		MOut.print( copy );

		final SelectionSort selection = new SelectionSort();
		copy = list.copy();
		selection.sort( copy );
		MOut.print( copy );
	}

}
