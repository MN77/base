/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.table;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.sort.Sort;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 2022-03-01
 */
public class ArrayTable<T> implements I_Table<T> {

	private static final String   ERR_ADD_WIDTH = "The amount of items doesn't fit to the width of the table!";
	private final SimpleList<T[]> table;
	private final int             width;


	public ArrayTable( final int width ) {
		Err.ifTooSmall( 1, width );
		this.width = width;
		this.table = new SimpleList<>();
	}

	public ArrayTable( final int width, final int capacity ) {
		Err.ifTooSmall( 1, width );
		this.width = width;
		this.table = new SimpleList<>( capacity );
	}


	@SuppressWarnings( "unchecked" )
	@Override
	public void add( final Collection<T> row ) {
		Err.ifNot( row.size(), this.width, ArrayTable.ERR_ADD_WIDTH );
		final T[] rowArray = (T[])new Object[this.width];
		int i = 0;

		for( final T t : row ) {
			rowArray[i] = t;
			i++;
		}

		this.table.add( rowArray );
	}

	@Override
	public void add( final int index, final T[] e ) {
		Err.ifNot( e.length, this.width, ArrayTable.ERR_ADD_WIDTH );
		this.table.add( index, e );
	}

	@Override
	public boolean add( final T[] e ) {
		Err.ifNot( e.length, this.width, ArrayTable.ERR_ADD_WIDTH );
		this.table.add( e );
		return true; // was changed
	}

	@Override
	public boolean addAll( final Collection<? extends T[]> c ) {
		for( final T[] e : c )
			this.add( e );
		return true;
	}

	@Override
	public boolean addAll( final int index, final Collection<? extends T[]> c ) {
		throw Err.todo( c );
	}

	@Override
	@SuppressWarnings( "unchecked" )
	public boolean addRow( final T... row ) {
		Err.ifNot( row.length, this.width, ArrayTable.ERR_ADD_WIDTH );
		this.table.add( row );
		return true; // was changed
	}

	@Override
	public void clear() {
		this.table.clear();
	}

	@Override
	public boolean contains( final Object o ) {
		for( final T[] row : this.table )
			for( final T cell : row )
				if( cell.equals( o ) )
					return true;
		return false;
	}

	@Override
	public boolean containsAll( final Collection<?> c ) {
		throw Err.todo( c );
	}

	public ArrayTable<T> copy() {
		final ArrayTable<T> result = new ArrayTable<>( this.width );
		result.addAll( this );
		return result;
	}

	public void exchange( final int indexA, final int indexB ) {
		Err.ifOutOfBounds( 0, this.table.size(), indexA );
		Err.ifOutOfBounds( 0, this.table.size(), indexB );

		if( indexA == indexB ) {
			MOut.warning( "Change of " + indexA + " and " + indexB + " not necessary!" );
			return;
		}

		final T[] buffer = this.table.get( indexA );
		this.table.set( indexA, this.table.get( indexB ) );
		this.table.set( indexB, buffer );
	}

	@Override
	public T[] get( final int index ) {
		return this.getRow( index );
	}

	@Override
	public T get( final int col, final int row ) {
		return this.table.get( row )[col];
	}

	@Override
	public I_List<T> getColumn( final int index ) {
		Err.ifOutOfBounds( 0, this.width - 1, index );

//		@SuppressWarnings("unchecked")
//		final T[] result = (T[])new Object[this.table.size()];
////		final T[] result = (T[]) Array.newInstance(, this.table.size());
//
//		for(int i = 0; i < this.table.size(); i++) {
//			final T[] row = this.table.get(i);
//			result[i] = row[index];
//		}

		final SimpleList<T> result = new SimpleList<>( this.table.size() );

		for( final T[] row : this.table )
			result.add( row[index] );

		return result;
	}

	@Override
	public T[] getRow( final int index ) {
		return this.table.get( index );
	}

	public int indexOf( final int column, final Object search ) {
		int result = 0;

		for( final T[] row : this.table ) {
			if( row[column].equals( search ) )
				return result;
			result++;
		}

		return -1;
	}

	@Override
	public int indexOf( final Object o ) {
		throw Err.todo( o );
	}

	@Override
	@SuppressWarnings( "unchecked" )
	public void insertRow( final int index, final T... row ) {
		Err.ifOutOfBounds( 0, this.size(), index );
		Err.ifNot( row.length, this.width, ArrayTable.ERR_ADD_WIDTH );
		this.table.add( index, row );
	}

	@Override
	public boolean isEmpty() {
		return this.table.isEmpty();
	}

	@Override
	public Iterator<T[]> iterator() {
		return this.table.iterator();
	}

	@Override
	public int lastIndexOf( final Object o ) {
		throw Err.todo( o );
	}

	@Override
	public ListIterator<T[]> listIterator() {
		return this.table.listIterator();
	}

	@Override
	public ListIterator<T[]> listIterator( final int index ) {
		return this.table.listIterator( index );
	}

	@Deprecated
	@Override
	public T[] remove( final int index ) {
		return this.removeIndex( index );
	}

	@Override
	public boolean remove( final Object o ) {
		throw Err.todo( o );
	}

	@Override
	public boolean removeAll( final Collection<?> c ) {
		throw Err.todo( c );
	}

	@Override
	public T[] removeFirst() {
		return this.table.remove( 0 );
	}

	@Override
	public T[] removeIndex( final int index ) {
		return this.table.remove( index );
	}

	@Override
	public T[] removeLast() {
		return this.table.remove( this.table.size() - 1 );
	}

	@Override
	public boolean retainAll( final Collection<?> c ) {
		throw Err.todo( c );
	}

	public void reverse() {
		T[] buffer = null;
		int left = 0;
		int right = this.table.size() - 1;

		while( left < right ) {
			buffer = this.table.get( left );
			this.table.set( left, this.table.get( right ) );
			this.table.set( right, buffer );
			left++;
			right--;
		}
	}

	@Override
	public void set( final int col, final int row, final T o ) {
		this.table.get( row )[col] = o;
	}

	@Override
	public T[] set( final int index, final T[] element ) {
		throw Err.todo( index, element );
	}

	@Override
	public int size() {
		return this.table.size();
	}

	public void sort() {
		Sort.table( this, null );
	}

	public void sort( final int... columnOrder ) {
		Sort.table( this, columnOrder );
	}

	@SuppressWarnings( "unchecked" )
	public void sortLike( final int[] newIndexes ) {
		final int size = this.table.size();
		Err.ifNot( size, newIndexes.length, "Table and order indexes have different length" );

		final Object[][] buffer = new Object[size][];

		for( int i = 0; i < size; i++ )
			buffer[i] = this.table.get( newIndexes[i] );
		for( int i = 0; i < buffer.length; i++ )
			this.table.set( i, (T[])buffer[i] );
	}

	public void sortRandom() {
		Sort.sortableRandom( this );
	}

	@Override
	public List<T[]> subList( final int fromIndex, final int toIndex ) {
		return this.table.subList( fromIndex, toIndex );
	}

	@Override
	public Object[] toArray() {
		return this.table.toArray();
	}

	@Override
	public <TA> TA[] toArray( final TA[] a ) {
		return this.table.toArray( a );
	}

	public String toDescribe() {
		return this.toString();
	}

	public String toIdent() {
		final StringBuilder sb = new StringBuilder();
		sb.append( this.getClass().getSimpleName() );
		sb.append( '<' );
		sb.append( this.size() );
		sb.append( '>' );
		return sb.toString();
	}

	@Override
	public String toString() {
		return this.toString( null );
	}

	public String toString( final String[] titles ) {
		return Lib_Table.toString( this, titles );
	}

	@Override
	public int width() {
		return this.width;
	}

}
