/*******************************************************************************
 * Copyright (C): 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.table;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.util.Lib_Describe;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 04.10.2022
 *
 * @implNote This Table is Row/Column-Based (YX)
 */
public class ArrayTitleTable<T> implements List<T[]>, I_Table<T> {

	private final ArrayTable<T> tab;
	private String[]            titles = null;


	public ArrayTitleTable( final ArrayTable<T> table ) {
		this.tab = table;
	}

//	public ArrayTitleTable(final int width, final int rows, final T fill) {
//		this.tab = new ArrayTable<>(width, rows, fill);
//	}

	public ArrayTitleTable( final int width ) {
		this.tab = new ArrayTable<>( width );
	}


	public void add( final Collection<T> row ) {
		this.tab.add( row );
	}

	public void add( final int index, final T[] element ) {
		this.tab.add( index, element );
	}

	public boolean add( final T[] e ) {
		return this.tab.add( e );
	}

	public boolean addAll( final Collection<? extends T[]> c ) {
		return this.tab.addAll( c );
	}

	public boolean addAll( final int index, final Collection<? extends T[]> c ) {
		return this.tab.addAll( index, c );
	}

	public boolean addRow( final T... row ) {
		return this.tab.addRow( row );
	}

	public void clear() {
		this.tab.clear();
		this.titles = null;
	}

	public boolean contains( final Object o ) {
		return this.tab.contains( o );
	}

	public boolean containsAll( final Collection<?> c ) {
		return this.tab.containsAll( c );
	}

	public ArrayTitleTable<T> copy() {
		final ArrayTitleTable<T> result = new ArrayTitleTable<>( this.tab.copy() );
		result.setTitles( this.titles );
		return result;
	}

	public void exchange( final int indexA, final int indexB ) {
		this.tab.exchange( indexA, indexB );
	}

	public T[] get( final int index ) {
		return this.tab.get( index );
	}

	public T get( final int col, final int row ) {
		return this.tab.get( col, row );
	}

	public I_List<T> getColumn( final int index ) {
		return this.tab.getColumn( index );
	}

	public T[] getRow( final int index ) {
		return this.getRow( index );
	}

	public String[] getTitles() {
		return this.titles;
	}

	public int indexOf( final int col, final Object search ) {
		return this.tab.indexOf( col, search );
	}

	public int indexOf( final Object o ) {
		return this.tab.indexOf( o );
	}

	public void insertRow( final int index, final T... row ) {
		this.tab.insertRow( index, row );
	}

	public boolean isEmpty() {
		return this.tab.isEmpty();
	}

	public Iterator<T[]> iterator() {
		return this.tab.iterator();
	}

	public int lastIndexOf( final Object o ) {
		return this.tab.lastIndexOf( o );
	}

	public ListIterator<T[]> listIterator() {
		return this.tab.listIterator();
	}

	public ListIterator<T[]> listIterator( final int index ) {
		return this.tab.listIterator( index );
	}

	@Deprecated // Please use .removeIndex(int index)
	public T[] remove( final int index ) {
		return this.removeIndex( index );
	}

	public boolean remove( final Object o ) {
		return this.tab.remove( o );
	}

	public boolean removeAll( final Collection<?> c ) {
		return this.tab.removeAll( c );
	}

	public T[] removeFirst() {
		return this.tab.removeFirst();
	}

	public T[] removeIndex( final int index ) {
		return this.tab.removeIndex( index );
	}

	public T[] removeLast() {
		return this.tab.removeLast();
	}

	public boolean retainAll( final Collection<?> c ) {
		return this.tab.retainAll( c );
	}

	public void reverse() {
		this.tab.reverse();
	}

	public void set( final int col, final int row, final T o ) {
		this.tab.set( col, row, o );
	}

	public T[] set( final int index, final T[] element ) {
		return this.tab.set( index, element );
	}

	public void setTitles( final String... titles ) {
		Err.ifNot( titles.length, this.tab.width() );
		this.titles = titles;
	}

	public int size() {
		return this.tab.size();
	}

	public void sort() {
		this.tab.sort();
	}

	public void sort( final int... columnOrder ) {
		this.tab.sort( columnOrder );
	}

	public void sortLike( final int[] newOrder ) {
		this.tab.sortLike( newOrder );
	}

	public void sortRandom() {
		this.tab.sortRandom();
	}

	public List<T[]> subList( final int fromIndex, final int toIndex ) {
		return this.tab.subList( fromIndex, toIndex );
	}

	public Object[] toArray() {
		return this.tab.toArray();
	}

	public <TA> TA[] toArray( final TA[] a ) {
		return this.tab.toArray( a );
	}

	public String toDescribe() {
		return this.toString();
	}

	public String toIdent() {
		return Lib_Describe.toIdent( this, this.tab.size() );
	}

	@Override
	public String toString() {
		return this.tab.toString( this.titles );
	}

	public int width() {
		return this.tab.width();
	}

}
