/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.table;

import java.util.Collection;
import java.util.List;

import de.mn77.base.data.I_Copyable;
import de.mn77.base.data.I_Describe;
import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.sort.I_TableSort;


/**
 * @author Michael Nitsche
 * @created 04.10.2022
 *          TODO extend I_List ?!?
 */
public interface I_Table<T> extends List<T[]>, I_Describe, I_TableSort<T[]>, I_Copyable<I_Table<T>> {

	void add( Collection<T> row );

	boolean addRow( T... row );

	T get( int col, int row );

	//	T[] getColumn(int index); 	// Would be nice, but it's not possible to create a generic array as result!
	I_List<T> getColumn( int index );

	T[] getRow( int index );

	int indexOf( int col, Object search );

	void insertRow( int index, T... row );

	@Deprecated // Please use .removeIndex(int index)
	T[] remove( final int index );

	T[] removeFirst();

	T[] removeIndex( int index );

	T[] removeLast();

	void set( int col, int row, T o );

	String toString();

	int width();

}
