/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.table;

import de.mn77.base.data.form.FormString;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 11.10.2022
 */
public class Lib_Table {

	private static final String ERR_WIDTH = "The amount of titles doesn't fit to the width of the table!";


	public static <T> String toString( final I_Table<T> table, final String[] titles ) {
		final StringBuilder sb = new StringBuilder();
		final int[] max = new int[table.width()];

		// Calc max width
		for( int y = 0; y < table.size(); y++ )
			for( int x = 0; x < table.width(); x++ ) {
				final T o = table.get( x, y );
				final int len = ("" + o).length(); // converts also null to a string
				if( len > max[x] )
					max[x] = len;
			}

		// Titles
		if( titles != null ) {
			Err.ifNot( titles.length, table.width(), Lib_Table.ERR_WIDTH );

			for( int x = 0; x < table.width(); x++ ) {
				final int len = titles[x].length();
				if( len > max[x] )
					max[x] = len;
			}

			for( int x = 0; x < table.width(); x++ ) {
				if( x > 0 )
					sb.append( " | " );
				sb.append( FormString.width( max[x], "" + titles[x], false ) );
			}

			sb.append( '\n' );

			for( int x = 0; x < table.width(); x++ ) {
				if( x > 0 )
					sb.append( " | " );
				sb.append( Lib_String.sequence( '=', max[x] ) );
			}

			sb.append( '\n' );
		}

		// Generate output
		for( int y = 0; y < table.size(); y++ ) {

			for( int x = 0; x < table.width(); x++ ) {
				if( x > 0 )
					sb.append( " | " );
				sb.append( FormString.width( max[x], "" + table.get( x, y ), false ) );
			}

			if( y < table.size() - 1 ) // No linebreak at the end
				sb.append( "\n" );
		}

		return sb.toString();
	}

}
