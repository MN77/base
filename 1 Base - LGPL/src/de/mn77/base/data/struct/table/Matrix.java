/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.table;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.sort.Sort;
import de.mn77.base.data.util.Lib_Array;
import de.mn77.base.data.util.Lib_Describe;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.sys.MOut;


public class Matrix<T> implements I_Table<T> {

	private final Class<T> clazz;
	private final int      length;
	private final T[][]    data;	// [row][column]
	private final int      width;


	public Matrix( final Class<T> clazz, final int width, final int height ) {
		this.clazz = clazz;
		this.data = Lib_Array.newArray2( clazz, height, width );
		this.width = width;
		this.length = height;
	}


	public void add( final Collection<T> row ) {
		throw this.iErrorResize();
	}

	public void add( final int index, final T[] element ) {
		throw this.iErrorResize();
	}

	public boolean add( final T[] e ) {
		throw this.iErrorResize();
	}

	public boolean addAll( final Collection<? extends T[]> c ) {
		throw this.iErrorResize();
	}

	public boolean addAll( final int index, final Collection<? extends T[]> c ) {
		throw this.iErrorResize();
	}

	public boolean addRow( final T... row ) {
		throw this.iErrorResize();
	}

	public void clear() {
		for( int column = 0; column < this.width; column++ )
			for( int row = 0; row < this.length; row++ )
				this.data[row][column] = null;
	}

	public boolean contains( final Object o ) {
		for( int column = 0; column < this.width; column++ )
			for( int row = 0; row < this.length; row++ )
				if( this.data[row][column].equals( o ) )
					return true;
		return false;
	}

	public boolean containsAll( final Collection<?> c ) {
		for( final Object o : c )
			if( !this.contains( o ) )
				return false;
		return true;
	}

	public Matrix<T> copy() {
		final Matrix<T> result = new Matrix<>( this.clazz, this.width, this.length );

		for( int column = 0; column < this.width; column++ )
			for( int row = 0; row < this.length; row++ )
				result.set( column, row, this.get( column, row ) );

		return result;
	}

	public void exchange( final int indexA, final int indexB ) {
		this.exchangeRows( indexA, indexB );
	}

	public void exchangeRows( final int indexA, final int indexB ) {
		Err.ifOutOfBounds( 0, this.length, indexA );
		Err.ifOutOfBounds( 0, this.length, indexB );

		if( indexA == indexB ) {
			MOut.warning( "Change of " + indexA + " and " + indexB + " not necessary!" );
			return;
		}

		final T[] buffer = this.data[indexA];
		this.data[indexA] = this.data[indexB];
		this.data[indexB] = buffer;
	}

	public T[] get( final int index ) {
		return this.getRow( index );
	}

	public T get( final int column, final int row ) {
		Err.ifOutOfBounds( 0, this.width - 1, column );
		Err.ifOutOfBounds( 0, this.length - 1, row );
		return this.data[row][column];
	}

	public I_List<T> getColumn( final int index ) {
		final SimpleList<T> result = new SimpleList<>( this.length );

		for( int row = 0; row < result.size(); row++ )
			result.add( this.data[row][index] );

		return result;
	}

	@SuppressWarnings( "unchecked" )
	public T[] getRow( final int index ) {
		final T[] result = (T[])Array.newInstance( Object.class, this.width );
		System.arraycopy( this.data[index], 0, result, index, this.width );
		return result;
	}

	public int indexOf( final int column, final Object search ) {
		int result = 0;

		for( final T[] row : this.data ) {
			if( row[column].equals( search ) )
				return result;
			result++;
		}

		return -1;
	}

	public int indexOf( final Object o ) {
		throw Err.todo();
	}

	public void insertRow( final int index, final T... row ) {
		throw this.iErrorResize();
	}

	public boolean isEmpty() {
		for( int column = 0; column < this.width; column++ )
			for( int row = 0; row < this.length; row++ )
				if( this.data[row][column] != null )
					return false;
		return true;
	}

	public Iterator<T[]> iterator() {
		return new Iterator<>() {

			private int nextRow = 0;


			public boolean hasNext() {
				return this.nextRow < Matrix.this.length;
			}

			public T[] next() {
				return Matrix.this.getRow( this.nextRow++ );
			}

			public void remove() {
				throw Matrix.this.iErrorResize();
			}

		};
	}

	public int lastIndexOf( final Object o ) {
		throw Err.todo();
	}

	public ListIterator<T[]> listIterator() {
		throw Err.todo();
	}

	public ListIterator<T[]> listIterator( final int index ) {
		throw Err.todo();
	}

	@Deprecated // Please use .removeIndex(int index)
	public T[] remove( final int index ) {
		throw this.iErrorResize();
	}

	public boolean remove( final Object o ) {
		throw this.iErrorResize();
	}

	public boolean removeAll( final Collection<?> c ) {
		throw this.iErrorResize();
	}

	public T[] removeFirst() {
		throw this.iErrorResize();
	}

	public T[] removeIndex( final int index ) {
		throw this.iErrorResize();
	}

	public T[] removeLast() {
		throw this.iErrorResize();
	}

	public boolean retainAll( final Collection<?> c ) {
		throw this.iErrorResize();
	}

	public void reverse() {
		Err.todo( "Tests needed" );
		final int mid = this.length / 2;
		for( int i = 0; i < mid; i++ )
			this.exchangeRows( i, this.length - 1 - i );
	}

	public void set( final int column, final int row, final T object ) {
		Err.ifOutOfBounds( 0, this.width - 1, column );
		Err.ifOutOfBounds( 0, this.length - 1, row );
		this.data[row][column] = object;
	}

	public T[] set( final int index, final T[] element ) {
		Err.todo( "Tests needed" );
		Err.ifNot( element.length, this.width, "Invalid argument length" );
		final T[] result = this.getRow( index );
//		this.matrix2[index] = element;
		System.arraycopy( element, 0, this.data[index], 0, this.width );
		return result;
	}

	public int size() {
		return this.length;
	}

	public void sort() {
		Sort.table( this );
	}

	public void sort( final int... columnOrder ) {
		Sort.table( this, columnOrder );
	}

	@SuppressWarnings( "unchecked" )
	public void sortLike( final int[] newIndexes ) {
		Err.todo( "Tests needed" );
		final int size = this.data.length;
		Err.ifNot( size, newIndexes.length, "Matrix and order indexes have different length" );

		final T[][] ordered = (T[][])new Object[size][];

		for( int i = 0; i < size; i++ )
			ordered[i] = this.data[newIndexes[i]];
		for( int i = 0; i < size; i++ )
			this.data[i] = ordered[i];
	}

	public void sortRandom() {
		Sort.sortableRandom( this );
	}

	public List<T[]> subList( final int fromIndex, final int toIndex ) {
		throw Err.todo();
	}

	public Object[] toArray() {
		final Matrix<T> copy = this.copy();
		return copy.data;
	}

	public <T> T[] toArray( final T[] a ) {
		throw Err.todo();
	}

	public String toDescribe() {
		return this.toString();
	}

	public String toIdent() {
		return Lib_Describe.toIdent( this, this.length );
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();

		for( int row = 0; row < this.length; row++ ) {

			for( int col = 0; col < this.width; col++ ) {
				sb.append( this.get( col, row ) );
				sb.append( col != this.width ? "," : "" );
			}

			sb.append( '\n' );
		}

		return sb.toString();
	}

	public int width() {
		return this.width;
	}

	private Err_Runtime iErrorResize() {
		throw Err.impossible( "A fixed size matrix can not resized!" );
	}

}
