/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.table.type;

import java.util.Iterator;

import de.mn77.base.data.convert.ConvertObject;
import de.mn77.base.data.form.FormString;
import de.mn77.base.data.struct.I_Collection;
import de.mn77.base.data.struct.sort.Sort;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


public abstract class A_TypeTable<TG> implements I_TypeTable<TG> {

	private static final String ERR_ADD_WIDTH = "The number of items does not match the width of the table";
	private final Class<?>[]    classes;


	protected A_TypeTable( final Class<?>... classes ) {
		this.classes = classes;
	}


	// --- Read ---

//	public abstract int width();
//
//	public abstract TG getRow(int row);

	public void add( final Object... row ) {
		Err.ifNot( this.width(), row.length, A_TypeTable.ERR_ADD_WIDTH );
		for( int col = 0; col < this.width(); col++ )
			this.isValid( col, row[col] );
		this.pAdd( row );
	}

	public I_TypeTable<TG> copy() {
//		Class<?> ca=Object.class;
//		S_TypTabelle erg=TypTabelle(this.typen.zuArray(ca.getClass()));
//		for(Object[] zeile : this) erg.add(zeile);
//		return erg;
		throw Err.todo();
	}

	public void exchange( final int rowA, final int rowB ) {
		Err.ifOutOfBounds( 0, this.size() - 1, rowA );
		Err.ifOutOfBounds( 0, this.size() - 1, rowB );

		if( rowA == rowB ) {
			MOut.warning( "Exchange of " + rowA + " and " + rowB + " not necessary!" );
			return;
		}

		this.pExchange( rowA, rowB );
	}

	public TG get( final int row ) {
		return this.getRow( row );
	}

	public Object get( final int col, final int row ) {
		return this.getColumn( col ).get( row );
	}

	public void insert( final int row, final Object... objects ) {
		Err.ifNot( this.width(), objects.length, A_TypeTable.ERR_ADD_WIDTH );
		for( int col = 0; col < this.width(); col++ )
			this.isValid( col, objects[col] );
		this.pInsert( row, objects );
	}

	public boolean isEmpty() {
		return this.size() == 0;
	}

	public Iterator<TG> iterator() {
		final A_TypeTable<TG> table = this;
		return new Iterator<>() {

			private int nextRow = 0;


			public boolean hasNext() {
				return this.nextRow < table.size();
			}

			public TG next() {
				Err.ifToBig( table.size() - 1, this.nextRow );
				return table.getRow( this.nextRow++ );
			}

			public void remove() {
				Err.forbidden();
			}

		};
	}


	// --- Change ---

	public TG remove( final int rowIndex ) {
		final TG row = this.getRow( rowIndex );
		this.pRemove( rowIndex );
		return row;
	}

	public TG removeLast() {
		final int last = this.size() - 1;
		final TG row = this.getRow( last );
		this.pRemove( last );
		return row;
	}

	public void replace( final int row, final Object... objects ) {
		Err.ifOutOfBounds( row, this.size() );
		Err.ifEqual( row, 0 );
		for( int s = 0; s <= this.width(); s++ )
			this.isValid( s, objects[s] );
		this.pReplace( row, objects );
	}

	public void set( final int col, final int row, final Object object ) {
		Err.ifOutOfBounds( 0, this.size() - 1, row );
		Err.ifOutOfBounds( 0, this.width() - 1, col );
		this.isValid( col, object );
		this.pSet( col, row, object );
	}

	public int size() {
		return this.getColumn( 0 ).size();
	}

	public void sort() {
		this.sort( null );
	}

	public void sort( final int... columnOrder ) {
		Sort.table( this, columnOrder );
	}

	public void sortLike( final int[] indexes ) {

		for( int col = 0; col < this.width(); col++ ) {
//			Sort.sortLike(this.pGetColumn(col), like);
			final I_Collection<?> data = this.pGetColumn( col );
			final int size = data.size();
			Err.ifNot( size, indexes.length, "Length of list and indexes must be equal" );

			final Object[] buffer = new Object[size];
			for( int i = 0; i < size; i++ )
				buffer[i] = data.get( indexes[i] );
			for( int i = 0; i < size; i++ )
				this.set( col, i, buffer[i] );
		}
	}

	public void sortRandom() {
		Sort.sortableRandom( this );
	}

	public String toDescribe() {
		final StringBuilder sb = new StringBuilder();
		sb.append( "\n" );
		final int[] width = new int[this.width()];
		for( int i = 0; i < this.width(); i++ )
			for( final Object o : this.getColumn( i ) ) {
				final int osl = ConvertObject.toStringIdent( o ).length();
				if( osl > width[i] )
					width[i] = osl;
			}

		for( int y = 0; y < this.size(); y++ ) {
			sb.append( "| " );

			for( int x = 0; x < this.width(); x++ ) {
				if( x > 0 )
					sb.append( " | " );
				sb.append( FormString.width( width[x], "" + ConvertObject.toStringIdent( this.get( x, y ) ), false ) );
			}

			sb.append( " |" );
			if( y != this.size() )
				sb.append( "\n" );
		}

		return sb.toString();
	}

	public String toIdent() {
		final StringBuilder sb = new StringBuilder();
		sb.append( this.getClass().getSimpleName() );
		sb.append( '<' );
		sb.append( this.size() );
		sb.append( '>' );
		return sb.toString();
	}


	// --- Abstract ---

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();

		for( int row = 0; row < this.size(); row++ ) {
			for( int col = 0; col < this.width(); col++ )
				sb.append( this.get( col, row ) + (col < this.width() - 1
					? ","
					: "") );
			sb.append( "\n" );
		}

		return sb.toString();

//		TextDesigner td=new TextDesigner();
//		for(int zeile=1; zeile<=size(); zeile++) {
//			TD_Zeile z=td.newLine();
//			for(int spalte=1; spalte<=gBreite(); spalte++)
//				z.l(ConvObject.zuTextDebug(get(spalte,zeile))).f();
//		}
//		return td.get();
	}

	protected void isValid( final int col, final Object o ) {
		if( o != null && !this.classes[col].isAssignableFrom( o.getClass() ) )
			Err.invalid( o.getClass().getSimpleName() );
	}

	protected abstract void pAdd( Object[] row );

	protected abstract void pExchange( int rowA, int rowB );

	/** Kopie **/
//	public abstract I_List<?> getColumn(int col);
//
//	public abstract <T> I_List<T> getColumn(int col, Class<T> type);

	protected abstract I_Collection<?> pGetColumn( int col );

	protected abstract void pInsert( int row, Object[] objects );

	// --- Protected ---

	protected abstract void pRemove( int row );

	protected abstract void pReplace( int row, Object[] object );

	protected abstract void pSet( int col, int row, Object object );

}
