/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.table.type;

import de.mn77.base.data.I_Describe;
import de.mn77.base.data.struct.I_Collection;
import de.mn77.base.data.struct.I_Iterable;
import de.mn77.base.data.struct.sort.I_TableSort;


/**
 * @author Michael Nitsche
 */
public interface I_TypeTable<TG> extends I_Describe, I_TableSort<TG>, I_Iterable<TG> {

	void add( Object... row );

	I_TypeTable<TG> copy();

	I_Collection<?> getColumn( int column );

	<T> I_Collection<T> getColumn( int column, Class<T> type );

	TG getRow( int row );

	void insert( int row, Object... objects );

	boolean isEmpty();

	TG remove( int position );

	TG removeLast();

	void replace( int row, Object... objects );

	void set( int column, int row, Object object );

}
