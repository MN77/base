/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.table.type;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.I_Collection;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


public class TypeTable2<TA, TB> extends A_TypeTable<Group2<TA, TB>> {

	private final SimpleList<TA> column0;
	private final SimpleList<TB> column1;


	public TypeTable2( final Class<TA> class0, final Class<TB> class1 ) {
		super( class0, class1 );
		this.column0 = new SimpleList<>();
		this.column1 = new SimpleList<>();
	}


	// --- Read ---

	@Override
	public I_Collection<?> getColumn( final int col ) {
		if( col == 0 )
			return this.getColumn0();
		if( col == 1 )
			return this.getColumn1();
		throw Err.invalid( col );
	}

	@Override
	@SuppressWarnings( "unchecked" )
	public <T> I_Collection<T> getColumn( final int col, final Class<T> type ) {
		if( col == 0 )
			return (I_Collection<T>)this.getColumn0();
		if( col == 1 )
			return (I_Collection<T>)this.getColumn1();
		throw Err.invalid( col );
	}

	public SimpleList<TA> getColumn0() {
		return this.column0.copy();
	}

	public SimpleList<TB> getColumn1() {
		return this.column1.copy();
	}

	@Override
	public Group2<TA, TB> getRow( final int row ) {
		return new Group2<>( this.column0.get( row ), this.column1.get( row ) );
	}

	public boolean knows( final TA obj_col1, final TB obj_col2 ) {

		for( int row = 0; row < this.size(); row++ ) {
			final Group2<TA, TB> g = this.getRow( row );
			if( g.o1.equals( obj_col1 ) && g.o2.equals( obj_col2 ) )
				return true;
		}

		return false;
	}

	public void reverse() {
		this.column0.reverse();
		this.column1.reverse();
	}

	@Override
	public int width() {
		return 2;
	}


	// --- Change ---

	@Override
	@SuppressWarnings( "unchecked" )
	protected void pAdd( final Object[] row ) {
		this.column0.add( (TA)row[0] );
		this.column1.add( (TB)row[1] );
	}

	@Override
	protected void pExchange( final int rowA, final int rowB ) {
		this.column0.exchange( rowA, rowB );
		this.column1.exchange( rowA, rowB );
	}

	@Override
	protected I_Collection<?> pGetColumn( final int col ) {
		if( col == 0 )
			return this.column0;
		if( col == 1 )
			return this.column1;
		throw Err.invalid( col );
	}

	@Override
	@SuppressWarnings( "unchecked" )
	protected void pInsert( final int row, final Object[] objects ) {
		this.column0.insert( row, (TA)objects[0] );
		this.column1.insert( row, (TB)objects[1] );
	}

	@Override
	protected void pRemove( final int row ) {
		this.column0.removeIndex( row );
		this.column1.removeIndex( row );
	}

	@Override
	@SuppressWarnings( "unchecked" )
	protected void pReplace( final int row, final Object[] objects ) {
		this.column0.set( row, (TA)objects[0] );
		this.column1.set( row, (TB)objects[1] );
	}

	@Override
	@SuppressWarnings( "unchecked" )
	protected void pSet( final int col, final int row, final Object object ) {
		if( col == 0 )
			this.column0.set( row, (TA)object );
		if( col == 1 )
			this.column1.set( row, (TB)object );
	}

}
