/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.table.type;

import de.mn77.base.data.group.Group3;
import de.mn77.base.data.struct.I_Collection;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


public class TypeTable3<TA, TB, TC> extends A_TypeTable<Group3<TA, TB, TC>> {

	private final SimpleList<TA> column0;
	private final SimpleList<TB> column1;
	private final SimpleList<TC> column2;


	public TypeTable3( final Class<TA> class0, final Class<TB> class1, final Class<TC> class2 ) {
		super( class0, class1, class2 );
		this.column0 = new SimpleList<>();
		this.column1 = new SimpleList<>();
		this.column2 = new SimpleList<>();
	}


	// LESEN

	@Override
	public I_Collection<?> getColumn( final int col ) {
		if( col == 0 )
			return this.getColumn0();
		if( col == 1 )
			return this.getColumn1();
		if( col == 2 )
			return this.getColumn2();
		throw Err.invalid( col );
	}

	@Override
	@SuppressWarnings( "unchecked" )
	public <T> I_Collection<T> getColumn( final int col, final Class<T> type ) {
		if( col == 0 )
			return (I_Collection<T>)this.getColumn0();
		if( col == 1 )
			return (I_Collection<T>)this.getColumn1();
		if( col == 2 )
			return (I_Collection<T>)this.getColumn2();
		throw Err.invalid( col );
	}

	public SimpleList<TA> getColumn0() {
		return this.column0.copy();
	}

	public SimpleList<TB> getColumn1() {
		return this.column1.copy();
	}

	public SimpleList<TC> getColumn2() {
		return this.column2.copy();
	}

	@Override
	public Group3<TA, TB, TC> getRow( final int row ) {
		return new Group3<>( this.column0.get( row ), this.column1.get( row ), this.column2.get( row ) );
	}

	public boolean knows( final TA obj_col1, final TB obj_col2, final TC obj_col3 ) {

		for( int row = 0; row < this.size(); row++ ) {
			final Group3<TA, TB, TC> g = this.getRow( row );
			if( g.o1.equals( obj_col1 ) && g.o2.equals( obj_col2 ) && g.o3.equals( obj_col3 ) )
				return true;
		}

		return false;
	}

	@Override
	public I_Collection<?> pGetColumn( final int col ) {
		if( col == 0 )
			return this.column0;
		if( col == 1 )
			return this.column1;
		if( col == 2 )
			return this.column2;
		throw Err.invalid( col );
	}

	public void reverse() {
		this.column0.reverse();
		this.column1.reverse();
		this.column2.reverse();
	}


	// --- Change ---

	@Override
	public int width() {
		return 3;
	}

	@Override
	@SuppressWarnings( "unchecked" )
	protected void pAdd( final Object[] row ) {
		this.column0.add( (TA)row[0] );
		this.column1.add( (TB)row[1] );
		this.column2.add( (TC)row[2] );
	}

	@Override
	protected void pExchange( final int rowA, final int rowB ) {
		this.column0.exchange( rowA, rowB );
		this.column1.exchange( rowA, rowB );
		this.column2.exchange( rowA, rowB );
	}

	@Override
	@SuppressWarnings( "unchecked" )
	protected void pInsert( final int row, final Object[] objects ) {
		this.column0.insert( row, (TA)objects[0] );
		this.column1.insert( row, (TB)objects[1] );
		this.column2.insert( row, (TC)objects[2] );
	}

	@Override
	protected void pRemove( final int row ) {
		this.column0.removeIndex( row );
		this.column1.removeIndex( row );
		this.column2.removeIndex( row );
	}

	@Override
	@SuppressWarnings( "unchecked" )
	protected void pReplace( final int row, final Object[] objects ) {
		this.column0.set( row, (TA)objects[0] );
		this.column1.set( row, (TB)objects[1] );
		this.column2.set( row, (TC)objects[2] );
	}

	@Override
	@SuppressWarnings( "unchecked" )
	protected void pSet( final int col, final int row, final Object object ) {
		if( col == 0 )
			this.column0.set( row, (TA)object );
		if( col == 1 )
			this.column1.set( row, (TB)object );
		if( col == 2 )
			this.column2.set( row, (TC)object );
	}

}
