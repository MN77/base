/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.struct.tree;

import java.util.Collections;
import java.util.Iterator;

import de.mn77.base.data.convert.ConvertSequence;
import de.mn77.base.data.struct.I_Iterable;
import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.util.Lib_Relative;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 * @created 2020-11-23
 */
public class TreeNode<TKey, TValue> implements I_Iterable<TreeNode<TKey, TValue>> {

	public final SimpleList<TreeNode<TKey, TValue>> nodes = new SimpleList<>();
	private TKey                                    name  = null;
	private TValue                                  value = null;


	public TreeNode( final TKey name ) {
		this.name = name;
	}

	public TreeNode( final TKey name, final TValue value ) {
		Err.ifNull( name ); // value can be null
		this.name = name;
		this.value = value;
	}

	public TreeNode<TKey, TValue> add( final TKey key ) {
		final TreeNode<TKey, TValue> result = new TreeNode<>( key );
		this.nodes.add( result );
		return result;
	}

	public TreeNode<TKey, TValue> add( final TKey key, final TValue value ) {
		final TreeNode<TKey, TValue> result = new TreeNode<>( key, value );
		this.nodes.add( result );
		return result;
	}


	public void add( final TreeNode<TKey, TValue> node ) {
		this.nodes.add( node );
	}

	@SuppressWarnings( "unchecked" )
	public void addNodes( final TKey... keys ) {
		this.nodes.ensureGrow( keys.length );
		for( final TKey key : keys )
			this.nodes.add( new TreeNode<>( key ) );
	}

	public void addNodes( final TreeNode<TKey, TValue>... nodes ) {
		Collections.addAll( this.nodes, nodes );
	}

	public TreeNode<TKey, TValue> copy() {
		throw Err.todo();
	}

	public TreeNode<TKey, TValue> first() {
		return this.nodes.size() == 0 ? null : this.nodes.get( 0 );
	}

	public TreeNode<TKey, TValue> get( final int index ) {
		return this.nodes.get( index );
	}

	public TKey getName() {
		return this.name;
	}

	public TValue getValue() {
		return this.value;
	}

	public boolean isEmpty() {
		return this.nodes.size() == 0;
	}

	public Iterator<TreeNode<TKey, TValue>> iterator() {
		return this.nodes.iterator();
	}

	public TreeNode<TKey, TValue> last() {
		return this.nodes.size() == 0 ? null : this.nodes.get( this.nodes.size() - 1 );
	}

	public void put( final TKey key, final TValue value ) {
		this.iSetPut( key, value, true );
	}

	public TreeNode<TKey, TValue> relGet( final int relPosition ) {
		final int index = Lib_Relative.realIndex( relPosition, this.nodes.size(), false );
		return this.nodes.get( index );
	}

	@SuppressWarnings( "unchecked" )
	public void removeNodes( final TKey... keys ) {

		for( final TKey key : keys ) {
			boolean removed = false;

			for( int i = 0; i < this.nodes.size(); i++ ) {
				final TreeNode<TKey, TValue> node = this.nodes.get( i );

				if( node.name.equals( key ) ) {
					this.nodes.removeIndex( i );
					removed = true;
					break;
				}
			}

			if( !removed )
				throw new Err_Runtime( "Can't find node with name: \"" + key + '"' );
		}
	}

	public void set( final TKey key, final TValue value ) {
		this.iSetPut( key, value, false );
	}

	public void setName( final TKey name ) {
		Err.ifNull( name );
		this.name = name;
	}

	public void setValue( final TValue value ) {
		this.value = value;
	}

	public int size() {
		return this.nodes.size();
	}

	public String toDescribe() {
		final SimpleList<String> sum = new SimpleList<>();

		for( final TreeNode<TKey, TValue> node : this.nodes )
			node.iToDescribe( sum, 0 );

		return ConvertSequence.toString( '\n', sum );
	}

	public String toIdent() {
		return this.toString();
	}

	@Override
	public String toString() {
		return (this.name == null ? "Tree" : this.name.toString()) + (this.value == null ? "" : ": " + this.value.toString()); //->
	}

	private TreeNode<TKey, TValue> get( final TKey key ) {
		return this.iGetPull( key, false );
	}

	private TreeNode<TKey, TValue> iGetPull( final TKey key, final boolean lazy ) {
		for( final TreeNode<TKey, TValue> node : this.nodes )
			if( node.name.equals( key ) )
				return node;

		if( lazy ) {
//			if(arg instanceof JMo_TreeNode)
//				this.nodes.add((JMo_TreeNode)arg);
//			else
			final TreeNode<TKey, TValue> node = new TreeNode<>( key );
			this.nodes.add( node );
			return node;
		}

		throw new Err_Runtime( "Can't find node with name: \"" + key + '"' );
	}

	private void iSetPut( final TKey key, final TValue value, final boolean lazy ) {
		TreeNode<TKey, TValue> found = null;

		for( final TreeNode<TKey, TValue> node : this.nodes )
			if( node.name.equals( key ) )
				found = node;

		if( lazy && found == null ) {
//			if(arg instanceof JMo_TreeNode)
//				this.nodes.add((JMo_TreeNode)arg);
//			else
			final TreeNode<TKey, TValue> node = new TreeNode<>( key );
			this.nodes.add( node );
			found = node;
		}

		if( found == null )
			throw new Err_Runtime( "Can't find node with name: \"" + key + '"' );

		found.value = value;
	}

	private void iToDescribe( final I_List<String> target, final int left ) {
		target.add( Lib_String.sequence( ' ', left * 2 ) + this.toString() );
		for( final TreeNode<TKey, TValue> node : this.nodes )
			node.iToDescribe( target, left + 1 );
	}

	private TreeNode<TKey, TValue> pull( final TKey key ) {
		return this.iGetPull( key, true );
	}

//	public void removeValues(TValue... values) {
//		for(final TValue val : values)
//			for(int i = 0; i < this.nodes.size(); i++) {
//				final MTreeNode node = this.nodes.get(i);
//				if(node.value.equals(val))
//					this.nodes.remove(i);
//			}
////			throw new Err_Runtime(cr, "Node not found", "Can't find node with name: "+arg.toString());
//	}

}

