/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.tablestyle;

import java.util.HashMap;

import de.mn77.base.data.constant.ALIGN;
import de.mn77.base.data.group.XY;
import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.data.tablestyle.calc.TableStyleCalc;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 20.10.2022
 *
 *          Default No Variants
 *          colAlign: _ LCR<^>
 *          colLines: . ' ',(0) |!12
 *          rowLines: * ' ' n-=
 *
 *          Possible rowLines: %+
 */
public class TableStyler {

	private boolean                       border              = false;
	private boolean                       unicode             = false;
	private final HashMap<XY, ALIGN>      cellAligns          = new HashMap<>();
	private Character                     colLineStyleDefault = null;
	private String                        colLineStyles       = null;
	private final HashMap<Integer, ALIGN> columnAligns        = new HashMap<>();
	private ALIGN                         defaultAlign        = null;
	private Integer                       maxColWidth         = null;
	private final HashMap<Integer, ALIGN> rowAligns           = new HashMap<>();
	private Character                     rowLineStyleDefault = null;
	private String                        rowLineStyles       = null;
	private int                           indent              = 0;


	public void alignCell( final int x, final int y, final ALIGN align ) {
		Err.ifTooSmall( 0, x );
		Err.ifTooSmall( 0, y );
		final XY p = new XY( x, y );
		this.cellAligns.put( p, align );
	}

	public void alignColumn( final int column, final ALIGN style ) {
		Err.ifTooSmall( 0, column );
		this.columnAligns.put( column, style );
	}

	public void alignDefault( final ALIGN align ) {
		this.defaultAlign = align;
	}

	public void alignRow( final int row, final ALIGN align ) {
		Err.ifTooSmall( 0, row );
		this.rowAligns.put( row, align );
	}

	public void colLineStyleDefault( final char cStyle ) {
		this.colLineStyleDefault = cStyle;
	}

	public void colLineStyles( final String cStyles ) {
		this.colLineStyles = cStyles;
	}

	public String compute( final I_Table<?> tab ) {
		final TableStyleCalc tsc = new TableStyleCalc( tab );
		tsc.alignDefault( this.defaultAlign );
		tsc.alignColumns( this.columnAligns );
		tsc.alignRows( this.rowAligns );
		tsc.alignCells( this.cellAligns );

		tsc.columnLineDefault( this.colLineStyleDefault );
		tsc.columnLines( this.colLineStyles );

		tsc.rowLineDefault( this.rowLineStyleDefault );
		tsc.rowLines( this.rowLineStyles );

		tsc.indent( this.indent );
		return tsc.compute( this.border, this.unicode, this.maxColWidth );
	}

	public void rowLineStyleDefault( final char rStyle ) {
		this.rowLineStyleDefault = rStyle;
	}

	public void rowLineStyles( final String rStyles ) {
		this.rowLineStyles = rStyles;
	}

	public void setBorder( final boolean b ) {
		this.border = b;
	}

	public void setIndent( final int indent ) {
		Err.ifTooSmall( 0, indent );
		this.indent = indent;
	}

	public void setMaxColumnWidth( final int w ) {
		Err.ifTooSmall( 1, this.indent );
		this.maxColWidth = w;
	}

	public void setUnicode( final boolean b ) {
		this.unicode = b;
	}

}
