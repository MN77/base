/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.tablestyle.calc;

import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 23.10.2022
 * @implNote ─│┌┐└┘├┤┬┴┼
 */
public class TableBorderCalc {

	private static final String COL_LINE_UNI        = "│";
	private static final String COL_LINE2_UNI       = " │ ";
	private static final String COL_LINE_ASC        = "|";
	private static final String COL_LINE2_ASC       = " | ";
	private final boolean       border;
	private final boolean       unicode;
	private final Character     columnLineDefault;
	private final String        columnLines;
	private final Character     rowLineDefault;
	private final String        rowLines;
	private final int[]         widths;
	private final int           tabLen;
	private final String[]      xCol;
	private final String[]      xRow;
	private final String        indent;
	private String              bufferRowLineSingle = null;
	private String              bufferRowLineEmpty  = null;
	private String              bufferRowLineDouble = null;


	public TableBorderCalc( final boolean border, final boolean unicode, final Character colDefault, final String cols, final Character rowDefault, final String rows, final int[] widths,
		final int tabLen, final String indent ) {
		this.border = border;
		this.unicode = unicode;
		this.columnLineDefault = colDefault;
		this.columnLines = cols;
		this.rowLineDefault = rowDefault;
		this.rowLines = rows;
		this.widths = widths;
		this.tabLen = tabLen;
		this.indent = indent;

		this.xCol = this.computeColLines();
		this.xRow = this.computeRowLines();
	}


	public void afterColumn( final StringBuilder sb, final int col ) {
		if( col >= this.widths.length - 1 )
			return;

		final String line = this.xCol[col];
		if( line != null )
			sb.append( line );
	}

	public boolean borderBottom( final StringBuilder sb ) {
		if( !this.border )
			return false;

		if( this.unicode )
			this.iComputeLine( sb, '└', '┘', '─', '┴', "─┴─" );
		else
			this.iComputeLine( sb, '\'', '\'', '-', '-', "---" );
		return true;
	}

	public void borderTop( final StringBuilder sb ) {
		if( !this.border )
			return;

		if( this.unicode )
			this.iComputeLine( sb, '┌', '┐', '─', '┬', "─┬─" );
		else
			this.iComputeLine( sb, '.', '.', '-', '-', "---" );
		sb.append( '\n' );
	}

	public void lineEnd( final StringBuilder sb, final int row, final boolean last ) {
		if( this.border )
			sb.append( this.unicode ? '│' : '|' );

		if( row != this.tabLen - 1 ) { // last line
			final String line = this.xRow[row];

			if( line != null && last ) {
				sb.append( '\n' );
				sb.append( line );
			}
		}

		sb.append( '\n' );
	}

	public void lineStart( final StringBuilder sb ) {
		if( this.border )
			sb.append( this.unicode ? '│' : '|' );
	}

	private String[] computeColLines() {
		final int lines = this.widths.length - 1;
		final String[] result = new String[lines];

		// Default
		if( this.columnLineDefault != null )
			for( int i = 0; i < lines; i++ )
				result[i] = this.iColLineStyle( this.columnLineDefault, null );

		// Specific
		if( this.columnLines != null ) {
			int max = lines;
			if( this.columnLines.length() < max )
				max = this.columnLines.length();

			for( int i = 0; i < max; i++ )
				result[i] = this.iColLineStyle( this.columnLines.charAt( i ), result[i] );
		}

		return result;
	}

	private String[] computeRowLines() {
		final int lines = this.tabLen - 1;
		final String[] result = new String[lines];

		// Default
		if( this.rowLineDefault != null )
			for( int i = 0; i < lines; i++ )
				result[i] = this.iRowLineStyle( this.rowLineDefault, null );

		// Specific
		if( this.rowLines != null ) {
			int max = lines;
			if( this.rowLines.length() < max )
				max = this.rowLines.length();

			for( int i = 0; i < max; i++ )
				result[i] = this.iRowLineStyle( this.rowLines.charAt( i ), result[i] );
		}

		return result;
	}

	private String iColLineStyle( final char c, final String def ) {

		switch( c ) {
			case '.':
				return def;
			case '|':
				return this.unicode ? TableBorderCalc.COL_LINE_UNI : TableBorderCalc.COL_LINE_ASC;
			case '!':
				return this.unicode ? TableBorderCalc.COL_LINE2_UNI : TableBorderCalc.COL_LINE2_ASC;
			case ' ':
			case '0':
				return "";
			case '1':
				return " ";
			case '2':
				return "  ";
//			case '3':
//				return "   ";

			default:
				throw Err.invalid( c );
		}
	}

	private void iComputeLine( final StringBuilder sb, final char left, final char right, final char space, final char cross1, final String cross3 ) {
		if( this.indent != null )
			sb.append( this.indent );
		if( this.border )
			sb.append( left );

		for( int i = 0; i < this.widths.length; i++ ) {

			if( i != 0 ) {
				final String colLine = this.xCol[i - 1];

				if( colLine != null )
					if( colLine.equals( this.unicode ? TableBorderCalc.COL_LINE_UNI : TableBorderCalc.COL_LINE_ASC ) )
						sb.append( cross1 );
					else if( colLine.equals( this.unicode ? TableBorderCalc.COL_LINE2_UNI : TableBorderCalc.COL_LINE2_ASC ) )
						sb.append( cross3 );
					else
						for( int t = 0; t < colLine.length(); t++ )
							sb.append( space );
			}

			sb.append( Lib_String.sequence( space, this.widths[i] ) );
		}

		if( this.border )
			sb.append( right );
	}

	private String iComputeRowLineDouble() {
		if( this.bufferRowLineDouble != null )
			return this.bufferRowLineDouble;

		final StringBuilder sb = new StringBuilder();
		if( this.unicode )
			this.iComputeLine( sb, '╞', '╡', '═', '╪', "═╪═" );
		else
			this.iComputeLine( sb, '|', '|', '=', '|', "=|=" );
		return this.bufferRowLineDouble = sb.toString();
	}

	private String iComputeRowLineSingle() {
		if( this.bufferRowLineSingle != null )
			return this.bufferRowLineSingle;

		final StringBuilder sb = new StringBuilder();
		if( this.unicode )
			this.iComputeLine( sb, '├', '┤', '─', '┼', "─┼─" );
		else
			this.iComputeLine( sb, '|', '|', '-', '|', "-|-" );
		return this.bufferRowLineSingle = sb.toString();
	}

	private String iComputeRowLineWhitespace() {
		if( this.bufferRowLineEmpty != null )
			return this.bufferRowLineEmpty;

		final StringBuilder sb = new StringBuilder();
		if( this.unicode )
			this.iComputeLine( sb, '│', '│', ' ', '│', " │ " );
		else
			this.iComputeLine( sb, '|', '|', ' ', '|', " | " );
		return this.bufferRowLineEmpty = sb.toString();
	}

	private String iRowLineStyle( final char c, final String def ) {

		switch( c ) {
			case '*':
				return def; // Default
			case '-':
				return this.iComputeRowLineSingle();
//			case '+':
			case 'n':
				return this.iComputeRowLineWhitespace();
			case '=':
				return this.iComputeRowLineDouble();
			case ' ':
				return null; // No line

			default:
				throw Err.invalid( c );
		}
	}

}
