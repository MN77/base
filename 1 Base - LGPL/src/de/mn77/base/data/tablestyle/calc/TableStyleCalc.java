/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.tablestyle.calc;

import java.util.HashMap;
import java.util.Map.Entry;

import de.mn77.base.data.constant.ALIGN;
import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.group.XY;
import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.data.util.Lib_Unicode;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 20.10.2022
 */
public class TableStyleCalc {

	private final I_Table<?> table;
	private final ALIGN[][]  aligns;
	private final int        columns;
	private final int        rows;
	private Character        colDefault = null;
	private Character        rowDefault = null;
	private String           colLines   = null;
	private String           rowLines   = null;
	private int              indent     = 0;


	public TableStyleCalc( final I_Table<?> table ) {
		this.table = table;
		this.columns = table.width();
		this.rows = table.size();
		this.aligns = new ALIGN[this.columns][this.rows];
	}


	public void alignCells( final HashMap<XY, ALIGN> defs ) {

		for( final Entry<XY, ALIGN> e : defs.entrySet() ) {
			final int row = e.getKey().y;
			final int col = e.getKey().x;
			Err.ifOutOfBounds( 0, this.columns - 1, col );
			Err.ifOutOfBounds( 0, this.rows - 1, row );

			this.aligns[col][row] = e.getValue();
		}
	}

	public void alignColumns( final HashMap<Integer, ALIGN> defs ) {

		for( final Entry<Integer, ALIGN> e : defs.entrySet() ) {
			final int col = e.getKey();
			Err.ifOutOfBounds( 0, this.columns - 1, col );

			for( int y = 0; y < this.rows; y++ )
				this.aligns[col][y] = e.getValue();
		}
	}

	public void alignDefault( final ALIGN style ) {
		for( int y = 0; y < this.rows; y++ )
			for( int x = 0; x < this.columns; x++ )
				this.aligns[x][y] = style;
	}

	public void alignRows( final HashMap<Integer, ALIGN> defs ) {

		for( final Entry<Integer, ALIGN> e : defs.entrySet() ) {
			final int row = e.getKey();
			Err.ifOutOfBounds( 0, this.rows - 1, row );

			for( int x = 0; x < this.columns; x++ )
				this.aligns[x][row] = e.getValue();
		}
	}

	public void columnLineDefault( final Character s ) {
		this.colDefault = s;
	}

	public void columnLines( final String s ) {
		this.colLines = s;
	}

	public String compute( final boolean border, final boolean unicode ) {
		return this.compute( border, unicode, null );
	}

	public String compute( final boolean border, final boolean unicode, final Integer maxColumnWidth ) {
		if( this.table.size() == 0 )
			return "";

		// Prepare
		final boolean limit = maxColumnWidth != null;
		final int[] colWidth = new int[this.columns];
		final String[][] stringTable = new String[this.columns][this.rows];
		final String indentString = this.indent > 0
			? Lib_String.sequence( ' ', this.indent )
			: null;

		for( int x = 0; x < this.columns; x++ ) {

			for( int y = 0; y < this.rows; y++ ) {
				final Object obj = this.table.get( x, y );
				stringTable[x][y] = this.iObjectToString( obj );

				// Set default align
				if( this.aligns[x][y] == null )
					this.aligns[x][y] = obj instanceof Number ? ALIGN.RIGHT : ALIGN.LEFT;
			}

			colWidth[x] = this.computeColumnWidth( stringTable, x );
			if( limit && colWidth[x] > maxColumnWidth )
				colWidth[x] = maxColumnWidth;
		}

		// Compute
		final TableBorderCalc bc = new TableBorderCalc( border, unicode, this.colDefault, this.colLines, this.rowDefault, this.rowLines, colWidth, this.rows, indentString );
		final StringBuilder sb = new StringBuilder();
		bc.borderTop( sb );

		for( int y = 0; y < this.rows; y++ ) {
			if( indentString != null )
				sb.append( indentString );

			final String[][] row = new String[this.columns][];
			int maxLines = 1;

			for( int x = 0; x < this.columns; x++ ) {
				row[x] = ConvertString.toStringArray( '\n', stringTable[x][y] );
				if( row[x].length > maxLines )
					maxLines = row[x].length;
			}

			for( int line = 0; line < maxLines; line++ ) {
				bc.lineStart( sb );

				for( int x = 0; x < this.columns; x++ ) {
					final String fieldLine = row[x].length <= line ? "" : row[x][line];
					final String s = Lib_Unicode.width( fieldLine, colWidth[x], ' ', this.aligns[x][y], limit );
					sb.append( s );

					bc.afterColumn( sb, x );
				}

				bc.lineEnd( sb, y, line == maxLines - 1 );
			}

//			bc.lineStart(sb);
//
//			for(int x = 0; x < this.columns; x++) {
//				final String s = Lib_Unicode.cpWidth(colWidth[x], ' ', stringTable[x][y], this.aligns[x][y], limit);
//				sb.append(s);
//
//				bc.afterColumn(sb, x);
//			}
//
//			bc.lineEnd(sb, y);
		}

		final boolean borderAdded = bc.borderBottom( sb );
		if( !borderAdded )
			sb.setLength( sb.length() - 1 );
		return sb.toString();
	}

	public void indent( final int indent ) {
		this.indent = indent;
	}

	public void rowLineDefault( final Character c ) {
		this.rowDefault = c;
	}

	public void rowLines( final String s ) {
		this.rowLines = s;
	}

	private int computeColumnWidth( final String[][] stringTable, final int x ) {
		Integer maxText = 0;

		for( int y = 0; y < this.rows; y++ ) {
			final String[] fieldLines = ConvertString.toStringArray( '\n', stringTable[x][y] );

			for( final String fieldLine : fieldLines ) {
				final int lineWidth = Lib_Unicode.length(fieldLine); // fieldLine.length();
				if( lineWidth > maxText )
					maxText = lineWidth;
			}
		}

		return maxText;
	}

	private String iObjectToString( final Object obj ) {
		return obj == null
			? "null"
			: obj.toString();
	}

}
