/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util;

import java.lang.reflect.Array;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class Lib_Array {

	/**
	 * @apiNote Creates a new array with length+1, where the new item will be appended at the last position.
	 */
	public static <T> T[] append( final Class<T> clazz, final T[] array, final T item ) {
		final int len = array.length;
		final T[] na = Lib_Array.newArray( clazz, len + 1 );
		System.arraycopy( array, 0, na, 0, len );
		na[len] = item;
		return na;
	}

	/**
	 * @apiNote Creates a new object array with length+1, where the new item will be appended at the last position.
	 */
	public static Object[] append( final Object[] array, final Object item ) {
		final int len = array.length;
		final Object[] na = new Object[len + 1];
		System.arraycopy( array, 0, na, 0, len );
		na[len] = item;
		return na;
	}

	/**
	 * @apiNote Creates a new array with length+1, where the new item will be inserted at the first position.
	 */
	public static <T> T[] begin( final Class<T> type, final T[] array, final T item ) {
		final int len = array.length;
		final T[] na = Lib_Array.newArray( type, len + 1 );
		System.arraycopy( array, 0, na, 1, len );
		na[0] = item;
		return na;
	}

	public static <T> T[] concat( final Class<T> type, final T value, final T[] array ) {
		final T[] result = Lib_Array.newArray( type, array.length + 1 );
		System.arraycopy( array, 0, result, 1, array.length );
		result[0] = value;
		return result;
	}

	public static <T> T[] concat( final Class<T> type, final T[] array, final T value ) {
		final T[] result = Lib_Array.newArray( type, array.length + 1 );
		System.arraycopy( array, 0, result, 0, array.length );
		result[result.length - 1] = value;
		return result;
	}

	/**
	 * @deprecated TODO Muß erst noch ausführlich getestet werden!!!
	 */
	@Deprecated
	public static <T> T[] concat( final Class<T> type, final T[] array1, final T[] array2 ) {
		Err.ifNull( type, array1, array2 );
		final T[] result = Lib_Array.newArray( type, array1.length + array2.length );
		System.arraycopy( array1, 0, result, 0, array1.length );
		System.arraycopy( array2, 0, result, array1.length, array2.length );
		return result;
	}

	public static boolean contains( final int[] array, final int search ) {
		for( final int i : array )
			if( i == search )
				return true;
		return false;
	}

	public static <T> boolean contains( final T[] array, final Object search ) {
		for( final T o : array )
			if( o == null && search == null || o != null && o.equals( search ) )
				return true;
		return false;
	}

	/**
	 * @return Returns true if one of the given array objects is not null and has a length greater zero.
	 */
	public static boolean containsData( final Object[]... array ) {
		for( final Object[] sa : array )
			if( sa != null && sa.length > 0 )
				return true;
		return false;
	}

	public static <T> T[] cut( final Class<T> type, final T[] source, final int start, final int length ) {
		final T[] result = Lib_Array.newArray( type, length );
		System.arraycopy( source, start - 1, result, 0, length );
		return result;
	}

	/**
	 * @return Returns the next duplicate value. If no duplicate value found, null will be returned.
	 */
	public static Integer getNextDuplicate( final int[] arr ) {
		Err.ifNull( arr );
		for( int i = 0; i < arr.length; i++ )
			for( int j = arr.length - 1; j > i; j-- )
				if( arr[i] == arr[j] )
					return arr[i];
		return null;
	}

	public static <T> int indexOf( final T search, final T[] arr ) {
		for( int i = 0; i < arr.length; i++ )
			if( arr[i].equals( search ) )
				return i;
		return -1;
	}

	/**
	 * @return Returns a int[] with all numbers from start to end.
	 */
	public static int[] intRange( final int start, final int end ) {
		Err.ifTooSmall( start, end );
		final int size = end - start + 1;
		final int[] result = new int[size];
		for( int i = 0; i < size; i++ )
			result[i] = i + start;
		return result;
	}

	//TODO Vielleicht performanter lösen!
	public static <T> T[] minus( final Class<T> type, final T[] array, final int index ) {
		final SimpleList<T> l = new SimpleList<>();
		l.addMore( array );
		l.removeIndex( index );
		return l.toArray( type );
	}

	@SuppressWarnings( "unchecked" )
	public static <T> T[] newArray( final Class<T> type, final int length ) {
		return (T[])Array.newInstance( type, length );
	}

	@SuppressWarnings( "unchecked" )
	public static <T> T[][] newArray2( final Class<T> type, final int length1, final int length2 ) {
		return (T[][])Array.newInstance( type, new int[]{ length1, length2 } );
	}

	public static <T> T[] removeIndex( final Class<T> type, final T[] array, final int idx ) {
		Err.ifTooSmall( 1, array.length );
		Err.ifOutOfBounds( 0, array.length - 1, idx );

		final T[] array2 = Lib_Array.newArray( type, array.length - 1 );
		if( idx > 0 )
			System.arraycopy( array, 0, array2, 0, idx );
		if( idx < array.length - 1 )
			;
		System.arraycopy( array, idx + 1, array2, idx, array.length - idx - 1 );
//		MOut.temp(array, idx, array2);
		return array2;
	}

	/**
	 * Removes every drop of this object from the array
	 */
	public static <T> T[] removeObject( final Class<T> type, final T[] array, final Object o ) {
		final SimpleList<T> al = new SimpleList<>( array.length );
		for( final T t : array )
			if( !t.equals( o ) )
				al.add( t );
		return al.toArray( Lib_Array.newArray( type, al.size() ) );
	}

	public static <T> void replace( final T[] arr, final T objOld, final T objNew ) {
		for( int i = 0; i < arr.length; i++ )
			if( arr[i].equals( objOld ) )
				arr[i] = objNew;
	}

	public static <TA> void reverse( final TA[] array ) {
		int left = 0;
		int right = array.length - 1;
		TA buffer = null;

		while( left < right ) {
			buffer = array[left];
			array[left] = array[right];
			array[right] = buffer;
			left++;
			right--;
		}
	}

	public static void sortGnome( final int[] arr, final boolean reverse ) {
		Err.ifNull( arr );
		final int len = arr.length;

		for( int changes = 1; changes > 0; ) {
			changes = 0;

			if( reverse ) {
				for( int l = len - 1; l >= 1; l-- )
					if( arr[l] > arr[l - 1] ) {
						final int buffer = arr[l];
						arr[l] = arr[l - 1];
						arr[l - 1] = buffer;
						changes++;
					}
			}
			else
				for( int l = 0; l < len - 1; l++ )
					if( arr[l] > arr[l + 1] ) {
						final int buffer = arr[l];
						arr[l] = arr[l + 1];
						arr[l + 1] = buffer;
						changes++;
					}
		}
	}

	public static <T> T[] unique( final Class<T> type, T[] arr ) {
		for( int i = 0; i < arr.length; i++ )
			for( int j = arr.length - 1; j > i; j-- )
				if( arr[i].equals( arr[j] ) )
					arr = Lib_Array.removeIndex( type, arr, j );
		return arr;
	}

}
