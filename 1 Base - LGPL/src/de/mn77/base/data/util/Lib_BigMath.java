/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;

import de.mn77.base.error.Err_Runtime;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 10.10.2021
 */
public class Lib_BigMath {

	public static final BigDecimal TWO   = BigDecimal.valueOf( 2 );
	public static final BigDecimal THREE = BigDecimal.valueOf( 3 );

	public static final BigDecimal EULER_256 = new BigDecimal(
		"2.7182818284590452353602874713526624977572470936999595749669676277240766303535475945713821785251664274274663919320030599218174135966290435729003342952605956307381323286279434907632338298807531952510190115738341879307021540891499348841675092447614606680822648" );

	// MathContext.DECIMAL128 == 34, HALF_EVEN
	public static final MathContext CONTEXT256  = new MathContext( 256 + 2, RoundingMode.HALF_EVEN );
	public static final MathContext CONTEXT1024 = new MathContext( 1024 + 2, RoundingMode.HALF_EVEN );


	public static void checkMathContextNoUnlimited( final MathContext mc ) {
		if( mc.getPrecision() == 0 )
			throw new UnsupportedOperationException( "Unlimited MathContext is not supported" );
	}

	public static BigInteger convertToBigInteger( final double v ) {
//		String vString = "" + v;
//		final int indexDot = vString.indexOf('.');
//		vString = indexDot > -1 ? vString.substring(0, indexDot) : vString;
//		return new BigInteger(vString);
		return BigDecimal.valueOf( v ).toBigInteger();
	}

	public static int convertToInt( final BigInteger value ) {
		if( value.compareTo( BigInteger.valueOf( Integer.MIN_VALUE ) ) == -1 || value.compareTo( BigInteger.valueOf( Integer.MAX_VALUE ) ) == 1 )
			throw new Err_Runtime( "Value out of Integer-Range", "" + value );
		return value.intValueExact();
	}

	public static BigDecimal euler( final int cycles, final MathContext mc ) {
		BigDecimal result = BigDecimal.ONE;
		BigDecimal f = BigDecimal.ONE;

		for( int cycle = 1; cycle <= cycles; cycle++ ) {
			f = f.multiply( BigDecimal.ONE.divide( new BigDecimal( cycle ), mc ), mc );
			if( f.signum() == 0 )
				break;
			else
				result = result.add( f, mc );
		}

		return result;
	}

	public static boolean isBetween( final BigDecimal value, final double min, final double max ) {
		final BigDecimal biMin = BigDecimal.valueOf( min );
		final BigDecimal biMax = BigDecimal.valueOf( max );
		return value.compareTo( biMin ) >= 0 && value.compareTo( biMax ) <= 0;
	}

	public static boolean isBetween( final BigInteger value, final long min, final long max ) {
		final BigInteger biMin = BigInteger.valueOf( min );
		final BigInteger biMax = BigInteger.valueOf( max );
		return value.compareTo( biMin ) >= 0 && value.compareTo( biMax ) <= 0;
	}

	public static BigDecimal log( final BigDecimal base, final BigDecimal x, final MathContext mc ) {
		Lib_BigMath.checkMathContextNoUnlimited( mc );
		BigDecimal input = x;

//		if(base.signum() <= 0)
//			throw new ArithmeticException("Illegal log with base: " + base);
//		if(base.compareTo(BigDecimal.ONE) == 0)
//			return BigDecimal.ZERO;

		BigDecimal result = BigDecimal.ZERO;
		BigDecimal inputOld = null;

		while( input.compareTo( base ) > 0 ) {
			result = result.add( BigDecimal.ONE );
			input = input.divide( base, mc );

			if( input == inputOld ) {
				MOut.dev( "Calculation error", "" + base + ".log(" + x + ")" );
				return BigDecimal.ZERO;
			}

			inputOld = input;
		}

		BigDecimal fraction = new BigDecimal( 0.5 );
		input = input.multiply( input, mc );
		String mem = "";

		while( result.add( fraction ).compareTo( result ) == 1 && input.compareTo( BigDecimal.ONE ) == 1 ) {

			if( input.compareTo( base ) == 1 ) {
				input = input.divide( base, mc );
				result = result.add( fraction, mc );

				final String plain = result.toPlainString();
				if( plain.equals( mem ) )
					return result;
				mem = plain;
			}

			input = input.multiply( input, mc );
			fraction = fraction.divide( new BigDecimal( 2 ), mc );
		}

		return Lib_BigMath.round( result, mc );
	}

	public static BigDecimal modulo( final BigDecimal left, final BigDecimal right ) {
		return left.remainder( right );
	}

	public static BigInteger pow( final long value, final int exp ) {
		final BigInteger bigValue = BigInteger.valueOf( value );
		return bigValue.pow( exp );
	}

	public static BigDecimal round( final BigDecimal value, final MathContext mc ) {
		return value.round( mc );
	}

}
