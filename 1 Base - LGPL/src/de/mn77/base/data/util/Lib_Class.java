/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util;

import java.lang.reflect.Constructor;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 12.12.2010
 */
public class Lib_Class {

	public static Class<?> getClassByPackage( final Iterable<String> packages, final String name ) {
		// --- Try imports ---
		for( final String pack : packages )
			try {
				return Class.forName( pack + "." + name );
			}
			catch( final ClassNotFoundException e ) {} // nothing found, try next

		// --- Try all available packages ---    // This is possible and working, but may return wrong classes!
//		final Package[] packages2 = Package.getPackages();
//		for(final Package p : packages2)
//			try {
//				return Class.forName(p.getName() + "." + name);
//			}
//			catch(final ClassNotFoundException e) {} // nothing found, try next

		// --- Nothing found ---
		return null;
	}

	public static Class<?> getClassByPath( final Iterable<String> classPathes, final String name ) {
		// --- Try imports ---
		for( final String path : classPathes )
			if( path.endsWith( "." + name ) )
				try {
					return Class.forName( path );
				}
				catch( final ClassNotFoundException e ) {
//					Err.exit(e);
					return null;
				}
		return null;
	}

	/**
	 * Returns the matching constructor
	 *
	 * @param type-class,
	 *            arguments
	 */
	public static Constructor<?> getConstructor( final Class<?> type, final Object[] args ) {
		final Constructor<?>[] cs = type.getConstructors();

		for( final Constructor<?> c : cs ) {
			final Class<?>[] cpars = c.getParameterTypes();

			if( cpars.length == args.length ) {
				boolean ok = true;

				for( int i = 0; i < cpars.length; i++ ) {
					final Class<?> cl = cpars[i];
					final Class<?> ocl = args[i].getClass();

					if( cl == ocl || Lib_Class.isAtomicValid( cl, ocl ) )
						continue;

//					if(!Lib_Class.descent(ocl, cl)) {
					if( !cl.isInstance( args[i] ) ) {
						ok = false;
						break;
					}
				}

				if( ok )
					return c;
			}
		}

		return null;
	}

	public static String getName( final Class<?> k ) {
		return k.getSimpleName();
	}

	public static String getName( final Object o ) {
		if( o == null )
			return "null";
		return Lib_Class.getName( o instanceof Class<?>
			? (Class<?>)o
			: o.getClass() );
	}

	/**
	 * DE: Stammt "objClass" von der Klasse "ofClass" ab?
	 * EN: true if objClass extends ofClass.
	 */
	public static boolean isChildOf( final Class<?> objClass, final Class<?> ofClass ) {
		return ofClass.isAssignableFrom( objClass );
	}

	/**
	 * DE: Stammt "obj" von der KLasse "ofClass" ab?
	 * EN: true if obj extends from ofClass.
	 */
	public static boolean isChildOf( final Object obj, final Class<?> ofClass ) {
		return ofClass.isAssignableFrom( obj.getClass() );
	}

	@SuppressWarnings( "unchecked" )
	public static <T> T newInstance( final Class<T> type, final Object... args ) {
		Err.ifNull( type, args );
		final Constructor<?> c = Lib_Class.getConstructor( type, args );
		if( c != null )
			try {
				return (T)c.newInstance( args );
			}
			catch( final Exception e ) {
				throw Err.exit( e, "Can't create the Instance!" );
			}

		throw Err.invalid( "No constructor found", type, args, type.getConstructors() );
	}

	private static boolean isAtomicValid( final Class<?> cl, final Class<?> ocl ) {
		if( !cl.isPrimitive() || ocl.isPrimitive() )
			return false;

		if( cl == boolean.class && ocl == Boolean.class || cl == byte.class && ocl == Byte.class )
			return true;
		if( cl == short.class && ocl == Short.class || cl == int.class && ocl == Integer.class )
			return true;
		if( cl == long.class && ocl == Long.class || cl == char.class && ocl == Character.class )
			return true;

		if( cl == float.class && ocl == Float.class || cl == double.class && ocl == Double.class )
			return true;

//		return false;
		throw Err.invalid( cl, ocl, ocl );
	}

}
