/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util;

import de.mn77.base.data.ComplexUnicodeChar;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class Lib_Compare {

	public static boolean isEqual( final Object o1, final Object o2 ) {
		if( o1 == null && o2 == null )
			return true;
		if( o1 == null || o2 == null || o1.getClass() != o2.getClass() )
			return false;

		// ".equals" must be the same as ".compareTo == 0"
//		if(o1 instanceof Comparable)
//			return ((Comparable<Object>)o1).compareTo(o2) == 0;
		return o1.equals( o2 );
	}

	@SuppressWarnings( "unchecked" )
	public static boolean isGreater( final Object base, final Object than ) {
		if( Lib_Compare.isEqual( base, than ) )
			return false;

		if( base != null && than == null )
			return true;
		if( base == null && than != null )
			return false;

		// Use own sorting routine for Character & String
		if( base instanceof Character && than instanceof Character )
			return Lib_Compare.isGreaterCodepoint( (char)base, (char)than );
		if( base instanceof String && than instanceof String )
			return Lib_Compare.isGreaterString( (String)base, (String)than );

		// Use Comparable
		if( base.getClass() == than.getClass() && base instanceof Comparable ) // Implizit:  && than instanceof Comparable
			return ((Comparable<Object>)base).compareTo( than ) > 0;

		// Default: Sort as String
		return Lib_Compare.isGreaterString( base.toString(), than.toString() );
	}

	public static boolean isGreaterCodepoint( final int cp1, final int cp2 ) {
		if( cp1 == cp2 )
			return false;
		else {
			final int v1 = Lib_Compare.iNormalizeCodepoint( cp1 );
			final int v2 = Lib_Compare.iNormalizeCodepoint( cp2 );

			if( v1 != v2 )
				return v1 > v2;

			final int greaterCase = Lib_Compare.iCompareCase( cp1, cp2 );

			if( greaterCase > 0 )
				return true;
			if( greaterCase < 0 )
				return false;

			return cp1 > cp2;
		}
	}

	public static boolean isGreaterChar( final ComplexUnicodeChar c1, final ComplexUnicodeChar c2 ) {
		if( c1 == c2 )
			return false;
		else if(c1.getCharCount() == 1 && c2.getCharCount() == 1)
			return Lib_Compare.isGreaterCodepoint(c1.getChars()[0], c2.getChars()[0]);
		else
			return c1.compareTo( c2 ) > 0;
	}

	public static boolean isGreaterString( final String s1, final String s2 ) {
		Err.ifNull( s1, s2 );
		final int min = Math.min( s1.length(), s2.length() );

		for( int i = 0; i < min; i++ ) {
			final char c1 = s1.charAt( i );
			final char c2 = s2.charAt( i );

			if( c1 != c2 )
				return Lib_Compare.isGreaterCodepoint( c1, c2 );
		}

		if( s1.length() != s2.length() )
			return s1.length() > s2.length();

		// If strings are equal:
//		return s1.compareTo(s2) > 0;
		return false;
	}

	public static boolean isSmaller( final Object base, final Object than ) {
		return Lib_Compare.isGreater( than, base );
	}

	private static int iCompareCase( final int cp1, final int cp2 ) {
		final boolean u1 = Character.isUpperCase( cp1 );
		final boolean u2 = Character.isUpperCase( cp2 );

		if( u1 == u2 )
			return 0;
		if( u1 && !u2 )
			return -1;
//		if(u2 && ! u1)
		return 1;
	}

	private static int iNormalizeCodepoint( final int cp ) {
		if( cp >= 'A' && cp <= 'Z' )
			return cp;
		if( cp >= 'a' && cp <= 'z' )
			return (char)(cp - 'a' + 'A');

		if( cp < 'À' )
			return cp;

		if( cp >= 'À' && cp <= 'Æ' )
			return 'A';
		if( cp == 'Ç' )
			return 'C';
		if( cp >= 'È' && cp <= 'Ë' )
			return 'E';
		if( cp >= 'Ì' && cp <= 'Ï' )
			return 'I';
		// Ð
		if( cp == 'Ñ' )
			return 'N';
		if( cp >= 'Ò' && cp <= 'Ö' ) // Ø
			return 'O';
		if( cp >= 'Ù' && cp <= 'Ü' )
			return 'U';
		if( cp == 'Ý' )
			return 'Y';
		if( cp == 'ẞ' )
			return 'S';

		if( cp >= 'à' && cp <= 'æ' )
			return 'A';
		if( cp == 'ç' )
			return 'C';
		if( cp >= 'è' && cp <= 'ë' )
			return 'E';
		if( cp >= 'ì' && cp <= 'ï' )
			return 'I';
		// ð
		if( cp == 'ñ' )
			return 'N';
		if( cp >= 'ò' && cp <= 'ö' ) // ø
			return 'O';
		if( cp >= 'ù' && cp <= 'ü' )
			return 'U';
		if( cp == 'ý' || cp == 'ÿ' )
			return 'Y';
		if( cp == 'ß' )
			return 'S';

		return cp;
	}

}
