/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util;

import de.mn77.base.data.form.FormString;
import de.mn77.base.error.Err_Exception;


/**
 * @author Michael Nitsche
 *         18.10.2010 Erstellt
 *
 *         BenutzerEingaben auf richtiges Format prüfen!
 *
 *         Evtl. auf anderen Fehlertyp umsteigen und verallgemeinern.
 *         Ist eine gute und einfache Sache!
 */
public class Lib_Compliance {

	public static boolean toBool( final String s ) {
		return Boolean.parseBoolean( s );
	}

	public static int toInt( final String s ) throws Err_Exception {

		try {
			return Integer.parseInt( s );
		}
		catch( final NumberFormatException e ) {
			throw new Err_Exception( "Ungültige Zahl", e );
		}
	}

	public static Integer toInteger( final String s ) throws Err_Exception {
		return s == null || s.equalsIgnoreCase( "null" )
			? null
			: Lib_Compliance.toInt( s );
	}

	public static String toStringSQL( final String s ) {
		return FormString.quoteForSQL( s );
	}

}
