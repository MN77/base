/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util;

import java.util.Map;
import java.util.Map.Entry;

import de.mn77.base.data.convert.ConvertObject;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 19.03.2021
 */
public class Lib_Describe {

	private static final String linebreak = System.lineSeparator();


	public static void describe( final Object o ) {
		MOut.print( Lib_Describe.toDescribe( o ) );
	}

	public static void ident( final Object o, final int size ) {
		MOut.print( Lib_Describe.toIdent( o, size ) );
	}

	public static String toDescribe( final Object o ) {
		final StringBuilder sb = new StringBuilder();
		sb.append( o.getClass().getName() + Lib_Describe.linebreak );
		Lib_Describe.iDescribe( sb, 1, o );
		return sb.toString();
	}

	public static String toIdent( final Object o, final int size ) {
		final StringBuilder sb = new StringBuilder();
		sb.append( o.getClass().getSimpleName() );
		sb.append( '<' );
		sb.append( size );
		sb.append( '>' );
		return sb.toString();
	}

	private static void iDescribe( final StringBuilder sb, final int level, final Object o ) {
		final String indent = Lib_String.sequence( ' ', level * 4 );
		if( o == null )
			sb.append( indent + "null" + Lib_Describe.linebreak );
		else if( o instanceof String || o instanceof Number || o instanceof Boolean )
			sb.append( indent + ConvertObject.toStringIdent( o ) + Lib_Describe.linebreak );
		else if( o instanceof Entry ) {
			sb.append( indent + ConvertObject.toStringIdent( ((Entry<?, ?>)o).getKey() ) + ":" + Lib_Describe.linebreak );
			Lib_Describe.iDescribe( sb, level + 1, ((Entry<?, ?>)o).getValue() );
		}

		else if( o instanceof String[] )
			for( final String i : (String[])o )
				Lib_Describe.iDescribe( sb, level + 1, i );

		else if( o instanceof byte[] )
			for( final byte i : (byte[])o )
				Lib_Describe.iDescribe( sb, level + 1, i );
		else if( o instanceof short[] )
			for( final short i : (short[])o )
				Lib_Describe.iDescribe( sb, level + 1, i );
		else if( o instanceof int[] )
			for( final int i : (int[])o )
				Lib_Describe.iDescribe( sb, level + 1, i );
		else if( o instanceof long[] )
			for( final long i : (long[])o )
				Lib_Describe.iDescribe( sb, level + 1, i );
		else if( o instanceof float[] )
			for( final float i : (float[])o )
				Lib_Describe.iDescribe( sb, level + 1, i );
		else if( o instanceof double[] )
			for( final double i : (double[])o )
				Lib_Describe.iDescribe( sb, level + 1, i );

		else if( o instanceof Byte[] )
			for( final Byte i : (Byte[])o )
				Lib_Describe.iDescribe( sb, level + 1, i );
		else if( o instanceof Short[] )
			for( final Short i : (Short[])o )
				Lib_Describe.iDescribe( sb, level + 1, i );
		else if( o instanceof Integer[] )
			for( final Integer i : (Integer[])o )
				Lib_Describe.iDescribe( sb, level + 1, i );
		else if( o instanceof Long[] )
			for( final Long i : (Long[])o )
				Lib_Describe.iDescribe( sb, level + 1, i );
		else if( o instanceof Float[] )
			for( final Float i : (Float[])o )
				Lib_Describe.iDescribe( sb, level + 1, i );
		else if( o instanceof Double[] )
			for( final Double i : (Double[])o )
				Lib_Describe.iDescribe( sb, level + 1, i );
		else if( o instanceof Map )
			for( final Entry<?, ?> e : ((Map<?, ?>)o).entrySet() )
				Lib_Describe.iDescribe( sb, level, e );
		else if( o instanceof Iterable )
			for( final Object e : (Iterable<?>)o )
				Lib_Describe.iDescribe( sb, level, e );
		else
//			Err.invalid( o.getClass().getSimpleName() );
			sb.append( indent + ConvertObject.toStringIdent( o ) + Lib_Describe.linebreak );
	}

}
