/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util;

import java.math.BigDecimal;

import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 29.12.2022
 */
public class Lib_E_Notation {

	public static void main( final String[] args ) {
		MOut.print(
			Lib_E_Notation.toNotationE( "1234567890123456789012345678901234567890", 5 ),
			Lib_E_Notation.toNotationE( "12345678901234567890.12345678901234567890", 5 ),
			Lib_E_Notation.toNotationE( "1.234567890123456789012345678901234567890", 5 ),
			Lib_E_Notation.toNotationE( "0.1234567890123456789012345678901234567890", 5 ),
			Lib_E_Notation.toNotationE( "0.0001234567890123456789012345678901234567890", 5 ),
			Lib_E_Notation.toNotationE( "-0.0001234567890123456789012345678901234567890", 5 ),
			Lib_E_Notation.toNotationE( "12", 5 ),
			Lib_E_Notation.toNotationE( "12.34", 5 ) );
	}


	/**
	 * @implNote
	 *           Upper case 'E' is the correct notation, 'e' is better to read but can confuse because of the mathematical constant e or the exponential function e^x
	 */
	public static String toNotationE( String number, final int decimals ) {
		final StringBuilder sb = new StringBuilder();

		if( number.startsWith( "-" ) ) {
			number = number.substring( 1 );
			sb.append( '-' );
		}

		boolean leadingZeros = true;
		boolean beforeDot = true;
		int outNums = 0;
		int e = -1; // Ignore first number

		for( int i = 0; i < number.length(); i++ ) {
			final char c = number.charAt( i );

			if( c == '.' ) {
				beforeDot = false;
				continue;
			}

			if( leadingZeros && c != '0' )
				leadingZeros = false;

			// --------------------------------------

			if( !leadingZeros && outNums <= decimals ) {
				sb.append( c );
				outNums++;
			}

			if( !leadingZeros && outNums == 1 )
				sb.append( '.' );

			// --------------------------------------

			if( leadingZeros )
				e--;
			if( beforeDot )
				e++;
		}

		while( !beforeDot && sb.length() > 3 && sb.charAt( sb.length() - 1 ) == '0' )
			sb.setLength( sb.length() - 1 );

		sb.append( 'E' );
		sb.append( e );
		return sb.toString();
	}

	public static String unfold( final String num ) { // TODO Speed this up
		return new BigDecimal( num ).toPlainString();
	}

}
