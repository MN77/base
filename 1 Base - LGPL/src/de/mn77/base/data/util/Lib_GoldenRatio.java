/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util;

import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 25.04.2021
 *
 *          Goldener Schnitt / Golden Ratio
 *          Zwei Strecken stehen im Verhältnis des Goldenen Schnittes, wenn sich die größere zur kleineren Strecke verhält wie die Summe aus beiden zur größeren.
 *          = 1.618033988749895D
 */
public class Lib_GoldenRatio {

	public static final double PHI = (1 + Math.sqrt( 5 )) / 2;


	public static int fullFromLongSideAsInt( final int value ) {
		return (int)Math.round( value * Lib_GoldenRatio.PHI );
	}

	public static int longSideAsInt( final int value ) {
		return (int)Math.round( value / Lib_GoldenRatio.PHI );
	}

	public static void main( final String[] args ) {
		MOut.print(
			Lib_GoldenRatio.PHI,
			Lib_GoldenRatio.longSideAsInt( 600 ),
			Lib_GoldenRatio.shortSideAsInt( 600 ),
			Lib_GoldenRatio.fullFromLongSideAsInt( 371 )
//			Lib_GoldenRatio.shortFromLongSideAsInt(600)
		);
	}

	public static int shortSideAsInt( final int value ) {
		return value - Lib_GoldenRatio.longSideAsInt( value );
	}

//	public static int shortFromLongSideAsInt(int value) {
//		return (int)Math.round(value * Lib_GoldenRatio.PHI)-value;
//	}

}
