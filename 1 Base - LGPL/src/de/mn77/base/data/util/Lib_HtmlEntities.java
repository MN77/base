/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util;

/**
 * @author Michael Nitsche
 * @created 14.05.2022
 */
public class Lib_HtmlEntities {

	public static String charToHtml( final char c ) {

		switch( c ) {
			case 'Â':
				return "&Acirc;";
			case 'â':
				return "&acirc;";
			case '´':
				return "&acute;";
			case 'Æ':
				return "&AElig;";
			case 'æ':
				return "&aelig;";
			case 'À':
				return "&Agrave;";
			case 'à':
				return "&agrave;";
			case 'ℵ':
				return "&alefsym;";
			case 'Α':
				return "&Alpha;";
			case 'α':
				return "&alpha;";
			case '&':
				return "&amp;";
			case '∧':
				return "&and;";
			case '∠':
				return "&ang;";
			case '\'':
				return "&apos;";
			case 'Å':
				return "&Aring;";
			case 'å':
				return "&aring;";
			case '≈':
				return "&asymp;";
			case 'Ã':
				return "&Atilde;";
			case 'ã':
				return "&atilde;";
			case 'Ä':
				return "&Auml;";
			case 'ä':
				return "&auml;";
			case '„':
				return "&bdquo;";
			case 'Β':
				return "&Beta;";
			case 'β':
				return "&beta;";
			case '¦':
				return "&brvbar;";
			case '•':
				return "&bull;";
			case '∩':
				return "&cap;";
			case 'Ç':
				return "&Ccedil;";
			case 'ç':
				return "&ccedil;";
			case '¸':
				return "&cedil;";
			case '¢':
				return "&cent;";
			case 'Χ':
				return "&Chi;";
			case 'χ':
				return "&chi;";
			case 'ˆ':
				return "&circ;";
			case '♣':
				return "&clubs;";
			case '≅':
				return "&cong;";
			case '©':
				return "&copy;";
			case '↵':
				return "&crarr;";
			case '∪':
				return "&cup;";
			case '¤':
				return "&curren;";
			case '‡':
				return "&Dagger;";
			case '†':
				return "&dagger;";
			case '⇓':
				return "&dArr;";
			case '↓':
				return "&darr;";
			case '°':
				return "&deg;";
			case 'Δ':
				return "&Delta;";
			case 'δ':
				return "&delta;";
			case '♦':
				return "&diams;";
			case '÷':
				return "&divide;";
			case 'É':
				return "&Eacute;";
			case 'é':
				return "&eacute;";
			case 'Ê':
				return "&Ecirc;";
			case 'ê':
				return "&ecirc;";
			case 'È':
				return "&Egrave;";
			case 'è':
				return "&egrave;";
			case '∅':
				return "&empty;";
			case ' ':
				return "&emsp;";
			case ' ':
				return "&ensp;";
			case 'Ε':
				return "&Epsilon;";
			case 'ε':
				return "&epsilon;";
			case '≡':
				return "&equiv;";
			case 'Η':
				return "&Eta;";
			case 'η':
				return "&eta;";
			case 'Ð':
				return "&ETH;";
			case 'ð':
				return "&eth;";
			case 'Ë':
				return "&Euml;";
			case 'ë':
				return "&euml;";
			case '€':
				return "&euro;";
			case '∃':
				return "&exist;";
			case 'ƒ':
				return "&fnof;";
			case '∀':
				return "&forall;";
			case '½':
				return "&frac12;";
			case '¼':
				return "&frac14;";
			case '¾':
				return "&frac34;";
			case '⁄':
				return "&frasl;";
			case 'Γ':
				return "&Gamma;";
			case 'γ':
				return "&gamma;";
			case '≥':
				return "&ge;";
			case '>':
				return "&gt;";
			case '⇔':
				return "&hArr;";
			case '↔':
				return "&harr;";
			case '♥':
				return "&hearts;";
			case '…':
				return "&hellip;";
			case 'Í':
				return "&Iacute;";
			case 'í':
				return "&iacute;";
			case 'Î':
				return "&Icirc;";
			case 'î':
				return "&icirc;";
			case '¡':
				return "&iexcl;";
			case 'Ì':
				return "&Igrave;";
			case 'ì':
				return "&igrave;";
			case 'ℑ':
				return "&image;";
			case '∞':
				return "&infin;";
			case '∫':
				return "&int;";
			case 'Ι':
				return "&Iota;";
			case 'ι':
				return "&iota;";
			case '¿':
				return "&iquest;";
			case '∈':
				return "&isin;";
			case 'Ï':
				return "&Iuml;";
			case 'ï':
				return "&iuml;";
			case 'Κ':
				return "&Kappa;";
			case 'κ':
				return "&kappa;";
			case 'Λ':
				return "&Lambda;";
			case 'λ':
				return "&lambda;";
			case '⟨':
				return "&lang;";
			case '«':
				return "&laquo;";
			case '⇐':
				return "&lArr;";
			case '←':
				return "&larr;";
			case '⌈':
				return "&lceil;";
			case '“':
				return "&ldquo;";
			case '≤':
				return "&le;";
			case '⌊':
				return "&lfloor;";
			case '∗':
				return "&lowast;";
			case '◊':
				return "&loz;";
			case '‎':
				return "&lrm;";
			case '‹':
				return "&lsaquo;";
			case '‘':
				return "&lsquo;";
			case '<':
				return "&lt;";
			case '¯':
				return "&macr;";
			case '—':
				return "&mdash;";
			case 'µ':
				return "&micro;";
			case '·':
				return "&middot;";
			case '−':
				return "&minus;";
			case 'Μ':
				return "&Mu;";
			case 'μ':
				return "&mu;";
			case '∇':
				return "&nabla;";
			case ' ':
				return "&nbsp;";
			case '–':
				return "&ndash;";
			case '≠':
				return "&ne;";
			case '∋':
				return "&ni;";
			case '¬':
				return "&not;";
			case '∉':
				return "&notin;";
			case '⊄':
				return "&nsub;";
			case 'Ñ':
				return "&Ntilde;";
			case 'ñ':
				return "&ntilde;";
			case 'Ν':
				return "&Nu;";
			case 'ν':
				return "&nu;";
			case 'Ó':
				return "&Oacute;";
			case 'ó':
				return "&oacute;";
			case 'Ô':
				return "&Ocirc;";
			case 'ô':
				return "&ocirc;";
			case 'Œ':
				return "&OElig;";
			case 'œ':
				return "&oelig;";
			case 'Ò':
				return "&Ograve;";
			case 'ò':
				return "&ograve;";
			case '‾':
				return "&oline;";
			case 'Ω':
				return "&Omega;";
			case 'ω':
				return "&omega;";
			case 'Ο':
				return "&Omicron;";
			case 'ο':
				return "&omicron;";
			case '⊕':
				return "&oplus;";
			case '∨':
				return "&or;";
			case 'ª':
				return "&ordf;";
			case 'º':
				return "&ordm;";
			case 'Ø':
				return "&Oslash;";
			case 'ø':
				return "&oslash;";
			case 'Õ':
				return "&Otilde;";
			case 'õ':
				return "&otilde;";
			case '⊗':
				return "&otimes;";
			case 'Ö':
				return "&Ouml;";
			case 'ö':
				return "&ouml;";
			case '¶':
				return "&para;";
			case '∂':
				return "&part;";
			case '‰':
				return "&permil;";
			case '⊥':
				return "&perp;";
			case 'Φ':
				return "&Phi;";
			case 'φ':
				return "&phi;";
			case 'Π':
				return "&Pi;";
			case 'π':
				return "&pi;";
			case 'ϖ':
				return "&piv;";
			case '±':
				return "&plusmn;";
			case '£':
				return "&pound;";
			case '″':
				return "&Prime;";
			case '′':
				return "&prime;";
			case '∏':
				return "&prod;";
			case '∝':
				return "&prop;";
			case 'Ψ':
				return "&Psi;";
			case 'ψ':
				return "&psi;";
			case '"':
				return "&quot;";
			case '√':
				return "&radic;";
			case '⟩':
				return "&rang;";
			case '»':
				return "&raquo;";
			case '⇒':
				return "&rArr;";
			case '→':
				return "&rarr;";
			case '⌉':
				return "&rceil;";
			case '”':
				return "&rdquo;";
			case 'ℜ':
				return "&real;";
			case '®':
				return "&reg;";
			case '⌋':
				return "&rfloor;";
			case 'Ρ':
				return "&Rho;";
			case 'ρ':
				return "&rho;";
			case '‏':
				return "&rlm;";
			case '›':
				return "&rsaquo;";
			case '’':
				return "&rsquo;";
			case '‚':
				return "&sbquo;";
			case 'Š':
				return "&Scaron;";
			case 'š':
				return "&scaron;";
			case '⋅':
				return "&sdot;";
			case '§':
				return "&sect;";
			case '­':
				return "&shy;";
			case 'Σ':
				return "&Sigma;";
			case 'σ':
				return "&sigma;";
			case 'ς':
				return "&sigmaf;";
			case '∼':
				return "&sim;";
			case '♠':
				return "&spades;";
			case '⊂':
				return "&sub;";
			case '⊆':
				return "&sube;";
			case '∑':
				return "&sum;";
			case '⊃':
				return "&sup;";
			case '¹':
				return "&sup1;";
			case '²':
				return "&sup2;";
			case '³':
				return "&sup3;";
			case '⊇':
				return "&supe;";
			case 'ß':
				return "&szlig;";
			case 'Τ':
				return "&Tau;";
			case 'τ':
				return "&tau;";
			case '∴':
				return "&there4;";
			case 'Θ':
				return "&Theta;";
			case 'θ':
				return "&theta;";
			case 'ϑ':
				return "&thetasym;";
			case ' ':
				return "&thinsp;";
			case 'Þ':
				return "&THORN;";
			case 'þ':
				return "&thorn;";
			case '˜':
				return "&tilde;";
			case '×':
				return "&times;";
			case '™':
				return "&trade;";
			case 'Ú':
				return "&Uacute;";
			case 'ú':
				return "&uacute;";
			case '⇑':
				return "&uArr;";
			case '↑':
				return "&uarr;";
			case 'Û':
				return "&Ucirc;";
			case 'û':
				return "&ucirc;";
			case 'Ù':
				return "&Ugrave;";
			case 'ù':
				return "&ugrave;";
			case '¨':
				return "&uml;";
			case 'ϒ':
				return "&upsih;";
			case 'Υ':
				return "&Upsilon;";
			case 'υ':
				return "&upsilon;";
			case 'Ü':
				return "&Uuml;";
			case 'ü':
				return "&uuml;";
			case '℘':
				return "&weierp;";
			case 'Ξ':
				return "&Xi;";
			case 'ξ':
				return "&xi;";
			case 'Ý':
				return "&Yacute;";
			case 'ý':
				return "&yacute;";
			case '¥':
				return "&yen;";
			case 'Ÿ':
				return "&Yuml;";
			case 'ÿ':
				return "&yuml;";
			case 'Ζ':
				return "&Zeta;";
			case 'ζ':
				return "&zeta;";
			case '‍':
				return "&zwj;";
			case '‌':
				return "&zwnj;";

			default:
				return null;
		}
	}

	public static Character htmlToChar( final String entity ) {

		switch( entity ) {
			case "&Acirc;":
				return 'Â';
			case "&acirc;":
				return 'â';
			case "&acute;":
				return '´';
			case "&AElig;":
				return 'Æ';
			case "&aelig;":
				return 'æ';
			case "&Agrave;":
				return 'À';
			case "&agrave;":
				return 'à';
			case "&alefsym;":
				return 'ℵ';
			case "&Alpha;":
				return 'Α';
			case "&alpha;":
				return 'α';
			case "&amp;":
				return '&';
			case "&and;":
				return '∧';
			case "&ang;":
				return '∠';
			case "&apos;":
				return '\'';
			case "&Aring;":
				return 'Å';
			case "&aring;":
				return 'å';
			case "&asymp;":
				return '≈';
			case "&Atilde;":
				return 'Ã';
			case "&atilde;":
				return 'ã';
			case "&Auml;":
				return 'Ä';
			case "&auml;":
				return 'ä';
			case "&bdquo;":
				return '„';
			case "&Beta;":
				return 'Β';
			case "&beta;":
				return 'β';
			case "&brvbar;":
				return '¦';
			case "&bull;":
				return '•';
			case "&cap;":
				return '∩';
			case "&Ccedil;":
				return 'Ç';
			case "&ccedil;":
				return 'ç';
			case "&cedil;":
				return '¸';
			case "&cent;":
				return '¢';
			case "&Chi;":
				return 'Χ';
			case "&chi;":
				return 'χ';
			case "&circ;":
				return 'ˆ';
			case "&clubs;":
				return '♣';
			case "&cong;":
				return '≅';
			case "&copy;":
				return '©';
			case "&crarr;":
				return '↵';
			case "&cup;":
				return '∪';
			case "&curren;":
				return '¤';
			case "&Dagger;":
				return '‡';
			case "&dagger;":
				return '†';
			case "&dArr;":
				return '⇓';
			case "&darr;":
				return '↓';
			case "&deg;":
				return '°';
			case "&Delta;":
				return 'Δ';
			case "&delta;":
				return 'δ';
			case "&diams;":
				return '♦';
			case "&divide;":
				return '÷';
			case "&Eacute;":
				return 'É';
			case "&eacute;":
				return 'é';
			case "&Ecirc;":
				return 'Ê';
			case "&ecirc;":
				return 'ê';
			case "&Egrave;":
				return 'È';
			case "&egrave;":
				return 'è';
			case "&empty;":
				return '∅';
			case "&emsp;":
				return ' ';
			case "&ensp;":
				return ' ';
			case "&Epsilon;":
				return 'Ε';
			case "&epsilon;":
				return 'ε';
			case "&equiv;":
				return '≡';
			case "&Eta;":
				return 'Η';
			case "&eta;":
				return 'η';
			case "&ETH;":
				return 'Ð';
			case "&eth;":
				return 'ð';
			case "&Euml;":
				return 'Ë';
			case "&euml;":
				return 'ë';
			case "&euro;":
				return '€';
			case "&exist;":
				return '∃';
			case "&fnof;":
				return 'ƒ';
			case "&forall;":
				return '∀';
			case "&frac12;":
				return '½';
			case "&frac14;":
				return '¼';
			case "&frac34;":
				return '¾';
			case "&frasl;":
				return '⁄';
			case "&Gamma;":
				return 'Γ';
			case "&gamma;":
				return 'γ';
			case "&ge;":
				return '≥';
			case "&gt;":
				return '>';
			case "&hArr;":
				return '⇔';
			case "&harr;":
				return '↔';
			case "&hearts;":
				return '♥';
			case "&hellip;":
				return '…';
			case "&Iacute;":
				return 'Í';
			case "&iacute;":
				return 'í';
			case "&Icirc;":
				return 'Î';
			case "&icirc;":
				return 'î';
			case "&iexcl;":
				return '¡';
			case "&Igrave;":
				return 'Ì';
			case "&igrave;":
				return 'ì';
			case "&image;":
				return 'ℑ';
			case "&infin;":
				return '∞';
			case "&int;":
				return '∫';
			case "&Iota;":
				return 'Ι';
			case "&iota;":
				return 'ι';
			case "&iquest;":
				return '¿';
			case "&isin;":
				return '∈';
			case "&Iuml;":
				return 'Ï';
			case "&iuml;":
				return 'ï';
			case "&Kappa;":
				return 'Κ';
			case "&kappa;":
				return 'κ';
			case "&Lambda;":
				return 'Λ';
			case "&lambda;":
				return 'λ';
			case "&lang;":
				return '⟨';
			case "&laquo;":
				return '«';
			case "&lArr;":
				return '⇐';
			case "&larr;":
				return '←';
			case "&lceil;":
				return '⌈';
			case "&ldquo;":
				return '“';
			case "&le;":
				return '≤';
			case "&lfloor;":
				return '⌊';
			case "&lowast;":
				return '∗';
			case "&loz;":
				return '◊';
			case "&lrm;":
				return '‎';
			case "&lsaquo;":
				return '‹';
			case "&lsquo;":
				return '‘';
			case "&lt;":
				return '<';
			case "&macr;":
				return '¯';
			case "&mdash;":
				return '—';
			case "&micro;":
				return 'µ';
			case "&middot;":
				return '·';
			case "&minus;":
				return '−';
			case "&Mu;":
				return 'Μ';
			case "&mu;":
				return 'μ';
			case "&nabla;":
				return '∇';
			case "&nbsp;":
				return ' ';
			case "&ndash;":
				return '–';
			case "&ne;":
				return '≠';
			case "&ni;":
				return '∋';
			case "&not;":
				return '¬';
			case "&notin;":
				return '∉';
			case "&nsub;":
				return '⊄';
			case "&Ntilde;":
				return 'Ñ';
			case "&ntilde;":
				return 'ñ';
			case "&Nu;":
				return 'Ν';
			case "&nu;":
				return 'ν';
			case "&Oacute;":
				return 'Ó';
			case "&oacute;":
				return 'ó';
			case "&Ocirc;":
				return 'Ô';
			case "&ocirc;":
				return 'ô';
			case "&OElig;":
				return 'Œ';
			case "&oelig;":
				return 'œ';
			case "&Ograve;":
				return 'Ò';
			case "&ograve;":
				return 'ò';
			case "&oline;":
				return '‾';
			case "&Omega;":
				return 'Ω';
			case "&omega;":
				return 'ω';
			case "&Omicron;":
				return 'Ο';
			case "&omicron;":
				return 'ο';
			case "&oplus;":
				return '⊕';
			case "&or;":
				return '∨';
			case "&ordf;":
				return 'ª';
			case "&ordm;":
				return 'º';
			case "&Oslash;":
				return 'Ø';
			case "&oslash;":
				return 'ø';
			case "&Otilde;":
				return 'Õ';
			case "&otilde;":
				return 'õ';
			case "&otimes;":
				return '⊗';
			case "&Ouml;":
				return 'Ö';
			case "&ouml;":
				return 'ö';
			case "&para;":
				return '¶';
			case "&part;":
				return '∂';
			case "&permil;":
				return '‰';
			case "&perp;":
				return '⊥';
			case "&Phi;":
				return 'Φ';
			case "&phi;":
				return 'φ';
			case "&Pi;":
				return 'Π';
			case "&pi;":
				return 'π';
			case "&piv;":
				return 'ϖ';
			case "&plusmn;":
				return '±';
			case "&pound;":
				return '£';
			case "&Prime;":
				return '″';
			case "&prime;":
				return '′';
			case "&prod;":
				return '∏';
			case "&prop;":
				return '∝';
			case "&Psi;":
				return 'Ψ';
			case "&psi;":
				return 'ψ';
			case "&quot;":
				return '"';
			case "&radic;":
				return '√';
			case "&rang;":
				return '⟩';
			case "&raquo;":
				return '»';
			case "&rArr;":
				return '⇒';
			case "&rarr;":
				return '→';
			case "&rceil;":
				return '⌉';
			case "&rdquo;":
				return '”';
			case "&real;":
				return 'ℜ';
			case "&reg;":
				return '®';
			case "&rfloor;":
				return '⌋';
			case "&Rho;":
				return 'Ρ';
			case "&rho;":
				return 'ρ';
			case "&rlm;":
				return '‏';
			case "&rsaquo;":
				return '›';
			case "&rsquo;":
				return '’';
			case "&sbquo;":
				return '‚';
			case "&Scaron;":
				return 'Š';
			case "&scaron;":
				return 'š';
			case "&sdot;":
				return '⋅';
			case "&sect;":
				return '§';
			case "&shy;":
				return '­';
			case "&Sigma;":
				return 'Σ';
			case "&sigma;":
				return 'σ';
			case "&sigmaf;":
				return 'ς';
			case "&sim;":
				return '∼';
			case "&spades;":
				return '♠';
			case "&sub;":
				return '⊂';
			case "&sube;":
				return '⊆';
			case "&sum;":
				return '∑';
			case "&sup;":
				return '⊃';
			case "&sup1;":
				return '¹';
			case "&sup2;":
				return '²';
			case "&sup3;":
				return '³';
			case "&supe;":
				return '⊇';
			case "&szlig;":
				return 'ß';
			case "&Tau;":
				return 'Τ';
			case "&tau;":
				return 'τ';
			case "&there4;":
				return '∴';
			case "&Theta;":
				return 'Θ';
			case "&theta;":
				return 'θ';
			case "&thetasym;":
				return 'ϑ';
			case "&thinsp;":
				return ' ';
			case "&THORN;":
				return 'Þ';
			case "&thorn;":
				return 'þ';
			case "&tilde;":
				return '˜';
			case "&times;":
				return '×';
			case "&trade;":
				return '™';
			case "&Uacute;":
				return 'Ú';
			case "&uacute;":
				return 'ú';
			case "&uArr;":
				return '⇑';
			case "&uarr;":
				return '↑';
			case "&Ucirc;":
				return 'Û';
			case "&ucirc;":
				return 'û';
			case "&Ugrave;":
				return 'Ù';
			case "&ugrave;":
				return 'ù';
			case "&uml;":
				return '¨';
			case "&upsih;":
				return 'ϒ';
			case "&Upsilon;":
				return 'Υ';
			case "&upsilon;":
				return 'υ';
			case "&Uuml;":
				return 'Ü';
			case "&uuml;":
				return 'ü';
			case "&weierp;":
				return '℘';
			case "&Xi;":
				return 'Ξ';
			case "&xi;":
				return 'ξ';
			case "&Yacute;":
				return 'Ý';
			case "&yacute;":
				return 'ý';
			case "&yen;":
				return '¥';
			case "&Yuml;":
				return 'Ÿ';
			case "&yuml;":
				return 'ÿ';
			case "&Zeta;":
				return 'Ζ';
			case "&zeta;":
				return 'ζ';
			case "&zwj;":
				return '‍';
			case "&zwnj;":
				return '‌';

			default:
				return null;
		}
	}

}
