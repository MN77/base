/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Locale;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @license LGPL
 */
public class Lib_Math {

	//--- Sequence of numbers ---

	public static float abs( final float value ) {
		return Math.abs( value );
	}

	public static int abs( final int value ) {
		return Math.abs( value );
	}

	public static long abs( final long value ) {
		return Math.abs( value );
	}

	public static double absBetrag( final double value ) {
		return Math.abs( value );
	}

	public static int absBetrag( final int value ) {
		return Math.abs( value );
	}

	@SuppressWarnings( "unchecked" )
	public static <T extends Number> double average( final T... numbers ) {
		Err.ifTooSmall( 1, numbers.length );
		double result = 0d;
		for( final T num : numbers )
			result += num.doubleValue() / numbers.length;
		return result;
	}


	//--- Round ---

	/**
	 * DE: Größte Abeichung vom Durchschnitt
	 */
	@SuppressWarnings( "unchecked" )
	public static <T extends Number> T biggestDiffFromAvg( final T... numbers ) {
		final double center = Lib_Math.average( numbers );
		T result = numbers[0];
		double resultDiff = 0;
		for( final T num : numbers )
			if( Lib_Math.absBetrag( center - num.doubleValue() ) > resultDiff || resultDiff == 0 ) {
				resultDiff = Lib_Math.absBetrag( center - num.doubleValue() );
				result = num;
			}
		return result;
	}

	public static boolean bitIsSet( final int value, final int bits ) {
		return (value & bits) == bits;
	}

	public static int bitRemove( final int value, final int bits ) {
		return (value | bits) ^ bits;
	}

	public static int bitSet( final int value, final int bits ) {
		return value | bits;
	}

	public static int bitsNeeded( final long value ) {
		Err.ifTooSmall( 0, value ); // TODO
		int result = 0;
		int curBit = 0;

		while( value >= curBit ) {
			if( curBit == 0 )
				curBit = 1;
			curBit *= 2;
			result++;
		}

		return result;
	}

	/**
	 * @apiNote Returns the bitvalue, where values[0] = 1, [1] = 2, [2] = 4, [3] = 8, ...
	 */
	public static int bitValue( final boolean... values ) {
		int result = 0;
		for( int i = 0; i < values.length; i++ )
			result += values[i] ? 1 << i : 0;
		return result;
	}

	/**
	 * DE: Quersumme
	 */
	public static int checksum( long l ) {
		l = Math.abs( l );
		final String sl = "" + l;

		int sum = 0;
		for( final char c : sl.toCharArray() )
			sum += c - '0';
		return sum;
	}

	/** Kürzt alle doppelten und gibt die Anzahl der gleichen Elemente zurück. **/
	public static int differentNumbers( final int... values ) {
		Arrays.sort( values );
		int count = 0;
		int base = 0;
		for( final int i : values )
			if( count == 0 || i != base ) {
				count++;
				base = i;
			}
		return count;
	}

	public static double diffPercent( final double lower, final double upper ) {
		return lower < upper
			? upper / lower * 100 - 100
			: upper < lower
				? -(100 - upper / lower * 100)
				: 0d;
	}

	/**
	 * @apiNote Computes the Euler constant
	 */
	public static double euler( final int cycles ) {
		double e = 1;
		double f = 1;

		for( int cycle = 1; cycle <= cycles; cycle++ ) {
			f = f * (1.0 / cycle);
			if( f == 0 )
				break;
			else
				e += f;
		}

		return e;
	}

	public static double getFraction( final double value ) {
		return value - (int)value;
	}

	/**
	 * DE: Ganzzahl
	 */
	public static int getInt( final double d ) {
		return (int)Math.floor( d );
	}


	// --- Bit-Operations ---

	public static boolean isBetween( final int value, final int min, final int max ) {
		return value >= min && value <= max;
	}

	public static boolean isEven( final long value ) {
		return value % 2 == 0;
	}

	/**
	 * @return Returns the amount of digits of an integer number.
	 */
	public static int length( final int value ) {
//		int length = 0;
//		long temp = 1;
//		while(temp <= number) {
//		    length++;
//		    temp *= 10;
//		}
//		return length;

		/*
		 * Log10 0,000 /Sek
		 * Multiply temp*10 / 1,156
		 * String.length / 2,000
		 */
		return (int)(Math.log10( value ) + 1);
	}

	public static double limit( final double min, final double max, final double value ) {
		Err.ifTooSmall( min, max );
		return value < min
			? min
			: value > max
				? max
				: value;
	}

	public static int limit( final int min, final int max, final int value ) {
		Err.ifTooSmall( min, max );
		return value < min
			? min
			: value > max
				? max
				: value;
	}

	public static double limit12Decimals( final double value ) {
		// Don't modify NaN and Infinity!
		if( !Double.isFinite( value ) )
			return value;

		// Prevent "out of bounds"
		final int e = Math.getExponent( value ); // 0d --> -1023 = This is okay, return value directly!
		if( e > 950 )	// max_value / 10^20 = exponent 957
			return value;
		if( e <= -42 )	// max_value / 10^20 = exponent 957
			return 0d;
//		-40  <--  0.000000000001
//		-40  <--  0.000000000000999
//		-41  <--  0.0000000000009

		final String s = String.format( Locale.ROOT, "%.12f", value ); // Without Locale / Locale.ENGLISH
//		s = Lib_String.replace(s, ',', '.');
		return Double.parseDouble( s );
	}


	// --- Various ---

	public static double log( final int base, final double x ) {
		double result = 0;
		double input = x;

		while( input > base ) {
			result++;
			input /= base;
		}

		double fraction = 1 / 2;
		input *= input;

		while( result + fraction > result && input > 1 ) {

			if( input > base ) {
				input /= base;
				result += fraction;
			}

			input *= input;
			fraction /= 2.0;
		}

		return result;
	}

	/**
	 * a^x=b >> x=log(a,b)
	 */
	public static double logX( final double base, final double value ) {
		return Math.log( value ) / Math.log( base );
	}

	@SuppressWarnings( "unchecked" )
	public static <T extends Comparable<? super T>> T max( final T... numbers ) {
		Err.ifTooSmall( 1, numbers.length );
		T result = numbers[0];
		for( final T n : numbers )
			if( n.compareTo( result ) > 0 )
				result = n;
		return result;
	}

	/**
	 * Item could be null, this will be ignored!
	 */
	@SuppressWarnings( "unchecked" )
	public static <T extends Comparable<? super T>> T maxNull( final T... numbers ) {
		Err.ifTooSmall( 1, numbers.length );
		T result = null;

		for( final T num : numbers ) {
			if( num == null )
				continue;
			if( result == null || num.compareTo( result ) > 0 )
				result = num;
		}

		return result;
	}

	@SuppressWarnings( "unchecked" )
	public static <T extends Comparable<? super T>> T min( final T... numbers ) {
		Err.ifTooSmall( 1, numbers.length );
		T result = numbers[0];
		for( final T num : numbers )
			if( num.compareTo( result ) < 0 )
				result = num;
		return result;
	}

	/**
	 * Item could be null, this will be ignored!
	 */
	@SuppressWarnings( "unchecked" )
	public static <T extends Comparable<? super T>> T minNull( final T... numbers ) {
		Err.ifTooSmall( 1, numbers.length );
		T result = null;

		for( final T num : numbers ) {
			if( num == null )
				continue;
			if( result == null || num.compareTo( result ) < 0 )
				result = num;
		}

		return result;
	}

	public static Integer minus( final Integer minuend, final Integer subtrahend ) {
		if( minuend == null )
			return null;
		if( subtrahend == null )
			return minuend;
		return minuend - subtrahend;
	}

	/**
	 * Fixes the problem with the fractal values.
	 * Example 1: 3 * 1/3 = 0.9999999999999
	 * Examble 2: 4.5 * 6.7 = 30.150000000000002
	 * Examble 3: 50000000000d = 4.999999999999999E10d
	 *
	 * String.format("%.12f", value)
	 */
	public static double normalize( final double value, final int decimals ) {
		if( decimals < 1 || decimals > 20 )
			Err.ifOutOfBounds( 1, 20, decimals );

		// Don't modify NaN and Infinity!
		if( !Double.isFinite( value ) )
			return value;

		// Prevent "out of bounds"
		final int e = Math.getExponent( value ); // 0d --> -1023 = This is okay, return value directly!
		if( e > 950 || e < -950 )	// max_value / 10^20 = exponent 957
			return value;

		// In most cases correct! But sometimes the result is worse than the original:
//		final double m = Math.pow(10, decimals);
//		return Math.rint(value * m) / m;

		// This was correct in every test:
		final String s = String.format( Locale.ROOT, "%." + decimals + "f", value ); // Locale.ENGLISH
//		s = Lib_String.replace(s, ',', '.');
		return Double.parseDouble( s );
	}

	public static double power( final double base, final double exponent ) {
		return Math.pow( base, exponent );
	}

	public static double round( final double value, final int decimals ) {
		Err.ifOutOfBounds( 0, 10, decimals );
		final double multi = decimals == 0
			? 1
			: Lib_Math.power( 10, decimals );
		return Math.round( value * multi ) / multi;
	}

	public static int roundDown( final double value ) {
		return (int)value;
	}

	/**
	 * With 2 decimals
	 *
	 * Mit kaufmännischer Rundung!!!
	 * Hier auf alle Fälle!
	 */
	public static String roundToCurrency( double value ) {
		value = Lib_Math.normalize( value, 2 );
		final NumberFormat nf = NumberFormat.getCurrencyInstance();
//		nf.setRoundingMode(RoundingMode.HALF_UP); // Java6
		value = Lib_Math.round( value, 2 );
		return nf.format( value );
	}

	public static int roundToInt( final double value ) {
		return (int)Lib_Math.round( value, 0 );
	}

	public static Integer roundToInteger( final Double value ) {
		if( value == null )
			return null;
		return (int)Lib_Math.round( value, 0 );
	}

	public static short roundToShort( final double value ) {
		return (short)Lib_Math.round( value, 0 );
	}

	public static Short roundToShort( final Double value ) {
		if( value == null )
			return null;
		return (short)Lib_Math.round( value, 0 );
	}

	/**
	 * Mit kaufmännischer Rundung!!!
	 * TODO Evtl. hier zwei Methoden: EVEN und HALF_UP!
	 */
	public static String roundToString( double value, final int decimals ) {
//		return rundenZuText(zahl, stellen, RoundingMode.HALF_UP);
		value = Lib_Math.normalize( value, decimals + 1 );
		final String format =
//			(zahl<0 ? "-" : "") //- wird automatisch gesetzt!
			"0" // # würde hier u.U. zu ",123" führen
				+ (decimals > 0
					? "."
					: "")
				+ Lib_String.sequence( '0', decimals );
		final DecimalFormat df = new DecimalFormat( format );

		value = Lib_Math.round( value, decimals );
		//Das ist die einfache Variante für Java6
//		if(rm!=null)
//			df.setRoundingMode(rm);
		return df.format( value );
	}

	/**
	 * Round-to-EVEN ist mathematisch korrekt
	 * HALF_UP ist kaufmännisch
	 */
	public static String roundToString( final double value, final int decimals, final RoundingMode rm ) {
		if( rm != null && rm == RoundingMode.HALF_UP )
			return Lib_Math.roundToString( value, decimals );
		else
			throw Err.todo();
	}

	public static int roundUp( final double value ) {
		int result = (int)value;
		if( value > result )
			result++;
		return result;
	}

	public static double roundUp( final double value, final int decimals ) {
		Err.ifTooSmall( 0, decimals );
		final int factor = (int)Math.pow( 10, decimals );
		int result = (int)(value * factor);
		if( value * factor > result )
			result++;
		return (double)result / factor;
	}

	public static String roundUpToString( final double value, final int decimals ) {
		return Lib_Math.roundToString( value, decimals, RoundingMode.UP );
	}

	public static double subPercent( final double value, final double percent ) {
		Err.ifOutOfBounds( 0, 100, percent );
		return value * ((100 - percent) / 100);
	}

	public static int sum( final int... values ) {
		int result = 0;
		for( final int val : values )
			result += val;
		return result;
	}

//	public static Point schnitt(final int start1, final int laenge1, final int start2, final int laenge2) {
//		final int ende1 = start1 + laenge1;
//		final int ende2 = start2 + laenge2;
//		if(ende1 < start2 || ende2 < start1)
//			return null;
//		final int start = Math.max(start1, start2);
//		final int laenge = Math.min(ende1, ende2) - start;
//		if(laenge == 0)
//			return null;
//		return new Point(start, laenge);
//	}

}
