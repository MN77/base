/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util;

import java.util.List;
import java.util.Random;

import de.mn77.base.data.constant.CHARS;
import de.mn77.base.data.datetime.I_Date;
import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 18.03.2007
 *
 * @implNote TODO: rename .get to .next or .random or .create
 */
public class Lib_Random {

	private static final Random rnd = new Random();


	public static boolean getBool() {
		return Lib_Random.rnd.nextBoolean();
	}

	public static boolean[] getBoolArray( final int length ) {
		Err.ifTooSmall( 0, length );
		final boolean[] result = new boolean[length];
		for( int i = 0; i < length; i++ )
			result[i] = Lib_Random.rnd.nextBoolean();
		return result;
	}

	public static boolean[] getBoolArray( final int length, final int amountOfTrues ) {
		Err.ifTooSmall( 0, length );
		Err.ifToBig( length, amountOfTrues );
		final boolean[] result = new boolean[length]; // all false
		int trues = 0;

		while( trues < amountOfTrues ) {
			final int i = Lib_Random.getInt( 0, length - 1 );

			if( !result[i] ) {
				result[i] = true;
				trues++;
			}
		}

		return result;
	}

	public static byte[] getByteArray( final int length ) {
		Err.ifTooSmall( 0, length );
		final byte[] result = new byte[length];
		Lib_Random.rnd.nextBytes( result );
		return result;
	}

	public static I_Date getDate( final I_Date from, final I_Date to ) { //TODO Testen und ggf. optimieren!
		Err.ifNull( from, to );
		final int diff = from.diffDays( to );
		Err.ifTooSmall( 1, diff );
		return from.getAddDays( Lib_Random.getInt( 0, diff ) );
	}

	public static double getDouble( final double min, final double max ) {
		Err.ifTooSmall( min, max );
		if( min == max )
			return min;

		//TODO: HACK
		double result = 0d;
		do
			result = Lib_Random.getInt( (int)min, Lib_Math.roundUp( max ) - 1 ) + Lib_Random.rnd.nextDouble();
		while( result < min || result > max );
		return result;
	}

	public static int getInt() {
		return Lib_Random.rnd.nextInt();
	}

	public static int getInt( final int max ) {
		return Lib_Random.getInt( 0, max );
	}

	public static int getInt( final int min, final int max ) {
		Err.ifTooSmall( min, max );
		if( min == max )
			return min;
		return min + Lib_Random.rnd.nextInt( max - min + 1 );
	}

	public static int[] getIntArray( final int length, final int min, final int max ) {
		Err.ifTooSmall( 0, length );
		final int[] result = new int[length];
		for( int i = 0; i < length; i++ )
			result[i] = Lib_Random.getInt( min, max );
		return result;
	}

	/**
	 * Example: 1-5 = [3,5,1,2,4]
	 * Every number only once
	 **/
	public static int[] getIntArraySet( final int from, final int to ) {
		if( to == from )
			return new int[]{ from };

		Err.ifTooSmall( from + 1, to );
		final int[] result = new int[to - from + 1];
		final boolean[] used = new boolean[to - from + 1];

		for( int z = from; z <= to; z++ ) {
			boolean ok = false;

			do {
				final int i = Lib_Random.getInt( 0, result.length - 1 );

				if( !used[i] ) {
					result[i] = z;
					used[i] = true;
					ok = true;
				}
			}
			while( !ok );
		}

		return result;
	}

	public static int[] getIntArraySet( int anzahl, final int from, final int to, final int not ) {
		if( to == from )
			return new int[]{ from };
		Err.ifTooSmall( from + 1, to );
		Err.ifOutOfBounds( from, to, not );
		anzahl = Math.min( anzahl, to - from );
		final int[] result = new int[anzahl];
		final boolean[] used = new boolean[to - from + 1];
		used[not - from] = true;

		for( int i = 0; i < anzahl; i++ ) {
			boolean ok = false;

			do {
				final int num = Lib_Random.getInt( from, to );

				if( !used[num - from] ) {
					result[i] = num;
					used[num - from] = true;
					ok = true;
				}
			}
			while( !ok );
		}

		return result;
	}

	public static <T> I_List<T> getSelection( final List<T> pool, final int count, final boolean doubles ) {
		if( !doubles )
			Err.ifToBig( pool.size(), count );
		final SimpleList<T> result = new SimpleList<>( count );

		if( doubles ) {
			final SimpleList<Integer> pointers = new SimpleList<>( count );
			for( int nr = 0; nr < count; nr++ )
				pointers.add( Lib_Random.getInt( 0, pool.size() - 1 ) );
			for( final Integer i : pointers )
				result.add( pool.get( i ) );
		}
		else {
			final boolean[] used = new boolean[pool.size()];
			int curLen = 0;

			while( curLen < count ) {
				final int rnd = Lib_Random.getInt( 0, pool.size() - 1 );

				if( !used[rnd] ) {
					used[rnd] = true;
					curLen++;
				}
			}

			for( int i = 0; i < pool.size(); i++ )
				if( used[i] )
					result.add( pool.get( i ) );
		}

		return result;
	}

	public static String getString( final int length, final boolean upperAndLowerCase, final boolean numbers ) {
		return Lib_Random.getString( length, true, upperAndLowerCase, numbers, null );
	}

	public static String getString( final int length, final boolean lowercase, final boolean uppercase, final boolean numbers, final String specialChars ) {
		String pool = "";
		if( lowercase )
			pool += CHARS.ABC_LOWER.toLowerCase();
		if( uppercase )
			pool += CHARS.ABC_LOWER.toUpperCase();
		if( numbers )
			pool += CHARS.NUMBERS;
		if( specialChars != null )
			pool += specialChars;
		final StringBuilder sb = new StringBuilder();
		for( int i = 0; i < length; i++ )
			sb.append( pool.charAt( Lib_Random.getInt( 0, pool.length() - 1 ) ) );
		return sb.toString();
	}

	public static I_List<String> getStringList( final int count, final int minStringLength, final int maxStringLength, final boolean upAndDownCase, final boolean numbers ) {
		final SimpleList<String> result = new SimpleList<>( count );

		for( int i = 0; i < count; i++ ) {
			final int curLen = Lib_Random.getInt( minStringLength, maxStringLength );
			final String s = Lib_Random.getString( curLen, upAndDownCase, numbers );
			result.add( s );
		}

		return result;
	}

	public static String getStringNumbers( final int length ) {
		return Lib_Random.getString( length, false, false, true, null );
	}

	public static String shuffleChars( final String s ) {
		final int len = s.length();
		final int[] rnd = Lib_Random.getIntArraySet( 0, len - 1 );
		final char[] ca = new char[len];
		for( int i = 0; i < len; i++ )
			ca[i] = s.charAt( rnd[i] );
		return new String( ca );
	}

}
