/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util;

import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 * @created 12.10.2022
 */
public class Lib_Relative {

	/**
	 * @return Returns the real index of the relative position. If 'lazy' is selected, invalid indexes will return -1 instead of throwing an error.
	 */
	public static int realIndex( final int relPosition, final int size, final boolean lazy ) {
		if( relPosition == 0 )
			throw new Err_Runtime( "Invalid position", "Position '0' is not allowed!" );
		if( Math.abs( relPosition ) > size )
			if( lazy )
				return -1;
			else
				throw new Err_Runtime( "Position out of bounds", "Size is " + size + ", invalid position: " + relPosition );
		return relPosition < 1
			? size + relPosition
			: relPosition - 1;
	}

}
