/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util;

import de.mn77.base.error.Err;
import de.mn77.base.error.Warning;


/**
 * @author Michael Nitsche
 * @created 23.05.2020
 */
public class Lib_String {

	public static String capitalize( final String s, final boolean onlyFirst ) {
		Err.ifNull( s );
		if( s.length() == 0 )
			return s;
//		if(onlyFirst)
//			return Character.toUpperCase(s.charAt(0)) + s.substring(1);

		boolean inside = false;
		boolean firstDone = false;
		final StringBuilder sb = new StringBuilder();

		for( final char c : s.toCharArray() )
			if( c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c >= (char)128 && c <= (char)165 ) {

				if( inside )
					sb.append( c );
				else {
					sb.append( Character.toUpperCase( c ) );
					inside = true;
					firstDone = true;
				}
			}
			else {
				sb.append( c );
				inside = onlyFirst && firstDone;
			}

		return sb.toString();
	}

	/**
	 * @apiNote
	 *          "ich bin da" --> "Ich Bin Da"
	 */
	public static String capitalizeWords( final String s ) {
		final char[] ca = s.toCharArray();
		char last = ' ';
		int i = 0;

		while( i >= 0 && i < ca.length ) {
			final Character current = ca[i];
			if( Character.isLetter( current ) && !Character.isLetter( last ) )
				ca[i] = Character.toUpperCase( ca[i] );

			i++;
			last = current;
		}

		return new String( ca );
	}

	/**
	 * Compares to Strings and returns the index of the first different char.
	 * If no differences are found, -1 will be returned.
	 */
	public static int diffFirst( final String s1, final String s2 ) {
		Err.ifNull( s1, s2 );
		final int min = Math.min( s1.length(), s2.length() );

		for( int i = 0; i < min; i++ )
			if( s1.charAt( i ) != s2.charAt( i ) )
				return i;

		if( s1.length() != s2.length() )
			return min;

		return -1;
	}

	public static String insert( final String insertion, final int index, final String base ) {
		Err.ifNull( insertion, base );
		Err.ifOutOfBounds( 0, base.length(), index );
		return base.substring( 0, index ) + insertion + base.substring( index );
	}

	public static String relInsert( final String insertion, int pos, final String base ) {
		Err.ifNull( insertion, base );
		Err.ifEqual( 0, pos );
		Err.ifOutOfBounds( base.length() + 1, pos );
		if( pos < 0 )
			pos = base.length() + pos + 1;
		else
			pos--;
		return base.substring( 0, pos ) + insertion + base.substring( pos );
	}

	public static String relRemove( final String s, final int posStart, final int posEnd ) {
		Err.ifNull( s );
		Err.ifEqual( 0, posStart );
		Err.ifOutOfBounds( s.length() + 1, posStart );
		Err.ifOutOfBounds( s.length() + 1, posEnd );
		Err.ifTooSmall( posStart, posEnd );

		return s.substring( 0, posStart - 1 ) + s.substring( posEnd );
	}

	public static String remove( final String s, final int firstIndex, final int lastIndex ) {
		Err.ifNull( s );
		Err.ifOutOfBounds( 0, s.length() - 1, firstIndex );
		Err.ifOutOfBounds( 0, s.length() - 1, lastIndex );
		Err.ifTooSmall( firstIndex, lastIndex );

		return s.substring( 0, firstIndex ) + s.substring( lastIndex + 1 );
	}

	public static String replace( final String base, final char replace, final char with ) {
		Err.ifNull( base );
		final char[] ca = base.toCharArray();

		for( int i = 0; i < ca.length; i++ )
			if( ca[i] == replace )
				ca[i] = with;

		return new String( ca );
	}

	public static String replace( final String base, final char replace, final String with ) {
		Err.ifNull( base, with );
		final char[] ca = base.toCharArray();
		final StringBuilder sb = new StringBuilder( ca.length * 2 );

		for( final char c : ca )
			if( c == replace )
				sb.append( with );
			else
				sb.append( c );
		return sb.toString();
	}

	public static String replace( final String base, final String replace, final String with ) {
		Err.ifNull( base, with );
		final char[] ca = base.toCharArray();
		final StringBuilder sb = new StringBuilder( ca.length * 2 );

		for( int i = 0; i < base.length(); )
			if( base.startsWith( replace, i ) ) {
				sb.append( with );
				i += replace.length();
			}
			else {
				sb.append( base.charAt( i ) );
				i++;
			}
		return sb.toString();
	}

	public static String sequence( final char c, final long width ) {
		Warning.ifOver( (long)10000, width );
		Err.ifTooSmall( 0, width );
		final StringBuilder result = new StringBuilder();
		for( int i = 0; i < width; i++ )
			result.append( c );
		return result.toString();
	}

	public static String sequence( final int codepoint, final long width ) {
		Warning.ifOver( (long)10000, width );
		Err.ifTooSmall( 0, width );
		final StringBuilder result = new StringBuilder();
		for( int i = 0; i < width; i++ )
			result.appendCodePoint( codepoint );
		return result.toString();
	}

	public static String sequence( final String s, final long count ) {
		final StringBuilder sb = new StringBuilder();
		for( int i = 0; i < count; i++ )
			sb.append( s );
		return sb.toString();
	}

	/**
	 * @return Returns a string, where every 'width' chars a '\n' is inserted
	 */
	public static String cutLines( final String s, final int width ) {
		final int len = s.length();
		final char[] source = s.toCharArray();
		final StringBuilder sb = new StringBuilder( len * 2 );

		for( int i = 0; i < len; i++ ) {
			if( i != 0 && i % width == 0 )
				sb.append( '\n' );

			sb.append( source[i] );
		}

		return sb.toString();
	}

	/**
	 * @return Returns true, if the given String starts with one of the given chars.
	 */
	public static boolean startsWith( final String s, final char[] chars ) {
		Err.ifNull( s );
		if( s.length() == 0 )
			return false;

		final char c0 = s.charAt( 0 );
		for( final char c : chars )
			if( c == c0 )
				return true;

		return false;
	}

	/**
	 * @deprecated: Use FormString.unquote!!!
	 */
	public static String unquote( String s ) {
		if( s.startsWith( "\"" ) )
			s = s.substring( 1 );
		if( s.endsWith( "\"" ) )
			s = s.substring( 0, s.length() - 1 );
		return s;
	}

	public static boolean isNumberIntPositive( String s ) {
		final int len = s.length();

		if(len == 0 || len > 9) // 2,147,483,647 // Korrekt wäre 10 mit weitere Prüfung des Maximums. So ist es aber schneller.
			return false;

		if(len > 1 && s.charAt( 0 ) == '0')
			return false;

		for(char c : s.toCharArray())
			if(c < '0' || c > '9')
				return false;

		return true;
	}

	/**
	 * @apiNote Splits a string and returns the left and the right side of the delimiter. This function returns always a String[2].
	 */
	public static String[] splitFirst(String s, char delimiter) {
		for( int i = 0; i < s.length(); i++ )
			if( s.charAt( i ) == delimiter )
				return new String[] { s.substring( 0, i ), s.substring( i+1 ) };

		return new String[] { s, "" };
	}

	/**
	 * @apiNote Splits a string and returns the left and the right side of the delimiter. This function returns always a String[2].
	 */
	public static String[] splitLast(String s, char delimiter) {
		for( int i = s.length()-1; i >= 0; i-- )
			if( s.charAt( i ) == delimiter )
				return new String[] { s.substring( 0, i ), s.substring( i+1 ) };

		return new String[] { "", s }; // TODO correct order?
	}

	// TODO splitAny oder splitEvery oder splitAll

}
