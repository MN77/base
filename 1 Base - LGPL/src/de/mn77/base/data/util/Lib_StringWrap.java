/*******************************************************************************
 * Copyright (C) 2023-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util;

/**
 * @author Michael Nitsche
 * @created 30.05.2023
 */
public class Lib_StringWrap {

	public static String wrap( final String s, final int max, final boolean trimLines ) {
		final int len = s.length();
		int start = 0;
		final StringBuilder sb = new StringBuilder( len * 2 );

		while( start < len ) {
			final int gap = Lib_StringWrap.iSearchWrapGap( s, start, Math.min( start + max - 1, len - 1 ), max ); // gap == char after last char
			final String part = s.substring( start, gap );
			final String trimmed = trimLines
				? part.trim()
				: part;

			sb.append( trimmed );
			sb.append( '\n' );

			start = gap;

			if( start < len && Lib_StringWrap.iIsSpace( s, start ) )
				start++;
		}

		if( len > 0 )
			sb.setLength( sb.length() - 1 );
		return sb.toString();
	}

	private static boolean iIsGap( final String s, final int index ) {
		final char c = s.charAt( index );

		if( c == '\n' || c == ' ' || c < 48 )
			return true;
		if( c == ')' || c == ']' || c == '}' ) // || c == '>'
			return true;

		return false;
	}

	private static boolean iIsSpace( final String s, final int index ) {
		final char c = s.charAt( index );
		return Character.isWhitespace( c );
	}

	private static int iSearchWrapGap( final String s, final int first, final int last, final int max ) {
		// Contains linebreak
		for( int i = first; i <= last; i++ )
			if( s.charAt( i ) == '\n' )
				return i + 1;

		// Next, after this part is a whitespace
		if( last < s.length() - 1 && Lib_StringWrap.iIsSpace( s, last + 1 ) )
			return last + 1;

		// Regular search from last to first
		for( int i = last; i >= first; i-- )
			if( Lib_StringWrap.iIsGap( s, i ) )
				return i + 1;

		return Math.min( s.length(), first + max );
	}

}
