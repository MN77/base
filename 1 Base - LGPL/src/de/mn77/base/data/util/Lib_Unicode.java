/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util;

import java.util.List;

import de.mn77.base.data.ComplexUnicodeChar;
import de.mn77.base.data.constant.ALIGN;
import de.mn77.base.data.numsys.Lib_Hex;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 11.06.2021
 * @implNote
 *          https://www.marcelwicki-softwaredesign.ch/2018/08/08/zeichencodierungen/
 */
public class Lib_Unicode {

	private record VarSelResult(boolean isVarSelector, int value, int charCount) {};


	public static String cpToUTF16( int cp ) {
		if( cp < 65536 )
			return "" + (char)cp;

		cp -= 65536;
		final int m = cp % 1024;
		final char low = (char)(m + 56320); // 53534%1024+0b1101110000000000
		final char high = (char)((cp - m) / 1024 + 55296); // 53248/1024+0b1101100000000000

//		MOut.print((int)high, (int)low);
		return "" + high + low;
	}

	public static String cpToUTF8( int cp ) {
		if( cp < 128 )
			return "" + (char)cp;
		else if( cp < 2048 ) {
			final int m = cp % 64;
			final char b2 = (char)(m + 128);
			final char b1 = (char)((cp - m) / 64 + 192);
//			MOut.print(b1, b2);
			return "" + b1 + b2;
		}
		else if( cp < 63488 ) {
			int m = cp % 64;
			final char b3 = (char)(m + 128);
			cp -= m;
			cp = cp / 64;
			m = cp % 64;
			final char b2 = (char)(m + 128);
			cp -= m;
			cp = cp / 64;
//			m = i % 64;
			final char b1 = (char)(cp + 224);
//			MOut.print(b1, b2, b3);
			return "" + b1 + b2 + b3;
		}
		else {
			int m = cp % 64;
			final char b4 = (char)(m + 128);
			cp -= m;
			cp = cp / 64;
			m = cp % 64;
			final char b3 = (char)(m + 128);
			cp -= m;
			cp = cp / 64;
			m = cp % 64;
			final char b2 = (char)(m + 128);
			cp -= m;
			cp = cp / 64;
//			m = i % 64;
			final char b1 = (char)(cp + 240);
//			MOut.print(b1, b2, b3, b4);
			return "" + b1 + b2 + b3 + b4;
		}
	}

	public static int length(String s) {
		final char[] chars = s.toCharArray();
		final int sLen = s.length();
		int result = sLen;

        for( int i=0; i < sLen; i++ ) {
        	char ch = chars[i];

            if( i+1 < sLen && Character.isHighSurrogate(ch) && Character.isLowSurrogate(chars[i+1])) {
                final int cp = Character.codePointAt( chars, i );
                result--;
                i++;

                if(Lib_Unicode.isVariationSelector( cp ))
                	result--;
            }
            else if(Lib_Unicode.isVariationSelector( ch )) {
            	result--;
            }
        }

        return result;
	}

	public static ComplexUnicodeChar charAt(String s, int index) {
		int curIndex = 0;

		final int sLen = s.length();
		int codepoint = -1;
		int cpCharCount = -1;

        VarSelResult buffer = null;

        for(int sIdx = 0; sIdx < sLen; ) {
        	if(buffer == null) {
        		codepoint = s.codePointAt(sIdx);
        		cpCharCount = Character.charCount(codepoint);
         	}
        	else {
        		codepoint = buffer.value;
        		cpCharCount = buffer.charCount;
        	}

      		sIdx += cpCharCount;
      		buffer = null;

            if(sIdx < sLen) {
            	buffer = Lib_Unicode.iCheckVariationSelector(s, sIdx);

	        	if(buffer.isVarSelector) {
	        		if(curIndex == index)
	        			return new ComplexUnicodeChar(codepoint, cpCharCount, buffer.value, buffer.charCount);

	        		sIdx += buffer.charCount;
	        		buffer = null;
	        	}
            }

            if(curIndex == index)
            	return new ComplexUnicodeChar(codepoint, cpCharCount);

            curIndex++;
        }

        throw Err.invalid( "Index out of string" );
	}

	public static boolean cpIsComplex( int cp ) {
		return Character.charCount( cp ) == 2;
	}

	public static void main( final String[] args ) {
//		MOut.print('0'-48);

//		int i = Lib_Unicode.hexToInt("1d11e");
//		int i = Lib_Unicode.hexToInt("20ac");	// 8364
		final int i = Lib_Hex.hexToInt( "1d11e" ); // 119070
		MOut.print( i );

//		int i = 119070;
		final String s16 = Lib_Unicode.cpToUTF16( i );
		MOut.print( "--------", s16, s16.length(), s16.getBytes() );

		final String s8 = Lib_Unicode.cpToUTF8( i );
		MOut.print( "--------", s8, s8.length(), s8.getBytes() );
	}

	/**
	 * @apiNote Converts a String with one or two "\\uXXXX\\uXXXX" to a codepoint
	 */
	public static final int parseUnicodeToCP( final String s ) {
//		return s.codePointAt( 0 );
		final char c0 = Lib_Unicode.parseUnicodeToChar(s);
		if(Character.isHighSurrogate( c0 )) {
			char c1 = Lib_Unicode.parseUnicodeToChar(s.substring( 6 ));
			char[] chars = new char[] {c0, c1};
			return Character.codePointAt( chars, 0 );
		}
		else
			return c0;
	}

	/**
	 * @apiNote Converts a String "\u0061" to a single char (Only up to Unicode 3.0)
	 */
	public static final char parseUnicodeToChar( final String s ) {
		if( s.length() < 6 )
			throw new Err_Runtime( "Invalid unicode sentence", s );
		return (char)Integer.parseInt( s.substring( 2, 6 ), 16 );

//      char[] c = { '\\', 'u', '0', '0', 'F', 'C' };
//      return new String(c);
	}

	/**
	 * @apiNote Converts a single codepoint to a unicode formated string: 'a' --> "\\u0061"
	 */
	public static final String cpToUnicode( final int codepoint ) {
		StringBuilder sb = new StringBuilder();
		char[] chars = Character.toChars( codepoint );

		for(char c : chars) {
			sb.append( "\\u" );
			sb.append( Integer.toHexString( c | 0x10000 ).substring( 1 ).toUpperCase() );
		}

		return sb.toString();
	}

	/**
	 * @apiNote Converts a single char to a unicode format string (Only up to Unicode 3.0)
	 */
	public static final String charToUnicode( final char c ) {
		return "\\u" + Integer.toHexString( c | 0x10000 ).substring( 1 ).toUpperCase();
	}

	public static String substring( final String s, int startIndex ) {
		int curIndex = 0;
		final int sLen = s.length();
		int codepoint = -1;

        for( int sIdx = 0; sIdx < sLen;  ) {
    		if(curIndex == startIndex)
    			return s.substring( sIdx );

    		codepoint = s.codePointAt(sIdx);
    		sIdx += Character.charCount(codepoint);

    		if( !Lib_Unicode.isVariationSelector( codepoint ) )
    			curIndex++;
        }

        if(curIndex == startIndex)
        	return "";
        else {
//        	MOut.exit( s, startIndex, curIndex, sLen );
        	throw Err.invalid( "Index out of string" );
        }
	}

	public static String substring( String s, int startIndex, int endIndex ) {
		final int sLen = s.length();
        final int resultLen = endIndex-startIndex;
        if(resultLen == 0)
        	return "";

        StringBuilder sb = new StringBuilder();
        int codepoint;
        boolean active = false;

        for(int sIndex=0, currentIndex=0; sIndex < sLen; currentIndex++) {
            codepoint = s.codePointAt(sIndex);

            if(currentIndex >= endIndex)
            	break;
            if(currentIndex == startIndex)
            	active = true;

            if( active )
        		sb.appendCodePoint( codepoint );

            if(sIndex+1 < sLen) {
        		final int cp1 = s.codePointAt( sIndex+1 );

        		if( Lib_Unicode.isVariationSelector( cp1 ) ) {
        			if(active)
        				sb.appendCodePoint( cp1 );

                    sIndex += Character.charCount( cp1 );
        		}
            }

            sIndex += Character.charCount(codepoint);
       }

		return sb.toString();
	}

	public static int[] toCodepointArray(String s) {
		final int strLen = s.length();
        int codepoint;
        int sIndex = 0;
        final int resultLen = Character.codePointCount( s, 0, strLen );
        int[] result = new int[resultLen];
        int resultPointer = 0;

//      MOut.print(s.toCharArray(), s.getBytes());

        while(sIndex < strLen) {
            codepoint = s.codePointAt(sIndex);
           	result[resultPointer++] = codepoint;
            sIndex += Character.charCount(codepoint);
        }

		return result;
	}

	public static String reverse( String s ) {
		List<ComplexUnicodeChar> chars = Lib_Unicode.toComplexCharList(s);
		StringBuilder sb = new StringBuilder( s.length() );

		for( int i=chars.size()-1; i>= 0; i--)
			sb.append( chars.get( i ).getChars() );

		return sb.toString();
	}

	public static int indexOf( String s, String search ) {
		return Lib_Unicode.indexOf(s, search, 0);
	}

	public static int indexOf( String s, String search, int offset ) {
		final int sLen = s.length();
        int codepoint;
        int curIndex = 0;
        int sIdx = 0;

        while(sIdx < sLen) {
        	if(curIndex == offset)
        		return s.indexOf( search, sIdx );

            codepoint = s.codePointAt(sIdx);
            sIdx += Character.charCount(codepoint);

    		if( !Lib_Unicode.isVariationSelector( codepoint ) )
    			curIndex++;
        }

        throw Err.invalid( "Search string not found" );
	}

	public static boolean isVariationSelector(int cp) {
		// fast return
//		if(cp < 0xFE00) // 0x180B
//			return false;

//		if(cp >= 0x180B && cp <= 0x180D) // Currently ignored by StrScan and here.
//			return true;
		if(cp >= 0xFE00 && cp <= 0xFE0F)
			return true;
//		if(cp >= 0xE0100 && cp <= 0xE01EF) // Defined but not really used
//			return true;

		return false;
	}

	private static VarSelResult iCheckVariationSelector( String s, int sIdx ) {
   		int codepoint = s.codePointAt(sIdx);
		int count = Character.charCount(codepoint);
		boolean isVarSpec = Lib_Unicode.isVariationSelector(codepoint);
		return new VarSelResult(isVarSpec, codepoint, count);
	}

	public static String width( String s, final int width, char fill, final ALIGN align, boolean cutOverflow ) {
		Err.ifTooSmall( 0, width );
		Err.ifNull( s, align );

		int len = Lib_Unicode.length(s);
		if(len == width)
			return s;

		if(cutOverflow && len > width)
			return Lib_Unicode.substring( s, 0, width );

		// len < width
		final StringBuilder sb = new StringBuilder(width);
		int left = 0;
		int right = 0;
		final int space = width - len;

		switch(align) {
			case CENTER:
				left = (int)Math.floor( space / 2d );
				right = (int)Math.ceil( space / 2d );
				break;
			case LEFT:
				right = space;
				break;
			case RIGHT:
				left = space;
				break;
		}

		for(int i=0; i<left; i++)
			sb.append( fill );

		sb.append( s );

		for(int i=0; i<right; i++)
			sb.append( fill );

		return sb.toString();
	}

	public static List<ComplexUnicodeChar> toComplexCharList( String s ) {
		final SimpleList<ComplexUnicodeChar> result = new SimpleList<>();

		final int sLen = s.length();
		int codepoint = -1;
		int cpCharCount = -1;

        VarSelResult cpBuffer = null;
        VarSelResult vsBuffer = null;
        VarSelResult tmpBuffer = null;
        ComplexUnicodeChar item = null;

        for(int sIdx = 0; sIdx < sLen; ) {
        	if(cpBuffer == null) {
        		codepoint = s.codePointAt(sIdx);
        		cpCharCount = Character.charCount(codepoint);
         	}
        	else {
        		codepoint = cpBuffer.value;
        		cpCharCount = cpBuffer.charCount;
        	}

      		sIdx += cpCharCount;
      		cpBuffer = null;
      		vsBuffer = null;

            if(sIdx < sLen) {
            	tmpBuffer = Lib_Unicode.iCheckVariationSelector(s, sIdx);

	        	if(tmpBuffer.isVarSelector) {
	        		vsBuffer = tmpBuffer;
	        		sIdx += vsBuffer.charCount;
	        	}
	        	else
	        		cpBuffer = tmpBuffer;
            }

            item = vsBuffer == null
            	? new ComplexUnicodeChar(codepoint, cpCharCount)
            	: new ComplexUnicodeChar(codepoint, cpCharCount, vsBuffer.value, vsBuffer.charCount);
            result.add( item );
        }

       return result;
	}

	public static boolean isControlChar( int cp ) {
		// Control chars C0
		if(cp < 32)
			return true;

		// Quick sidepass
		if(cp > 31 && cp < 127)
			return false;

		// Control chars C1
		if(cp >= 127  && cp <= 159)
			return true;

		// Variation selectors
		if(cp >= 0x180b  && cp <= 0x180d)
			return true;
		if(cp >= 0xfe00  && cp <= 0xfe0f)
			return true;
		if(cp >= 0xe0100  && cp <= 0xe01ef)
			return true;

		// Surrogates
		if(cp >= 0xd800  && cp <= 0xdfff)
			return true;

		// Forbidden
		if(cp >= 0xFDD0  && cp <= 0xFDEF)
			return true;
		if(cp == 0xFFFE || cp == 0xFFFF ||
			cp == 0x1FFFE || cp == 0x1FFFF ||
			cp == 0x2FFFE || cp == 0x2FFFF ||
			cp == 0x3FFFE || cp == 0x3FFFF ||
			cp == 0x4FFFE || cp == 0x4FFFF ||
			cp == 0x5FFFE || cp == 0x5FFFF ||
			cp == 0x6FFFE || cp == 0x6FFFF ||
			cp == 0x7FFFE || cp == 0x7FFFF ||
			cp == 0x8FFFE || cp == 0x8FFFF ||
			cp == 0x9FFFE || cp == 0x9FFFF ||
			cp == 0x10FFFE || cp == 0x10FFFF )
			return true;

		// Default
		return false;
	}

}
