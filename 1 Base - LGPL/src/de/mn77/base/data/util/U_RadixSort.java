/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util;

import java.util.ArrayList;
import java.util.List;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 2021-05-18
 */
public class U_RadixSort {

	private U_RadixSort() {}


	public static void main( final String[] args ) {
		final ArrayList<String> al1 = new ArrayList<>();
		final ArrayList<Integer> al2 = new ArrayList<>();

		al1.add( "Fooo" );
		al1.add( "Bak" );
		al1.add( "Bartl" );
		al1.add( "Xav" );
		al1.add( "Di" );

		al2.add( 11 );
		al2.add( 12 );
		al2.add( 13 );
		al2.add( 14 );
		al2.add( 15 );

		MOut.temp( al1, al2 );
		U_RadixSort.sortText( false, al1, al2 );
		MOut.temp( al1, al2 );
		U_RadixSort.sortNumbers( false, al2, al1 );
		MOut.temp( al1, al2 );
	}


	public static void sortNumbers( final boolean decrement, final List<Integer> main, final List<?>... others ) {
		final int sorted[] = new int[main.size()];

		final SimpleList<String> mainCopy = new SimpleList<>();
		for( final Integer i : main )
			mainCopy.add( "" + i );

		final int laengster = U_RadixSort.widthInit( mainCopy, sorted );
//		MOut.temp(main, mainCopy, main.size(), mainCopy.size());

		final SimpleList<Integer>[] bucket = new SimpleList[11];
		String s;
		char c;

		for( int sl = laengster; sl >= 1; sl-- ) {
			for( int i = 0; i < bucket.length; i++ )
				bucket[i] = new SimpleList<>();

			for( final int element : sorted ) {
				s = mainCopy.get( element );
				if( s == null )
					bucket[10].add( element );
				else if( sl < laengster - s.length() + 1 )
					bucket[0].add( element );
				else {
					c = s.charAt( sl - 1 - (laengster - s.length()) );
					if( c > '9' || c < '0' )
						bucket[10].add( element );
					else
						bucket[c - '0'].add( element );
				}
			}
//			for(int i=0, sortpos=0; i<bucket.length; i++)
//				for(Iterator<Integer> l=bucket[i].iterator(); l.hasNext(); )
//					sortiert[sortpos++]=l.next();

			int sortpos = 0;
			if( decrement )
				for( int i = bucket.length - 2; i >= 0; i-- )
					for( final Integer integer : bucket[i] ) {
						sorted[sortpos] = integer;
						sortpos++;
					}
			else
				for( int i = 0; i < bucket.length - 1; i++ )
					for( final Integer integer : bucket[i] )
						sorted[sortpos++] = integer;

			for( final Integer integer : bucket[10] )
				sorted[sortpos++] = integer;
		}

		U_RadixSort.sortLike( sorted, main );

		for( final List<?> other : others )
			U_RadixSort.sortLike( sorted, other );
	}

	public static void sortNumbers( final boolean decrement, final int[] main, final List<?>... others ) {
		final int sorted[] = new int[main.length];

		final SimpleList<String> mainCopy = new SimpleList<>();
		for( final Integer i : main )
			mainCopy.add( "" + i );

		final int width = U_RadixSort.widthInit( mainCopy, sorted );
//		MOut.temp(main, mainCopy, main.size(), mainCopy.size());

		final SimpleList<Integer>[] bucket = new SimpleList[11];
		String s;
		char c;

		for( int sl = width; sl >= 1; sl-- ) {
			for( int i = 0; i < bucket.length; i++ )
				bucket[i] = new SimpleList<>();

			for( final int element : sorted ) {
				s = mainCopy.get( element );
				if( s == null )
					bucket[10].add( element );
				else if( sl < width - s.length() + 1 )
					bucket[0].add( element );
				else {
					c = s.charAt( sl - 1 - (width - s.length()) );
					if( c > '9' || c < '0' )
						bucket[10].add( element );
					else
						bucket[c - '0'].add( element );
				}
			}
//			for(int i=0, sortpos=0; i<bucket.length; i++)
//				for(Iterator<Integer> l=bucket[i].iterator(); l.hasNext(); )
//					sortiert[sortpos++]=l.next();
			int sortpos = 0;
			if( decrement )
				for( int i = bucket.length - 2; i >= 0; i-- )
					for( final Integer integer : bucket[i] ) {
						sorted[sortpos] = integer;
						sortpos++;
					}
			else
				for( int i = 0; i < bucket.length - 1; i++ )
					for( final Integer integer : bucket[i] )
						sorted[sortpos++] = integer;

			for( final Integer integer : bucket[10] )
				sorted[sortpos++] = integer;
		}

		U_RadixSort.sortLike( sorted, main );

		for( final List<?> other : others )
			U_RadixSort.sortLike( sorted, other );
	}



	public static void sortText( final boolean decrement, final List<String> main, final List<?>... others ) {
		final int sorted[] = new int[main.size()];

		final int width = U_RadixSort.widthInit( main, sorted );
//		MOut.temp(main, mainCopy, main.size(), mainCopy.size());

		final SimpleList<Integer>[] bucket = new SimpleList[256]; // TODO Wirklich so viele?

		String s;
		char c;

		for( int sl = width - 1; sl >= 0; sl-- ) {
			// Clear / Init
			for( int i = 0; i < 256; i++ ) // bucket-length
				bucket[i] = new SimpleList<>();

			// Walk through all elements
			for( final int element : sorted ) {
				s = main.get( element );
				if( s == null )
					bucket[255].add( element );
				else if( sl >= s.length() )
					bucket[0].add( element );
				else {
					c = s.charAt( sl );
					bucket[c > 255 ? 255 : c].add( element );
				}
			}
//			for(int i=0, sortpos=0; i<bucket.length; i++)
//				for(Iterator<Integer> l=bucket[i].iterator(); l.hasNext(); )
//					sortiert[sortpos++]=l.next();

			// Collect
			int sortpos = 0;
			if( decrement )
				for( int i = bucket.length - 2; i >= 0; i-- )
					for( final Integer integer : bucket[i] ) {
						sorted[sortpos] = integer;
						sortpos++;
					}
			else
				for( int i = 0; i < bucket.length - 1; i++ )
					for( final Integer integer : bucket[i] )
						sorted[sortpos++] = integer;

			for( final Integer integer : bucket[255] )
				sorted[sortpos++] = integer;
		}

		// Sort
		U_RadixSort.sortLike( sorted, main );

		for( final List<?> other : others )
			U_RadixSort.sortLike( sorted, other );
	}

	private static int widthInit( final List<String> list, final int[] buffer ) {
		int laengster = 0, spos = 0, t;

		for( final String s : list ) {
			if( s != null && (t = s.length()) > laengster )
				laengster = t;
			buffer[spos] = spos++;
		}
		return laengster;
	}

//	private static int[] revert(int[] sorted) {
//		int[] sorted2=new int[sorted.length];
//		for(int i=0; i<sorted.length; i++) sorted2[sorted.length-i-1]=sorted[i];
//		return sorted2;
//	}


	private static <T> void sortLike( final int[] sorted, final List<T> main ) {
		final int size = main.size();
		Err.ifNot( size, sorted.length, "List-items and order-positions must be equal" );

		final Object[] buffer = new Object[size];

		for( int i = 0; i < size; i++ )
			buffer[i] = main.get( sorted[i] );
		for( int i = 0; i < buffer.length; i++ )
			main.set( i, (T)buffer[i] );
	}

	private static void sortLike( int[] sorted, int[] main ) {
		final int size = main.length;
		Err.ifNot( size, sorted.length, "List-items and order-positions must be equal" );

		final int[] buffer = new int[size];

		for( int i = 0; i < size; i++ )
			buffer[i] = main[ sorted[i] ];
		for( int i = 0; i < buffer.length; i++ )
			main[i] = buffer[i];
	}

}
