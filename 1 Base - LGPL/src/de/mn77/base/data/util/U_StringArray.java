/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 12.10.2024
 */
public class U_StringArray {

	/**
	 * @apiNote Creates a new string array with length+1, where the new item will be appended at the last position.
	 */
	public static String[] append( final String[] array, final String item ) {
		final int len = array.length;
		final String[] na = new String[len + 1];
		System.arraycopy( array, 0, na, 0, len );
		na[len] = item;
		return na;
	}

	public static String[] concat( final String[] array1, final String[] array2 ) {
		Err.ifNull( array1, array2 );
		final String[] result = new String[array1.length + array2.length];
		System.arraycopy( array1, 0, result, 0, array1.length );
		System.arraycopy( array2, 0, result, array1.length, array2.length );
		return result;
	}

	public static boolean containsOne( final String[] sa, final String[] oneOfThem ) {
		for( final String search : oneOfThem )
			for( final String element : sa )
				if( element.equals( search ) )
					return true;

		return false;
	}

	public static boolean containsAll( final String[] sa, final String[] allOfThem ) {
		for( final String search : allOfThem ) {
			boolean found = false;

			for( final String element : sa )
				if( element.equals( search ) ) {
					found = true;
					break;
				}

			if( !found )
				return false;
		}

		return true;
	}

	/**
	 * Returns a new String[] with all items from 'start' to 'length'
	 */
	public static String[] cutFrom( final String[] args, final int startIndex ) {
		if( startIndex == 0 )
			return args;
		Err.ifToBig( args.length, startIndex );
		if( startIndex == args.length ) // >=
			return new String[0];

		final int newLen = args.length - startIndex;
		final String[] args2 = new String[newLen];
		System.arraycopy( args, startIndex, args2, 0, newLen );
		return args2;
	}

	public static String[] cutLeft( final String[] sa, final int items ) {
		Err.ifNull( sa );
		if( items > sa.length )
			return sa;
		if( items <= 0 )
			return new String[0];

		final String[] args2 = new String[items];
		System.arraycopy( sa, 0, args2, 0, items );
		return args2;
	}

	public static boolean equals( final String[] arr1, final String[] arr2 ) {
		Err.ifNull( arr1, arr2 );
		if( arr1.length != arr2.length )
			return false;

		for( int i = 0; i < arr1.length; i++ )
			if( !arr1[i].equals( arr2[i] ) )
				return false;

		return true;
	}

	public static String[] remove( final String[] from, final String[] allOfThis ) {
		final SimpleList<String> result = new SimpleList<>( from.length );

		for( final String item : from ) {
			boolean add = true;

			for( final String search : allOfThis )
				if( item.equals( search ) ) {
					add = false;
					break;
				}

			if( add )
				result.add( item );
		}

		return U_StringArray.iListToArr( result );
	}

	private static String[] iListToArr( final SimpleList<String> list ) {
		final String[] resultArr = new String[list.size()];
		list.toArray( resultArr );
		return resultArr;
	}

	
	private U_StringArray() {}

}
