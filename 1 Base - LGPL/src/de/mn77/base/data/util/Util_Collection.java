/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util;

import de.mn77.base.data.struct.I_Collection;


/**
 * @author Michael Nitsche
 * @created 28.03.2024
 */
public class Util_Collection {

	private Util_Collection() {}


	public static void sortAndUnique( final I_Collection<?> coll ) {
		coll.sort();

		for( int i = coll.size() - 1; i >= 1; i-- )
			if( coll.get( i ).equals( coll.get( i - 1 ) ) )
				coll.removeIndex( i );
	}

}
