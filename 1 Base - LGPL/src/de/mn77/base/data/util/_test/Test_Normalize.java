/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.data.util._test;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Locale;

import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 12.12.2022
 */
public class Test_Normalize {

	private static int counter = 0;


	public static void main( final String[] args ) {
		final double value = 0.000000000001;
		MOut.print( value + "" );
		MOut.print( "" + value );
		MOut.print( new DecimalFormat( "##.################" ).format( value ) );
		final DecimalFormat df2 = new DecimalFormat( "#.#" );
		df2.setMaximumFractionDigits( 20 );
		MOut.print( df2.format( value ) );
		MOut.print( String.format( "%.20f", value ) );
		MOut.print( Double.toString( value ) );
		MOut.print( new BigDecimal( value ).toPlainString() );

//		MOut.print(Math.getExponent(0.0000000001));			// -34
//		MOut.print(Math.getExponent(0.00000000001));		// -37

//		MOut.print(Math.getExponent(0.000000000001));		// -40
//		MOut.print(Math.getExponent(0.000000000001001));	// -40
//		MOut.print(Math.getExponent(0.00000000000101));		// -40
//		MOut.print(Math.getExponent(0.0000000000011));		// -40
//		MOut.print(Math.getExponent(0.0000000000019));		// -39
//		MOut.print(Math.getExponent(0.000000000002));		// -39
//		MOut.print(Math.getExponent(0.0000000000009));		// -41
//		MOut.print(Math.getExponent(0.000000000000999));	// -40
//		MOut.print(Math.getExponent(0.0000000000005));		// -41
//		MOut.print(Math.getExponent(0.00000000000049999999));// -41
//		MOut.print(Math.getExponent(0.00000000000029999999));// -42
//		MOut.print(Math.getExponent(0.00000000000009999999));// -44

		Test_Normalize.test( 0.000000000001 );
		Test_Normalize.test( 0.000000000001001 );
		Test_Normalize.test( 0.00000000000101 );
		Test_Normalize.test( 0.0000000000011 );
		Test_Normalize.test( 0.0000000000019 );
		Test_Normalize.test( 0.000000000002 );
		Test_Normalize.test( 0.0000000000009 );
		Test_Normalize.test( 0.000000000000999 );
		Test_Normalize.test( 0.00000000000099999 );
		Test_Normalize.test( 0.0000000000005 );
		Test_Normalize.test( 0.00000000000049999999 );
		Test_Normalize.test( 0.00000000000029999999 );
		Test_Normalize.test( 0.00000000000009999999 );

//		MOut.print(Math.getExponent(0.0000000000001));		// -44
//		MOut.print(Math.getExponent(0.00000000000001));		// -47
//		MOut.print(Math.getExponent(0.000000000000001));	// -50
//		MOut.print(Math.getExponent(0.0000000000000001));	// -54
//		MOut.print(Math.getExponent(0.00000000000000001));	// -57
//		MOut.print(Math.getExponent(0.000000000000000001));	// -60
//		MOut.print(Math.getExponent(0.0000000000000000001));	// -64
//		MOut.print(Math.getExponent(0.00000000000000000001));	// -67
//		MOut.print(Math.getExponent(0.000000000000000000011));	// -67
//		MOut.print(Math.getExponent(0.0000000000000000000101));	// -67
//		MOut.print(Math.getExponent(0.00000000000000000001001));	// -67
//		MOut.print(Math.getExponent(0.000000000000000000009));	// -67
//		MOut.print(Math.getExponent(0.00000000000000000000999));	// -67
//		MOut.print(Math.getExponent(0.00000000000000000002));	// -67

		Test_Normalize.test( 0.0000000001 ); // 10
		Test_Normalize.test( 0.00000000001 ); // 11
		Test_Normalize.test( 0.000000000001 ); // 12
		Test_Normalize.test( 0.0000000000001 ); // 13
		Test_Normalize.test( 0.00000000000001 ); // 14
		Test_Normalize.test( 0.000000000000001 ); // 15
		Test_Normalize.test( 0.0000000000000001 ); // 16
		Test_Normalize.test( 0.00000000000000001 ); // 17
		Test_Normalize.test( 0.000000000000000001 ); // 18
		Test_Normalize.test( 0.0000000000000000001 ); // 19
		Test_Normalize.test( 0.00000000000000000001 ); // 20


		Test_Normalize.test( 1d / 3 );
		Test_Normalize.test( 3d * 1 / 3 );
		Test_Normalize.test( 4.5 * 6.7 );
		Test_Normalize.test( 50000000000d );
		Test_Normalize.test( 362.2 - 362.6 );
		Test_Normalize.test( 362.2d - 362.6d );
		Test_Normalize.test( 362.2f - 362.6f );
		Test_Normalize.test( 0.7 + 0.1 );
		Test_Normalize.test( 0.9 - 0.1 );
		Test_Normalize.test( 0.7f + 0.1f );

		Test_Normalize.test( 0.9f - 0.1f );
		Test_Normalize.test( 1.0 - 9 * 0.10 );
		Test_Normalize.test( 1.0d - 9d * 0.10d );
		Test_Normalize.test( 1.0f - 9f * 0.10f );
		Test_Normalize.test( 2.9876543218f );
		Test_Normalize.test( 2.9876543218d );
		Test_Normalize.test( 0.0175 * 100000 );
		Test_Normalize.test( 0.0175d * 100000 );
		Test_Normalize.test( 0.0175f * 100000 );
		Test_Normalize.test( Math.pow( 4.5, 8 ) );

		Test_Normalize.test( 4.5 * 4.5 * 4.5 * 4.5 * 4.5 * 4.5 * 4.5 * 4.5 );
		Test_Normalize.test( 1d / 3 );
		Test_Normalize.test( 1d / 3d );
		Test_Normalize.test( 1d / 4.5 );
		Test_Normalize.test( 1d / 6.7 );
		Test_Normalize.test( 2d / 3 );
		Test_Normalize.test( 2d / 4.5 );
		Test_Normalize.test( Math.pow( 2, 4.5 ) );
		Test_Normalize.test( 2d / 6.7 );
		Test_Normalize.test( Math.pow( 2, 6.7 ) );

		Test_Normalize.test( 3d / 1 );
		Test_Normalize.test( 3d / 6.7 );
		Test_Normalize.test( 8 - 6.7 );
		Test_Normalize.test( 8d - 6.7 );
		Test_Normalize.test( 8 % 6.7 );
		Test_Normalize.test( 8d % 6.7 );
		Test_Normalize.test( 6.7 % 1 );
		Test_Normalize.test( 6.7 % 1d );
		Test_Normalize.test( 6.7 % 2 );
		Test_Normalize.test( 6.7 % 2d );

		Test_Normalize.test( 6.7 / 1 );
		Test_Normalize.test( 6.7 * 6.7 * 6.7 );
		Test_Normalize.test( 6.7 - 8 );
		Test_Normalize.test( 6.7 - 8.0 );
		Test_Normalize.test( 6.7 * 4.5 );
		Test_Normalize.test( -1d / -3 );
		Test_Normalize.test( -1d / -4.5 );
		Test_Normalize.test( -2d / -3 );
		Test_Normalize.test( -2d / -4.5 );
		Test_Normalize.test( -3d / -4.5 );

		Test_Normalize.test( -3d / -6.7 );
		Test_Normalize.test( -8d / -3 );
		Test_Normalize.test( -8d / -4.5 );
		Test_Normalize.test( -8d - -6.7 );
		Test_Normalize.test( -8d / -6.7 );
		Test_Normalize.test( -8d % -6.7 );
		Test_Normalize.test( -4.5 * -6.7 );
		Test_Normalize.test( -6.7 % -1 );
		Test_Normalize.test( -6.7 % -2 );
		Test_Normalize.test( -6.7 / -3 );

		Test_Normalize.test( -6.7 % -3 );
		Test_Normalize.test( -6.7 - -8 );
		Test_Normalize.test( -6.7 * -4.5 );

		MOut.print( "==================================================" );

		Test_Normalize.test( Float.MAX_VALUE );
		Test_Normalize.test( Float.MIN_NORMAL );
		Test_Normalize.test( Float.MIN_VALUE );

		Test_Normalize.test( Double.MAX_VALUE );
		Test_Normalize.test( Double.MIN_NORMAL );
		Test_Normalize.test( Double.MIN_VALUE );

//		Test_Normalize.test();
	}

	public static double normalize1( final double value, final int decimals ) {
		final double m = Math.pow( 10, decimals );
		return Math.rint( value * m ) / m;
	}

	public static double normalize2( final double value, final int decimals ) {
		if( decimals < 1 || decimals > 20 )
			Err.ifOutOfBounds( 1, 20, decimals );

		// Don't modify NaN and Infinity!
		if( !Double.isFinite( value ) )
			return value;

		final double m = Math.pow( 10, decimals );
		return Math.rint( value * m ) / m;
	}

	public static double normalize3( final double value, final int decimals ) {
		if( decimals < 1 || decimals > 20 )
			Err.ifOutOfBounds( 1, 20, decimals );

		// Don't modify NaN and Infinity!
		if( !Double.isFinite( value ) )
			return value;

		// Prevent "out of bounds"
		final int e = Math.getExponent( value ); // 0d --> -1023 = This is okay, return value directly!
		if( e > 950 || e < -950 )	// max_value / 10^20 = exponent 957
			return value;

		// Alternate: Filter by bit-value
//		long bits = Double.doubleToLongBits(value);

		final double m = Math.pow( 10, decimals );
		return Math.rint( value * m ) / m;
	}

	public static double normalize4( final double value, final int decimals ) {
		if( decimals < 1 || decimals > 20 )
			Err.ifOutOfBounds( 1, 20, decimals );

		// Don't modify NaN and Infinity!
		if( !Double.isFinite( value ) )
			return value;

		// Prevent "out of bounds"
		final int e = Math.getExponent( value ); // 0d --> -1023 = This is okay, return value directly!
//		if(e > 950 || e < -950)	// max_value / 10^20 = exponent 957
		if( e > 950 )	// max_value / 10^20 = exponent 957
			return value;
		if( e <= -42 )	// max_value / 10^20 = exponent 957
			return 0d;

//		String s = String.format("%." + decimals + "f", value);
		final String s = String.format( Locale.ROOT, "%." + decimals + "f", value );
//		String s = String.format(Locale.ENGLISH, "%." + decimals + "f", value);
//		s = Lib_String.replace(s, ',', '.');
		return Double.parseDouble( s );
	}

	// Ungeeignet, rundet nicht!
	public static double normalize5( final double value, final int decimals ) {
		if( decimals < 1 || decimals > 20 )
			Err.ifOutOfBounds( 1, 20, decimals );

		// Don't modify NaN and Infinity!
		if( !Double.isFinite( value ) )
			return value;

		// Prevent "out of bounds"
		final int e = Math.getExponent( value ); // 0d --> -1023 = This is okay, return value directly!
		if( e > 950 || e < -950 )	// max_value / 10^20 = exponent 957
			return value;

//		ConvertNumber.
//		String raw = value + "";
		final String raw = "" + value;
		final int ie = raw.indexOf( 'E' );
		if( ie >= 0 )
			return value;

		final int id = raw.indexOf( '.' );
		final String s = raw.substring( 0, Math.min( raw.length(), id + decimals + 1 ) );
//		s = Lib_String.replace(s, ',', '.');
		return Double.parseDouble( s );
	}

	public static double normalize6( final double value, final int decimals ) {
		if( decimals < 1 || decimals > 20 )
			Err.ifOutOfBounds( 1, 20, decimals );

		// Don't modify NaN and Infinity!
		if( !Double.isFinite( value ) )
			return value;

		// Prevent "out of bounds"
		final int e = Math.getExponent( value ); // 0d --> -1023 = This is okay, return value directly!
		if( e > 950 || e < -950 )	// max_value / 10^20 = exponent 957
			return value;

		// Alternate: Filter by bit-value
//		long bits = Double.doubleToLongBits(value);

		final double m = Math.pow( 10, decimals );
		final boolean isNegative = value < 0;

		if( isNegative )
			return Math.rint( value * m ) / m;
		else {
			double a = value * m;

			final double twoToThe52 = 1L << 52; // 2^52

			if( value < twoToThe52 )
				a = twoToThe52 + a - twoToThe52;

			return a / m; // restore original sign
		}
	}

	public static double normalize7( final double value, final int decimals ) {
		if( decimals < 1 || decimals > 20 )
			Err.ifOutOfBounds( 1, 20, decimals );

		// Don't modify NaN and Infinity!
		if( !Double.isFinite( value ) )
			return value;

		final DecimalFormat df2 = new DecimalFormat( "#.#" );
		df2.setMaximumFractionDigits( decimals );
		String raw = df2.format( value );

//		final int id = raw.indexOf(',');
//		final String s = raw.substring(0, Math.min(raw.length(), id + decimals + 1));
		raw = Lib_String.replace( raw, ',', '.' );
		return Double.parseDouble( raw );
	}

	public static double normalize8( final double value ) {
		// Don't modify NaN and Infinity!
		if( !Double.isFinite( value ) )
			return value;

		final long l = Math.round( value * 1000000000000l );
		return l / 1000000000000d;
	}

	public static double normalize9( final double value ) {
		// Don't modify NaN and Infinity!
		if( !Double.isFinite( value ) )
			return value;

		final long l = Math.round( value * 1099511627776l );
//		return l / 1099511627776d;
		return (double)l / 1099511627776l;
//		return l >> 40;
//		           1099511627776
	}

	public static void test( final double value ) {
		final int decimals = 12; // JayMo = 12
		Test_Normalize.counter++;

//		final String n1 = "" + Test_Normalize.normalize1(value, decimals);
//		final String n2 = "" + Test_Normalize.normalize2(value, decimals);

//		final double d3 = Test_Normalize.normalize3(value, decimals);
		final double d4 = Test_Normalize.normalize4( value, decimals );
//		final double d6 = Test_Normalize.normalize6(value, decimals);
//		final double d7 = Test_Normalize.normalize7(value, decimals);
		final double d8 = Test_Normalize.normalize8( value );
		final double d9 = Test_Normalize.normalize9( value );

//		final String n3 = "" + d3;
		final String n4 = "" + d4;
//		final String n6 = "" + d6;
//		final String n7 = "" + d7;
		final String n8 = "" + d8;
		final String n9 = "" + d9;

//		final String n5 = "" + Test_Normalize.normalize5(value, decimals);

//		if(!(n1.equals(n2) && n1.equals(n3) && n1.equals(n4))) {	//  && n1.equals(n5)
		if( !(n4.equals( n8 ) && n8.equals( n9 )) ) {
//		if(!n3.equals(n4)) {
//		{
			MOut.print( "--- " + Test_Normalize.counter + " ----------------- " + Math.getExponent( value ) );
			MOut.print( "" + value );
//			MOut.print(n1);
//			MOut.print(n2);
//			MOut.print(n3);
			MOut.print( n4 );
//			MOut.print(n5);
//			MOut.print(n6);
//			MOut.print(n7);
			MOut.print( n8 );
			MOut.print( n9 );

//			MOut.print(Double.doubleToLongBits(value), Double.doubleToLongBits(d3), Double.doubleToLongBits(d4));
		}
//		MOut.print();
//		MOut.print(String.format("%.10f", value));
//		MOut.print(String.format("%.10f", Test_Normalize.normalize1(value, 10)));
//		MOut.print(String.format("%.10f", Test_Normalize.normalize2(value, 10)));
	}

}
