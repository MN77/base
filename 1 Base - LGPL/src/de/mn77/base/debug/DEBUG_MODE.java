/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.debug;

/**
 * @author Michael Nitsche
 * @created 28.01.2019
 * @apiNote
 *          Log-Level:
 *          0) NO = Text-Ausgaben
 *          1) MINIMAL = Text-Ausgaben, Kanal-Info, kurze Debug-Meldungen
 *          2) DETAIL = Text-Ausgaben, Kanal-Info, detaillierte Debug-Meldungen, Entwickler-Ausgaben
 *          3) PARANOID = Alle Ausgaben mit ausführlichster Info
 *
 */
public enum DEBUG_MODE {

	NO,
	MINIMAL,
	DETAIL,
	PARANOID;

}
