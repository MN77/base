/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.debug;

import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 22.03.2021
 *
 * @apiNote
 *          To measure a block of code between .in() and .out()
 *          Assign a new SpeedControl-Object to a static variable:
 *          private static SpeedControl sc = new SpeedControl();
 */
public class SpeedControl {

	private long start;
	private long max    = 0;
	private long sum    = 0;
	private int  amount = 0;


	public void in() {
		this.start = System.currentTimeMillis();
	}

	public void out() {
		final long end = System.currentTimeMillis();
		final long ms = end - this.start;
		if( ms > this.max )
			this.max = ms;
		this.sum += ms;
		this.amount++;

		MOut.print( "Speed-Control --> Current: " + ms + " ms, Max: " + this.max + " ms, Avg: " + this.sum / this.amount + " ms, Amount: " + this.amount + ", Sum: " + this.sum + " ms" );
	}

}
