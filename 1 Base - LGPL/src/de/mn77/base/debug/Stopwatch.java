/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.debug;

import de.mn77.base.data.datetime.MTime;
import de.mn77.base.data.util.Lib_Math;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 *
 */
public class Stopwatch {

	private final long start;
	private Long       diff;


	public Stopwatch() {
		this.start = System.currentTimeMillis();
		this.diff = this.start;
	}


	public long getDiffMillisec() {
		return System.currentTimeMillis() - this.diff;
	}

	public double getDiffSeconds() {
		return (System.currentTimeMillis() - this.diff) / 1000d;
	}

	public long getMillisec() {
		return System.currentTimeMillis() - this.start;
	}

	public double getSeconds() {
		return (System.currentTimeMillis() - this.start) / 1000d;
	}

	public String getStringDiffHHMMSS() {
		return new MTime( (int)this.getDiffMillisec() ).toString(); // + " Min";
	}

	public String getStringDiffMillisec() {
		return this.getDiffMillisec() + " ms";
	}

	public String getStringDiffSeconds() {
		return Lib_Math.roundToString( this.getDiffSeconds(), 1 ) + " Sec";
	}

	public String getStringHHMMSS() {
		return new MTime( (int)this.getMillisec() ).toString();
	}

	public String getStringSeconds() {
		return Lib_Math.roundToString( this.getSeconds(), 1 ) + " Sec";
	}

	public String gStringMillisec() {
		return this.getMillisec() + " ms";
	}

	public void print() {
		this.iPrint( null, false );
	}

	public void print( final String info ) {
		this.iPrint( info, false );
	}

	public void printShort() {
		this.iPrint( null, true );
	}

	public void printShort( final String info ) {
		this.iPrint( info, true );
	}

	public void resetDiff() {
		this.diff = System.currentTimeMillis();
	}

	@Override
	public String toString() {
		return "Duration: " + this.getStringSeconds() + "   (" + this.gStringMillisec() + ")";
	}

	private void iPrint( final String info, final boolean compact ) {
		final boolean extended = this.getSeconds() >= 120;

		final String diffTime = extended
			? this.getStringDiffHHMMSS()
			: this.getStringDiffSeconds();
		final String totalTime = extended
			? this.getStringHHMMSS()
			: this.getStringSeconds();
		final String s1 = "Diff : " + diffTime + "   (" + this.getStringDiffMillisec() + ")";
		final String s2 = "Total: " + totalTime + "   (" + this.gStringMillisec() + ")";
		if( compact )
			MOut.print( (info == null
				? ""
				: info) + ":  " + diffTime + " / " + totalTime );
		else if( info == null )
			MOut.print( s1, s2 );
		else
			MOut.print( info, s1, s2 );
//		MOut.originLine(3); // TODO Reaktivieren?
		this.resetDiff();
	}

}
