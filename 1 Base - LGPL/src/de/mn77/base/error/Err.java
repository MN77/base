/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.error;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;

import de.mn77.base.data.util.Lib_Class;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.I_File;


/**
 * @author Michael Nitsche
 * @created 09.08.2019
 * @implNote
 *           TODO: Rework for all Arguments: String ident, borders, values ...
 */
public class Err {

	public static Err_Runtime accessProtected( final Object... oa ) {
		throw new Err_Runtime( "Access is prohibited!", oa );
	}

//	public static Err_Runtime failed(Object... oa) {
//		throw new Err_Runtime("Action failed!", oa);
//	}

	public static Err_Runtime direct( final String message, final Object... oa ) {
		throw new Err_Runtime( message, oa );
	}

	/** Abbruch mit vollständiger Fehler-Ausgabe **/
	public static RuntimeException exit( final Throwable t ) {
		return Err.exit( t, null );
	}

	/** Abbruch mit vollständiger Fehler-Ausgabe **/
	public static RuntimeException exit( final Throwable t, final String text ) {
		MOut.error( t, text );
		System.exit( 1 );
		return null;
	}

	public static Err_Runtime forbidden( final Object... oa ) {
		throw new Err_Runtime( "Execution is forbidden!", oa );
	}

	public static Err_FileSys fsAccess( final Object... oa ) throws Err_FileSys {
		throw new Err_FileSys( "File/Directory-Access-Error", oa );
	}

	// --- New ---

	public static Err_FileSys fsFailed( final Object... oa ) throws Err_FileSys {
		throw new Err_FileSys( "Action failed!", oa );
	}

	public static void fsIfExist( final Object o ) throws Err_FileSys {

		if( o instanceof File ) {
			if( ((File)o).exists() )
				throw new Err_FileSys( "File/Directory still exists!", o );
		}
		else if( o instanceof String ) {
			if( new File( "" + o ).exists() )
				throw new Err_FileSys( "File/Directory still exists!", o );
		}
		else
			Err.todo();
	}

	public static void fsIfMissing( Object o ) throws Err_FileSys {
		if( o instanceof String )
			o = new File( "" + o );
		if( o instanceof I_File )
			o = ((I_File)o).getFile();

		if( o instanceof File ) {
			if( !((File)o).exists() )
				throw new Err_FileSys( "File/Directory doesn't exist!", o );
		}
		else
			Err.todo();
	}

	public static void ifContainsNotNull( final Object... oa ) {
		Err.ifNull( oa );
		for( int i = 0; i < oa.length; i++ )
			if( oa[i] != null )
				throw new Err_Runtime( "Object " + (i + 1) + " is not null!" );
	}

	public static void ifContainsNull( final Object... oa ) {
		Err.ifNull( oa );
		for( int i = 0; i < oa.length; i++ )
			if( oa[i] == null )
				throw new Err_Runtime( "Object " + (i + 1) + " is null!" );
	}

	public static void ifEmpty( final String... oa ) {
		Err.ifNull( oa );
		for( int i = 0; i < oa.length; i++ )
			if( oa[i].length() == 0 )
				throw new Err_Runtime( "String " + (i + 1) + " is empty!" );
	}

	public static void ifEqual( final Object a, final Object b ) {
		if( a.equals( b ) )
			throw new Err_Runtime( "Objects are equal: " + a );
	}

	public static void ifInstanceNot( final Object o, final Class<?>... allowed ) {
		for( final Class<?> a : allowed )
			if( Lib_Class.isChildOf( o, a ) )
				return;
		throw new Err_Runtime( "Invalid class: " + o.getClass(), (Object)allowed );
	}

	public static void ifInstanceNot( final Object[] arr, final Class<?>... allowed ) {
		for( final Object o : arr )
			Err.ifInstanceNot( o, allowed );
	}

	public static void ifNot( final boolean value ) {
		if( !value )
			throw new Err_Runtime( "Value is not as expected: " + value );
	}

	public static void ifNot( final int value, final int expected ) {
		if( value != expected )
			throw new Err_Runtime( "Value is not as expected! Got " + value +", but expected: "+expected );
	}

	public static void ifNot( final Object value, final Object expected ) {
		Err.ifNull( value, expected );
		if( !value.equals( expected ) )
			throw new Err_Runtime( "Value is not as expected: " + value + " != " + expected );
	}

	public static void ifNot( final Object value, final Object expected, final String message ) {
		Err.ifNull( value, expected );
		if( !value.equals( expected ) )
			throw new Err_Runtime( message + ": " + value + " != " + expected ); //Value is not as expected
	}

	public static void ifNotNull( final Object o ) {
		if( o != null )
			throw new Err_Runtime( "Object is not null!" );
	}

	public static void ifNull( final Object o ) {
		if( o == null )
			throw new Err_Runtime( "Object is null!" );
	}

	/**
	 * @implNote Only single objects are allowed here! If needed, please use "ifContainsNull".
	 */
	public static void ifNull( final Object o1, final Object o2 ) {
		if( o1 == null )
			throw new Err_Runtime( "1. Object is null!" );
		if( o2 == null )
			throw new Err_Runtime( "2. Object is null!" );
	}

	/**
	 * @implNote Only single objects are allowed here! If needed, please use "ifContainsNull".
	 */
	public static void ifNull( final Object o1, final Object o2, final Object o3 ) {
		if( o1 == null )
			throw new Err_Runtime( "1. Object is null!" );
		if( o2 == null )
			throw new Err_Runtime( "2. Object is null!" );
		if( o3 == null )
			throw new Err_Runtime( "3. Object is null!" );
	}

	/**
	 * @implNote Only single objects are allowed here! If needed, please use "ifContainsNull".
	 */
	public static void ifNull( final Object o1, final Object o2, final Object o3, final Object o4 ) {
		if( o1 == null )
			throw new Err_Runtime( "1. Object is null!" );
		if( o2 == null )
			throw new Err_Runtime( "2. Object is null!" );
		if( o3 == null )
			throw new Err_Runtime( "3. Object is null!" );
		if( o4 == null )
			throw new Err_Runtime( "4. Object is null!" );
	}

	public static void ifOutOfBounds( final double abs, final double value ) {
		Err.ifTooSmall( 0, abs );
		if( value > abs || value < -abs )
			throw new Err_Runtime( "Out of bounds", "Value: " + value, "Min: " + -abs, "Max: " + abs );
	}

	public static void ifOutOfBounds( final double min, final double max, final double... values ) {
		for( final double d : values )
			if( d > max || d < min )
				throw new Err_Runtime( "Out of bounds", "Value: " + d, "Min: " + min, "Max: " + max );
	}

	public static void ifOutOfBounds( final int min, final int max, final int... values ) {
		for( final int d : values )
			if( d > max || d < min )
				throw new Err_Runtime( "Out of bounds", "Value: " + d, "Min: " + min, "Max: " + max );
	}

	// TODO Rework ... alle mit Ident an erster Stelle?
	public static void ifOutOfBounds( final String valueIdent, final double min, final double max, final double... values ) {
		for( final double d : values )
			if( d > max || d < min )
				throw new Err_Runtime( valueIdent + " out of bounds", "Got: " + d, "Min: " + min, "Max: " + max );
	}

	public static void ifOutOfSize( final Collection<?> data, final int... indexes ) {
		final int size = data.size();

		for( final int i : indexes ) {
			if( size == 0 )
				throw new Err_Runtime( "Out of bounds", "Can't get index " + i + " from empty " + data.getClass().getSimpleName() );
			if( i < 0 )
				throw new Err_Runtime( "Out of bounds", "Value: " + i, "Min: 0" );
			if( i > size - 1 )
				throw new Err_Runtime( "Out of bounds", "Value: " + i, "Max: " + (size - 1) );
		}
	}

	public static void ifToBig( final double max, final double value ) {
		if( value > max )
			throw new Err_Runtime( "Value is to big", "Value: " + value, "Max: " + max );
	}

	public static void ifTooSmall( final double min, final double value ) {
		if( value < min )
			throw new Err_Runtime( "Value is to small", "Value: " + value, "Min: " + min );
	}

//	public static void ifOutOfBounds(final long min, final long max, final BigInteger value) {
//		if(!Lib_BigMath.isBetween(value, min, max))
//			throw new Err_Runtime("Out of bounds", "Value: " + value, "Min: " + min, "Max: " + max);
//	}
//
//	public static void ifOutOfBounds(final double min, final double max, final BigDecimal value) {
//		if(!Lib_BigMath.isBetween(value, min, max))
//			throw new Err_Runtime("Out of bounds", "Value: " + value, "Min: " + min, "Max: " + max);
//	}

	public static Err_Runtime impossible( final Object... oa ) {
		throw new Err_Runtime( "This error can not occur. It's impossible!", oa );
	}

	public static Err_Runtime invalid( final Object... oa ) {
		throw new Err_Runtime( "Invalid condition!", oa );
	}

	public static Err_Exception newException( final String info, final Object... oa ) throws Err_Exception {
		throw new Err_Exception( info, oa );
	}

	public static Err_FileSys newFileSys( final String info, final Object... oa ) throws Err_FileSys {
		throw new Err_FileSys( info, oa );
	}

	public static Err_Runtime newRuntime( final String info, final Object... oa ) {
		throw new Err_Runtime( info, oa );
	}

	public static RuntimeException show( final Throwable t ) {
		return Err.show( t, null );
	}

	public static RuntimeException show( final Throwable t, final String text ) {
		MOut.error( t, text );
		return new Err_Runtime( new RuntimeException(t), text );
	}

	public static Err_Runtime todo( final Object... oa ) {
		throw new Err_Runtime( "This functionality is not implemented yet!", oa );
	}

	public static Err_Exception wrap( final Exception e, final Object... details ) {
		Err.ifNull( e );
		return new Err_Exception( e, details );
	}

	public static Err_FileSys wrap( final IOException e, final Object... details ) {
		Err.ifNull( e );
		return new Err_FileSys( e, details );
	}

	public static Err_Runtime wrap( final RuntimeException e, final Object... details ) {
		Err.ifNull( e );
		return new Err_Runtime( e, details );
	}

	public static Err_DataBase wrap( final SQLException e, final Object... details ) {
		Err.ifNull( e );
		return new Err_DataBase( e, details );
	}

	public static Err_Exception wrap( Throwable t, final Object... details  ) {
		Err.ifNull( t );
		return new Err_Exception( t, details );
	}

}
