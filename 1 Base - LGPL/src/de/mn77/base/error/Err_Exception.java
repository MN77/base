/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.error;

import java.util.Collection;
import java.util.Collections;

import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 2019-08-09
 */
public class Err_Exception extends Exception implements I_ErrorDetails {

	private static final long  serialVersionUID = 7371608900155161922L;
	private SimpleList<Object> details;


	public Err_Exception( final String message, final Object... oa ) {
		super( message );
		this.addDetail( oa );
	}

	public Err_Exception( final Throwable cause, final Object... oa ) {
		super( cause.getMessage(), cause );
		this.addDetail( oa );
	}

	/**
	 * TODO Allow only one detail? Is more really needed?
	 */
	public void addDetail( final Object... oa ) {
		if( this.details == null )
			this.details = new SimpleList<>();

//		if(oa.length != 1)
//			MOut.line("Err_Exception.addDetail: Got "+oa.length);

//		details.add("Attachment:");
		Collections.addAll( this.details, oa );
	}

	public Collection<Object> getDetails() {
		if( this.details == null )
			this.details = new SimpleList<>();

		return this.details;
	}

}
