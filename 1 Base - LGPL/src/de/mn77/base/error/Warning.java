/*******************************************************************************
 * Copyright (C) 2009-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.error;

import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 17.04.2009
 */
public class Warning {

	public static void ifNull( final Object... oa ) {
		int i = 0;

		for( final Object o : oa ) {
			i++;
			if( o == null )
				MOut.warning( "Object " + i + " is null!" );
		}
	}

	public static <T extends Comparable<? super T>> void ifOutOfLimit( final T min, final T max, final T wert ) {
		if( min != null && wert.compareTo( min ) < 0 )
			MOut.warning( "Value is under the limit! (" + wert + " < " + min + ")" );
		if( max != null && wert.compareTo( max ) > 0 )
			MOut.warning( "Value is over the limit! (" + wert + " > " + max + ")" );
	}

	public static <T extends Comparable<? super T>> void ifOver( final T max, final T wert ) {
		Warning.ifOutOfLimit( null, max, wert );
	}

	public static <T extends Comparable<? super T>> void ifUnder( final T min, final T wert ) {
		Warning.ifOutOfLimit( min, null, wert );
	}

}
