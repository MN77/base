/*******************************************************************************
 * Copyright (C) 2016-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.event;

import java.util.function.Consumer;

import de.mn77.base.data.struct.keypot.EnumPotList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @apiNote
 *          Example:
 *          ========
 *          private static enum EVENTS { UPDATE, ACCESS };
 *
 *          public void eUpdate(Consumer<Object> z) {
 *          this.eventAddHandler(EVENTS.UPDATE, z);
 *          }
 *
 *          ...
 *          this.eventStart(EVENTS.UPDATE);
 */
public abstract class A_EnumEventHandler {

	private final EnumPotList<Consumer<?>> events;


	public A_EnumEventHandler() {
		this.events = new EnumPotList<>();
	}


	protected void eventAdd( final Enum<?> ident, final Consumer<?> c ) { //TODO rename to eventAddHandler oder addListener ...
		Err.ifNull( ident, c );
		this.events.add( ident, c );
//		this.events.ausgeben();
	}

	protected void eventStart( final Enum<?> ident ) {
		this.eventStart( ident, null );
	}

	@SuppressWarnings( "unchecked" )
	protected <T1> void eventStart( final Enum<?> ident, final T1 data ) {
		Err.ifNull( ident );
//		MOut.dev(ident.toString());

		final Iterable<Consumer<?>> consumers = this.events.get( ident );
		if( consumers != null )
			for( final Consumer<?> c : consumers ) {
				final Consumer<T1> typedC = (Consumer<T1>)c; //Wirft ggf. Fehler!
				typedC.accept( data );
			}
	}

}
