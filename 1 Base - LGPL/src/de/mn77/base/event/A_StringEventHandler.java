/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.event;

import java.util.function.Consumer;

import de.mn77.base.data.struct.keypot.StringPotList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 2021-02-04
 */
public abstract class A_StringEventHandler {

	private final StringPotList<Consumer<?>> events;


	public A_StringEventHandler() {
		this.events = new StringPotList<>();
	}


	protected void eventAdd( final String ident, final Consumer<?> c ) {
		Err.ifNull( ident, c );
		this.events.add( ident, c );
	}

	protected void eventStart( final String ident ) {
		this.eventStart( ident, null );
	}

	@SuppressWarnings( "unchecked" )
	protected <T1> void eventStart( final String ident, final T1 data ) {
		Err.ifNull( ident ); // data can be null
//		this.events.ausgeben();
//		MOut.dev(ident.toString());

		final Iterable<Consumer<?>> consumers = this.events.get( ident );
		if( consumers != null )
			for( final Consumer<?> c : consumers ) {
				final Consumer<T1> typedC = (Consumer<T1>)c; //Wirft ggf. Fehler!
				typedC.accept( data );
			}
	}

}
