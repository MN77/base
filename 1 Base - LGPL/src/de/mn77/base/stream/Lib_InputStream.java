/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.stream;

import java.io.InputStream;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;


/**
 * @author Michael Nitsche
 * @created 14.08.2022
 */
public class Lib_InputStream {

	/**
	 * @apiNote Read all available bytes and split it to lines. This function is blocking and don't closes the InputStream.
	 */
	public static SimpleList<String> readLines( final InputStream stream, final boolean utf ) throws Err_FileSys {
		Err.ifNull( stream );
		final SimpleList<String> result = new SimpleList<>();

		try {
			final byte[] allBytes = stream.readAllBytes(); // Blocking!

			if( utf ) {
				final String allBytesString = new String( allBytes, "UTF-8" );
				int start = 0;

				for( int pointer = 0; pointer < allBytesString.length(); pointer++ )
					if( allBytesString.charAt( pointer ) == '\n' ) {
						result.add( allBytesString.substring( start, pointer ) );
						start = pointer + 1;
					}
			}
			else
				Err.todo();
		}
		catch( final Exception err ) {
			throw Err.fsAccess( err );
		}

		return result;
	}

}
