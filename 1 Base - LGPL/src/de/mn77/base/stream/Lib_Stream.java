/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.stream;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.thread.A_AutoDaemon;
import de.mn77.base.thread.A_ParallelProcess;
import de.mn77.base.thread.ThreadGate;


/**
 * @author Michael Nitsche
 */
public class Lib_Stream {

	public static final int DEFAULT_BLOCKSIZE = 1024;


	/**
	 * Connects a InputStream with a OutputStream.
	 * No buffer will be used.
	 */
	public static A_ParallelProcess connect( final InputStream in, final OutputStream out ) {
		return new A_ParallelProcess() {

			@Override
			protected void process() {

				try {
					int readed = -1;

					while( (readed = in.read()) > -1 ) { // Blocking!!!
						out.write( readed );
						out.flush();
					}
				}
				catch( final Throwable t ) {
					Err.show( t );
				}
			}

		};
	}

	//ACHTUNG kein synchronized, da sonst nur immer einer kopieren könnnte wenn nicht parallel.
	public static ThreadGate connect( final InputStream in, final OutputStream out, final boolean parallel ) {
		return Lib_Stream.connect( in, out, parallel, true );
	}


	// --- Pumps data from Stream to Stream ---

	public static ThreadGate connect( final InputStream in, final OutputStream out, final boolean parallel, final boolean closeTarget ) {
		return Lib_Stream.connect( in, out, parallel, closeTarget, Lib_Stream.DEFAULT_BLOCKSIZE );
	}

	/*
	 * TODO finally
	 */
	public static ThreadGate connect( final InputStream in, final OutputStream out, final boolean parallel, final boolean closeTarget, final int block ) {
		final ThreadGate gate = parallel
			? new ThreadGate( false )
			: null;
		final Runnable run = () -> {

			try {
				final BufferedInputStream bis = new BufferedInputStream( in );
				final BufferedOutputStream bos = new BufferedOutputStream( out );
				final byte[] ba = new byte[block];

				for( int next = 0; (next = bis.read()) > -1; ) {
					bos.write( next );
					while( (next = bis.read( ba )) == block )
						bos.write( ba );
					if( next > 0 )
						bos.write( ba, 0, next );
				}

				bis.close();
				if( closeTarget )
					bos.close();
				else
					bos.flush();
				if( parallel )
					gate.open();
			}
			catch( final Throwable t ) {
				Err.show( t );
			}
		};
		if( parallel )
			new Thread( run ).start();
		else
			run.run();
		return gate;
	}

	/**
	 * Daemon, der zyklisch den Input prüft, ohne zu blockieren, bis der Process beendet ist.
	 * Verursacht ggf. eine hohe SystemLast!!!
	 */
	public static void connectNonBlockingWithInputBuffer( final InputStream in, final OutputStream out, final Process p ) {
		new A_AutoDaemon() {

			BufferedInputStream bis = null; // Bringt Geschwindigkeitsvorteil
//			out: direkt schreiben, keine Verzögerung durch Buffer


			@Override
			protected void cycle() {
				if( this.bis == null )
					this.bis = new BufferedInputStream( in ); // Direkt und mit Final zuweisen führt ggf. zu bis == null

				try {
					final int av = this.bis.available(); //!!! InputStream liefert immer 0 !!!

					if( av == 0 && !p.isAlive() ) {
						this.finish();
						return;
					}

					if( av > 0 ) {
						final byte[] ba = new byte[av];
						this.bis.read( ba );
						out.write( ba );
						out.flush(); //Für Terminal wichtig
					}
					else
//						Thread.sleep(10);
						Thread.yield();
				}
				catch( final Throwable t ) {
					Err.show( t );
					this.finish();
				}
			}

		};
	}

	/**
	 * Connects a Inputstream with a Outputstream.
	 * The InputStream will be buffered and everything will be written to the OutputStream.
	 * InputBuffer is faster than wait and block on every byte.
	 */
	public static A_ParallelProcess connectWithInputBuffer( final InputStream in, final OutputStream out ) {
		return new A_ParallelProcess() {

			BufferedInputStream bis = null; // Bringt Geschwindigkeitsvorteil
//			out: direkt schreiben, keine Verzögerung durch Buffer


			@Override
			protected void process() {
//				if(this.bis == null)
//					this.bis = new BufferedInputStream(in); // Direkt und mit Final zuweisen führt ggf. zu bis == null
//				In while integrated, cause bis was often null

				try {
					final byte[] ba = new byte[Lib_Stream.DEFAULT_BLOCKSIZE];
					int readed = -1;

//					Err.ifNull(this.bis, ba);
					while( (readed = (this.bis != null ? this.bis : (this.bis = new BufferedInputStream( in ))).read( ba )) > -1 ) { // Blocking!!!
						out.write( ba, 0, readed );
						out.flush();
					}
				}
				catch( final Throwable t ) {
					Err.show( t );
				}
			}

		};
	}

	/**
	 * Connects a InputStream with a OutputStream.
	 */
	public static A_ParallelProcess connectWithOutputBuffer( final InputStream in, final OutputStream out ) {
		return new A_ParallelProcess() {

			final BufferedOutputStream bos = new BufferedOutputStream( out );


			@Override
			protected void process() {

				try {
					int readed = -1;
					while( (readed = in.read()) > -1 )
						this.bos.write( readed );
//						bos.flush();
				}
				catch( final Throwable t ) {
					Err.show( t );
				}
			}

		};
	}

	/**
	 * @apiNote
	 *          Doesn't consider UTF-8!
	 */
	public static String readASCII( final InputStream is ) throws Err_FileSys {
		final StringBuilder sb = new StringBuilder();

		try {
			int readed = 0;

			while( readed >= 0 ) {
				readed = is.read();
				if( readed < 0 )
					is.close();
				else
					sb.append( (char)readed );
			}
		}
		catch( final IOException e ) {
			throw Err.wrap( e, "Stream-Read-Error" );
		}

		return sb.toString();
	}

	public static String readUTF8( final InputStream is ) throws Exception {
		byte[] buffer = new byte[Lib_Stream.DEFAULT_BLOCKSIZE];
		int pointer = 0;

		int read = 0;

		while( read >= 0 ) {
			read = is.read( buffer, pointer, buffer.length - pointer );

			if( read < 0 )
				is.close();
			else {
				pointer += read;

				if( pointer >= buffer.length ) {
					final byte[] buffer2 = new byte[buffer.length + Lib_Stream.DEFAULT_BLOCKSIZE];
					System.arraycopy( buffer, 0, buffer2, 0, buffer.length );
					buffer = buffer2;
				}
			}
		}

		final byte[] buffer2 = new byte[pointer];
		System.arraycopy( buffer, 0, buffer2, 0, pointer );
		return new String( buffer2, "UTF-8" );
	}

}
