/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.stream;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 13.03.2021
 */
public class UTF8InputStream extends InputStream {

	boolean              closed  = false;
	private final byte[] buffer;
	private int          pointer = 0;


	public UTF8InputStream( final String s ) {
		this.buffer = s.getBytes( StandardCharsets.UTF_8 );
	}

	@Override
	public void close() throws IOException {
		super.close();
		this.closed = true;
	}

	@Override
	public int read() throws IOException {
		if( this.closed )
			Err.forbidden( "Stream is closed!" );
		return this.pointer >= this.buffer.length ? -1 : this.buffer[this.pointer++];
	}

}
