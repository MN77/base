/*******************************************************************************
 * Copyright (C) 2018-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.stream;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import de.mn77.base.data.struct.atomic.ByteList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 13.03.2018
 * @implNote
 *           TODO Evtl. mit StreamZuText kombinieren!
 */
public class UTF8OutputStream extends OutputStream {

//	private final ThreadGate gate = new ThreadGate(false);
	boolean                closed = false;
	private final ByteList buffer = new ByteList();


//	public Writer getWriter() {
//		return new OutputStreamWriter(this);
//	}


	@Override
	public void close() throws IOException {
		super.close();
		this.closed = true;
//		gate.open();
	}

	public String getString() {
		if( !this.closed )
			Err.forbidden( "Stream not closed!" );

//		try {
//			gate.traffic();
//		}
//		catch(final InterruptedException e1) {
//			Err.show(e1, true);
//		}
		try {
			return new String( this.buffer.toArray(), "UTF-8" );
		}
		catch( final UnsupportedEncodingException e2 ) {
			throw Err.exit( e2 );
		}
	}

	@Override
	public void write( final int c ) throws IOException {
		if( this.closed )
			Err.forbidden( "Stream is closed!" );
		this.buffer.add( (byte)c );
	}

}
