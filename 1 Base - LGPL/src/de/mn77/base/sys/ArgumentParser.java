/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys;

import java.util.HashMap;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.util.U_StringArray;
import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 * @created 24.04.2022
 * @apiNote
 *          Switches: -n, -v, -vn, --dry, --verbose
 *          Propertys: --include=foo, -include=foo
 *          Other: anything else
 *
 * @TODO: Propertys only with 2 leading '-'!
 */
public class ArgumentParser {

	private final boolean allowSwitches;
	private final boolean allowPropertys;
	private final boolean allowOthers;

	private boolean                 parsed    = false;
	private String[]                switches  = null;
	private String[]                others    = null;
	private HashMap<String, String> propertys = null;


	public ArgumentParser( final boolean allowSwitches, final boolean allowPropertys, final boolean allowOthers ) {
		this.allowSwitches = allowSwitches;
		this.allowPropertys = allowPropertys;
		this.allowOthers = allowOthers;
	}


	public String[] getOthers() {
		this.iCheckParsed();
		return this.others;
	}

	public HashMap<String, String> getPropertys() {
		this.iCheckParsed();
		return this.propertys;
	}

	public String[] getSwitches() {
		this.iCheckParsed();
		return this.switches;
	}

	public void parse( final String[] args ) {
		if( this.parsed )
			throw new Err_Runtime( "Parsing already done!" );

		final SimpleList<String> switches = new SimpleList<>();
		this.propertys = new HashMap<>();

		for( int i = 0; i < args.length; i++ ) {
			final String arg = args[i];

			if( arg.startsWith( "--" ) ) {
				this.iParse( switches, arg, 2, false );
				continue;
			}
			else if( arg.startsWith( "-" ) ) {
				this.iParse( switches, arg, 1, true );
				continue;
			}
			else {
				this.others = U_StringArray.cutFrom( args, i );
				break;
			}
		}

		// Set
		this.switches = switches.toArray( new String[switches.size()] );

		if( this.others == null )
			this.others = new String[0];

		// Check
		if( !this.allowSwitches && this.switches.length > 0 )
			throw new Err_Runtime( "No switches allowed" );
		if( !this.allowPropertys && this.propertys.size() > 0 )
			throw new Err_Runtime( "No propertys allowed" );
		if( !this.allowOthers && this.others.length > 0 )
			throw new Err_Runtime( "Other arguments are not allowed" );

		this.parsed = true;
	}

	private void iCheckKey( final String key ) {
		for( final char c : key.toCharArray() )
			if( !(c >= '0' && c <= '9' || c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z') )
				throw new Err_Runtime( "Illegal argument key: " + key );
	}

	private void iCheckParsed() {
		if( !this.parsed )
			throw new Err_Runtime( "Nothing parsed!" );
	}

	private void iParse( final SimpleList<String> switches, final String arg, final int indent, final boolean split ) {
		String key = arg.substring( indent );
		String value = null;

		final int eIdx = key.indexOf( '=' );

		if( eIdx >= 0 ) {
			value = key.substring( eIdx + 1 );
			key = key.substring( 0, eIdx );
		}

		this.iCheckKey( key );

		if( value != null )
			this.propertys.put( key, value );
		else if( split )
			for( final char c : key.toCharArray() )
				switches.add( "" + c );
		else
			switches.add( key );
	}

}
