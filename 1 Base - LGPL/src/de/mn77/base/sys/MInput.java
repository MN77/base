/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys;

import java.util.NoSuchElementException;
import java.util.Scanner;


/**
 * @author Michael Nitsche
 * @created 27.02.2022
 */
public class MInput {

	private static Scanner sc = new Scanner( System.in ); // close, open another one ... doesn't work


	public static Boolean readBool() {
		String s = MInput.sc.nextLine();
		if( s == null )
			return null;
		s = s.trim().toLowerCase();

		switch( s ) {
			case "true":
			case "1":
				return true;
			case "false":
			case "0":
				return false;
			default:
				return null;
		}
	}

	public static boolean readBoolDefault( final boolean b ) {
		final Boolean in = MInput.readBool();
		return in == null ? b : in;
	}

	public static Character readChar() {
		final String s = MInput.sc.nextLine();
		if( s == null || s.length() != 1 )
			return null;
		return s.charAt( 0 );
	}

	public static char readCharDefault( final char c ) {
		final Character in = MInput.readChar();
		return in == null ? c : in;
	}

	public static Double readDouble() {

		try {
			String s = MInput.sc.nextLine();
			if( s == null )
				return null;
			s = s.replace( ',', '.' );
			return Double.parseDouble( s );
		}
		catch( final NumberFormatException e ) {
			return null;
		}
	}

	public static double readDoubleDefault( final double d ) {
		final Double in = MInput.readDouble();
		return in == null ? d : in;
	}

	public static Integer readInt() {

		try {
			String s = MInput.sc.nextLine();
			if( s == null )
				return null;
			s = s.replace( ',', '.' );
			return Integer.parseInt( s );
		}
		catch( final NumberFormatException e ) {
			return null;
		}
	}

	public static int readIntDefault( final int i ) {
		final Integer in = MInput.readInt();
		return in == null ? i : in;
	}

	public static String readString() {
		try {
			return MInput.sc.nextLine();
		}
		catch(NoSuchElementException e) { // When input empty or no line found
			return null;
		}
	}

	/**
	 * Returns the default string, if the result is null or empty
	 */
	public static String readStringDefault( final String s ) {
		final String in = MInput.readString();
		return in == null || in.length() == 0 ? s : in;
	}

}
