/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import de.mn77.base.data.datetime.MDateTime;
import de.mn77.base.data.util.Lib_Array;
import de.mn77.base.debug.DEBUG_MODE;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.file.I_Directory;
import de.mn77.base.sys.file.SysDir;
import de.mn77.base.sys.out.CHANNEL_IN;
import de.mn77.base.sys.out.CHANNEL_OUT;
import de.mn77.base.sys.out.MOutWriter;
import de.mn77.base.sys.out.OUTPUT_DETAIL;
import de.mn77.base.sys.out.OUTPUT_STYLE;
import de.mn77.base.sys.out.TRACE;


/**
 * @author Michael Nitsche
 * @implNote
 *           Everything is static!
 */
public class MOut {

	private static boolean          fastOut   = true;
	private static boolean          useLog    = false;
	private static String           linebreak = System.lineSeparator(); //Sys.getSeparatorLine();
	private static final MOutWriter writer    = new MOutWriter();


	public static void describe( final Object... oa ) {
		MOut.writer.write( CHANNEL_IN.OUT, OUTPUT_DETAIL.DESCRIBE, TRACE.NO, false, true, null, oa );
	}

	public static void dev( final Object... oa ) {
		if( !MOut.fastOut )
			MOut.writer.write( CHANNEL_IN.DEV, OUTPUT_DETAIL.RAW, TRACE.NO, false, true, null, oa );
	}

	public static void echo( final Object... oa ) {
		MOut.writer.write( CHANNEL_IN.OUT, OUTPUT_DETAIL.RAW, TRACE.NO, false, false, null, oa );
	}

	public static void echo( final String s ) {
		if( MOut.fastOut )
			MOut.writer.directOut( s );
		else
			MOut.echo( new Object[]{ s } );
	}

	public static void echoErr( final Object... oa ) {
		MOut.writer.write( CHANNEL_IN.ERR, OUTPUT_DETAIL.RAW, TRACE.NO, false, false, null, oa );
	}

//	public static void echoErr(final String s) {
//		if(MOut.raw) {
//			MOut.writer.directErr(s);
//			MOut.writer.writeLog(CHANNEL_IN.ERR, TRACE.NO, false, null, new Object[]{s});
//		}
//		else
//			MOut.echoErr(new Object[]{s});
//	}

	public static void error( final String text ) {
		MOut.error( new RuntimeException(), text );
	}

	public static void error( final Throwable t ) {
		MOut.error( t, null );
	}

	public static void error( final Throwable t, final String text ) {
		MOut.writer.writeError( t, text, false );
	}

	public static void exit( final Object... oa ) {
		final StackTraceElement[] ste = new Throwable().getStackTrace();
		final String message = "!!! DEBUG EXIT !!!";
		final Object[] oa2 = Lib_Array.append( oa, message );
		MOut.writer.write( CHANNEL_IN.ERR, OUTPUT_DETAIL.RAW, TRACE.FULL, false, true, ste, oa2 );
		System.exit( 1 );
	}

	public static boolean getJavaErrors() {
		return MOut.writer.getJavaErrors();
	}

	public static String getLineBreak() {
		return MOut.linebreak;
	}

	public static void ident( final Object... oa ) {
		MOut.writer.write( CHANNEL_IN.OUT, OUTPUT_DETAIL.IDENT, TRACE.NO, false, true, null, oa );
	}

	public static boolean isLogUsed() {
		return MOut.useLog;
	}

	public static boolean isModeDev() {
		return !MOut.fastOut;
	}

	public static void line( final Object... oa ) {	// TODO use TRACE.ONE
		final StackTraceElement[] ste = new Throwable().getStackTrace();
		MOut.writer.write( CHANNEL_IN.OUT, OUTPUT_DETAIL.RAW, TRACE.ONE, false, true, ste, oa );
	}

	public static void log( final Object... oa ) {
		MOut.writer.write( CHANNEL_IN.LOG, OUTPUT_DETAIL.RAW, TRACE.NO, false, true, null, oa );
	}

	public static void logErr( final Object... oa ) {
		MOut.writer.writeLog( CHANNEL_IN.ERR, TRACE.NO, false, null, oa );
	}

	public static void print() {
		if( MOut.fastOut )
			MOut.writer.directOut( MOut.linebreak );
		else
			MOut.print( new Object[]{ "" } );
	}

	public static void print( final Object... oa ) {
		MOut.writer.write( CHANNEL_IN.OUT, OUTPUT_DETAIL.RAW, TRACE.NO, false, true, null, oa );
	}

	public static void print( final String s ) {
		if( MOut.fastOut )
			MOut.writer.directOut( s + MOut.linebreak );
		else
			MOut.print( new Object[]{ s } );
	}

//	public static void printErr(final String s) {
//		if(MOut.raw) {
//			MOut.writer.directErr(s + MOut.linebreak);
//			MOut.writer.writeLog(CHANNEL_IN.ERR, TRACE.NO, false, null, new Object[]{s});
//		}
//		else
//			MOut.printErr(new Object[]{s});
//	}

	public static void printErr( final Object... oa ) {
		MOut.writer.write( CHANNEL_IN.ERR, OUTPUT_DETAIL.RAW, TRACE.NO, false, true, null, oa );
	}

	public static void reset() {
		MOut.setDebug( DEBUG_MODE.NO );
	}

	public static void setDebug() {
		MOut.setDebug( DEBUG_MODE.MINIMAL );
	}

	public static void setDebug( final DEBUG_MODE debug ) {

		if( debug == DEBUG_MODE.NO ) {
			MOut.setDeveloper( false );
			MOut.writer.setStyle( OUTPUT_STYLE.RAW );
		}
		else {
			MOut.setDeveloper( true );
			if( debug == DEBUG_MODE.MINIMAL )
				MOut.writer.setStyle( OUTPUT_STYLE.CHANNEL );
			else if( debug == DEBUG_MODE.DETAIL )
				MOut.writer.setStyle( OUTPUT_STYLE.TIMESTAMP );
			else {// PARANOID
				MOut.writer.setStyle( OUTPUT_STYLE.TIMESTAMP );
				MOut.writer.setDefaultTrace( TRACE.ONE );
			}
		}
	}

	public static void setDeveloper( final boolean enable ) {

		if( enable ) {
			MOut.fastOut = false;
			MOut.writer.setModeDeveloper();
		}
		else {
			MOut.fastOut = true;
			MOut.writer.setModeRaw();
		}
	}

	public static void setJavaErrors( final boolean b ) {
		MOut.writer.setJavaErrors( b );
	}

	public static void setLineBreak( final String s ) {
		MOut.linebreak = s;
		MOut.writer.setLineBreak( s );
	}

	public static void setLog( final String appName ) throws Err_FileSys {
		Err.ifEmpty( appName );
		final I_Directory dir = SysDir.userConfig_MN77( appName ).dirFlex( "logs" );
		MOut.setLogDir( dir );
	}

	public static void setLogDir( final I_Directory dir ) {
		Err.ifNull( dir );
		MOut.useLog = true;
		MOut.writer.setLogFile( dir.fileMay( new MDateTime().toStringFileSysSlim(), "txt" ).getFile() );
	}

	public static void setLogFile( final File file ) {
		Err.ifNull( file );
		MOut.useLog = true;
		MOut.writer.setLogFile( file );
	}

	public static void setRouteDev( final CHANNEL_OUT... targets ) {
		MOut.writer.routeDev( targets );
	}

	public static void setRouteErr( final CHANNEL_OUT... targets ) {
		MOut.writer.routeErr( targets );
	}

	public static void setRouteLog( final CHANNEL_OUT... targets ) {
		MOut.writer.routeLog( targets );
	}

	public static void setRouteOut( final CHANNEL_OUT... targets ) {
		MOut.writer.routeOut( targets );
		MOut.fastOut = false;
	}

	public static void setRouteWrn( final CHANNEL_OUT... targets ) {
		MOut.writer.routeWrn( targets );
	}

	public static void setStyle( final OUTPUT_STYLE style ) {
		MOut.writer.setStyle( style );
		MOut.fastOut = style == OUTPUT_STYLE.RAW;
	}

	public static OutputStream streamError() {
		return MOut.iStream( CHANNEL_IN.ERR );
	}

	public static OutputStream streamText() {
		return MOut.iStream( CHANNEL_IN.OUT );
	}

	public static void temp( final Object... oa ) {
		final StackTraceElement[] ste = new Throwable().getStackTrace();
		MOut.writer.write( CHANNEL_IN.ERR, OUTPUT_DETAIL.IDENT, TRACE.ONE, false, true, ste, oa );
	}

	public static void trace( final Object... oa ) {	// TODO use TRACE.FULL
		final StackTraceElement[] ste = new Throwable().getStackTrace();
		MOut.writer.write( CHANNEL_IN.OUT, OUTPUT_DETAIL.RAW, TRACE.FULL, false, true, ste, oa );
	}

	public static void warning( final Object... oa ) {
		final StackTraceElement[] ste = new Throwable().getStackTrace();
		MOut.writer.write( CHANNEL_IN.WRN, OUTPUT_DETAIL.RAW, TRACE.ONE, false, true, ste, oa );
	}

	private static PrintStream iStream( final CHANNEL_IN channel ) {
		return new PrintStream( new OutputStream() {

			private final StringBuilder sb = new StringBuilder();


			@Override
			public void close() throws IOException {
				super.close();
				MOut.writer.write( channel, OUTPUT_DETAIL.RAW, TRACE.NO, false, false, null, new Object[]{ this.sb.toString() } );
			}

			@Override
			public void write( final int i ) throws IOException {
				this.sb.append( new String( new byte[]{ (byte)i } ) );
			}

		} );
	}

//	public static void debugOffset(final int offset, final Object... oa) {
//		if(MOut.debugLevel == DEBUG_LEVEL.NO)
//			return;
//		final StackTraceElement[] trace = new Throwable().getStackTrace();
//
//		Err.ifOutOfBounds(0, Math.min(trace.length - 2, 3), offset);
//		final int destLen = trace.length - offset;
//		final StackTraceElement[] trace2 = new StackTraceElement[destLen];
//		System.arraycopy(trace, offset, trace2, 0, destLen); //TODO trace2 wird nicht verwendet?!?!?
//
//		MOut.iWrite(CHANNEL.STDOUT, false, true, false, trace2, oa);
//	}

//	public static void originLine(int offset) {
//		final StackTraceElement[] trace = new Throwable().getStackTrace();
//		offset = Math.min(trace.length - 1, offset);
//		final String msg = offset >= trace.length ? "" : "   @ " + trace[offset].toString();
//		MOut.iWrite(CHANNEL.STDOUT, false, true, false, trace, msg);
//	}

}
