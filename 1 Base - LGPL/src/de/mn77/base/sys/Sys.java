/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.lang.management.ManagementFactory;
import java.util.Enumeration;
import java.util.Properties;

import de.mn77.base.data.filter.FilterString;
import de.mn77.base.data.form.FormString;
import de.mn77.base.error.Err;
import de.mn77.base.event.Procedure;


/**
 * @author Michael Nitsche
 */
public class Sys {

	public static void correctCharset() {
		String cpConsole = System.getProperty( "console.encoding" );

		if( cpConsole == null ) {
			final String osName = System.getProperty( "os.name" );
			final String cpFile = System.getProperty( "file.encoding" );

			// Guess code page, maybe not right: (Cp850 is only for western europe):
			cpConsole = osName != null && osName.contains( "Windows" ) ? "Cp850" : cpFile;
		}

		if( !cpConsole.startsWith( "UTF-" ) ) // .equals("UTF-8")
			try {
				System.setOut( new PrintStream( System.out, true, cpConsole ) );
				System.setErr( new PrintStream( System.err, true, cpConsole ) );
			}
			catch( final UnsupportedEncodingException e ) {
				Err.show( e );
			}
	}

	public static String getArchitectur() {
		return System.getProperty( "os.arch" );
	}

	public static String getArchitecturModel() {
		return System.getProperty( "sun.arch.data.model" );
	}

	public static String getClassPath() {
		return System.getProperty( "java.class.path" );
	}

	public static String getCurrentDir() {
		return System.getProperty( "user.dir" );
	}

	public static String getDesktop() {
		return System.getProperty( "sun.desktop" );
	}

	public static String getFileEncoding() {
		return System.getProperty( "file.encoding" );
	}

	public static String getJavaVersion() {
		return System.getProperty( "java.version" );
	}

	public static String getLand() {
		return System.getProperty( "user.country" );
	}

	public static String getLanguage() {
		return System.getProperty( "user.language" );
	}

	/**
	 * Möglichkeiten:
	 * Linux,
	 * Windows XP, Windows NT, Windows 2000, Windows 2003
	 * Solaris, SunOS,
	 */
	public static String getOsName() {
		return System.getProperty( "os.name" );
	}

	public static String getOsVersion() {
		return System.getProperty( "os.version" );
	}

	public static String getPathHome() {
		return System.getProperty( "user.home" );
	}

	public static String getPathTemp() {
		return System.getProperty( "java.io.tmpdir" );
	}

	public static Integer getProcessID() {
		String s = ManagementFactory.getRuntimeMXBean().getName();
//		MOut.dev(s);
		if( s == null )
			return null;
		if( !s.matches( "^([0-9]+).*$" ) )
			s = "";
		s = s.replaceFirst( "^([0-9]+).*$", "$1" );
		if( s.length() > 0 )
			return Integer.parseInt( s );
		return null;
	}

	public static long getProcessUptimeMSek() {
		return ManagementFactory.getRuntimeMXBean().getUptime();
	}

	public static String getSeparatorLine() {
		return System.getProperty( "line.separator" );
	}

	/** : **/
	public static String getSeparatorPath() {
		return System.getProperty( "path.separator" );
	}

	/** / **/
	public static String getSeperatorDir() {
		return System.getProperty( "file.separator" );
	}

	public static Thread[] getThreadsActive() {
		ThreadGroup start = Thread.currentThread().getThreadGroup();
		while( start.getParent() != null )
			start = start.getParent();

		final Thread result[] = new Thread[start.activeCount()];
		start.enumerate( result, true );
		return result;
	}

	public static String getUserName() {
		return System.getProperty( "user.name" );
	}

	public static boolean isLinux() {
		final String system = Sys.iToShortLowerCase( Sys.getOsName() );
		return system.contains( "nux" ) || system.contains( "nix" ); // linux, unix
	}

	public static boolean isMac() {
		final String system = Sys.iToShortLowerCase( Sys.getOsName() );
		return system.contains( "mac" ) || system.contains( "darwin" );
	}

	public static boolean isWindows() {
		final String system = Sys.iToShortLowerCase( Sys.getOsName() );
		return system.contains( "windows" ) || system.contains( "win" );
	}

	/**
	 * Shows all System-Properties
	 */
	public static void main( final String[] args ) {

		try {
			MOut.print( Sys.getProcessID(), "" );
			final Properties props = System.getProperties();
			final Enumeration<Object> e = props.keys();

			while( e.hasMoreElements() ) {
				final Object key = e.nextElement();
				final Object val = props.get( key );
				MOut.print( FormString.width( 30, "" + key, false ) + val );
			}
		}
		catch( final Throwable t ) {
			Err.show( t );
		}
	}


	/*
	 * --- SYSTEM-Ident ---
	 *
	 * http://www.osgi.org/Specifications/Reference
	 * https://www.osgi.org/developer/specifications/reference/
	 */

	public static void shutdownHook( final Procedure p ) {
		Runtime.getRuntime().addShutdownHook( new Thread() {

			@Override
			public void run() {
				p.execute();
			}

		} );
	}

	public static boolean sleep( final int ms ) {

		try {
			Thread.sleep( ms );
		}
		catch( final InterruptedException e ) {
			return false;
		}

		return true;
	}

	public static boolean sleepMinutes( final int minutes ) {
		return Sys.sleep( minutes * 60 * 1000 );
	}

	public static boolean sleepSeconds( final int seconds ) {
		return Sys.sleep( seconds * 1000 );
	}

	// ---

	private static String iToShortLowerCase( final String name ) {
		return FilterString.removeChars( new char[]{ ' ' }, name.toLowerCase() );
	}

	//Wenn nötig und wirklich sinnvoll
//	public static final String separator_Line_Linux=""+(char) 10;
//	public static final String separator_Line_Win=  ""+(char) 13 + (char) 10;
//	public static final String separator_Line_Mac=  """+(char) 13;
//	public static final char separator_Dir_Linux=	'/';
//	public static final char separator_Dir_Windows=	'\\';
//	public static final char separator_Dir_Mac=		'/';

}
