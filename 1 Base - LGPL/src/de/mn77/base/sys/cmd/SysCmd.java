/*******************************************************************************
 * Copyright (C) 2008-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.cmd;

import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Exception;
import de.mn77.base.stream.Lib_Stream;
import de.mn77.base.stream.UTF8OutputStream;
import de.mn77.base.sys.Sys;
import de.mn77.base.sys.file.I_Directory;
import de.mn77.base.thread.A_ParallelProcess;


/*
 * The Runtime.exec(String) method takes a single command string that it splits into a command and a sequence of arguments.
 * The ProcessBuilder constructor takes a (varargs) array of strings. The first string is the command name and the rest of them are the arguments.
 * So take care, and don't telling ProcessBuilder to execute a "command" whose name has spaces and other junk in it.
 * Of course, the operating system can't find a command with that name, and the command execution fails.
 */
/**
 * @author Michael Nitsche
 */
public class SysCmd {

	// --- Static ---

//	public static SysCmdResult start(final String cmd) throws Exception {
//		return SysCmd.start(cmd, false);
//	}
//
//	public static SysCmdResult start(final String cmd, final boolean wait) throws Exception {
//		return SysCmd.start(cmd, wait);
//	}
//
//	public static SysCmdResult start(final String cmd, final String... args) throws Exception {
//		return SysCmd.start(null, cmd, false, args);
//	}

	public static SysCmdResult exec( final boolean shellEnv, final boolean wait, final I_Directory workingDir, final String cmd, final String... args ) throws Exception {
		final SysCmdData data = new SysCmdData( shellEnv, wait, SYSCMD_IO.LIVE, cmd, args );
		data.setWorkingDir( workingDir );
		return new SysCmd().exec( data );
	}

	public static SysCmdResult exec( final boolean shellEnv, final boolean wait, final String cmd, final String... args ) throws Exception {
		return SysCmd.exec( shellEnv, wait, null, cmd, args );
	}

	public SysCmdResult exec( final SysCmdData data ) throws Err_Exception {
		Err.ifNull( data, data.command );

//		if(data.startDir.get() != null)
//			cmd = "cd \"" + data.startDir.get() + "\"; " + cmd;
//		MOut.dev(cmd);

		A_ParallelProcess handleOutput = null;
		A_ParallelProcess handleError = null;
		A_ParallelProcess handleInput = null;
		UTF8OutputStream bufferOutput = null;
		UTF8OutputStream bufferError = null;

		final String[] args = data.environment
			? this.iCommandWithEnv( data )
			: this.iCommandWithoutEnv( data );

		try {
//			final Process p = Runtime.getRuntime().exec(args);
			final ProcessBuilder pb = new ProcessBuilder( args );

			if( data.getWorkingDir() != null )
				pb.directory( data.getWorkingDir().getFile() );

//			REDIRECT = DISCARD, INHERIT, PIPE
			switch( data.inout ) {
				case LIVE:
					pb.inheritIO();
					break;
				case DISCARD:
//					pb.redirectInput(Redirect.PIPE);	// PIPE is default, DISCARD is not allowed!
					pb.redirectOutput( Redirect.DISCARD );
					pb.redirectError( Redirect.DISCARD );
					break;
				case CUSTOM:
//					pb.redirectInput(Redirect.PIPE);	// PIPE is default
//					pb.redirectOutput(Redirect.PIPE);	// PIPE is default
//					pb.redirectError(Redirect.PIPE);	// PIPE is default
					// TODO Pipe data to a process, exp: 'cat'
					break;
				case BUFFER:
					pb.redirectInput( Redirect.INHERIT );
//					pb.redirectOutput(Redirect.PIPE);	// PIPE is default
//					pb.redirectError(Redirect.PIPE);	// PIPE is default
					break;
			}

			// Start process
			final Process process = pb.start();

			if( !data.wait )
				return new SysCmdResult( process );
			else {
				// DON'T CLOSE System.in OR System.out // Don't do it!

				switch( data.inout ) {
					case BUFFER:
						bufferOutput = new UTF8OutputStream();
						handleOutput = Lib_Stream.connectWithInputBuffer( process.getInputStream(), bufferOutput );
						bufferError = new UTF8OutputStream();
						handleError = Lib_Stream.connectWithInputBuffer( process.getErrorStream(), bufferError );
						break;
					case CUSTOM:
						if( data.getStreamOutput() != null ) // Ginge auch ohne waitFor
							handleOutput = Lib_Stream.connectWithInputBuffer( process.getInputStream(), data.getStreamOutput() );
						if( data.getStreamError() != null ) // Ginge auch ohne waitFor
							handleError = Lib_Stream.connectWithInputBuffer( process.getErrorStream(), data.getStreamError() );
						if( data.getStreamInput() != null )
							handleInput = Lib_Stream.connectWithOutputBuffer( data.getStreamInput(), process.getOutputStream() ); // Vmtl. okay, aber beendet sich nicht!
//							Lib_Stream.connectIOnonBlockingWithInputBuffer(data.streamInput.get(), process.getOutputStream(), process);
						break;
					default:
				}

				final int result = process.waitFor(); // Wait for process finish

				if( handleOutput != null ) {
					handleOutput.interrupt();
					while( !handleOutput.isFinished() )
//						Sys.sleep(10);
						Thread.yield();
				}

				if( handleError != null ) {
					handleError.interrupt();
					while( !handleError.isFinished() )
//						Sys.sleep(10);
						Thread.yield();
				}

//				if(handleInput != null)
//					while(!handleInput.isFinished())
////					Sys.sleep(10);
//						Thread.yield();

				if( bufferOutput != null )
					bufferOutput.close();
				if( bufferError != null )
					bufferError.close();

				return new SysCmdResult( process, bufferOutput == null ? null : bufferOutput.getString(), bufferError == null ? null : bufferError.getString(), result );
			}
		}
		catch( final IOException e ) {
			throw Err.wrap( e, "External process was not executed cleanly!", data );
		}
		catch( final InterruptedException e1 ) {
			throw Err.wrap( e1, "External process was not executed cleanly!", data );
		}
		finally {

			try {
				if( bufferOutput != null )
					bufferOutput.close();
				if( bufferError != null )
					bufferError.close();
				if( handleOutput != null )
					handleOutput.interrupt();
				if( handleError != null )
					handleError.interrupt();
				if( handleInput != null )
					handleInput.interrupt();
			}
			catch( final IOException e ) {
				throw Err.wrap( e );
			}
		}

//		if(data.result.get() != null && data.result.get() != 0)
//			throw Err.newRuntime("The execution of the command failed. Error code: " + data.result.get(), data); //TODO Evtl. in eigenem Sys-Fehler?
	}

	private String[] iCommandWithEnv( final SysCmdData data ) {
		// Note: ProcessBuilder quotes all arguments which contains whitespace

		final StringBuilder cmd = new StringBuilder();

		if( data.arguments != null && data.arguments.length > 0 ) {
			cmd.append( data.command );

			for( final String arg : data.arguments ) {
				cmd.append( " " );
				cmd.append( arg );
			}
		}
		else
			cmd.append( data.command );

		if( Sys.isLinux() )
			return new String[]{ "bash", "-c", cmd.toString() };	// 'sh' will maybe lead to 'dash'
		else if( Sys.isWindows() )
			return new String[]{ "cmd", "/c", cmd.toString() };
		else if( Sys.isMac() )
			return new String[]{ "open", "-a", cmd.toString() }; // /usr/bin/open
		else // Fallback
			return new String[]{ cmd.toString() };
//			throw Err.todo("Unknown system: " + Sys.getOsName() + " " + Sys.getOsVersion());
	}

	private String[] iCommandWithoutEnv( final SysCmdData data ) {

		if( data.arguments == null || data.arguments.length == 0 )
			return new String[]{ data.command };
		else {
			final String[] result = new String[1 + data.arguments.length];
			result[0] = data.command;
			System.arraycopy( data.arguments, 0, result, 1, data.arguments.length );
			return result;
		}
	}

}
