/*******************************************************************************
 * Copyright (C) 2008-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.cmd;

import java.io.InputStream;
import java.io.OutputStream;

import de.mn77.base.sys.file.I_Directory;


/**
 * @author Michael Nitsche
 * @created 22.08.2008
 * @rework 2021-09-01
 * @rework 2022-06-24
 */
public class SysCmdData {

	public final String    command;
	public final String[]  arguments;
	public final boolean   wait;
	public final boolean   environment;
	public final SYSCMD_IO inout;

	private I_Directory  workingDir   = null;
	private InputStream  streamInput  = null;
	private OutputStream streamOutput = null;
	private OutputStream streamError  = null;


	public SysCmdData( final boolean shellEnv, final boolean wait, final SYSCMD_IO io, final String command, final String[] arguments ) {
		this.command = command;
		this.arguments = arguments;
		this.environment = shellEnv;
		this.wait = wait;
		this.inout = io;
	}

	public OutputStream getStreamError() {
		return this.streamError;
	}

	public InputStream getStreamInput() {
		return this.streamInput;
	}

	public OutputStream getStreamOutput() {
		return this.streamOutput;
	}

	public I_Directory getWorkingDir() {
		return this.workingDir;
	}

	public void setStreams( final InputStream input, final OutputStream output, final OutputStream error ) {
		this.streamInput = input;
		this.streamOutput = output;
		this.streamError = error;
	}

	public void setWorkingDir( final I_Directory dir ) {
		this.workingDir = dir;
	}

}
