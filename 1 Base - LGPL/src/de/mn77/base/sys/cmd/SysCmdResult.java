/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.cmd;

/**
 * @author Michael Nitsche
 * @created 01.09.2021
 */
public class SysCmdResult {

	public final Process process;
	public final String  output;
	public final String  error;
	public final Integer result;


	public SysCmdResult( final Process process ) {
		this.process = process;
		this.output = null;
		this.error = null;
		this.result = null;
	}

	public SysCmdResult( final Process process, final String output, final String error, final Integer result ) {
		this.process = process;
		this.output = output;
		this.error = error;
		this.result = result;
	}

}
