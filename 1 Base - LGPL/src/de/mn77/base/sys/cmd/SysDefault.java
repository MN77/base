/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.cmd;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.Sys;


/**
 * @author Michael Nitsche
 * @created 23.03.2021
 *
 *          Runs a default application on every platform
 */
public class SysDefault {

	public static void openBrowser( final URI uri ) throws Exception {
		if( Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported( Desktop.Action.BROWSE ) )
			Desktop.getDesktop().browse( uri );
		else
//			SysDefault.fallback( uri.toString() );
			SysDefault.open( uri.toString() );
	}

	public static Process open( final String s ) throws Err_FileSys {
		try {
			if( Sys.isWindows() )
				return new ProcessBuilder( "rundll32", "url.dll,FileProtocolHandler", s ).start();
			if( Sys.isMac() )
				return new ProcessBuilder( "open", s ).start();
			if( Sys.isLinux() )
				return new ProcessBuilder( "xdg-open", s ).start();
//				String env=System.getenv("DESKTOP_SESSION");
//				Err.ifNull(env);
//				args = new String[]{"xdg-open", path}; // Leitet automatisch an den zugehörigen Desktop-Open-Befehl weiter.
//				if(env.equals("gnome"))			parameter=new String[]{"gnome-open",pfad};
//				if(env.equals("gnome-shell"))	parameter=new String[]{"gnome-open",pfad};
//				if(env.equals("xfce4"))			parameter=new String[]{"gnome-open",pfad};
//				if(env.equals("kde"  ))			parameter=new String[]{"kfmclient","exec",pfad};
//				if(env.equals("default"))		parameter=new String[]{"gnome-open",pfad}; //z.B. Gnome bei Martina
//				if(parameter==null)
//					Err.direct("Unbekanntes Desktop-System", env);

			throw Err.todo( "Unknown Operating-System", Sys.getOsName(), Sys.getOsVersion() );
		}
		catch( final IOException e ) {
			throw Err.newFileSys( "Starting failed", e, s );
		}
	}

//	private static void fallback( final String s ) throws Exception {
//		final Runtime rt = Runtime.getRuntime();
//		if( Sys.isLinux() )
//			rt.exec( "xdg-open " + s );
//		else if( Sys.isMac() )
//			rt.exec( "open " + s );
//		else if( Sys.isWindows() )
//			rt.exec( "rundll32 url.dll,FileProtocolHandler " + s );
//		// otherwise do nothing
//	}

}
