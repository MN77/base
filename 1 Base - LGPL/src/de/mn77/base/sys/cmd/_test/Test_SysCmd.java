/*******************************************************************************
 * Copyright (C) 2008-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.cmd._test;

import de.mn77.base.error.Err;
import de.mn77.base.sys.cmd.SysCmd;
import de.mn77.base.sys.file.SysDir;


/**
 * @author Michael Nitsche
 *         21.08.2008 Erstellt
 */
public class Test_SysCmd {

	public static void main( final String[] args ) {

		try {
			Test_SysCmd.start();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

	public static void start() throws Throwable {
		SysCmd.exec( true, true, SysDir.temp(), "eog" );
		SysCmd.exec( true, true, "pluma" );
	}

}
