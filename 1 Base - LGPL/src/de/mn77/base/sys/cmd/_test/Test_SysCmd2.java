/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.cmd._test;

import de.mn77.base.error.Err;
import de.mn77.base.sys.cmd.SysCmd;


/**
 * @author Michael Nitsche
 * @created 31.08.2021
 */
public class Test_SysCmd2 {

	public static void main( final String[] args ) {

		try {
			Test_SysCmd2.start();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

	public static void start() throws Throwable {
		final String[] commands = { "/bin/sh", "-c", "echo 123;sleep 5s;echo 456" };
//		final String[] commands = new String[]{"echo 1; read OK; echo $OK; echo 2´??.describe"};
//		final String[] commands = new String[]{"/bin/sh", "-c", "ffmpeg -i /tmp/test/test.mp4 -vcodec libx264 -acodec copy -to 0:05 /tmp/test/done.mp4"};

		try {
//			final Process p = Runtime.getRuntime().exec(commands);


			final ProcessBuilder pb = new ProcessBuilder();
			pb.command( commands );
			pb.inheritIO();
			final Process p = pb.start();
			p.waitFor();


			SysCmd.exec( true, true, String.join( " ", commands ) );
		}
		catch( final Throwable ex ) {
			Err.show( ex );
		}
	}

}
