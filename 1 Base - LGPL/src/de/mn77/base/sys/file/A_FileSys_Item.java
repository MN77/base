/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.file;

import java.io.File;

import de.mn77.base.data.A_Comparable;
import de.mn77.base.data.datetime.MDateTime;
import de.mn77.base.data.util.Lib_Compare;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;


/**
 * @author Michael Nitsche
 * @created 28.09.2007
 * @implNote TODO Könnte man wirklich besser globalisieren!
 */
public abstract class A_FileSys_Item extends A_Comparable<I_FileSys_Item> implements I_FileSys_Item {

	protected File file;


	public boolean exists() {
		return this.file.exists();
	}

	public File getFile() {
		return this.file;
	}

	public long getModified() {
		return this.file.lastModified();
	}

	public MDateTime getModifiedDateTime() {
		final long modified = this.file.lastModified();
		return modified == 0l ? null : new MDateTime( modified );
	}

	public I_Directory getParentDir() {

		try {
			return new MDir( this.file.getParent() );
		}
		catch( final Err_FileSys e ) {
			throw Err.impossible( e );
		}
	}

	public String getParentDirName() {
		return this.getParentDir().getName(); // TODO Go direct way!
	}

	public String getParentPathAbsolute() {
		return this.getParentDir().getPathAbsolute();
//		return this.file.getParent();
	}

	public String getPathAbsolute() {
		return this.file.getAbsolutePath();
	}

	@Override
	public boolean isEqual( final I_FileSys_Item to ) {
		final String path1 = this.getPathAbsolute();
		final String path2 = to.getPathAbsolute();
		return path1.equals( path2 );
//		return Lib_Compare.isEqual(path1, path2);
	}

	//Sortiert nach Pfad und Name, nicht nach größe ;-)
	@Override
	public boolean isGreater( final I_FileSys_Item than ) {
		final String path1 = this.getPathAbsolute();
		final String path2 = than.getPathAbsolute();
		return Lib_Compare.isGreaterString( path1, path2 );
	}

	public boolean isHidden() {
		return this.file.isHidden();
	}

	@Override
	public String toString() {
		return this.file.toString();
	}

}
