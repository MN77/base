/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.file;

import java.util.function.Function;

import de.mn77.base.data.struct.I_List;
import de.mn77.base.error.Err_FileSys;


public interface I_Directory extends I_FileSys_Item {

	// --- Content ---

	I_List<I_FileSys_Item> content();

	I_List<I_Directory> contentDirs();

	I_List<I_Directory> contentDirs( Function<String, Boolean> check );

	I_List<I_Directory> contentDirs( String regex );

	I_List<I_File> contentFiles();

	I_List<I_File> contentFiles( boolean recursive );

	I_List<I_File> contentFiles( boolean recursive, boolean hiddenDirs );

	I_List<I_File> contentFiles( Function<String, Boolean> check );

	I_List<I_File> contentFiles( String regex );

	I_List<I_File> contentFilesWithSuffix( boolean recursive, String... filter );

	I_List<I_File> contentFilesWithSuffix( String... suffixFilter );


	void deleteFiles();

	/**
	 * @apiNote Directory will be created if missing.
	 */
	I_Directory dirFlex( String name ) throws Err_FileSys;

	/**
	 * @apiNote Maybe the directory may exists, maybe the directory is missing.
	 */
	I_Directory dirMay( String name ) throws Err_FileSys;


	// --- Datei muss existieren ---

	/**
	 * @apiNote Directory must already exist. A error will be thrown if the directory is missing.
	 */
	I_Directory dirMust( String name ) throws Err_FileSys;

	/**
	 * @apiNote Directory will be created. A Error will be thrown if it already exist.
	 */
	I_Directory dirNew( String name ) throws Err_FileSys;


	// --- Datei darf nicht existieren --- // fileMissing

	I_File fileAbsent( String filename ) throws Err_FileSys;

	I_File fileAbsent( String name, String suffix ) throws Err_FileSys;

	/**
	 * If the file exists, add a incremented number to get the next absent file (exp: .1)
	 */
	I_File fileNextAbsent( String name, String suffix );


	// --- Datei kann existieren ---

	I_File fileMay( String filename );

	I_File fileMay( String name, String suffix );


	// Directorys

	I_File fileMust( String filename ) throws Err_FileSys;

	I_File fileMust( String filename, String suffix ) throws Err_FileSys;

	boolean knows( String name );

	boolean knowsDir( String name );

	// --- ---

	boolean knowsFile( String name );

	void setReadOnly() throws Err_FileSys;

}
