/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import de.mn77.base.error.Err_FileSys;


public interface I_File extends I_FileSys_Item {

	I_File copy( I_Directory to ) throws Err_FileSys;

	void copy( I_File target ) throws Err_FileSys;

	void create() throws Err_FileSys;

	void execDefault() throws Err_FileSys;

	long getSize();

	String getSuffix();

	void move( I_Directory to ) throws Err_FileSys;

	//	void read(OutputStream ziel) throws Err_FileSys;
	FileInputStream read() throws Err_FileSys;

	void setSuffix( String s );

	FileOutputStream write() throws Err_FileSys;
//	I_File write(InputStream quelle) throws Err_FileSys;

}
