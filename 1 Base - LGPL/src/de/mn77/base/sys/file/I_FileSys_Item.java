/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.file;

import java.io.File;

import de.mn77.base.data.I_Comparable;
import de.mn77.base.data.datetime.MDateTime;
import de.mn77.base.error.Err_FileSys;


/**
 * @author Michael Nitsche
 */
public interface I_FileSys_Item extends I_Comparable<I_FileSys_Item> {

	//	void move(I_Directory nach) throws F_DateiSys;
	void delete() throws Err_FileSys;

	boolean lazyDelete();

	boolean exists();

	File getFile();

	long getModified();

	MDateTime getModifiedDateTime();

	String getName();

	String getNameWithoutSuffix();

	I_Directory getParentDir();

	String getParentDirName();

	String getParentPathAbsolute();

	String getPathAbsolute();

	boolean isDirectory();

	boolean isHidden();

	void rename( String newName ) throws Err_FileSys;

}
