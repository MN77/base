/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.file;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import de.mn77.base.data.search.SearchString;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.Sys;
import de.mn77.base.sys.cmd.SysDefault;


/**
 * @author Michael Nitsche
 * @created 19.04.2019
 * @implNote
 *           05.12.2008 Kopieren überarbeitet, ist nun schneller, stabiler, besseres Fehlermanagement und funzt auch mit großen Dateien
 *
 *           "start" öffnet Dateien mit der Standard-Anwendung;
 *           Für den Start von speziellen Anwendungen usw, siehe DirektStarter
 */
public class Lib_FileSys {

	public static I_FileSys_Item convertFrom( final File file ) {

		try {
			return file.isDirectory()
				? new MDir( file )
				: new MFile( file );
		}
		catch( final Err_FileSys e ) {
			throw Err.impossible();
		}
	}

	public static void copyFile( final File from, final File to ) throws Err_FileSys {
		if( from.isDirectory() )
			Err.forbidden( "Can't copy a directory", from, to );
		Err.fsIfMissing( from );
		Err.fsIfExist( to );
		Lib_FileSys.iCopyFile( from, to );
	}

	/**
	 * Legt, wenn möglich, alle Verzeichnisse so an, dass dieser Pfad gültig ist.
	 */
	public static void createPathStructure( final String path ) throws Err_FileSys {
		final String dirSep = Sys.getSeperatorDir();
		final String[] elements = path.split( dirSep );
		MOut.dev( elements, dirSep );
		String current = Sys.isWindows()
			? ""
			: dirSep;
		boolean first = true;

		for( final String element : elements ) {
			if( element.length() == 0 )
				continue;
			if( first )
				first = false;
			else
				current += dirSep;
			current += element;
			final File f = new File( current );
			if( !f.exists() )
				if( !f.mkdir() )
					throw Err.newFileSys( "Couldn't create Directory!", current );
		}
	}

	/** Erzeugt eine temporäre Datei, die beim Beenden der Anwendung wieder entfernt wird **/
	public static MFile createTempFile( final String filename, String suffix ) throws IOException {
		if( suffix.charAt( 0 ) != '.' )
			suffix = "." + suffix;
		final File tempFile = File.createTempFile( filename + "_", suffix ); //Long.toString(System.nanoTime())+"_"+
		tempFile.deleteOnExit();
		return new MFile( tempFile );
	}

	/**
	 * @apiNote Open file with default application
	 */
	public static Process defaultOpen( final File f ) throws Err_FileSys {
		return SysDefault.open( f.getAbsolutePath() );
	}

	public static String getSuffix( final File file ) {
		final String name = file.getName();
		final int lastIndex = SearchString.lastIndexOf( '.', name );
		if( lastIndex < 0 ) // -1 == nothing found
			return "";
//		final String suffix = name.replaceFirst( "^.*\\.(.*)$", "$1" );
		final String suffix = name.substring( lastIndex + 1 );

		return suffix.length() > 10
			? ""
			: suffix;
	}

	public static String getTitle( final File file ) {
		final String name = file.getName();
		final String suffix = Lib_FileSys.getSuffix( file );
		final int endIndex = name.length() - suffix.length() - 1;
		return name.substring( 0, endIndex );
	}

	public static void move( final File from, final File to ) throws Err_FileSys {
//		final boolean result = from.renameTo( to ); // This maybe can not move file between filesystems!
//		if( !result )
//			throw Err.newFileSys( "Moving file failed", from, to );
		try {
			Files.move( from.toPath(), to.toPath());  // REPLACE_EXISTING, ATOMIC_MOVE
		}
		catch( IOException e ) {
			throw Err.newFileSys( "Moving file failed", from, to );
		}
	}

	public static boolean rename( final File from, final File to ) throws Err_FileSys {
		Err.fsIfMissing( from );
		Err.fsIfExist( to );
		return from.renameTo( to );
	}

	/**
	 * Returns all files, which do match the pattern. Wildcards are allowed: *.txt
	 */
	public static DirectoryStream<java.nio.file.Path> searchFiles( final File base, final String pattern ) throws IOException {
		final String path = base.getAbsolutePath();
		return Files.newDirectoryStream( Paths.get( path ), pattern );
	}

	/*
	 * Je größer der Puffer, um so schneller gehts, vor allem beim lesen und schreiben von der gleichen Platte
	 */
	private static void iCopyFile( final File from, final File to ) throws Err_FileSys {
		final byte[] buffer = new byte[8192];
		int read = 0;
		RandomAccessFile raf_source = null;
		RandomAccessFile raf_target = null;

		try {
			raf_source = new RandomAccessFile( from, "r" );
			raf_target = new RandomAccessFile( to, "rw" );
			raf_target.setLength( raf_source.length() ); //Beschleunigt etwas
			while( (read = raf_source.read( buffer )) > 0 )
				raf_target.write( buffer, 0, read );
		}
		catch( final IOException e ) {
			throw Err.wrap( e, "Copy-Error!" );
		}
		finally {

			try {
				raf_source.close();
			}
			catch( final IOException e ) {
//				throw Err.wrap(e, "Datei kann nicht geschlossen werden!");
				MOut.error( e, "Couldn't close source-file!" );
			}
			finally {

				try {
					raf_target.close();
				}
				catch( final IOException e ) {
//					throw Err.wrap(e, "Datei kann nicht geschlossen werden!");
					MOut.error( e, "Couln't close target-file!" );
				}
			}
		}
	}

}
