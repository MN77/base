/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.file;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;

import javax.swing.ImageIcon;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.stream.Lib_Stream;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @implNote
 *           On direct access with class, an '/' at start is needed.
 *           Not at using a ClassLoader
 */
public class Lib_Jar {

	/**
	 * @return Returns the absolute path of the file.
	 * @apiNote
	 *          At normal, this will return a String like: "/path/to/file.jmo"
	 *          If startet within a Jar, the result is: "file:/path/to/cool.jar!/path/to/file.jmo"
	 */
	public static String getAbsolutePath( String jarPath ) throws Err_FileSys { // try/throws nötig?
		jarPath = Lib_Jar.iCheckLeadingSlash( jarPath, true );

		try {
			final URL url = Lib_Jar.class.getResource( jarPath );
			if( url == null )
				Err.fsAccess( "File is missing", jarPath );

			return URLDecoder.decode( url.getPath(), "UTF-8" );
//			File file = new File(decoded);
//			return new Group2<>(file.getParent(), file.getName());
		}
		catch( final Err_FileSys e ) {
			throw Lib_Jar.iError( e, jarPath );
		}
		catch( final UnsupportedEncodingException e ) {
			throw Err.exit( e );
		}
	}

	public static File getFileCopy( String jarPath, final String jarFilename, final String jarSuffix ) throws IOException, Err_FileSys {
		if( jarPath.endsWith( "/" ) )
			jarPath = jarPath.substring( 0, jarPath.length() - 1 );

		final InputStream is = Lib_Jar.getStream( jarPath + "/" + jarFilename + "." + jarSuffix );
		final MFile tempFile = Lib_FileSys.createTempFile( jarFilename, jarSuffix );
		Lib_Stream.connect( is, tempFile.write(), false, true );
//		throw Err.wrap(e, "Kopieren der Datei aus dem Jar ist gescheitert", datei_im_jar, ziel_lokal);
		return tempFile.getFile();
	}

	public static ImageIcon getImageIcon( String jarPath ) {
		jarPath = Lib_Jar.iCheckLeadingSlash( jarPath, false );
		final URL res = Lib_Jar.class.getClassLoader().getResource( jarPath );
		if( res == null )
			throw new Err_Runtime( "Image read error", jarPath );
		return new ImageIcon( res );
	}

	public static InputStream getStream( String jarPath ) throws Err_FileSys {
		jarPath = Lib_Jar.iCheckLeadingSlash( jarPath, true );

		try {
			final URL url = Lib_Jar.class.getResource( jarPath );
			if( url == null )
				Err.fsAccess( "File is missing", jarPath );
			return Lib_Jar.class.getResourceAsStream( jarPath );
		}
		catch( final Err_FileSys e ) {
			throw Lib_Jar.iError( e, jarPath );
		}
	}


	// INTERN

	/**
	 * @apiNote Jar-Path must always be with leading slash. It's a clear way. Internally it will be formatted as required.
	 */
	private static String iCheckLeadingSlash( final String jarPath, final boolean leadingSlash ) {
		Err.ifEmpty( jarPath );
		final boolean hasLeadingSlash = jarPath.charAt( 0 ) == '/';
		if( !hasLeadingSlash )
			Err.invalid( jarPath, "Jar-Path must start with a '/'!" );

		return leadingSlash
			? jarPath
			: jarPath.substring( 1 );
	}

	private static Err_FileSys iError( final Err_FileSys e, final String file ) {
		MOut.print( "File-Error: " + file );
		if( file.indexOf( '/' ) < 0 )
			MOut.print( "Maybe used '.' instead of '/' for pathseparator?!" );
		e.addDetail( file );
		return e;
	}

}
