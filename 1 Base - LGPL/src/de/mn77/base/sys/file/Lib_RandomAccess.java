/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.file;

import java.io.File;
import java.io.RandomAccessFile;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;


/**
 * @author Michael Nitsche
 * @created 31.10.2022
 */
public class Lib_RandomAccess {

	public static void append( final File file, final byte[] ba ) throws Err_FileSys {
		Lib_RandomAccess.write( file, -1l, ba );
	}

	public static int checkReadSize( final File file, final long len ) throws Err_FileSys {
		if( len > Integer.MAX_VALUE - 8 )
			throw new Err_FileSys( "File too big to read: \"" + file.getAbsolutePath() + '"' );
		return (int)len;
	}

	public static void clear( final File file ) throws Err_FileSys {
		Lib_RandomAccess.write( file, null, new byte[0] );
	}

	public static byte[] read( final File file ) throws Err_FileSys {
		return Lib_RandomAccess.read( file, 0, null );
	}

	public static byte[] read( final File file, final long index ) throws Err_FileSys {
		return Lib_RandomAccess.read( file, index, null );
	}

	public static byte[] read( final File file, final long index, Long length ) throws Err_FileSys {
		Err.ifNull( file );
		RandomAccessFile raf = null;

		try {
			raf = new RandomAccessFile( file, "r" );

			if( index > 0 )
				raf.seek( index );
			if( length == null )
				length = raf.length();

			final int bytesToRead = Lib_RandomAccess.checkReadSize( file, length );

			final byte[] ba = new byte[bytesToRead];
			final int got = raf.read( ba );
			if( got != bytesToRead )
				throw new Err_FileSys( "File-Read-Error, got " + got + " bytes instead of " + bytesToRead + ": \"" + file.getAbsolutePath() + '"' );

			return ba;
		}
		catch( final Exception err ) {
			throw Err.fsAccess( "File read error", err );
		}
		finally {

			try {
				if( raf != null )
					raf.close();
			}
			catch( final Exception e ) {
				Err.show( e );
			}
		}
	}

	public static void set( final File file, final byte[] ba ) throws Err_FileSys {
		Lib_RandomAccess.write( file, null, ba );
	}

	/**
	 * @param index:
	 *            null = replace content, -1 = append, >=0 = write at index
	 */
	public static void write( final File file, final Long index, final byte[] ba ) throws Err_FileSys {
		Err.ifNull( file, ba );
		RandomAccessFile raf = null;

		try {
			raf = new RandomAccessFile( file, "rw" );
			if( index == null )
				raf.setLength( 0 );
			else if( index < 0 )
				raf.seek( raf.length() );
			else
				raf.seek( index );

//			raf.writeChars( text + "\n" ); // Results in trash
//			raf.writeUTF(text); // Special format ... UTF16?
			if( ba.length > 0 )
				raf.write( ba );
		}
		catch( final Exception err ) {
			throw Err.fsAccess( err, file );
		}
		finally {

			try {
				if( raf != null )
					raf.close();
			}
			catch( final Exception e ) {
				Err.show( e );
			}
		}
	}

}
