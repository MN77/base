/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.file;

import java.io.File;
import java.nio.charset.Charset;

import de.mn77.base.data.charset.ConvertCharset;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;


/**
 * @author Michael Nitsche
 * @created 2007-07-31
 * @reworked 2022-10-31
 */
public class Lib_TextFile {

	public static void append( final File file, final String s ) throws Err_FileSys {
		Lib_TextFile.append( file, s, null );
	}

	public static void append( final File file, final String s, final Charset cs ) throws Err_FileSys {
		Err.ifNull( file, s );

		final byte[] ba = cs == null
			? s.getBytes()
			: s.getBytes( cs );

		Lib_RandomAccess.append( file, ba );
	}

	public static String read( final File file ) throws Err_FileSys {
		final byte[] ba = Lib_RandomAccess.read( file );
		return new String( ba );	//, CHARSET.UTF8
	}

	public static String read( final File file, final Charset cs ) throws Err_FileSys {
		final byte[] ba = Lib_RandomAccess.read( file );
//		return new String(ba, cs);
		return ConvertCharset.toString( ba, cs );
	}

	public static void set( final File file, final String s ) throws Err_FileSys {
		Lib_TextFile.write( file, null, s, null );
	}

	public static void set( final File file, final String s, final Charset charset ) throws Err_FileSys {
		Lib_TextFile.write( file, null, s, charset );
	}

	public static void write( final File file, final Long index, final String s ) throws Err_FileSys {
		Lib_TextFile.write( file, index, s, null );
	}

	public static void write( final File file, final Long index, final String s, final Charset cs ) throws Err_FileSys {
		Err.ifNull( file, s );

		final byte[] ba = cs == null
			? s.getBytes() // Platforms default charset // s.getBytes("UTF-8"));
			: s.getBytes( cs );

		Lib_RandomAccess.write( file, index, ba );
	}

}
