/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.file;

import java.io.File;
import java.io.RandomAccessFile;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;


/**
 * @author Michael Nitsche
 * @created 31.10.2022
 * @apiNote A library for handling Textfiles, where only the low 8 bit will be used. High 8 bits will be discarded.
 */
public class Lib_TextFileLow {

	public static void append( final File file, final String s ) throws Err_FileSys {
		Err.ifNull( file, s );
		RandomAccessFile raf = null;

		try {
			raf = new RandomAccessFile( file, "rw" );
			raf.seek( raf.length() );
			raf.writeBytes( s );
		}
		catch( final Exception err ) {
			throw Err.fsAccess( err, file, s );
		}
		finally {

			try {
				if( raf != null )
					raf.close();
			}
			catch( final Exception e ) {
				Err.show( e );
			}
		}
	}

	/**
	 * @apiNote Reads the entire file (Low-8-Bytes to chars)
	 */
	public static String read( final File file ) throws Err_FileSys {
		Err.ifNull( file );
		RandomAccessFile raf = null;

		try {
			raf = new RandomAccessFile( file, "r" );

			final StringBuilder sb = new StringBuilder( Lib_RandomAccess.checkReadSize( file, raf.length() ) );
			while( raf.getFilePointer() < raf.length() )
				sb.append( (char)raf.readByte() );
			return sb.toString();
		}
		catch( final Exception err ) {
			throw Err.fsAccess( err );
		}
		finally {

			try {
				if( raf != null )
					raf.close();
			}
			catch( final Exception e ) {
				Err.show( e );
			}
		}
	}

	public static void set( final File file, final String s ) throws Err_FileSys {
		Lib_TextFileLow.write( file, null, s );
	}

	/**
	 * @apiNote Writes a string to a file. High bytes will be descarded!
	 */
	public static void write( final File file, final Long index, final String s ) throws Err_FileSys {
		Err.ifNull( file, s );
		RandomAccessFile raf = null;

		try {
			raf = new RandomAccessFile( file, "rw" );

			if( index == null )
				raf.setLength( 0 );
			else
				raf.seek( index );

			raf.writeBytes( s );
		}
		catch( final Exception err ) {
			throw Err.fsAccess( err, file, s );
		}
		finally {

			try {
				if( raf != null )
					raf.close();
			}
			catch( final Exception e ) {
				Err.show( e );
			}
		}
	}

}
