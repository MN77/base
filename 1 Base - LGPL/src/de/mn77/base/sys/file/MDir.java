/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.file;

import java.io.File;
import java.io.FileFilter;
import java.util.function.Function;

import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.Sys;


/**
 * @author Michael Nitsche
 * @implNote
 *           TODO!!! Trenner systemspezifisch!!!
 */
public class MDir extends A_FileSys_Item implements I_Directory {

	public static boolean exists( final String name ) throws Err_FileSys {
		final File f = new File( name );
		return f.exists();
	}


	public MDir( final Object dir ) throws Err_FileSys {
		this( dir, false );
	}

	public MDir( final Object dir, final boolean createIfMissing ) throws Err_FileSys {
		Err.ifNull( dir );

		if( createIfMissing )
			if( dir instanceof String )
				Lib_FileSys.createPathStructure( (String)dir );
			else
				Err.todo( dir );

		if( dir instanceof String )
			this.file = new File( (String)dir );
		else if( dir instanceof File )
			this.file = (File)dir;
		else
			Err.invalid( "Unknown Type", dir.getClass().getName(), dir );
		if( !this.file.exists() )
			Err.invalid( "Directory doesn't exist", this.file.getAbsolutePath() );
		if( !this.file.isDirectory() )
			Err.invalid( "Folder expected, file found", this.file.getAbsolutePath() );
	}


	public I_List<I_FileSys_Item> content() {
		final File[] files = this.file.listFiles();
		if( files == null )
			return new SimpleList<>();

		final I_List<I_FileSys_Item> result = new SimpleList<>( files.length );

		for( final File file : files )
			if( file.isFile() )
				result.add( new MFile( file ) );
			else
				try {
					result.add( new MDir( file ) );
				}
				catch( final Err_FileSys e ) {
					throw Err.impossible( e );
				}
		return result;
	}

	public I_List<I_Directory> contentDirs() {
		final File[] files = this.file.listFiles();
		if( files == null )
			return new SimpleList<>();

		final I_List<I_Directory> result = new SimpleList<>( files.length );

		for( final File datei : files )
			if( datei.isDirectory() )
				try {
					result.add( new MDir( datei ) );
				}
				catch( final Err_FileSys e ) {
					throw Err.impossible( e );
				}
		return result;
	}

	public I_List<I_Directory> contentDirs( final Function<String, Boolean> check ) {
		final File[] files = this.file.listFiles( (FileFilter)pathname -> pathname.isDirectory() && check.apply( pathname.getName() ) );
		return this.iDirArrayToList( files );
	}

	public I_List<I_Directory> contentDirs( final String regex ) {
		final File[] files = this.file.listFiles( (FileFilter)pathname -> pathname.isDirectory() && pathname.getName().matches( regex ) );
		return this.iDirArrayToList( files );
	}

	public I_List<I_File> contentFiles() {
		final File[] files = this.file.listFiles();
		if( files == null ) // Can' be null, if directory does not exist or is not accessible
			return new SimpleList<>();

		final I_List<I_File> result = new SimpleList<>( files.length );

		for( final File file : files )
			if( file.isFile() )
				result.add( new MFile( file ) );

		return result;
	}

	public I_List<I_File> contentFiles( final boolean recursive ) {
		return this.contentFiles( recursive, true );
	}

	public I_List<I_File> contentFiles( final boolean recursive, final boolean hiddenDirsToo ) {
		return recursive
			? this.iContentFiles( new SimpleList<>(), this, hiddenDirsToo )
			: this.contentFiles();
	}

	public I_List<I_File> contentFiles( final Function<String, Boolean> check ) {
		final File[] files = this.file.listFiles( (FileFilter)pathname -> check.apply( pathname.getName() ) );
		return this.iFileArrayToList( files );
	}

	public I_List<I_File> contentFiles( final String regex ) {
		final File[] files = this.file.listFiles( (FileFilter)pathname -> pathname.getName().matches( regex ) );
		return this.iFileArrayToList( files );
	}

	public I_List<I_File> contentFilesWithSuffix( final boolean recursive, final String... filter ) {
		Err.ifTooSmall( 1, filter.length );
		for( final String s : filter )
			Err.ifNull( s );
		return recursive
			? this.iContentFileWithSuffix( new SimpleList<>(), this, filter )
			: this.iContentFileWithSuffix( filter );
	}

	public I_List<I_File> contentFilesWithSuffix( final String... filter ) {
		return this.contentFilesWithSuffix( false, filter );
	}

	public void delete() throws Err_FileSys {

		if( this.file.exists() ) {
			MOut.warning( "Delete Directory: " + this.getPathAbsolute() );
			if( !this.file.delete() )
				Err.fsFailed( "Deletion of Directory failed", this.file );
		}
	}

	public boolean lazyDelete() {
//		MOut.warning( "Delete Directory: " + this.getPathAbsolute() );
		return this.file.exists()
			? this.file.delete()
			: true;
	}

	public void deleteFiles() {
		final File[] files = this.file.listFiles();

		if( files != null )
			for( final File file : files )
				if( file.isFile() )
					file.delete();
	}

	public I_Directory dirFlex( final String name ) throws Err_FileSys {
		final String newPath = this.file.getAbsolutePath() + Sys.getSeperatorDir() + name;
		final File f = new File( newPath );
		if( !f.exists() )
			if( !f.mkdir() )
				throw Err.fsFailed( "Can't create directory", f );

		try {
			return new MDir( f );
		}
		catch( final Err_FileSys e ) {
			e.addDetail( "Can't create directory" );
			throw e;
		}
	}

	public I_Directory dirMay( final String name ) throws Err_FileSys {
		final String newPath = this.file.getAbsolutePath() + Sys.getSeperatorDir() + name;
		final File f = new File( newPath );
		return new MDir( f );
	}

	public I_Directory dirMust( final String name ) throws Err_FileSys {
		final String newPath = this.file.getAbsolutePath() + Sys.getSeperatorDir() + name;
		final File f = new File( newPath );
		Err.fsIfMissing( f );
		return new MDir( f );
	}

	public I_Directory dirNew( final String name ) throws Err_FileSys {
		final String newPath = this.file.getAbsolutePath() + Sys.getSeperatorDir() + name;
		final File f = new File( newPath );
		Err.fsIfExist( f );
		if( !f.mkdir() )
			throw Err.fsFailed( "Can't create directory", f );
		return new MDir( f );
	}

	public I_File fileAbsent( final String filename ) throws Err_FileSys {
		final String newPath = this.file.getAbsolutePath() + Sys.getSeperatorDir() + filename;
		final File f = new File( newPath );
		Err.fsIfExist( f );
		return new MFile( f );
	}

	public I_File fileAbsent( final String name, final String suffix ) throws Err_FileSys {
		return this.fileAbsent( name + "." + suffix );
	}

	public I_File fileNextAbsent( String filename, String suffix ) {
		final String newPath = this.file.getAbsolutePath() + Sys.getSeperatorDir() + filename;

		File f = new File( newPath + '.' + suffix );
		int inc = 0;

		while( f.exists() ) {
			inc++;
			f = new File( newPath + '.' + inc + '.' + suffix );
		}

		return new MFile( f );
	}

	public I_File fileMay( final String filename ) {
		final String newPath = this.file.getAbsolutePath() + Sys.getSeperatorDir() + filename;
		final File f = new File( newPath );
		return new MFile( f );
	}

	public I_File fileMay( final String name, final String suffix ) {
		return this.fileMay( name + "." + suffix );
	}

	public I_File fileMust( final String filename ) throws Err_FileSys {
		final String newPath = this.file.getAbsolutePath() + Sys.getSeperatorDir() + filename;
		final File f = new File( newPath );
		Err.fsIfMissing( f );
		return new MFile( f );
	}

	public I_File fileMust( final String filename, final String suffix ) throws Err_FileSys {
		return this.fileMust( filename + "." + suffix );
	}

	public String getName() {
		return this.getNameWithoutSuffix();
	}

	public String getNameWithoutSuffix() {
		return this.file.getName();
	}

	public boolean isDirectory() {
		return true;
	}

	public boolean knows( final String name ) {
		throw Err.todo();
	}

	public boolean knowsDir( final String name ) {
		return this.contentDirs( name ).size() > 0;
	}

	public boolean knowsFile( final String name ) {
		return this.contentFiles( name ).size() > 0; //TODO Testen!!!
	}

//	public void verschieben(I_Directory nach) throws F_DateiSys {
//		MOut.dev(this.gFile().getAbsolutePath(), nach.getPathAbsolute());
//		Lib_FileSys.verschieben(this.gFile(), nach.gFile());
//	}

	public void rename( final String newName ) throws Err_FileSys {
		final String pathNew = this.getParentDir().getPathAbsolute() + Sys.getSeperatorDir() + newName;
		final File fileNew = new File( pathNew );
		final boolean ok = Lib_FileSys.rename( this.getFile(), fileNew );
		if( !ok )
			throw Err.fsFailed( "Rename failed", this.file, newName, pathNew );
		this.file = fileNew;
	}

	public void setReadOnly() throws Err_FileSys {
		final boolean b = this.file.setReadOnly();
		if( !b )
			Err.fsAccess( this.file );
	}

	private I_List<I_File> iContentFiles( final I_List<I_File> addToThis, final I_Directory dir, final boolean hiddenDirsToo ) {
		if( !hiddenDirsToo && (dir.getFile().isHidden() || dir.getNameWithoutSuffix().startsWith( "." )) )
			return addToThis;
		addToThis.addAll( dir.contentFiles() );
		final I_List<I_Directory> content = dir.contentDirs();
		for( final I_Directory o : content )
			this.iContentFiles( addToThis, o, hiddenDirsToo );
		return addToThis;
	}

	private I_List<I_File> iContentFileWithSuffix( final I_List<I_File> addToThis, final I_Directory dir, final String[] filter ) {
		addToThis.addAll( dir.contentFilesWithSuffix( filter ) );
		final I_List<I_Directory> content = dir.contentDirs();
		for( final I_Directory o : content )
			this.iContentFileWithSuffix( addToThis, o, filter );
		return addToThis;
	}

	private I_List<I_File> iContentFileWithSuffix( final String[] filter ) {
		Err.ifTooSmall( 1, filter.length );
		String regex = "^.*(";

		for( int i = 0; i < filter.length; i++ ) {
			if( i > 0 )
				regex += "|";
			regex += "\\." + filter[i].toLowerCase();
		}

		regex += ")$";
		final String regex2 = regex;
		final File[] files = this.file.listFiles( (FileFilter)pathname -> pathname.getName().toLowerCase().matches( regex2 ) && pathname.isFile() );
		if( files == null ) // Can' be null, if directory does not exist or is not accessible
			return new SimpleList<>();

		final I_List<I_File> result = new SimpleList<>( files.length );
		for( final File file : files )
			result.add( new MFile( file ) );
		return result;
	}

	private I_List<I_Directory> iDirArrayToList( final File[] files ) {
		if( files == null )
			return new SimpleList<>();

		final I_List<I_Directory> result = new SimpleList<>( files.length );

		for( final File file : files )
			try {
				result.add( new MDir( file ) );
			}
			catch( final Err_FileSys e ) {
				throw Err.impossible( e );
			}
		return result;
	}

	private I_List<I_File> iFileArrayToList( final File[] files ) {
		if( files == null ) // Can' be null, if directory does not exist or is not accessible
			return new SimpleList<>();

		final I_List<I_File> result = new SimpleList<>( files.length );

		for( final File file : files )
			if( file.isFile() )
				result.add( new MFile( file ) );
		return result;
	}

}
