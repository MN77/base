/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import de.mn77.base.data.filter.CutString;
import de.mn77.base.data.search.SearchString;
import de.mn77.base.data.struct.atomic.IntList;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.Sys;


/**
 * @author Michael Nitsche
 * @implNote
 *           MFile is not immutable! It will be updated at rename, move, ...
 *           TODO Nochmal Testen und evtl. so umbauen, daß nicht permanent ein File gehalten wird. Evtl. sperrt dieses z.B. rename oder del
 *
 *           TODO Umbenennen in MNFile
 */
public class MFile extends A_FileSys_Item implements I_File {

	public MFile( final File f ) {
		Err.ifNull( f );
		this.file = f;
	}

	public MFile( final String path ) {
		this( new File( path ) );
	}


	public I_File copy( final I_Directory to ) throws Err_FileSys {
		final I_File target = to.fileAbsent( this.getName() );
		Lib_FileSys.copyFile( this.getFile(), target.getFile() );
		return target;
	}

	public void copy( final I_File to ) throws Err_FileSys {
//		to.anlegen();
		Lib_FileSys.copyFile( this.getFile(), to.getFile() );
	}

	public void create() throws Err_FileSys {
		boolean ok = false;

		try {
			ok = this.file.createNewFile();
		}
		catch( final IOException e ) {
			Err.wrap( e, "Error while creating the file" );
			return;
		}

		if( !ok )
			Err.fsAccess( "File maybe already exists", this.getPathAbsolute() );
	}

	public void delete() throws Err_FileSys {
		if( this.file.exists() )
			if( !this.file.delete() )
				Err.fsFailed( "Can't delete File", this.file );
	}

	public boolean lazyDelete() {
		return this.file.exists()
			? this.file.delete()
			: true;
	}

	public void execDefault() throws Err_FileSys {
		Lib_FileSys.defaultOpen( this.getFile() );
	}

	public String getName() {
		return this.file.getName();
	}

	public String getNameWithoutSuffix() {
		return this.file.getName().replaceFirst( "^(.*)\\..*$", "$1" );
	}

	public long getSize() {
		return this.file.length();
	}

	public String getSuffix() {
		return Lib_FileSys.getSuffix( this.file );
	}

	public boolean isDirectory() {
		return false;
	}

	public void move( final I_Directory to ) throws Err_FileSys {
		final File target = to.fileAbsent( this.getName() ).getFile();
		Lib_FileSys.move( this.getFile(), target );
		this.file = target;
	}

	public void move( final I_File to ) throws Err_FileSys {
		Err.fsIfExist( to );
		Lib_FileSys.move( this.getFile(), to.getFile() );
		this.file = to.getFile();
	}

	public FileInputStream read() throws Err_FileSys {

		try {
			return new FileInputStream( this.file );
		}
		catch( final FileNotFoundException e ) {
			throw Err.wrap( e, "File not found" );
		}
	}

	public void rename( final String newName ) throws Err_FileSys {
		Err.ifNull( newName );
		final String tv = Sys.getSeperatorDir();
		final IntList res = SearchString.indexes( newName, tv );
		if( res.size() > 0 )
			throw Err.invalid( "No Path allowed!", newName ); // Darf keinen Slash(Pfad) enthalten!

		final File newFile = this.getParentDir().fileMay( newName ).getFile();
		Lib_FileSys.rename( this.file, newFile );

//		try {
//			Files.move(this.file.toPath(), newFile.toPath());
//		}
//		catch(final IOException e) {
//			throw Err.wrap(e, "Rename", this.file.toString(), newFile.toString());
//		}
		this.file = newFile;
	}

	public void setSuffix( final String s ) {
		final String suffix = this.getSuffix();
		String path = this.getPathAbsolute();
		path = CutString.relCut( path, 0 - suffix.length() - 1, true );
		path = path.replaceFirst( "\\.+$", "" );
		this.file = new File( path + "." + s );
	}

	public FileOutputStream write() throws Err_FileSys {

		try {
			return new FileOutputStream( this.file );
		}
		catch( final FileNotFoundException e ) {
			throw Err.wrap( e, "File not found" );
		}
	}

}
