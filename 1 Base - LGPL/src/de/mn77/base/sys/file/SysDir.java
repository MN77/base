/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.file;

import java.io.File;

import de.mn77.base.data.filter.FilterString;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.Sys;


/**
 * @author Michael Nitsche
 */
public class SysDir {

	public static I_Directory current() {

		try {
			return new MDir( Sys.getCurrentDir() );
		}
		catch( final Err_FileSys e ) {
			throw Err.impossible();
		}
	}

	public static I_Directory home() {

		try {
			return new MDir( Sys.getPathHome() ); // "/home/mike"
		}
		catch( final Err_FileSys e ) {
			throw Err.impossible();
		}
	}

	public static I_Directory root() {

		try {
			return new MDir( File.listRoots()[0] ); // "/"
		}
		catch( final Err_FileSys e ) {
			throw Err.impossible();
		}
	}

	public static I_Directory temp() {

		try {
			return new MDir( Sys.getPathTemp() ); // "/tmp"
		}
		catch( final Err_FileSys e ) {
			throw Err.impossible();
		}
	}

	public static I_Directory userCache() throws Err_FileSys {
		if( Sys.isLinux() )
			return SysDir.home().dirFlex( ".cache" );
		if( Sys.isWindows() )
			return SysDir.userConfig();
		if( Sys.isMac() )
			return SysDir.home().dirFlex( ".cache" );

		throw Err.todo( "Cache-Directory not configured for this System.", Sys.getOsName(), Sys.getOsVersion() );
	}

	public static I_Directory userCache_MN77( String app ) throws Err_FileSys {
		final String base = SysDir.iNormalize( "MN77" );
		app = SysDir.iNormalize( app );
		return SysDir.userCache().dirFlex( base ).dirFlex( app );
	}

	//TODO gOrdnerAnwendungskonfig ... mit . oder eben nicht ... ... siehe dirAppNitsche
	public static I_Directory userConfig() throws Err_FileSys {
		if( Sys.isLinux() )
			return SysDir.home().dirFlex( ".config" );

		if( Sys.isWindows() ) {
			if( Double.parseDouble( Sys.getOsVersion() ) >= 10d )
				return SysDir.home().dirFlex( "AppData" ).dirFlex( "Local" );
			return SysDir.home().dirFlex( "Anwendungsdaten" ); //TODO Auslesen! Oder z.B. %appdata% verwenden!
		}

		if( Sys.isMac() )
			return SysDir.home().dirFlex( ".config" );

		throw Err.todo( "App-Data-Directory not configured for this System.", Sys.getOsName(), Sys.getOsVersion() );
	}

	public static I_Directory userConfig( String app ) throws Err_FileSys {
		app = SysDir.iNormalize( app );
		return SysDir.userConfig().dirFlex( app );
	}

	public static I_Directory userConfig_MN77( String app ) throws Err_FileSys {
		final String base = SysDir.iNormalize( "MN77" );
		app = SysDir.iNormalize( app );
		return SysDir.userConfig().dirFlex( base ).dirFlex( app );
	}

	@Deprecated
	public static I_Directory userConfig_Nitsche( String app ) throws Err_FileSys {
		final String base = SysDir.iNormalize( "Nitsche Michael" );
		app = SysDir.iNormalize( app );
		return SysDir.userConfig().dirFlex( base ).dirFlex( app );
	}

	private static String iNormalize( String s ) {
		Err.ifEmpty( s );
		s = FilterString.trim( s );

		if( Sys.isLinux() || Sys.isMac() ) {
			s = s.toLowerCase();
			s = s.replace( ' ', '_' );
		}

		return s;
	}

}
