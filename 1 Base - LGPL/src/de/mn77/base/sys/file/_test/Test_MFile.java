/*******************************************************************************
 * Copyright (C) 2018-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.file._test;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.Sys;
import de.mn77.base.sys.file.MFile;


/**
 * @author Michael Nitsche
 *         09.06.2018 Erstellt
 */
public class Test_MFile {

	public static void main( final String[] args ) {

		try {
			final MFile f = new MFile( "/tmp/test1.txt" );
			f.create();
			Sys.sleepSeconds( 5 );
			f.rename( "test2.txt" );
			MOut.print( f );
			Sys.sleepSeconds( 5 );
			f.delete();
		}
		catch( final Throwable e ) {
			Err.exit( e );
		}
	}

}
