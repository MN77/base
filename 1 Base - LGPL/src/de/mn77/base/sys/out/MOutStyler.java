/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.out;

import de.mn77.base.data.convert.ConvertObject;
import de.mn77.base.data.datetime.MDateTime;
import de.mn77.base.data.datetime.MTime;
import de.mn77.base.data.util.Lib_Describe;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 08.11.2022
 */
public class MOutStyler {

	private String       linebreak    = System.lineSeparator(); //Sys.getSeparatorLine();
	private final String TRACE_INDENT = "  ";


	public void addLogHeader( final CHANNEL_IN channel, final StringBuilder sb ) {
		sb.append( new MDateTime().toStringFileSysSlim() );
//		sb.append(new MTime().toStringShort());
		sb.append( '\t' );
		sb.append( channel.name() );
		sb.append( '\t' );	//'|'
	}

	public void addTrace( final StringBuilder sb, final StackTraceElement[] ste, final TRACE trace ) { // TODO lines oder return String?
		int got = 0;
		final int maxLines = trace.amount;

		if(ste == null)
			return;

		for( int traceIdx = 0; traceIdx < ste.length - 1; traceIdx++ ) {
			final StackTraceElement st = ste[traceIdx];
			final String stt = st.toString();

			if( trace != TRACE.FULL ) {
				if( stt.matches( "^(sun|java)\\..*$" ) || stt.matches( "^.*:1\\)$" ) )
					continue;
				if( stt.contains( MOut.class.getName() ) || stt.contains( MOutWriter.class.getName() ) || stt.contains( Err.class.getName() ) || stt.indexOf( "Err" ) >= 0 || stt.matches( "^de.mn77.base.error.*$" ) )
					continue;
			}

			sb.append( this.TRACE_INDENT );
			sb.append( "@ " );
			sb.append( stt );
			sb.append( this.linebreak );

			got++;
			if( got >= maxLines )
				break;
		}

		if( trace != TRACE.ONE ) {
			sb.append( this.TRACE_INDENT );
			sb.append( "> " );
			sb.append( ste[ste.length - 1] );
			sb.append( this.linebreak );
		}
	}

	public String argsToDescribe( final String prefix, final boolean newLine, final Object[] oa ) {
		final StringBuilder sb = new StringBuilder();

		for( final Object o : oa ) {
			sb.append( prefix );
//			sb.append(ConvertObject.toStringDescribe(o));	// TODO
			sb.append( Lib_Describe.toDescribe( o ) );	// TODO

			if( newLine )
				sb.append( this.linebreak );
		}

		return sb.toString();
	}

	public String argsToIdent( final String prefix, final boolean newLine, final Object[] oa ) {
		final StringBuilder sb = new StringBuilder();

		for( final Object o : oa ) {
			sb.append( prefix );
			sb.append( ConvertObject.toStringIdent( o ) );

			if( newLine )
				sb.append( this.linebreak );
		}

		return sb.toString();
	}

	public String argsToString( final String prefix, final boolean newLine, final Object[] oa ) {
		final StringBuilder sb = new StringBuilder();

		for( final Object o : oa ) {
			sb.append( prefix );
			sb.append( ConvertObject.toStringOutput( o ) );

			if( newLine )
				sb.append( this.linebreak );
		}

		return sb.toString();
	}

	public void setLineBreak( final String s ) {
		this.linebreak = s;
	}

	public void style( final StringBuilder sb, final OUTPUT_STYLE style, final CHANNEL_IN channel, final OUTPUT_DETAIL detail, final boolean newLine, final Object[] oa ) {

		switch( style ) {
			case RAW:
				this.argsTo( sb, "", detail, newLine, oa );
				return;
			case CHANNEL:
				this.argsTo( sb, channel.name() + "| ", detail, newLine, oa );

				if( !newLine )
					sb.append( this.linebreak );
				return;
			case TIMESTAMP:
				this.argsTo( sb, channel.name() + ' ' + new MTime().toStringSlim() + "| ", detail, newLine, oa );

				if( !newLine )
					sb.append( this.linebreak );
				return;

			default:
				throw Err.todo( style );
		}
	}

	private void argsTo( final StringBuilder sb, final String prefix, final OUTPUT_DETAIL detail, final boolean newLine, final Object[] oa ) {

		for( final Object o : oa ) {
			sb.append( prefix );

			final String s = detail == OUTPUT_DETAIL.RAW
				? ConvertObject.toStringOutput( o )
				: detail == OUTPUT_DETAIL.IDENT
					? ConvertObject.toStringIdent( o )
					: Lib_Describe.toDescribe( o );

			sb.append( s );

			if( newLine )
				sb.append( this.linebreak );
		}
	}

}
