/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.out;

import java.io.File;

import de.mn77.base.data.datetime.MDateTime;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.error.I_ErrorDetails;
import de.mn77.base.error.I_ErrorInfo;
import de.mn77.base.sys.file.Lib_TextFile;


/**
 * @author Michael Nitsche
 * @created 08.11.2022
 */
public class MOutWriter {

	private boolean          outputDev    = false;
	private boolean          javaErrors   = true;
	private String           linebreak    = System.lineSeparator(); //Sys.getSeparatorLine();
	private File             logFile      = null;
	private CHANNEL_OUT[]    routeDev     = {};
	private CHANNEL_OUT[]    routeErr     = { CHANNEL_OUT.STDERR, CHANNEL_OUT.LOG };
	private CHANNEL_OUT[]    routeLog     = { CHANNEL_OUT.LOG };
	private CHANNEL_OUT[]    routeOut     = { CHANNEL_OUT.STDOUT };
	private CHANNEL_OUT[]    routeWrn     = { CHANNEL_OUT.LOG };
	private OUTPUT_STYLE     style        = OUTPUT_STYLE.RAW;
	private TRACE            traceDefault = null;
	private final MOutStyler styler       = new MOutStyler();
	private boolean          useRoute     = false;


	public synchronized void directErr( final String s ) {
		System.err.print( s );
	}

	public synchronized void directOut( final String s ) {
		System.out.print( s );
	}

	public boolean getJavaErrors() {
		return this.javaErrors;
	}

	public void routeDev( final CHANNEL_OUT[] targets ) {
		this.routeDev = targets;
		this.useRoute = true;
	}

	public void routeErr( final CHANNEL_OUT[] targets ) {
		this.routeErr = targets;
		this.useRoute = true;
	}

	public void routeLog( final CHANNEL_OUT[] targets ) {
		this.routeLog = targets;
		this.useRoute = true;
	}

	public void routeOut( final CHANNEL_OUT[] targets ) {
		this.routeOut = targets;
		this.useRoute = true;
	}

	public void routeWrn( final CHANNEL_OUT[] targets ) {
		this.routeWrn = targets;
		this.useRoute = true;
	}

	public void setDefaultTrace( final TRACE trace ) {
		this.traceDefault = trace;
	}

	public void setJavaErrors( final boolean b ) {
		this.javaErrors = b;
	}

	public void setLineBreak( final String lb ) {
		this.linebreak = lb;
		this.styler.setLineBreak( lb );
	}

	public void setLogFile( final File file ) {
		this.logFile = file;
	}

	public void setModeDeveloper() {
		this.outputDev = true;
	}

	public void setModeRaw() {
		this.outputDev = false;
	}

	public void setStyle( final OUTPUT_STYLE style ) {
		Err.ifNull( style );
		this.style = style;
	}

	public void write( final CHANNEL_IN channel, final OUTPUT_DETAIL detail, final TRACE trace, final boolean error, final boolean newLine, final StackTraceElement[] ste, final Object[] oa ) {
//		if(ste == null && trace != TRACE.NO || ste != null && trace == TRACE.NO) // Internal test
//			throw Err.invalid(ste, trace);

		if( this.useRoute ) {
			final CHANNEL_OUT[] targets = channel == CHANNEL_IN.OUT
				? this.routeOut
				: channel == CHANNEL_IN.ERR
					? this.routeErr
					: channel == CHANNEL_IN.DEV
						? this.routeDev
						: channel == CHANNEL_IN.WRN
							? this.routeWrn
							: this.routeLog;

			for( final CHANNEL_OUT ch : targets )
				if( ch == CHANNEL_OUT.STDOUT )
					this.writeStd( channel, detail, trace, error, newLine, ste, oa, false );
				else if( ch == CHANNEL_OUT.STDERR )
					this.writeStd( channel, detail, trace, error, newLine, ste, oa, true );
				else if( ch == CHANNEL_OUT.LOG )
					this.writeLog( channel, trace, error, ste, oa );
		}
		else
			switch( channel ) {
				case OUT:
					this.writeStd( channel, detail, trace, error, newLine, ste, oa, false );
					break;
				case ERR:
					this.writeStd( channel, detail, trace, error, newLine, ste, oa, true );
					this.writeLog( channel, trace, error, ste, oa );
					break;
				case WRN:
					if( this.outputDev )
						this.writeStd( channel, detail, trace, error, newLine, ste, oa, true );
					this.writeLog( channel, trace, error, ste, oa );
					break;
				case DEV:
					if( this.outputDev )
						this.writeStd( channel, detail, trace, error, newLine, ste, oa, false );
					break;
				case LOG:
					this.writeLog( channel, trace, error, ste, oa );
					break;
			}
	}

	public void writeError( Throwable t, final String text, final boolean isCause ) {
		if( t == null )
			t = new RuntimeException();

		final StackTraceElement[] sta = t.getStackTrace(); // Trace may be not available (exp. Stack Overflow)
		final SimpleList<Object> list = new SimpleList<>();
		if( text != null )
			list.add( text );

		if( t instanceof I_ErrorInfo )
			list.add( ((I_ErrorInfo)t).toInfo() );
		else {
			final String className = t.getClass().getSimpleName();
			final String message = t.getMessage();
			if( message == null )
				list.add( className );
			else
				list.add( className + ": " + t.getMessage() ); //(loglevel<=1 ? "   " : "  ")+

			if( t instanceof final I_ErrorDetails bf )
				for( final Object o : bf.getDetails() )
					list.add( o );
		}

		this.write( CHANNEL_IN.ERR, OUTPUT_DETAIL.RAW, TRACE.DETAIL, true, true, sta, list.toArray() );

		if( t.getCause() != null )
			this.writeError( t.getCause(), null, true );
	}

	public void writeLog( final CHANNEL_IN channel, final TRACE trace, final boolean error, final StackTraceElement[] ste, final Object[] oa ) {

		if( this.logFile != null ) {
			final StringBuilder sb = new StringBuilder();
			this.styler.addLogHeader( channel, sb );
			this.styler.style( sb, OUTPUT_STYLE.RAW, channel, OUTPUT_DETAIL.RAW, true, oa );	// TODO in styleLog integrieren!

			if( ste != null )
				this.styler.addTrace( sb, ste, trace );

			this.directLog( sb.toString() );
		}
	}

	private synchronized void directLog( final String s ) {

		try {
			Lib_TextFile.append( this.logFile, s );
		}
		catch( final Err_FileSys e ) {
			this.directErr( "Log-Write-Error: " + e.getMessage() );
//			Err.show(e);
		}
	}

	private void writeStd( final CHANNEL_IN channel, final OUTPUT_DETAIL detail, TRACE trace, final boolean error, boolean newLine, StackTraceElement[] ste, final Object[] oa, final boolean err ) {
		if( this.style != OUTPUT_STYLE.RAW )
			newLine = true;

		if( this.traceDefault != null && this.traceDefault.amount > trace.amount )
			trace = this.traceDefault;

		final boolean addTrace = error ? this.javaErrors : trace != TRACE.NO;
		final StringBuilder sb = new StringBuilder();

		if( addTrace )
			sb.append( this.linebreak );

//		// Add complete TimeStamp
		if( error ) {
			sb.append( new MDateTime().toString() );
			sb.append( this.linebreak );
		}

		this.styler.style( sb, this.style, channel, detail, newLine, oa );

//		// Add Trace-Info
		if( addTrace ) {
			if(ste == null) {
				RuntimeException t = new RuntimeException();
				ste = t.getStackTrace(); // Trace may be not available (exp. Stack Overflow)
			}

			this.styler.addTrace( sb, ste, trace );
//			sb.append(MOut.linebreak);
		}

		if( err )
			this.directErr( sb.toString() );
		else
			this.directOut( sb.toString() );
	}

}
