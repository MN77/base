/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.sys.out._test;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.SysDir;


/**
 * @author Michael Nitsche
 * @created 08.11.2022
 */
public class Test_MOut {

	public static void main( final String[] args ) {

		try {
			Test_MOut.start();
		}
		catch( final Throwable t ) {
			Err.show( t );
		}
	}

	public static void start() throws Exception {
//		MOut.setStyle( OUTPUT_STYLE.CHANNEL );
//		MOut.setStyle( OUTPUT_STYLE.TIMESTAMP );

		MOut.setLogDir( SysDir.temp() );

//		MOut.routeDev(CHANNEL_OUT.STDOUT, CHANNEL_OUT.STDERR);
//		MOut.routeDev(CHANNEL_OUT.STDOUT, CHANNEL_OUT.LOG);
//		MOut.routeLog(CHANNEL_OUT.STDOUT);

//		MOut.setModeDev();
//		MOut.setMode(DEBUG_MODE.MINIMAL);
//		MOut.setMode(DEBUG_MODE.DETAIL);

		MOut.echo( "ECHO" );
		MOut.echo( "Echo", "E2" );
		MOut.print( "PRINT" );
		MOut.print( "Print", "P2" );
		MOut.echoErr( "ECHO_ERR" );
		MOut.echoErr( "Eerr", "EE2" );
		MOut.printErr( "PRINT_ERR" );
		MOut.printErr( "Perr", "PE2" );
		MOut.ident( "Ident", "This" );
		MOut.describe( "Describe", "That" );
		MOut.dev( "DEV" );
		MOut.dev( "Dev", "Development" );

		MOut.line( "Line", "Another" );
		MOut.temp( "Temp", "Temporary" );
		MOut.trace( "Trace", "Follow" );

		MOut.log( "LOG" );
		MOut.log( "Logging", "This" );

		MOut.print( "-----------" );
		Err.show( new RuntimeException( "Foo" ) );

		MOut.print( "-----------" );
		MOut.line();
		MOut.line( "line1" );
		MOut.line( "line2", "line3" );

		MOut.print( "-----------" );
		MOut.error( "Fehler" );
		MOut.warning( "Warning", "Warn" );

		MOut.print( "-----------" );
		MOut.exit( "End" );
	}

}
