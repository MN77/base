/*******************************************************************************
 * Copyright (C) 2008-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.thread;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 2008-08-12
 *
 * @apiNote
 *          For inner Classes!!!
 *
 * @implNote
 *           Note about setDaemon: daemon threads do not keep the program from quitting; user threads keep the program from quitting.
 */
public abstract class A_AutoDaemon extends Thread {

	private boolean finished = false;
	private boolean toStop   = false;


	public A_AutoDaemon() {
		this.setDaemon( true );
		this.start();
	}


	public final void finish() {
		this.toStop = true;
	}

	/**
	 * @return Returns true, if the Thread is currently running, and not marked to end.
	 */
	public final boolean isActive() {
		return !this.finished && !this.toStop && !this.isInterrupted();
	}

	public final boolean isFinished() {
		return this.finished;
	}

	@Override
	public final void run() {

		try {
			do
				this.cycle();
			while( !this.isInterrupted() && !this.toStop );
		}
		catch( final Throwable t ) {
			Err.show( t );
		}

		this.finished = true;
//		this.finished();
	}

	protected abstract void cycle();
//	protected abstract void finished();

}
