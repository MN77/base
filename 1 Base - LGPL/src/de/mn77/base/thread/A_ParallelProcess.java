/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.thread;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 2020-02-09
 *
 * @apiNote
 *          For inner Classes!
 *          The Thread will be started immediately
 *
 * @implNote
 *           Note about setDaemon: daemon threads do not keep the program from quitting; user threads keep the program from quitting.
 *           There is only one cycle, so this.isInterrupted() will not be checked.
 */
public abstract class A_ParallelProcess extends Thread {

	private boolean finished = false;


	public A_ParallelProcess() {
		this.setDaemon( true );
		this.start();
	}

	public final boolean isFinished() {
		return this.finished;
	}

	@Override
	public final void run() {

		try {
			this.process();
		}
		catch( final Throwable t ) {
			Err.show( t );
		}

		this.finished = true;

		try {
			this.onFinished();
		}
		catch( final Throwable t ) {
			Err.show( t );
		}
	}

	/**
	 * Can be overwritten
	 */
	protected void onFinished() {}

	protected abstract void process();

}
