/*******************************************************************************
 * Copyright (C) 2008-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.thread;

import de.mn77.base.error.Err;


/**
 * @author mike
 * @created 24.08.2008
 *
 * @apiNote
 *          Thread with error handling
 */
public abstract class A_Thread extends Thread {

//	public A_Thread() {
	//start(); >> Wenn nötig > A_StartThread
//	}

	@Override
	public void interrupt() {
		super.interrupt();
	}

	@Override
	public void run() {

		try {
			this.task();
		}
		catch( final Throwable t ) {
			Err.show( t );
		}
	}

	public abstract void task();

}
