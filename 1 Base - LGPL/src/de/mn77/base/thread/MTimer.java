/*******************************************************************************
 * Copyright (C) 2009-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.thread;

import java.util.Timer;
import java.util.TimerTask;

import de.mn77.base.error.Err_Runtime;


/**
 * @author mike
 * @created 30.10.2009
 *
 * @apiNote
 *          Weckt einen TimerTask alle Millisekunden regelmäßig auf
 *
 *          Dauert die Befehlsverarbeitung etwas länger, fällt die Pause danach um so kürzer aus.
 *          Wenn die Bearbeitung länger dauert als die Intervall, wird der Task danach sofort wieder ohne Pause gestartet.
 */
public class MTimer {

	private final Timer timer  = new Timer( true ); //True for Daemon! Otherwise the application will not terminate correctly!
	boolean             closed = false;


	public void addCycle( final int start_ms, final int cycle_ms, final TimerTask task ) {
		this.iCheck();
		this.timer.schedule( task, start_ms, cycle_ms );
	}

	public void addCycle( final int cycle_ms, final TimerTask task ) {
		this.iCheck();
		this.timer.schedule( task, 0, cycle_ms );
	}

	public void execLater( final int start_ms, final TimerTask task ) {
		this.iCheck();
		this.timer.schedule( task, start_ms );
	}

	public Object isActive() {
		return !this.closed;
	}

	public void stop() {
//		MOut.line("cancel");
//		this.timer.purge();
		this.closed = true;
		this.timer.cancel();
	}

	private void iCheck() {
		if( this.closed )
			new Err_Runtime( "Timer is terminated!" );
	}

}
