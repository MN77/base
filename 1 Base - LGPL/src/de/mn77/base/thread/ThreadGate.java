/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.thread;

/**
 * Stoppt beliebig viele Prozesse, die bei "traffic" durch wollen. Sobald das Gate geöffnet wird, kann ein Prozess durch und
 * schließt das Gate sofort wieder. Der nachfolgende Thread muss somit weiter warten.
 * Kann sein, daß evtl. mal was dazwischen rutscht zwischen Traffic und schliessen.
 *
 * Nachteil: Es kommt nicht der Prozess dran, der am längsten wartet, sondern der, der grad vom System als nächstes die Rechenzeit bekommt.
 */
public class ThreadGate {

	private boolean open;


	public ThreadGate( final boolean open ) {
		this.open = open;
	}


	public synchronized void open() {
		this.open = true;
		this.notifyAll();
	}

//	public synchronized void close() {
//		this.open=false;
//	}

	public synchronized void traffic() throws InterruptedException {

		try {
			while( !this.open )
				this.wait();
		}
		catch( final InterruptedException e ) {
			throw e;
		}

		this.open = false; //Automatisch schließen, wenn durch.
	}

}
