/*******************************************************************************
 * Copyright (C) 2008-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.thread;

import de.mn77.base.data.struct.SimpleQueue;
import de.mn77.base.error.Err;


/**
 * Arbeitet fast wie ThreadGate, nur absolut fair. Wer zuerst kommt, kommt auch zuerst wieder weiter!
 * 19.02.2008 Erweitert, damit der eigene Thread mehrmals durch darf. Nötig für >MOut>Log>Fehler>MOut
 */
public class ThreadQueue {

	private final SimpleQueue<ThreadQueue> fifo;
	private Thread                         current;


	public ThreadQueue() {
		this.fifo = new SimpleQueue<>();
		this.current = null;
	}


	/**
	 * Gibt TRUE zurück, wenn der gleiche Thread das 2.mal durchkommt
	 *
	 * @throws Exception
	 *             TODO HACK prüfen!!!
	 */
	public synchronized boolean entrance() throws Exception {
		if( this.current != null && this.current.equals( Thread.currentThread() ) )
			return true;

		if( !this.fifo.isEmpty() || this.current != null ) {
			this.fifo.add( this );

			try {
				this.wait();
			}
			catch( final InterruptedException e ) {
				throw Err.wrap( e, this.current, this.fifo );
			}
		}
		else
			this.current = Thread.currentThread();

		return false;
	}

	public synchronized void leave() {
		this.current = null;
		if( !this.fifo.isEmpty() )
			this.fifo.next().notify();
	}

}
