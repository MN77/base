/*******************************************************************************
 * Copyright (C) 2008-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.thread._test;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.Sys;
import de.mn77.base.thread.ThreadQueue;


/**
 * @author Michael Nitsche
 *         21.08.2008 Erstellt
 *
 *         Guter Test! Einfach mal mit und mal ohne Schranke testen!
 */
public class Test_ThreadQueue {

//	private static final Schranke schranke=new Schranke(true);
	private static final ThreadQueue block = new ThreadQueue();


	public static void doIt( final int nr, final int run ) {

		try {
			Test_ThreadQueue.block.entrance();
		}
		catch( final Exception e ) {
			Err.exit( e );
		}

		MOut.print( nr + "/" + run );
		for( int i = 1; i <= 1000000; i++ ); // Sinnlose Berechnung
		MOut.print( "Fertig: " + nr );

		Test_ThreadQueue.block.leave();
	}

	public static void main( final String[] args ) {

		try {
			Test_ThreadQueue.start();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

	public static void start() throws Throwable {

		for( int t = 1; t <= 10; t++ ) {
			final int t2 = t;

			new Thread( () -> {
				Sys.sleep( 3000 );
				Test_ThreadQueue.doIt( t2, 1 );
				Sys.sleep( 30 );
				Test_ThreadQueue.doIt( t2, 2 );
			} ).start();
		}
	}

}
