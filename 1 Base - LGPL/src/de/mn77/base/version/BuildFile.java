/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.version;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import de.mn77.base.data.convert.ConvertSequence;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.stream.Lib_Stream;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.I_Directory;
import de.mn77.base.sys.file.Lib_Jar;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.base.sys.file.SysDir;


/**
 * @author Michael Nitsche
 * @created 13.12.2022
 */
public class BuildFile {

	private final String path;


	public BuildFile( final String path ) {
		this.path = path;
	}


	public String[] read() throws Err_FileSys {
		InputStream stream = null;

		try {
			stream = Lib_Jar.getStream( this.path );
		}
		catch( final Err_FileSys e ) {
//			Err.show(e);
		}

		if( stream == null ) {
			MOut.dev( "Stream-Error" );
			return null;
		}

		final String s = Lib_Stream.readASCII( stream );

		if( s == null || s.length() == 0 ) {
			MOut.dev( "Nothing to read!" );
			return null;
		}

//		MOut.dev(s);
		return s.trim().split( " " );
	}

	public void write( final List<Object> data ) {
		I_Directory current = SysDir.current();
		final String dataline = ConvertSequence.toString( " ", data );

		try {
			if( current.knowsDir( "src" ) )
				current = current.dirMust( "src" );
			final String filepath = current.getPathAbsolute() + this.path;

			Lib_TextFile.set( new File( filepath ), dataline );
		}
		catch( final Err_FileSys e ) {
//			Err.show(e);
			MOut.warning( "Build file write error: " + e.getMessage() );
		}
	}

}
