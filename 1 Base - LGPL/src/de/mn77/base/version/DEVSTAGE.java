/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.version;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created
 *          22.08.2008 Created
 *          05.12.2010 As Enum
 */
public enum DEVSTAGE {

	EXPERIMENTAL( "Experimental", 'E' ),
//	PRE_ALPHA("Pre-Alpha", 'X'),
	SID( "SID", 'D' ), // Still In Development

	ALPHA( "Alpha", 'A' ),

	BETA( "Beta", 'B' ), // = Test
//	TEST("Test", 'T'),

	RELEASE_CANDIDATE( "RC", 'C' ), //Release Candidate / Prerelease

	RELEASE( "Release", 'R' );


	public final String text;
	public final char   token;


	public static DEVSTAGE parse( final char token ) {
		for( final DEVSTAGE va : DEVSTAGE.values() )
			if( va.token == token )
				return va;
		throw Err.invalid( token );
	}

	DEVSTAGE( final String text, final char token ) {
		this.text = text;
		this.token = token;
	}

	public boolean isDevelopment() {
		return this != RELEASE;
	}

	public boolean isCritical() {
		return this == EXPERIMENTAL || this == SID; // Alpha?
	}


}
