/*******************************************************************************
 * Copyright (C) 2008-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.version;

import de.mn77.base.MN;
import de.mn77.base.data.datetime.I_DateTime;
import de.mn77.base.data.datetime.MDateTime;
import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MOut;
import de.mn77.base.version.data.I_VersionData;


/**
 * @author Michael Nitsche
 * @created 16.08.2008
 */
public class Lib_Version {

	private static final boolean online = MN.isProductive();


	public static Group2<Long, I_DateTime> fileDataToBuildTime( final String[] sa ) {
		final I_DateTime time = new MDateTime( sa[0] + " " + sa[1] );
		final Long build = Long.parseLong( sa[2] );
		return new Group2<>( build, time );
	}

	public static I_VersionData init( final I_VersionData info, final boolean create ) throws Err_FileSys {
		return Lib_Version.init( info, create, true );
	}

	public static I_VersionData init( final I_VersionData info, final boolean create, final boolean write ) throws Err_FileSys {
		if( Lib_Version.online || !write )
			Lib_Version.iLoadData( info );
		else
			Lib_Version.iIncrementData( info, create );
		return info;
	}

	/**
	 * Initialize info, but stay at same build-nr.
	 */
	public static I_VersionData initKeep( final I_VersionData info ) throws Err_FileSys {
		Lib_Version.iLoadData( info );
		return info;
	}

	private static String iComputeFilePath( final DEVSTAGE stage ) {
		final StackTraceElement[] trace = new Throwable().getStackTrace();
//		MOut.dev(trace);
//		String link=trace[trace.length-1].toString(); //Geht nicht, da ggf. vorher Wrapperklassen gestartet werden (z.B. Webstart)!

		String className = null;

		for( final StackTraceElement e : trace ) {
			if( e.getClassName().equals( Lib_Version.class.getName() ) )
				continue;
			className = e.getClassName();
			break;
		}

		Err.ifNull( className );
		final char t = '/'; //Sys.getSeperatorDir() geht nicht, da bei Win sonst ein Backslash verwendet wird.

		final StringBuilder sb = new StringBuilder();
		sb.append( t );
		sb.append( className.replace( '.', t ) );
		sb.append( ".build" );

		if( stage != null && stage != DEVSTAGE.RELEASE ) {
			sb.append( '_' );
			sb.append( stage.text.toLowerCase() );
		}

		return sb.toString();
	}

	private static void iIncrementData( final I_VersionData data, final boolean create ) throws Err_FileSys {
		if( Lib_Version.online )
			return;
//		else
//			MOut.dev("Build-Nr +1");

		final String filepath = Lib_Version.iComputeFilePath( data.getStage() );
		final BuildFile buildFile = new BuildFile( filepath );

		final String[] sa = buildFile.read();
		final I_VersionData info2 = sa == null
			? null
			: VersionDataFactory.createInstance( sa );

		boolean write = true;
		if( create && info2 != null )
			throw Err.direct( "Build-file already exist. Set 'create' to false!" );

		if( !create && info2 == null ) {
			MOut.dev( "Build-Nr +1 aborted, cause of no init-data!" );
			write = false;
		}

		long build = info2 == null
			? 0
			: info2.getBuild();

		final MDateTime timestamp = new MDateTime();
		build++;

		final SimpleList<Object> items = new SimpleList<>();
		items.add( timestamp.toStringShort() );
		items.add( build );
		items.add( data.getDataToken() );
		data.toFileData( items );

		if( write )
			buildFile.write( items );

		data.setBuildTime( build, timestamp );
	}

	private static void iLoadData( final I_VersionData data ) throws Err_FileSys {
		final String filepath = Lib_Version.iComputeFilePath( data.getStage() );
		final String[] sa = new BuildFile( filepath ).read();

		if( sa != null ) {
			final Group2<Long, I_DateTime> g = Lib_Version.fileDataToBuildTime( sa );
			data.setBuildTime( g.o1, g.o2 );
		}
	}

}
