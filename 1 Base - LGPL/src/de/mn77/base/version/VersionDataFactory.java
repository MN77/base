/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.version;

import de.mn77.base.data.datetime.I_DateTime;
import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;
import de.mn77.base.version.data.I_VersionData;
import de.mn77.base.version.data.VersionData_ABC;
import de.mn77.base.version.data.VersionData_ABCD;
import de.mn77.base.version.data.VersionData_Date;
import de.mn77.base.version.data.VersionData_Single;
import de.mn77.base.version.data.VersionData_Year;


/**
 * @author Michael Nitsche
 * @created 13.12.2022
 */
public class VersionDataFactory {

	public static I_VersionData createInstance( final String[] sa ) {
//		if(sa[0].matches("[0-9]+"))
//			return VersionData_Standard.fromOld(sa);

		final Group2<Long, I_DateTime> g = Lib_Version.fileDataToBuildTime( sa );

		final String[] sa2 = new String[sa.length - 4];
		System.arraycopy( sa, 4, sa2, 0, sa2.length );

		switch( sa[3].charAt( 0 ) ) {
			case 'a':
				return new VersionData_Single( g.o2, g.o1, sa2 );
			case 'b':
				return new VersionData_ABCD( g.o2, g.o1, sa2 );
			case 'c':
				return new VersionData_ABC( g.o2, g.o1, sa2 );
			case 'd':
				return new VersionData_Date( g.o2, g.o1, sa2 );
//			case 's':
//				return new VersionData_Standard(g.o2, g.o1, sa2);
			case 'y':
				return new VersionData_Year( g.o2, g.o1, sa2 );
		}

		throw Err.todo( (Object)sa );
	}

}
