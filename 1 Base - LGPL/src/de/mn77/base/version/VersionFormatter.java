/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.version;

import de.mn77.base.data.datetime.format.FORM_DATE;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.version.data.A_VersionData;


/**
 * @author Michael Nitsche
 * @created 04.05.2019
 */
public class VersionFormatter {

	private String format;


	public VersionFormatter( final String format ) {
		this.format = format;
	}


	public String compute( final A_VersionData data ) {
//		this.format = this.format.replaceAll("%n", "" + data.getNumberMain());

		final boolean isRelease = data.getStage() == DEVSTAGE.RELEASE;
		final String tz = isRelease
			? ""
			: ("" + data.getStage().token).toLowerCase();

//		this.format = this.format.replaceAll("%\\?tf", tz + (fix
//			? ""
//			: (isRelease
//				? "-"
//				: "") + data.getNumberFix()));

		this.format = this.format.replaceAll( "%t", "" + data.getStage().token );
		this.format = this.format.replaceAll( "%\\?t", tz );

		if( data.isLoaded() ) {
			this.format = this.format.replaceAll( "%DD.MM.YYYY", "" + data.getDateTime().getDate().toString( FORM_DATE.GROUP_DE ) );
			this.format = this.format.replaceAll( "%YYYY-MM-DD", "" + data.getDateTime().getDate().toString( FORM_DATE.GROUP_ISO ) );
			this.format = this.format.replaceAll( "%b", "" + data.getBuild() );
		}
		else {
			this.format = this.format.replaceAll( "%DD.MM.YYYY", "" );
			this.format = this.format.replaceAll( "%YYYY-MM-DD", "" );
			this.format = this.format.replaceAll( "%b", "" );
		}

		return this.format;
	}

	public void replace( final String search, final Object value ) {
		this.format = Lib_String.replace( this.format, search, "" + value );
	}

}
