/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.version.data;

import de.mn77.base.data.datetime.I_DateTime;
import de.mn77.base.error.Err;
import de.mn77.base.version.DEVSTAGE;


/**
 *
 * @author Michael Nitsche
 * @created 2019
 * @reworked 13.12.2022
 */
public abstract class A_VersionData implements I_VersionData {

	private Long           build = 0L;
	private final DEVSTAGE stage;
	private I_DateTime     time  = null;


	public A_VersionData( final DEVSTAGE stage ) {
		this.stage = stage;
	}

	public A_VersionData( final I_DateTime time, final Long build, final String[] sa ) {
		this.setBuildTime( build, time );
		this.stage = this.parse( sa );
	}


	public Long getBuild() {
		return this.build;
	}

	public I_DateTime getDateTime() {
		return this.time;
	}

	public DEVSTAGE getStage() {
		return this.stage;
	}

	public boolean isDevelopment() {
		return this.stage.isDevelopment();
	}

	public boolean isLoaded() {
		return this.build != 0;
	}

	public final void setBuildTime( final Long build, final I_DateTime zeit ) {
		Err.ifNull( build, zeit );
		this.build = build;
		this.time = zeit;
	}

	public String toBuildString() {
		return this.build + " - " + this.time.getDate();
	}

	@Override
	public abstract String toString();

	public final String toStringFull() {
		return this.toString();
	}

	protected abstract DEVSTAGE parse( String[] sa );

}
