/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.version.data;

import java.util.List;

import de.mn77.base.data.datetime.I_DateTime;
import de.mn77.base.version.DEVSTAGE;


/**
 * @author Michael Nitsche
 * @created 24.04.2019
 */
public interface I_VersionData {

	String format( String s );
	Long getBuild();
	char getDataToken();
	I_DateTime getDateTime();

	DEVSTAGE getStage();

	void setBuildTime( Long build, I_DateTime zeit );

	void toFileData( List<Object> list );

	String toStringFull();
	String toStringShort();

}
