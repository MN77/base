/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.version.data;

import java.util.List;

import de.mn77.base.data.datetime.I_DateTime;
import de.mn77.base.error.Err;
import de.mn77.base.version.DEVSTAGE;
import de.mn77.base.version.VersionFormatter;


/**
 * @author Michael Nitsche
 * @created 2022-12-12
 *
 *          Release:
 *          0.5
 *          1.3
 *          1.3.1
 *          1.3.5
 *          1.4.6
 */
public class VersionData_ABC extends A_VersionData {

	private int main;
	private int minor;
	private int bugfix; // 0 - ...


	public VersionData_ABC( final I_DateTime time, final Long build, final String[] sa ) {
		super( time, build, sa );
	}

	public VersionData_ABC( final int main, final int minor, final int fix ) {
		super( DEVSTAGE.RELEASE );

		Err.ifTooSmall( 0, main );
		Err.ifTooSmall( 0, minor );
		Err.ifTooSmall( 0, fix ); // -1
		this.main = main;
		this.minor = minor;
		this.bugfix = fix;
	}


	public String format( final String format ) {
//		"%1.%2-%b - %ddmmyyyy"

		final VersionFormatter form = new VersionFormatter( format );
		form.replace( "%1", this.main );
		form.replace( "%2", this.minor );
		form.replace( "%3", this.bugfix );
		return form.compute( this );
	}

	public char getDataToken() {
		return 'c';
	}

	public void toFileData( final List<Object> list ) {
		list.add( this.main );
		list.add( this.minor );
		list.add( this.bugfix );
	}

	@Override
	public String toString() {
		return this.iToString( true );
	}

	//TODO Anderer Name, und am besten schöner integrieren!
	public String toStringShort() {
		return this.iToString( false );
	}

	@Override
	protected DEVSTAGE parse( final String[] sa ) {
		this.main = Integer.parseInt( sa[0] );
		this.minor = Integer.parseInt( sa[1] );
		this.bugfix = Integer.parseInt( sa[2] );

		return DEVSTAGE.RELEASE;
	}

	private String iToString( final boolean withBuild ) {
		final StringBuilder sb = new StringBuilder();
		sb.append( this.main );
		sb.append( '.' );
		sb.append( this.minor );

		if( this.bugfix != 0 ) {
			sb.append( "." );
			sb.append( this.bugfix );
		}

		if( withBuild ) {
			sb.append( " (Build: " );
			sb.append( this.getBuild() );
			sb.append( " - " );
			sb.append( this.getDateTime().toStringShort() );
			sb.append( ')' );
		}

		return sb.toString();
	}

}
