/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.version.data;

import java.util.List;

import de.mn77.base.data.datetime.I_DateTime;
import de.mn77.base.version.DEVSTAGE;
import de.mn77.base.version.VersionFormatter;


/**
 * @author Michael Nitsche
 * @created 2019-09-07
 * @apiNote
 *          Release:
 *          0.5
 *          0.5-dev2
 *          1.3
 *          1.3.1
 *          1.3.1-dev1
 *          1.3.5
 *          1.4.6
 */
public class VersionData_ABCD extends A_VersionData {

	private int main;
	private int minor;
	private int bugfix; // 0 - ...
	private int dev;


	public VersionData_ABCD( final I_DateTime time, final Long build, final String[] sa ) {
		super( time, build, sa );
	}

	public VersionData_ABCD( final int main, final int minor, final int bugfix ) {
		super( DEVSTAGE.RELEASE );
		this.main = main;
		this.minor = minor;
		this.bugfix = bugfix;
		this.dev = 0;
	}

	/**
	 * Further development, but not yet a new version
	 */
	public VersionData_ABCD( final int main, final int minor, final int bugfix, final int dev ) {
		super( DEVSTAGE.RELEASE );
		this.main = main;
		this.minor = minor;
		this.bugfix = bugfix;
//		this.dev = Math.abs(dev);
		this.dev = 0;
	}


	public String format( final String format ) {
		final VersionFormatter form = new VersionFormatter( format );
		form.replace( "%1", this.main );
		form.replace( "%2", this.minor );
		form.replace( "%3", this.bugfix );
		form.replace( "%D", this.dev > 0 ? "-dev" + this.dev : "" ); // Further development, but not yet a new version
		return form.compute( this );
	}

	public char getDataToken() {
		return 'b';
	}

	public void toFileData( final List<Object> list ) {
		list.add( this.main );
		list.add( this.minor );
		list.add( this.bugfix );
		list.add( this.dev );
	}

	@Override
	public String toString() {
		return this.iToString( true );
	}

	//TODO Anderer Name, und am besten schöner integrieren!
	public String toStringShort() {
		return this.iToString( false );
	}

	@Override
	protected DEVSTAGE parse( final String[] sa ) {
		this.main = Integer.parseInt( sa[0] );
		this.minor = Integer.parseInt( sa[1] );
		this.bugfix = Integer.parseInt( sa[2] );
		this.dev = Integer.parseInt( sa[3] );

		return DEVSTAGE.RELEASE;
	}

	private String iToString( final boolean withBuild ) {
		final StringBuilder sb = new StringBuilder();
		sb.append( this.main );
		sb.append( '.' );
		sb.append( this.minor );

		if( this.bugfix != 0 ) {
			sb.append( "." );
			sb.append( this.bugfix );
		}

		if( this.dev != 0 ) {
			sb.append( "dev" );
			sb.append( this.dev );
		}

		if( withBuild ) {
			sb.append( " (Build: " );
			sb.append( this.getBuild() );
			sb.append( " - " );
			sb.append( this.getDateTime().toStringShort() );
			sb.append( ")" );
		}

		return sb.toString();
	}

}
