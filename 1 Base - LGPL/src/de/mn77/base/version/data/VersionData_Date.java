/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.version.data;

import java.util.List;

import de.mn77.base.data.datetime.I_DateTime;
import de.mn77.base.version.DEVSTAGE;
import de.mn77.base.version.VersionFormatter;


/**
 * @author Michael Nitsche
 * @created 14.12.2022
 * @apiNote
 *          2022-12-14
 *          2023-01-01
 */
public class VersionData_Date extends A_VersionData {

	public VersionData_Date() {
		this( DEVSTAGE.RELEASE );
	}

	public VersionData_Date( final DEVSTAGE stage ) {
		super( stage );
	}

	public VersionData_Date( final I_DateTime time, final Long build, final String[] sa ) {
		super( time, build, sa );
	}


	public String format( final String format ) {
		final VersionFormatter form = new VersionFormatter( format );
		return form.compute( this );
	}

	public char getDataToken() {
		return 'd';
	}

	public void toFileData( final List<Object> list ) {}

	@Override
	public String toString() {
		final String f = "%YYYY-MM-DD (Build: %b)";
		return this.format( f );
	}

	@Override
	public String toStringShort() {
		final String f = "%YYYY-MM-DD";
		return this.format( f );
	}

	@Override
	protected DEVSTAGE parse( final String[] sa ) {
		return DEVSTAGE.RELEASE;
	}

}
