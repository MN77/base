/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.version.data;

import java.util.List;

import de.mn77.base.data.datetime.I_DateTime;
import de.mn77.base.error.Err;
import de.mn77.base.version.DEVSTAGE;
import de.mn77.base.version.VersionFormatter;


/**
 * @author Michael Nitsche
 * @created 01.05.2019
 * @apiNote
 *          1
 *          2
 *          3
 */
public class VersionData_Single extends A_VersionData {

	private int main;


	public VersionData_Single( final I_DateTime time, final Long build, final String[] sa ) {
		super( time, build, sa );
	}

	public VersionData_Single( final int nr ) {
		this( nr, DEVSTAGE.RELEASE );
	}

	public VersionData_Single( final int nr, final DEVSTAGE stage ) {
		super( stage );

		Err.ifTooSmall( 0, this.main );
		this.main = nr;
	}


	public String format( final String format ) {
		final VersionFormatter form = new VersionFormatter( format );
		form.replace( "%1", this.main );
		return form.compute( this );
	}

	public char getDataToken() {
		return 'a';
	}

	public void toFileData( final List<Object> list ) {
		list.add( this.main );
	}

	@Override
	public String toString() {
//		final String f = "%n%?t%-f (Build: %b - %dd.mm.yyyy)";
		final String f = "%1 (Build: %b - %YYYY-MM-DD)";
		return this.format( f );
	}

	@Override
	public String toStringShort() {
		return "" + this.main;
	}

	@Override
	protected DEVSTAGE parse( final String[] sa ) {
//		return DEVSTAGE.get(sa[1].charAt(0)), Integer.parseInt(sa[2]));

		this.main = Integer.parseInt( sa[0] );
		return DEVSTAGE.RELEASE;
	}

}
