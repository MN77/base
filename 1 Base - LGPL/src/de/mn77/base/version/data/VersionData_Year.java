/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of MN77-Base-Library <https://www.mn77.de>.
 *
 * MN77-Base-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.base.version.data;

import java.util.List;

import de.mn77.base.data.datetime.I_DateTime;
import de.mn77.base.error.Err;
import de.mn77.base.version.DEVSTAGE;
import de.mn77.base.version.VersionFormatter;


/**
 * @author Michael Nitsche
 * @created 24.04.2019
 * @apiNote
 *          2019.1
 *          2019.2
 *          2019.2-1
 */
public class VersionData_Year extends A_VersionData {

	private int year;
	private int minor;
	private int bugfix;


	public VersionData_Year( final I_DateTime time, final Long build, final String[] sa ) {
		super( time, build, sa );
	}

	public VersionData_Year( final int year, final int nr ) {
		this( year, nr, DEVSTAGE.RELEASE );
	}

	public VersionData_Year( final int year, final int nr, final DEVSTAGE stage ) {
		this( year, nr, stage, 0 );
	}

	public VersionData_Year( final int year, final int nr, final DEVSTAGE stage, final int fixnr ) {
		super( stage );

		Err.ifOutOfBounds( 1900, 2200, year );
		Err.ifTooSmall( 1, nr );
		Err.ifTooSmall( 0, fixnr ); // -1
		this.year = year;
		this.minor = nr;
		this.bugfix = fixnr;
	}


	public String format( final String format ) {
		final VersionFormatter form = new VersionFormatter( format );
		form.replace( "%y", "" + this.year );
		form.replace( "%2", "" + this.minor );
		form.replace( "%3", "-" + this.bugfix );
		form.replace( "%?2", this.minor == 0 ? "" : "." + this.minor );
		form.replace( "%?3", this.bugfix == 0 ? "" : "-" + this.bugfix );
		return form.compute( this );
	}

	public char getDataToken() {
		return 'y';
	}

	public int getYear() {
		return this.year;
	}

	public void toFileData( final List<Object> list ) {
		list.add( this.year );
		list.add( this.minor );
		list.add( this.bugfix );
//		return new Object[]{this.year, this.getNumberMain(), this.getType().token, this.getNumberFix()};
	}

	@Override
	public String toString() {
//		final String f = "%y.%n%?t%-f (Build: %b - %dd.mm.yyyy)";
		final String f = "%y.%2%?3 (Build: %b - %YYYY-MM-DD)";
		return this.format( f );
	}

	@Override
	public String toStringShort() {
		final String f = "%y.%2%?3";
		return this.format( f );
	}

	@Override
	protected DEVSTAGE parse( final String[] sa ) {
		this.year = Integer.parseInt( sa[0] );
		this.minor = Integer.parseInt( sa[1] );
		this.bugfix = Integer.parseInt( sa[2] );

//		return DEVSTAGE.get(sa[2].charAt(0));
		return DEVSTAGE.RELEASE;
	}

}
